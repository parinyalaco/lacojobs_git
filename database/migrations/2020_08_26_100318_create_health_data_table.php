<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealthDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('congenital_disease',100)->nullable();
			$table->string('contagious_disease',100)->nullable();
			$table->string('allergy',100)->nullable();
			$table->string('back_pain',100)->nullable();
			$table->string('finger_pain',100)->nullable();
			$table->string('thalassemia',100)->nullable();
			$table->string('liver_virus',100)->nullable();
			$table->string('food_allergy',100)->nullable();
			$table->string('drug_allergy',100)->nullable();
			$table->string('other_disease',100)->nullable();
			$table->string('physical_exam_time',100)->nullable();
			$table->string('physical_exam_part',100)->nullable();
			$table->string('physical_exam_result',100)->nullable();
			$table->string('prev_social_sec_company',100)->nullable();
			$table->string('prev_social_sec_hospital',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_data');
    }
}
