<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExTrainingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ex_training_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->date('start_date_1')->nullable();
			$table->date('end_date_1')->nullable();
			$table->string('duration_1',100)->nullable();
			$table->string('title_1',200)->nullable();
			$table->string('by_1',200)->nullable();
			$table->date('start_date_2')->nullable();
			$table->date('end_date_2')->nullable();
			$table->string('duration_2',100)->nullable();
			$table->string('title_2',200)->nullable();
			$table->string('by_2',200)->nullable();
			$table->date('start_date_3')->nullable();
			$table->date('end_date_3')->nullable();
			$table->string('duration_3',100)->nullable();
			$table->string('title_3',200)->nullable();
			$table->string('by_3',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ex_training_data');
    }
}
