<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220523 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         //master_data
        Schema::table(
            'master_data',
            function (Blueprint $table) {
                $table->string('nickname_th', 50)->nullable()->change();
                $table->string('init_en', 20)->nullable()->change();
                $table->string('fname_en', 100)->nullable()->change();
                $table->string('lname_en', 100)->nullable()->change();
                $table->string('nickname_en', 50)->nullable()->change();
                $table->string('weight', 20)->nullable()->change();
                $table->string('height', 20)->nullable()->change();
                $table->string('blood', 20)->nullable()->change();
                $table->string('nationality', 50)->nullable()->change();
                $table->string('race', 50)->nullable()->change();
                $table->string('religion', 20)->nullable()->change();
                $table->string('addr_type', 50)->nullable()->change();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'master_data',
            function (Blueprint $table) {
                $table->string('nickname_th', 50)->change();
                $table->string('init_en', 20)->change();
                $table->string('fname_en', 100)->change();
                $table->string('lname_en', 100)->change();
                $table->string('nickname_en', 50)->change();
                $table->string('weight', 20)->change();
                $table->string('height', 20)->change();
                $table->string('blood', 20)->change();
                $table->string('nationality', 50)->change();
                $table->string('race', 50)->change();
                $table->string('religion', 20)->change();
                $table->string('addr_type', 50)->change();
            }
        );
    }
}
