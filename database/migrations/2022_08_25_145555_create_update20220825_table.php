<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdate20220825Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'master_data',
            function (Blueprint $table) {
                $table->string('email', 150)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('master_data', 'email')) {
            Schema::table('master_data', function (Blueprint $table) {
                $table->dropColumn('email');
            });
        }
    }
}
