<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220519 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //master_data
        Schema::table(
            'master_data',
            function (Blueprint $table) {
                $table->string('block', 150)->nullable();
                $table->string('road', 150)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('master_data', 'block')) {
            Schema::table('master_data', function (Blueprint $table) {
                $table->dropColumn('block');
            });
        }
        if (Schema::hasColumn('master_data', 'road')) {
            Schema::table('master_data', function (Blueprint $table) {
                $table->dropColumn('road');
            });
        }
    }
}
