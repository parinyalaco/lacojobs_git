<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('name_1',100)->nullable();
			$table->string('relation_1',100)->nullable();
			$table->string('company_job_1',100)->nullable();
			$table->string('tel_1',50)->nullable();
			$table->string('name_2',100)->nullable();
			$table->string('relation_2',100)->nullable();
			$table->string('company_job_2',100)->nullable();
			$table->string('tel_2',50)->nullable();
			$table->string('name_3',100)->nullable();
			$table->string('relation_3',100)->nullable();
			$table->string('company_job_3',100)->nullable();
			$table->string('tel_3',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_data');
    }
}
