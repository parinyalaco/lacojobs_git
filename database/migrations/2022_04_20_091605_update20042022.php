<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20042022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'daily_data',
            function (Blueprint $table) {
                $table->string('nickname',100)->nullable();
            }
        );
        Schema::table(
            'daily_data_exts',
            function (Blueprint $table) {
                $table->string('exp_job_1', 200)->nullable();
                $table->string('exp_job_2', 200)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('daily_data', 'nickname')) {
            Schema::table('daily_data', function (Blueprint $table) {
                $table->dropColumn('nickname');
            });
        }
        if (Schema::hasColumn('daily_data_exts', 'exp_job_1')) {
            Schema::table('daily_data_exts', function (Blueprint $table) {
                $table->dropColumn('exp_job_1');
            });
        }
        if (Schema::hasColumn('daily_data_exts', 'exp_job_2')) {
            Schema::table('daily_data_exts', function (Blueprint $table) {
                $table->dropColumn('exp_job_2');
            });
        }
    }
}
