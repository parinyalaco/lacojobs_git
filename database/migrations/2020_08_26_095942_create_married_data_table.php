<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarriedDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('married_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('status',50);
			$table->string('init',50)->nullable();
			$table->string('name',150)->nullable();
			$table->string('age',10)->nullable();
			$table->string('career',100)->nullable();
			$table->string('homeaddr',255)->nullable();
			$table->string('tel',100)->nullable();
			$table->string('companyaddr',255)->nullable();
			$table->string('job',100)->nullable();
			$table->string('companytel',100)->nullable();
			$table->string('child_status',50)->nullable();
			$table->string('child_number',50)->nullable();
			$table->string('boy_no',100)->nullable();
			$table->string('boy_age',100)->nullable();
			$table->string('daughter_no',100)->nullable();
			$table->string('daughter_age',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('married_data');
    }
}
