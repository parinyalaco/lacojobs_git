<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update20220429 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'daily_data',
            function (Blueprint $table) {
                $table->string('addr2', 150)->nullable();
            }
        );
        Schema::table(
            'daily_data_exts',
            function (Blueprint $table) {
                $table->string('contact_init', 100)->nullable();
                $table->string('contact_lname', 200)->nullable();
                $table->string('social_security_hospital_flag', 100)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('daily_data', 'addr2')) {
            Schema::table('daily_data', function (Blueprint $table) {
                $table->dropColumn('addr2');
            });
        }
        if (Schema::hasColumn('daily_data_exts', 'contact_init')) {
            Schema::table('daily_data_exts', function (Blueprint $table) {
                $table->dropColumn('contact_init');
            });
        }
        if (Schema::hasColumn('daily_data_exts', 'contact_lname')) {
            Schema::table('daily_data_exts', function (Blueprint $table) {
                $table->dropColumn('contact_lname');
            });
        }
        if (Schema::hasColumn('daily_data_exts', 'social_security_hospital_flag')) {
            Schema::table('daily_data_exts', function (Blueprint $table) {
                $table->dropColumn('social_security_hospital_flag');
            });
        }
    }
}
