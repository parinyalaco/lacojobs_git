<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('school_1',150)->nullable();
			$table->string('school_2',150)->nullable();
			$table->string('school_3',150)->nullable();
			$table->string('school_4',150)->nullable();
			$table->string('school_5',150)->nullable();
			$table->string('school_6',150)->nullable();
			$table->string('province_1',100)->nullable();
			$table->string('province_2',100)->nullable();
			$table->string('province_3',100)->nullable();
			$table->string('province_4',100)->nullable();
			$table->string('province_5',100)->nullable();
			$table->string('province_6',100)->nullable();
			$table->string('title_1',150)->nullable();
			$table->string('title_2',150)->nullable();
			$table->string('title_3',150)->nullable();
			$table->string('title_4',150)->nullable();
			$table->string('title_5',150)->nullable();
			$table->string('title_6',150)->nullable();
			$table->string('year_1',20)->nullable();
			$table->string('year_2',20)->nullable();
			$table->string('year_3',20)->nullable();
			$table->string('year_4',20)->nullable();
			$table->string('year_5',20)->nullable();
			$table->string('year_6',20)->nullable();
			$table->string('grade_1',20)->nullable();
			$table->string('grade_2',20)->nullable();
			$table->string('grade_3',20)->nullable();
			$table->string('grade_4',20)->nullable();
			$table->string('grade_5',20)->nullable();
			$table->string('grade_6',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_data');
    }
}
