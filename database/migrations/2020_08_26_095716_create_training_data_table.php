<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->date('start_date_1')->nullable();
			$table->date('end_date_1')->nullable();
			$table->string('title_1',255)->nullable();
			$table->string('province_1',100)->nullable();
			$table->string('level_1',150)->nullable();
			
			$table->date('start_date_2')->nullable();
			$table->date('end_date_2')->nullable();
			$table->string('title_2',255)->nullable();
			$table->string('province_2',100)->nullable();
			$table->string('level_2',150)->nullable();
			
			$table->date('start_date_3')->nullable();
			$table->date('end_date_3')->nullable();
			$table->string('title_3',255)->nullable();
			$table->string('province_3',100)->nullable();
			$table->string('level_3',150)->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_data');
    }
}
