<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimpleDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simple_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('current_edu_level',100)->nullable();
			$table->string('current_edu_dep',100)->nullable();
			$table->string('current_edu_year',100)->nullable();
			$table->string('current_edu_school',100)->nullable();
			$table->string('future_edu_level',100)->nullable();
			$table->string('future_edu_dep',100)->nullable();
			$table->string('future_edu_year',100)->nullable();
			$table->string('future_edu_school',100)->nullable();
			$table->string('hobby_1',100)->nullable();
			$table->string('hobby_2',100)->nullable();
			$table->string('hobby_3',100)->nullable();
			$table->string('drunker',100)->nullable();
			$table->string('smoker',100)->nullable();
			$table->string('smoker_other',100)->nullable();
			$table->string('criminal',100)->nullable();
			$table->string('control',100)->nullable();
			$table->string('can_other_country',100)->nullable();
			$table->string('apply_job_no',50)->nullable();
			$table->string('apply_job_name',150)->nullable();
			$table->string('apply_job_company',150)->nullable();
			$table->string('hear_from',100)->nullable();
			$table->string('refer_name',100)->nullable();
			$table->string('your_comments',500)->nullable();
			$table->string('can_start_in',50)->nullable();
			$table->string('can_start_desc',150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_data');
    }
}
