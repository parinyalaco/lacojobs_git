<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('father_name',100)->nullable();
			$table->string('father_age',50)->nullable();
			$table->string('father_occupation',100)->nullable();
			$table->string('father_company',150)->nullable();
			$table->string('father_job',100)->nullable();
			$table->string('father_company_tel',100)->nullable();
			$table->string('father_addr',150)->nullable();
			$table->string('father_tel',100)->nullable();
			$table->string('mother_name',100)->nullable();
			$table->string('mother_age',50)->nullable();
			$table->string('mother_occupation',100)->nullable();
			$table->string('mother_company',150)->nullable();
			$table->string('mother_job',100)->nullable();
			$table->string('mother_company_tel',100)->nullable();
			$table->string('mother_addr',150)->nullable();
			$table->string('mother_tel',100)->nullable();
			$table->string('total_nabor',50)->nullable();
			$table->string('total_brother',50)->nullable();
			$table->string('total_sisther',50)->nullable();
			$table->string('name_1',100)->nullable();
			$table->string('age_1',50)->nullable();
			$table->string('company_1',100)->nullable();
			$table->string('level_1',150)->nullable();
			$table->string('name_2',100)->nullable();
			$table->string('age_2',50)->nullable();
			$table->string('company_2',100)->nullable();
			$table->string('level_2',150)->nullable();
			$table->string('name_3',100)->nullable();
			$table->string('age_3',50)->nullable();
			$table->string('company_3',100)->nullable();
			$table->string('level_3',150)->nullable();
			$table->string('name_4',100)->nullable();
			$table->string('age_4',50)->nullable();
			$table->string('company_4',100)->nullable();
			$table->string('level_4',150)->nullable();
			$table->string('name_5',100)->nullable();
			$table->string('age_5',50)->nullable();
			$table->string('company_5',100)->nullable();
			$table->string('level_5',150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_data');
    }
}
