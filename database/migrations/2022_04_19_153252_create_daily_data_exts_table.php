<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyDataExtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_data_exts', function (Blueprint $table) {
            $table->id();
            $table->integer('daily_data_id');
            $table->string('marriedstatus', 50)->nullable();
            $table->string('educationname', 150)->nullable();

            $table->date('exp_start_date_1')->nullable();
            $table->date('exp_end_date_1')->nullable();
            $table->string('exp_company_1', 150)->nullable();
            $table->string('exp_province_1', 100)->nullable();
            $table->string('exp_salary_1', 100)->nullable();
            $table->string('exp_case_1', 255)->nullable();

            $table->date('exp_start_date_2')->nullable();
            $table->date('exp_end_date_2')->nullable();
            $table->string('exp_company_2', 150)->nullable();
            $table->string('exp_province_2', 100)->nullable();
            $table->string('exp_salary_2', 100)->nullable();
            $table->string('exp_case_2', 255)->nullable();


            $table->string('refer_name', 150)->nullable();
            $table->string('refer_relation', 100)->nullable();
            $table->string('contact_name', 150)->nullable();
            $table->string('refer_addr1', 100)->nullable();
            $table->string('refer_addr2', 100)->nullable();
            $table->string('contact_tumbon', 100)->nullable();
            $table->string('contact_aumpher', 100)->nullable();
            $table->string('contact_province', 100)->nullable();
            $table->string('contact_tel', 100)->nullable();
            $table->string('contact_relation', 100)->nullable();


            $table->string('criminal', 50)->nullable();
            $table->string('criminal_other', 200)->nullable();


            $table->string('congenital_disease', 50)->nullable();
            $table->string('congenital_disease_other', 200)->nullable();

            $table->date('health_check_date')->nullable();
            $table->string('health_check_case', 200)->nullable();
            $table->string('health_check_status', 50)->nullable();
            $table->string('health_check_status_note', 200)->nullable();

            $table->string('social_security_hospital', 200)->nullable();     
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_data_exts');
    }
}
