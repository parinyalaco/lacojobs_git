<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpMasterDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exp_master_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->date('start_date_1')->nullable();
			$table->date('end_date_1')->nullable();
			$table->string('company_1',150)->nullable();
			$table->string('province_1',100)->nullable();
			$table->string('tel_1',100)->nullable();
			$table->string('title_1',150)->nullable();
			$table->string('salary_1',50)->nullable();
			$table->string('case_1',255)->nullable();
			
			$table->date('start_date_2')->nullable();
			$table->date('end_date_2')->nullable();
			$table->string('company_2',150)->nullable();
			$table->string('province_2',100)->nullable();
			$table->string('tel_2',100)->nullable();
			$table->string('title_2',150)->nullable();
			$table->string('salary_2',50)->nullable();
			$table->string('case_2',255)->nullable();
			
			$table->date('start_date_3')->nullable();
			$table->date('end_date_3')->nullable();
			$table->string('company_3',150)->nullable();
			$table->string('province_3',100)->nullable();
			$table->string('tel_3',100)->nullable();
			$table->string('title_3',150)->nullable();
			$table->string('salary_3',50)->nullable();
			$table->string('case_3',255)->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exp_master_data');
    }
}
