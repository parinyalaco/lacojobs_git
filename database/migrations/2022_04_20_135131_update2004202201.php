<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update2004202201 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'daily_data',
            function (Blueprint $table) {
                $table->string('job', 150)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('daily_data', 'job')) {
            Schema::table('daily_data', function (Blueprint $table) {
                $table->dropColumn('job');
            });
        }
    }
}
