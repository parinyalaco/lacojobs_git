<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Update220604 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'exp_detail_data',
            function (Blueprint $table) {
                $table->string('expd_others', 255)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('exp_detail_data', 'expd_others')) {
            Schema::table('exp_detail_data', function (Blueprint $table) {
                $table->dropColumn('expd_others');
            });
        }
    }
}
