<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcceptDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accept_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('accept_1',20)->nullable();
			$table->string('accept_2',20)->nullable();
			$table->string('accept_3',20)->nullable();
			$table->string('accept_4',20)->nullable();
			$table->string('accept_5',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accept_data');
    }
}
