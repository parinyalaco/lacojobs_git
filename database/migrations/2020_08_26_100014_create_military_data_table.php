<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMilitaryDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('military_data', function (Blueprint $table) {
            $table->id();
			$table->integer('master_data_id');
			$table->string('status',100)->nullable();
			$table->string('year',50)->nullable();
			$table->string('duration',50)->nullable();
			$table->string('custom1',100)->nullable();
			$table->string('custom2',100)->nullable();
			$table->string('custom3',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('military_data');
    }
}
