<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDailyDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_data', function (Blueprint $table) {
            $table->id();
            $table->string('init', 50)->nullable();
            $table->string('fname', 150)->nullable();
            $table->string('lname', 150)->nullable();
            $table->string('sex', 20)->nullable();
            $table->date('birth')->nullable();
            $table->integer('age')->nullable();
            $table->float('weight')->nullable();
            $table->float('height')->nullable();
            $table->string('tel', 100)->nullable();
            $table->string('citizenid', 100)->nullable();
            $table->text('addr')->nullable();
            $table->string('subcity', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('province', 100)->nullable();
            $table->string('zipcode', 50)->nullable();
            $table->integer('education')->nullable();
            $table->string('image', 200)->nullable();
            $table->string('private_info', 200)->nullable();
            $table->string('covid_info', 200)->nullable();
            $table->string('status', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_data');
    }
}
