<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_data', function (Blueprint $table) {
            $table->id();
			$table->string('job_name1',100)->nullable();
			$table->string('job_name2',100)->nullable();
			$table->string('job_salary1',100)->nullable();
			$table->string('job_salary2',100)->nullable();
			$table->string('init_th',20);
			$table->string('fname_th',100);
			$table->string('lname_th',100);
			$table->string('nickname_th',50);
			$table->string('init_en',20);
			$table->string('fname_en',100);
			$table->string('lname_en',100);
			$table->string('nickname_en',50);
			$table->date('birth_date');
			$table->integer('age');
			$table->string('weight',20);
			$table->string('height',20);
			$table->string('blood',20);
			$table->string('nationality',50);
			$table->string('race',50);
			$table->string('religion',20);
			$table->string('addr1',255);
			$table->string('addr2',255);
			$table->string('subdistrict',100);
			$table->string('district',100);
			$table->string('province',100);
			$table->string('zipcode',50);
			$table->string('tel',50)->nullable();
			$table->string('mobile',50);
			$table->string('addr_type',10);
			$table->string('addr_type_custom1',100)->nullable();
			$table->string('addr_type_custom2',100)->nullable();
			$table->string('urgent_name',100);
			$table->string('urgent_addr1',255);
			$table->string('urgent_addr2',255)->nullable();
			$table->string('urgent_mobile',50);
			$table->string('urgent_relation',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_data');
    }
}
