<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyData extends Model
{
    protected $fillable = [
			'master_data_id',
			'father_name',
			'father_age',
			'father_occupation',
			'father_company',
			'father_job',
			'father_company_tel',
			'father_addr',
			'father_tel',
			'mother_name',
			'mother_age',
			'mother_occupation',
			'mother_company',
			'mother_job',
			'mother_company_tel',
			'mother_addr',
			'mother_tel',
			'total_nabor',
			'total_brother',
			'total_sisther',
			'name_1',
			'age_1',
			'company_1',
			'level_1',
			'name_2',
			'age_2',
			'company_2',
			'level_2',
			'name_3',
			'age_3',
			'company_3',
			'level_3',
			'name_4',
			'age_4',
			'company_4',
			'level_4',
			'name_5',
			'age_5',
			'company_5',
			'level_5'];
}
