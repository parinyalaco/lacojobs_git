<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyData extends Model
{
    protected $fillable = [
        'init',
        'fname',
        'lname',
        'nickname',
        'sex',
        'birth',
        'age',
        'weight',
        'height',
        'tel',
        'citizenid',
        'addr',
        'subcity',
        'city',
        'province',
        'zipcode',
        'education',
        'image',
        'private_info',
        'covid_info',
        'status',
        'job',
        'addr2',
        'job2'
    ];

    public function dailydataext()
    {
        return $this->hasOne('App\DailyDataExt', 'daily_data_id');
    }
}
