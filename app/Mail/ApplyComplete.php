<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use App\Models\MasterData;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplyComplete extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $masterdata;

    public function __construct($masterdata)
    {
        $this->masterdata = $masterdata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.applycomplete')->subject('New Apply Job')->from('noreply@lannaagro.com');
    }
}
