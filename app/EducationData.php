<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationData extends Model
{
    protected $fillable = ['master_data_id',
			'school_1',
			'school_2',
			'school_3',
			'school_4',
			'school_5',
			'school_6',
			'province_1',
			'province_2',
			'province_3',
			'province_4',
			'province_5',
			'province_6',
			'title_1',
			'title_2',
			'title_3',
			'title_4',
			'title_5',
			'title_6',
			'year_1',
			'year_2',
			'year_3',
			'year_4',
			'year_5',
			'year_6',
			'grade_1',
			'grade_2',
			'grade_3',
			'grade_4',
			'grade_5',
			'grade_6'];
}
