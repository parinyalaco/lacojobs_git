<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        // echo "+++++</br>";
        // dd(Auth::user());
        // if (Auth::user() &&  Auth::user()->is_admin == '1') {
        if (Auth::user() && Auth::user()->is_admin == '1') {
            return $next($request);
        }
        // else{
            // return view('auth.login');
        // }
        return redirect('/');
    }
}
