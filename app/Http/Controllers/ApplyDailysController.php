<?php

namespace App\Http\Controllers;

use App\DailyData;
use App\DailyDataExt;
use App\Mail\ApplyDailyComplete;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class ApplyDailysController extends Controller
{
    public function index()
    {
        $flag = ' required '; // required '
        $testdata = false;
        return view('applydaily.stepform', compact('flag', 'testdata'));
    }

    public function applyAction(Request $request)
    {
        $input = $request->all();

        //  var_dump($input);

        $request->validate([
            // 'job' => 'required',
            'init' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'sex' => 'required',
            'birth_date' => 'required',
            'weight' => 'required',
            'height' => 'required',
            'citizenid' => 'required|digits:13',
            'education' => 'required',
             'addr' => 'required',
            'addr2' => 'required',
             'subcity' => 'required',
             'city' => 'required',
            'height' => 'required',
             'province' => 'required',
             'zipcode' => 'required|digits:5',
            'tel' => 'required|digits:10',
        ]);

        
        
        

        $masterDatatmp = array();
        $masterDatatmp['job'] = $input['job1'];
        $masterDatatmp['job2'] = $input['job2'];
        $masterDatatmp['init'] = $input['init'];
        $masterDatatmp['fname'] = $input['fname'];
        $masterDatatmp['lname'] = $input['lname'];
        $masterDatatmp['sex'] = $input['sex'];
        $masterDatatmp['birth'] = $input['birth_date'];
        $masterDatatmp['age'] = 20;
        $masterDatatmp['weight'] = $input['weight'];
        $masterDatatmp['height'] = $input['height'];
        $masterDatatmp['citizenid'] = $input['citizenid'];
        $masterDatatmp['education'] = $input['education'];

        $masterDatatmp['addr'] = $input['addr'];
        $masterDatatmp['addr2'] = $input['addr2'];
        $masterDatatmp['subcity'] = $input['subcity'];
        $masterDatatmp['city'] = $input['city'];
        $masterDatatmp['province'] = $input['province'];
        $masterDatatmp['zipcode'] = $input['zipcode'];
        $masterDatatmp['tel'] = $input['tel'];
        $masterDatatmp['status'] = 'register';

        $dailyDataObj = DailyData::create($masterDatatmp);
        $dailyDataId = $dailyDataObj->id;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('files/daily/' . $dailyDataId);
            $image->move($destinationPath, $name);

            $dailyDataObj->image = 'files/daily/' . $dailyDataId  . "/" . $name;
        }
        if ($request->hasFile('private_info')) {
            $image = $request->file('private_info');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('files/daily/' . $dailyDataId);
            $image->move($destinationPath, $name);

            $dailyDataObj->private_info = 'files/daily/' . $dailyDataId  . "/" . $name;
        }
        if ($request->hasFile('covid_info')) {
            $image = $request->file('covid_info');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('files/daily/' . $dailyDataId);
            $image->move($destinationPath, $name);

            $dailyDataObj->covid_info = 'files/daily/' . $dailyDataId  . "/" . $name;
        }

        $dailyDataObj->update();

        $tmpDataDailyExt = [];
        $tmpDataDailyExt['daily_data_id'] = $dailyDataId; 
        // $tmpDataDailyExt['marriedstatus'] = $input['marriedstatus']; 
        $tmpDataDailyExt['educationname'] = $input['educationname']; 
        $tmpDataDailyExt['exp_start_date_1'] = $input['exp_start_date_1']; 
        $tmpDataDailyExt['exp_end_date_1'] = $input['exp_end_date_1'];  
        $tmpDataDailyExt['exp_company_1'] = $input['exp_company_1']; 
        $tmpDataDailyExt['exp_province_1'] = $input['exp_province_1']; 
        $tmpDataDailyExt['exp_salary_1'] = $input['exp_salary_1']; 
        $tmpDataDailyExt['exp_case_1'] = $input['exp_case_1']; 
        $tmpDataDailyExt['exp_start_date_2'] = $input['exp_start_date_2']; 
        $tmpDataDailyExt['exp_end_date_2'] = $input['exp_end_date_2'];  
        $tmpDataDailyExt['exp_company_2'] = $input['exp_company_2']; 
        $tmpDataDailyExt['exp_province_2'] = $input['exp_province_2'];  
        $tmpDataDailyExt['exp_salary_2'] = $input['exp_salary_2'];  
        $tmpDataDailyExt['exp_case_2'] = $input['exp_case_2']; 
        // $tmpDataDailyExt['refer_name'] = $input['refer_name'];
        // $tmpDataDailyExt['refer_relation'] = $input['refer_relation'];
        $tmpDataDailyExt['contact_init'] = $input['contact_init'];
        $tmpDataDailyExt['contact_name'] = $input['contact_name'];
        $tmpDataDailyExt['contact_lname'] = $input['contact_lname']; 
        // $tmpDataDailyExt['refer_addr1'] = $input['refer_addr1']; 
        // $tmpDataDailyExt['refer_addr2'] = $input['refer_addr2']; 
        // $tmpDataDailyExt['contact_tumbon'] = $input['contact_tumbon']; 
        // $tmpDataDailyExt['contact_aumpher'] = $input['contact_aumpher']; 
        // $tmpDataDailyExt['contact_province'] = $input['contact_province']; 
        $tmpDataDailyExt['contact_tel'] = $input['contact_tel']; 
        $tmpDataDailyExt['contact_relation'] = $input['contact_relation'];
        // $tmpDataDailyExt['criminal'] = $input['criminal']; 
        // $tmpDataDailyExt['criminal_other'] = $input['criminal_other']; 
        $tmpDataDailyExt['congenital_disease'] = $input['congenital_disease']; 
        $tmpDataDailyExt['congenital_disease_other'] = $input['congenital_disease_other'];
        // $tmpDataDailyExt['health_check_date'] = $input['health_check_date']; 
        // $tmpDataDailyExt['health_check_case'] = $input['health_check_case']; 
        // $tmpDataDailyExt['health_check_status'] = $input['health_check_status']; 
        // $tmpDataDailyExt['health_check_status_note'] = $input['health_check_status_note']; 
        $tmpDataDailyExt['social_security_hospital_flag'] = $input['social_security_hospital_flag'];
        $tmpDataDailyExt['social_security_hospital'] = $input['social_security_hospital'];

        // dd($tmpDataDailyExt);

        $dailyDataExtObj = DailyDataExt::create($tmpDataDailyExt);

        Mail::to('hr@lannaagro.com')->send(new ApplyDailyComplete($dailyDataObj));

        return redirect(url('/regdailycomplete'));
    }
}
