<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApplyComplete;
use App\MasterData;

class TestController extends Controller
{
    public function index(){
        $masterObj = MasterData::first();
        Mail::to('parinya.k@lannaagro.com')->send(new ApplyComplete($masterObj));
    }
}
