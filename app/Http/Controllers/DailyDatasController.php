<?php

namespace App\Http\Controllers;

use App\DailyData;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use DateTime;

class DailyDatasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $weightcond = $request->get('weightcond');
        $heightcond = $request->get('heightcond');
        $agecond = $request->get('agecond');
        $educatecond = $request->get('educatecond');
        $dtstart = $request->get('dtstart');
        $dtend = $request->get('dtend');
        $ftmtype = $request->get('ftmtype');
        $perPage = 25;
        // dd($request);

        $educationlist = [
            "1" => 'ประถมศึกษาชั้นปีที่6',
            "2" => "มัธยมศึกษาชั้นปีที่3",
            "3" => "มัธยมศึกษาชั้นปีที่6 / ปวช.",
            "4" => "ปวส.",
            "5" => "ปริญญาตรี"
        ];

        $dataDailyObj = new DailyData();

        if (!empty($keyword)) {
            $dataDailyObj = $dataDailyObj->where('fname', 'like', '%' . $keyword . '%')->orWhere('lname', 'like', '%' . $keyword . '%');
        }

        if (!empty($weightcond)) {
            $dataDailyObj = $dataDailyObj->where('weight', '<=', '100');
        }

        if (!empty($heightcond)) {
            $dataDailyObj = $dataDailyObj->where('height', '>=', '150');
        }

        if (!empty($educatecond)) {
            $dataDailyObj = $dataDailyObj->where('education', '>=', '1');
        }

        if (!empty($agecond)) {
            $lastdate = date('Y-m-d', strtotime('-45 years'));
            $dataDailyObj = $dataDailyObj->where('birth', '>=', $lastdate);
        }

        if (!empty($dtstart) && !empty($dtend)) {
            $dataDailyObj = $dataDailyObj->where('created_at', '>=', $dtstart . " 00:00")->where('created_at', '<=', $dtend . " 23:59");
        } else {
            if (!empty($dtstart) || !empty($dtend)) {
                if (!empty($dtstart)) {
                    $dataDailyObj = $dataDailyObj->where('created_at', '>=', $dtstart . " 00:00")->where('created_at', '<=', $dtstart . " 23:59");
                } else {
                    $dataDailyObj = $dataDailyObj->where('created_at', '>=', $dtend . " 00:00")->where('created_at', '<=', $dtend . " 23:59");
                }
            }
        }
        //dd($dataDailyObj);
        if (!empty($ftmtype) && $ftmtype == 'excel') {
            //gen excel 
            $data = $dataDailyObj->latest()->get();
            $filename = $this->_genexcel($data);
            return response()->download('storage/' . $filename);
        } else {
            $dailydatas = $dataDailyObj->latest()->paginate($perPage);
            return view('daily-datas.index', compact('dailydatas', 'educationlist'));
        }
    }

    private function _genexcel($data)
    {
        $educationlist = [
            "1" => 'ประถมศึกษาชั้นปีที่6',
            "2" => "มัธยมศึกษาชั้นปีที่3",
            "3" => "มัธยมศึกษาชั้นปีที่6 / ปวช.",
            "4" => "ปวส.",
            "5" => "ปริญญาตรี"
        ];

        $spreadsheet = new Spreadsheet();
        $count_sheet = 0;
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValueByColumnAndRow(1, 1, "ข้อมูลส่วนตัว");
        $sheet->mergeCellsByColumnAndRow(1, 1, 8, 1);
        $sheet->setCellValueByColumnAndRow(9, 1, "ที่อยู่");
        $sheet->mergeCellsByColumnAndRow(9, 1, 15, 1);
        $sheet->setCellValueByColumnAndRow(16, 1, "ข้อมูลบุคคลที่สามารถติดต่อได้กรณีฉุกเฉิน");
        $sheet->mergeCellsByColumnAndRow(16, 1, 18, 1);
        $sheet->setCellValueByColumnAndRow(19, 1, "สุขภาพ / ความพร้อมทางร่างกาย");
        $sheet->mergeCellsByColumnAndRow(19, 1, 21, 1);
        $sheet->setCellValueByColumnAndRow(22, 1, "ประวัติการศึกษา");
        $sheet->mergeCellsByColumnAndRow(22, 1, 23, 1);
        $sheet->setCellValueByColumnAndRow(24, 1, "ประวัติการทำงาน 1");
        $sheet->mergeCellsByColumnAndRow(24, 1, 30, 1);
        $sheet->setCellValueByColumnAndRow(31, 1, "ประวัติการทำงาน 2");
        $sheet->mergeCellsByColumnAndRow(31, 1, 37, 1);

        $sheet->setCellValueByColumnAndRow(1, 2, "วันที่สมัคร");
        $sheet->setCellValueByColumnAndRow(2, 2, "ชื่อ สกุล");
        $sheet->setCellValueByColumnAndRow(3, 2, "เพศ");
        $sheet->setCellValueByColumnAndRow(4, 2, "วันเดือนปีเกิด");
        $sheet->setCellValueByColumnAndRow(5, 2, "อายุ");
        $sheet->setCellValueByColumnAndRow(6, 2, "เลขบัตรประชาชน");
        $sheet->setCellValueByColumnAndRow(7, 2, "น้ำหนัก (ก.ก.)");
        $sheet->setCellValueByColumnAndRow(8, 2, "ส่วนสูง (ซม.)");

        $sheet->setCellValueByColumnAndRow(9, 2, "เลขที่");
        $sheet->setCellValueByColumnAndRow(10, 2, "หมู่ที่");
        $sheet->setCellValueByColumnAndRow(11, 2, "ตำบล");
        $sheet->setCellValueByColumnAndRow(12, 2, "อำเภอ");
        $sheet->setCellValueByColumnAndRow(13, 2, "จังหวัด");
        $sheet->setCellValueByColumnAndRow(14, 2, "รหัสไปรษณีย์");
        $sheet->setCellValueByColumnAndRow(15, 2, "โทรศัพท์");

        $sheet->setCellValueByColumnAndRow(16, 2, "ชื่อ สกุล");
        $sheet->setCellValueByColumnAndRow(17, 2, "โทรศัพท์");
        $sheet->setCellValueByColumnAndRow(18, 2, "ความสัมพันธ์");

        $sheet->setCellValueByColumnAndRow(19, 2, "สิทธิประกันสังคม");
        $sheet->setCellValueByColumnAndRow(20, 2, "โรงพยาบาลประกันสังคม(ถ้ามี)");
        $sheet->setCellValueByColumnAndRow(21, 2, "โรคประจำตัว");

        $sheet->setCellValueByColumnAndRow(22, 2, "วุฒิการศึกษาสูงสุด");
        $sheet->setCellValueByColumnAndRow(23, 2, "สถานศึกษา");

        $sheet->setCellValueByColumnAndRow(24, 2, "ตั้งแต่วันที่");
        $sheet->setCellValueByColumnAndRow(25, 2, "ถึงวันที่");
        $sheet->setCellValueByColumnAndRow(26, 2, "สถานที่ทำงาน ");
        $sheet->setCellValueByColumnAndRow(27, 2, "จังหวัด");
        $sheet->setCellValueByColumnAndRow(28, 2, "ตำแหน่ง / ลักษณะงานที่ทำ ");
        $sheet->setCellValueByColumnAndRow(29, 2, "เงินเดือน");
        $sheet->setCellValueByColumnAndRow(30, 2, "เหตุที่ลาออก ");

        $sheet->setCellValueByColumnAndRow(31, 2, "ตั้งแต่วันที่ ");
        $sheet->setCellValueByColumnAndRow(32, 2, "ถึงวันที่ ");
        $sheet->setCellValueByColumnAndRow(33, 2, "สถานที่ทำงาน");
        $sheet->setCellValueByColumnAndRow(34, 2, "จังหวัด ");
        $sheet->setCellValueByColumnAndRow(35, 2, "ตำแหน่ง / ลักษณะงานที่ทำ");
        $sheet->setCellValueByColumnAndRow(36, 2, "เงินเดือน");
        $sheet->setCellValueByColumnAndRow(37, 2, "เหตุที่ลาออก");

        $rowstart = 3;
        $runrow = $rowstart;
        foreach ($data as $item) {
            $sheet->setCellValueByColumnAndRow(1, $runrow, $item->created_at);
            $sheet->setCellValueByColumnAndRow(2, $runrow, $item->init . " " . $item->fname . " " . $item->lname);
            if (!empty($item->sex)) {
                $sheet->setCellValueByColumnAndRow(3, $runrow, $item->sex);
            }
            if (!empty($item->birth)) {
                $sheet->setCellValueByColumnAndRow(4, $runrow, $item->birth);
            }
            if (!empty($item->birth)) {
                $now = new DateTime('now');
                $OldDate = new DateTime($item->birth);
                $result = $OldDate->diff($now);
                //echo $result->y;
                $sheet->setCellValueByColumnAndRow(5, $runrow, $result->y);
            }
            if (!empty($item->citizenid)) {
                $sheet->setCellValueByColumnAndRow(6, $runrow, " " . $item->citizenid);
            }
            if (!empty($item->weight)) {
                $sheet->setCellValueByColumnAndRow(7, $runrow, number_format($item->weight, 2, '.', ','));
            }
            if (!empty($item->height)) {
                $sheet->setCellValueByColumnAndRow(8, $runrow, number_format($item->height, 2, '.', ','));
            }

            if (!empty($item->addr)) {
                $sheet->setCellValueByColumnAndRow(9, $runrow, $item->addr);
            }
            if (!empty($item->addr2)) {
                $sheet->setCellValueByColumnAndRow(10, $runrow, $item->addr2);
            }
            if (!empty($item->subcity)) {
                $sheet->setCellValueByColumnAndRow(11, $runrow, $item->subcity);
            }
            if (!empty($item->city)) {
                $sheet->setCellValueByColumnAndRow(12, $runrow, $item->city);
            }
            if (!empty($item->province)) {
                $sheet->setCellValueByColumnAndRow(13, $runrow, $item->province);
            }
            if (!empty($item->zipcode)) {
                $sheet->setCellValueByColumnAndRow(14, $runrow, $item->zipcode);
            }
            if (!empty($item->tel)) {
                $sheet->setCellValueByColumnAndRow(15, $runrow, " " . $item->tel);
            }

            if (!empty($item->dailydataext->contact_name)) {
                $ctname = "";
                if (!empty($item->dailydataext->contact_init)) {
                    $ctname .= $item->dailydataext->contact_init . " ";
                }
                if (!empty($item->dailydataext->contact_name)) {
                    $ctname .= $item->dailydataext->contact_name . " ";
                }
                if (!empty($item->dailydataext->contact_lname)) {
                    $ctname .= $item->dailydataext->contact_lname;
                }
                $sheet->setCellValueByColumnAndRow(16, $runrow, $ctname);
            }
            if (!empty($item->dailydataext->contact_tel)) {
                $sheet->setCellValueByColumnAndRow(17, $runrow, " " . $item->dailydataext->contact_tel);
            }
            if (!empty($item->dailydataext->contact_relation)) {
                $sheet->setCellValueByColumnAndRow(18, $runrow, $item->dailydataext->contact_relation);
            }

            if (!empty($item->dailydataext->social_security_hospital_flag)) {
                $sheet->setCellValueByColumnAndRow(19, $runrow, $item->dailydataext->social_security_hospital_flag);
            }
            if (!empty($item->dailydataext->social_security_hospital)) {
                $sheet->setCellValueByColumnAndRow(20, $runrow, $item->dailydataext->social_security_hospital);
            }
            if (!empty($item->dailydataext->congenital_disease)) {
                if($item->dailydataext->congenital_disease == 'มี' && isset($item->dailydataext->congenital_disease_note)){
                    $sheet->setCellValueByColumnAndRow(21, $runrow, $item->dailydataext->congenital_disease_note);
                }else{
                    $sheet->setCellValueByColumnAndRow(21, $runrow, $item->dailydataext->congenital_disease);
                }
                
            }

            if (!empty($item->education)) {
                $sheet->setCellValueByColumnAndRow(22, $runrow, $educationlist[$item->education]);
            }
            if (!empty($item->dailydataext->educationname)) {
                $sheet->setCellValueByColumnAndRow(23, $runrow, $item->dailydataext->educationname);
            }

            if (!empty($item->dailydataext->exp_start_date_1)) {
                $sheet->setCellValueByColumnAndRow(24, $runrow, $item->dailydataext->exp_start_date_1);
            }
            if (!empty($item->dailydataext->exp_end_date_1)) {
                $sheet->setCellValueByColumnAndRow(25, $runrow, $item->dailydataext->exp_end_date_1);
            }
            if (!empty($item->dailydataext->exp_company_1)) {
                $sheet->setCellValueByColumnAndRow(26, $runrow, $item->dailydataext->exp_company_1);
            }
            if (!empty($item->dailydataext->exp_province_1)) {
                $sheet->setCellValueByColumnAndRow(27, $runrow, $item->dailydataext->exp_province_1);
            }
            if (!empty($item->dailydataext->exp_job_1)) {
                $sheet->setCellValueByColumnAndRow(28, $runrow, $item->dailydataext->exp_job_1);
            }
            if (!empty($item->dailydataext->exp_salary_1)) {
                $sheet->setCellValueByColumnAndRow(29, $runrow, number_format($item->dailydataext-> exp_salary_1, 2, '.', ','));
            }
            if (!empty($item->dailydataext->exp_case_1)) {
                $sheet->setCellValueByColumnAndRow(30, $runrow, $item->dailydataext->exp_case_1);
            }

            if (!empty($item->dailydataext->exp_start_date_2)) {
                $sheet->setCellValueByColumnAndRow(31, $runrow, $item->dailydataext->exp_start_date_2);
            }
            if (!empty($item->dailydataext->exp_end_date_2)) {
                $sheet->setCellValueByColumnAndRow(32, $runrow, $item->dailydataext->exp_end_date_2);
            }
            if (!empty($item->dailydataext->exp_company_2)) {
                $sheet->setCellValueByColumnAndRow(33, $runrow, $item->dailydataext->exp_company_2);
            }
            if (!empty($item->dailydataext->exp_province_2)) {
                $sheet->setCellValueByColumnAndRow(34, $runrow, $item->dailydataext->exp_province_2);
            }
            if (!empty($item->dailydataext->exp_job_2)) {
                $sheet->setCellValueByColumnAndRow(35, $runrow, $item->dailydataext->exp_job_2);
            }
            if (!empty($item->dailydataext->exp_salary_2)) {
                $sheet->setCellValueByColumnAndRow(36, $runrow, number_format($item->dailydataext-> exp_salary_2, 2, '.', ','));
            }
            if (!empty($item->dailydataext->exp_case_2)) {
                $sheet->setCellValueByColumnAndRow(37, $runrow, $item->dailydataext->exp_case_2);
            }
            $runrow++;
        }

        $writer = new Xlsx($spreadsheet);
        $filename = "export-daily-apply-" . date('yymmdd-hi') . ".xlsx";

        $writer->save('storage/' . $filename);


        return $filename;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $educationlist = [
            "1" => 'ประถมศึกษาชั้นปีที่6',
            "2" => "มัธยมศึกษาชั้นปีที่3",
            "3" => "มัธยมศึกษาชั้นปีที่6 / ปวช.",
            "4" => "ปวส.",
            "5" => "ปริญญาตรี"
        ];
        $dailydata = DailyData::findOrFail($id);
        return view('daily-datas.show', compact('dailydata', 'educationlist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DailyData::destroy($id);

        return redirect(url('/dailydatas'));
    }

    public function changeStatus($id, $status)
    {
        $dailydata = DailyData::findOrFail($id);
        $dailydata->status = $status;
        $dailydata->update();
        return redirect(url('/dailydatas'));
    }
}
