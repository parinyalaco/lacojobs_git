<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\MasterData;
use Illuminate\Http\Request;

class MasterDatasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin');
    }
    

    public function index(Request $request)
    {        
        $keyword = $request->get('search');
        $perPage = 25;
        

        if (!empty($keyword)) {
            $masterdatas = MasterData::latest()->paginate($perPage);
        } else {
            $masterdatas = MasterData::latest()->paginate($perPage);
        }

        return view('master-datas.index', compact('masterdatas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('master-datas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        MasterData::create($requestData);

        return redirect('master-datas')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $masterdata = MasterData::findOrFail($id);
        
        // dd($masterdata);
        // echo date($masterdata->created_at).'</br>';
        if(date($masterdata->created_at)<'2022-07-08 16:00:00'){
            // echo 'old</br>';
            return view('master-datas.show', compact('masterdata'));
        }else{
            // echo 'new</br>';
            return view('master-datas.show2', compact('masterdata'));
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $masterdata = MasterData::findOrFail($id);

        return view('master-datas.edit', compact('masterdata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $masterdata = MasterData::findOrFail($id);
        $masterdata->update($requestData);

        return redirect('master-datas')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        MasterData::destroy($id);

        return redirect()->route('masterdatas.index')->with('flash_message', ' deleted!');
    }

    public static function to_show_sal($salary)
    {
        // dd($salary);
        // echo $salary.'</br>';
        $arr1 = array();
        $var3 = '';
        // $var = trim($masterdata->job_salary1);
        $var = trim($salary);
        $chk = array('0','1','2','3','4','5','6','7','8','9',',','.');
        if(!empty($var)){
            // echo $var.'</br>';
            $var1 = str_replace($chk,'*' ,$var);
            $var2 = str_split($var1);
            foreach($var2 as $key=>$value){                                        
                if($value <> '*')  $arr1[] = $key;                                         
            }  
        }
        if(empty($arr1))   $var3 = number_format(str_replace(',','',$var),0);
        else    $var3 = $var;
        return $var3;
    }
}
