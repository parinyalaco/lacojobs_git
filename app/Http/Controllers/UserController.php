<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin');
    }
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        // $perPage = 25;
        $user = User::latest();

        if (!empty($keyword)) {
            $user = $user->where('name', 'like', '%'.$keyword.'%');
        }
        $user = $user->get();
        

        return view('user.index', compact('user'));
    }

    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'username' => ['required', 'string',  'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ]);

        // echo "password : ".$requestData['password'].", password_confirmation : ".$requestData['password_confirmation'];
        // exit();

        if(!($requestData['password']==$requestData['password_confirmation'])){
            return back()->withErrors(['password'=>'กรุณาระบุรหัสผ่านทีตรงกันค่ะ']);
        } 

        $requestData['password'] = bcrypt($requestData['password']);

        User::create($requestData);

        return redirect('user')->with('flash_message', ' added!');
    }

    public function show($id)
    {
        // $user = User::findOrFail($id);
        // // dd($user);
        // return view('editUser', compact('user'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        // dd($user);

        return view('user.edit', compact('user'));
        return redirect()->route('user.edit');
    }

    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        $user = User::findOrFail($id);

        // $user = auth()->user();  
        // dd($user);
        $userPassword = $user->password;
        
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            // 'username' => ['required', 'string',  'max:255', 'unique:users'],
            'old_password' => 'required',
            'password' => 'required|same:password_confirmation|min:4',
            'password_confirmation' => 'required',
        ]);

        if(!Hash::check($requestData['old_password'], $userPassword)){
            return back()->withErrors(['old_password'=>'กรุณาระบุรหัสผ่านที่ถูกต้องค่ะ']);
        }        

        // $user->password = Hash::make($request->password);
        $requestData['password'] = bcrypt($requestData['password']);
        // $user->change_pass = '1';
        $user->update($requestData);
        // return redirect()->route('afterLogin');
        // return redirect('user')->with('success','User updated successfully');
        
        // $masterdata = User::findOrFail($id);
        // $masterdata->update($requestData);

        return redirect('user')->with('flash_message', ' updated!');
    }

    public function destroy($id)
    {
        User::destroy($id);

        return redirect('user')->with('flash_message', ' deleted!');
    }
}
