<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterData;
use App\AcceptData;
use App\EducationData;
use App\TrainingData;
use App\ExpMasterData;
use App\MarriedData;
use App\MilitaryData;
use App\ExTrainingData;
use App\ExpDetailData;
use App\SimpleData;
use App\FamilyData;
use App\HealthData;
use App\RefData;
use App\UploadData;
use App\Mail\ApplyComplete;
use Illuminate\Support\Facades\Mail;

class ApplyController extends Controller
{
    public function index()
    {
        $flag = ' required '; // required '
        $testdata = false;
        return view('apply.stepform', compact('flag', 'testdata'));
        // /return view('auth.login');
    }

    public function applyjobAction(Request $request)
    {
        $input = $request->all();

        // var_dump($input);

        //Step1
        $masterDatatmp = array();
        $masterDatatmp['job_name1'] = $input['job_name1'];
        $masterDatatmp['job_name2'] = $input['job_name2'];
        $masterDatatmp['job_salary1'] = $input['job_salary1'];
        $masterDatatmp['job_salary2'] = $input['job_salary2'];
        $masterDatatmp['init_th'] = $input['init_th'];
        $masterDatatmp['fname_th'] = $input['fname_th'];
        $masterDatatmp['lname_th'] = $input['lname_th'];
        $masterDatatmp['nickname_th'] = $input['nickname_th'];
        $masterDatatmp['init_en'] = $input['init_en'];
        $masterDatatmp['fname_en'] = $input['fname_en'];
        $masterDatatmp['lname_en'] = $input['lname_en'];
        $masterDatatmp['nickname_en'] = $input['nickname_en'];
        $masterDatatmp['birth_date'] = $input['birth_date'];
        $masterDatatmp['age'] = 20;
        $masterDatatmp['weight'] = $input['weight'];
        $masterDatatmp['height'] = $input['height'];
        $masterDatatmp['blood'] = $input['blood'];
        $masterDatatmp['nationality'] = $input['nationality'];
        $masterDatatmp['race'] = $input['race'];
        $masterDatatmp['religion'] = $input['religion'];
        $masterDatatmp['addr1'] = $input['addr1'];
        $masterDatatmp['addr2'] = $input['addr2'];
        $masterDatatmp['block'] = $input['block'];
        $masterDatatmp['road'] = $input['road'];
        $masterDatatmp['subdistrict'] = $input['subdistrict'];
        $masterDatatmp['district'] = $input['district'];
        $masterDatatmp['province'] = $input['province'];
        $masterDatatmp['zipcode'] = $input['zipcode'];
        $masterDatatmp['tel'] = $input['tel'];
        $masterDatatmp['mobile'] = $input['mobile'];
        $masterDatatmp['addr_type'] = $input['addr_type'];
        if ($masterDatatmp['addr_type'] == 'บ้านของญาติ'){
            $masterDatatmp['addr_type_custom1'] = $input['addr_type1_custom1'];
        }elseif ($masterDatatmp['addr_type'] == 'หอพัก') {
            $masterDatatmp['addr_type_custom1'] = $input['addr_type2_custom1'];
            $masterDatatmp['addr_type_custom2'] = $input['addr_type2_custom2'];
        }
        $masterDatatmp['citizenid'] = $input['citizenid'];
        $masterDatatmp['citizenstartdate'] = $input['citizenstartdate'];
        $masterDatatmp['citizenexpdate'] = $input['citizenexpdate'];
        $masterDatatmp['email'] = $input['email'];
        $masterDatatmp['urgent_init'] = $input['urgent_init'];
        $masterDatatmp['urgent_name'] = $input['urgent_name'];
        $masterDatatmp['urgent_addr1'] = $input['urgent_addr1'];
        // $masterDatatmp['urgent_addr2'] = $input['urgent_addr2'];
        $masterDatatmp['urgent_mobile'] = $input['urgent_mobile'];
        $masterDatatmp['urgent_relation'] = $input['urgent_relation'];

        $masterObj = MasterData::create($masterDatatmp);
        $masterId = $masterObj->id;

        // $acceptDataTmp = array();
        // $acceptDataTmp['master_data_id'] = $masterId;
        // $acceptDataTmp['accept_1'] = $input['accept1'];
        // $acceptDataTmp['accept_2'] = $input['accept2'];
        // $acceptDataTmp['accept_3'] = $input['accept3'];
        // $acceptDataTmp['accept_4'] = $input['accept4'];
        // $acceptDataTmp['accept_5'] = $input['accept5'];
        // $acceptDataTmp['accept_6'] = $input['accept6'];


        // AcceptData::create($acceptDataTmp);

        $educationDataTmp = array();
        $educationDataTmp['master_data_id'] = $masterId;
        $educationDataTmp['school_1'] = $input['school_1'];
        $educationDataTmp['school_2'] = $input['school_2'];
        $educationDataTmp['school_3'] = $input['school_3'];
        $educationDataTmp['school_4'] = $input['school_4'];
        $educationDataTmp['school_5'] = $input['school_5'];
        $educationDataTmp['school_6'] = $input['school_6'];
        $educationDataTmp['province_1'] = $input['province_1'];
        $educationDataTmp['province_2'] = $input['province_2'];
        $educationDataTmp['province_3'] = $input['province_3'];
        $educationDataTmp['province_4'] = $input['province_4'];
        $educationDataTmp['province_5'] = $input['province_5'];
        $educationDataTmp['province_6'] = $input['province_6'];
        $educationDataTmp['title_1'] = $input['title_1'];
        $educationDataTmp['title_2'] = $input['title_2'];
        $educationDataTmp['title_3'] = $input['title_3'];
        $educationDataTmp['title_4'] = $input['title_4'];
        $educationDataTmp['title_5'] = $input['title_5'];
        $educationDataTmp['title_6'] = $input['title_6'];
        $educationDataTmp['year_1'] = $input['year_1'];
        $educationDataTmp['year_2'] = $input['year_2'];
        $educationDataTmp['year_3'] = $input['year_3'];
        $educationDataTmp['year_4'] = $input['year_4'];
        $educationDataTmp['year_5'] = $input['year_5'];
        $educationDataTmp['year_6'] = $input['year_6'];
        $educationDataTmp['grade_1'] = $input['grade_1'];
        $educationDataTmp['grade_2'] = $input['grade_2'];
        $educationDataTmp['grade_3'] = $input['grade_3'];
        $educationDataTmp['grade_4'] = $input['grade_4'];
        $educationDataTmp['grade_5'] = $input['grade_5'];
        $educationDataTmp['grade_6'] = $input['grade_6'];

        EducationData::create($educationDataTmp);

        $trainingDataTmp = array();
        $trainingDataTmp['master_data_id'] = $masterId;
        $trainingDataTmp['start_date_1'] = $input['train_start_date_1'];
        $trainingDataTmp['end_date_1'] = $input['train_end_date_1'];
        $trainingDataTmp['title_1'] = $input['train_title_1'];
        $trainingDataTmp['province_1'] = $input['train_province_1'];
        $trainingDataTmp['level_1'] = $input['train_level_1'];
        $trainingDataTmp['start_date_2'] = $input['train_start_date_2'];
        $trainingDataTmp['end_date_2'] = $input['train_end_date_2'];
        $trainingDataTmp['title_2'] = $input['train_title_2'];
        $trainingDataTmp['province_2'] = $input['train_province_2'];
        $trainingDataTmp['level_2'] = $input['train_level_2'];

        TrainingData::create($trainingDataTmp);

        $expMasterDatatmp = array();

        $expMasterDatatmp['master_data_id'] = $masterId;
        for ($i = 1; $i < 6; $i++) {
            // if ($i != 3) {
                $expMasterDatatmp['start_date_' . $i] = $input['exp_start_date_' . $i];
                $expMasterDatatmp['end_date_' . $i] = $input['exp_end_date_' . $i];
                $expMasterDatatmp['company_' . $i] = $input['exp_company_' . $i];
                $expMasterDatatmp['province_' . $i] = $input['exp_province_' . $i];
                $expMasterDatatmp['tel_' . $i] = $input['exp_tel_' . $i];
                $expMasterDatatmp['title_' . $i] = $input['exp_job_' . $i];
                $expMasterDatatmp['salary_' . $i] = $input['exp_salary_' . $i];
                $expMasterDatatmp['case_' . $i] = $input['exp_case_' . $i];
            // }
        }

        ExpMasterData::create($expMasterDatatmp);

        $exTrainingDatatmp = array();

        $exTrainingDatatmp['master_data_id'] = $masterId;
        $exTrainingDatatmp['start_date_1'] = $input['train_ex_start_date_1'];
        $exTrainingDatatmp['end_date_1'] = $input['train_ex_end_date_1'];
        $exTrainingDatatmp['duration_1'] = $input['train_ex_duration_1'];
        $exTrainingDatatmp['title_1'] = $input['train_ex_title_1'];
        $exTrainingDatatmp['by_1'] = $input['train_ex_by_1'];
        $exTrainingDatatmp['start_date_2'] = $input['train_ex_start_date_2'];
        $exTrainingDatatmp['end_date_2'] = $input['train_ex_end_date_2'];
        $exTrainingDatatmp['duration_2'] = $input['train_ex_duration_2'];
        $exTrainingDatatmp['title_2'] = $input['train_ex_title_2'];
        $exTrainingDatatmp['by_2'] = $input['train_ex_by_2'];

        ExTrainingData::create($exTrainingDatatmp);

        $expDetailDataTmp = array();

        $expDetailDataTmp['master_data_id'] = $masterId;
        $expDetailDataTmp['expd_type_th'] = $input['expd_type_th'];
        $expDetailDataTmp['expd_type_en'] = $input['expd_type_en'];
        // $expDetailDataTmp['expd_type_note'] = $input['expd_type_note'];
        // $expDetailDataTmp['expd_extra'] = $input['expd_extra'];

        $expDetailDataTmp['expd_com_program1'] = $input['expd_com_program1'];
        $expDetailDataTmp['expd_com_program2'] = $input['expd_com_program2'];
        $expDetailDataTmp['expd_com_program3'] = $input['expd_com_program3'];
        // $expDetailDataTmp['expd_com_program4'] = $input['expd_com_program4'];
        // $expDetailDataTmp['expd_com_program5'] = $input['expd_com_program5'];
        // $expDetailDataTmp['expd_com_program6'] = $input['expd_com_program6'];

        $expDetailDataTmp['expd_com_level1'] = $input['expd_com_level1'];
        $expDetailDataTmp['expd_com_level2'] = $input['expd_com_level2'];
        $expDetailDataTmp['expd_com_level3'] = $input['expd_com_level3'];
        // $expDetailDataTmp['expd_com_level4'] = $input['expd_com_level4'];
        // $expDetailDataTmp['expd_com_level5'] = $input['expd_com_level5'];
        // $expDetailDataTmp['expd_com_level6'] = $input['expd_com_level6'];

        $expDetailDataTmp['expd_lang_ex'] = $input['expd_lang_ex'];
        $expDetailDataTmp['expd_lang_speak_th'] = $input['expd_lang_speak_th'];

        $expDetailDataTmp['expd_lang_speak_en'] = $input['expd_lang_speak_en'];
        $expDetailDataTmp['expd_lang_speak_ch'] = $input['expd_lang_speak_ch'];
        $expDetailDataTmp['expd_lang_speak_ex'] = $input['expd_lang_speak_ex'];

        $expDetailDataTmp['expd_lang_read_th'] = $input['expd_lang_read_th'];
        $expDetailDataTmp['expd_lang_read_en'] = $input['expd_lang_read_en'];
        $expDetailDataTmp['expd_lang_read_ch'] = $input['expd_lang_read_ch'];
        $expDetailDataTmp['expd_lang_read_ex'] = $input['expd_lang_read_ex'];

        $expDetailDataTmp['expd_lang_write_th'] = $input['expd_lang_write_th'];
        $expDetailDataTmp['expd_lang_write_en'] = $input['expd_lang_write_en'];
        $expDetailDataTmp['expd_lang_write_ch'] = $input['expd_lang_write_ch'];
        $expDetailDataTmp['expd_lang_write_ex'] = $input['expd_lang_write_ex'];

        $expDetailDataTmp['expd_can_drive'] = $input['expd_can_drive'];
        $expDetailDataTmp['expd_have_drive_licen'] = $input['expd_have_drive_licen'];
        $expDetailDataTmp['expd_drive_licen_type'] = $input['expd_drive_licen_type'];
        $expDetailDataTmp['expd_drive_licen_date'] = $input['expd_drive_licen_date'];
        $expDetailDataTmp['expd_drive_licen_exp'] = $input['expd_drive_licen_exp'];
        // $expDetailDataTmp['expd_my_car_brand'] = $input['expd_my_car_brand'];
        // $expDetailDataTmp['expd_my_car_serie'] = $input['expd_my_car_serie'];

        $expDetailDataTmp['expd_can_ride'] = $input['expd_can_ride'];
        $expDetailDataTmp['expd_have_ride_licen'] = $input['expd_have_ride_licen'];
        $expDetailDataTmp['expd_ride_licen_type'] = $input['expd_ride_licen_type'];
        $expDetailDataTmp['expd_ride_licen_date'] = $input['expd_ride_licen_date'];
        $expDetailDataTmp['expd_ride_licen_exp'] = $input['expd_ride_licen_exp'];
        // $expDetailDataTmp['expd_my_bike_brand'] = $input['expd_my_bike_brand'];
        // $expDetailDataTmp['expd_my_bike_serie'] = $input['expd_my_bike_serie'];


        $expDetailDataTmp['expd_others'] = $input['expd_others'];

        ExpDetailData::create($expDetailDataTmp);

        $marriedDataTmp = array();

        $marriedDataTmp['master_data_id'] = $masterId;
        $marriedDataTmp['status'] = $input['married_status'];
        $marriedDataTmp['init'] = $input['married_init'];
        $marriedDataTmp['name'] = $input['married_name'];
        $marriedDataTmp['age'] = $input['married_age'];
        $marriedDataTmp['career'] = $input['married_career'];
        $marriedDataTmp['homeaddr'] = $input['married_homeaddr'];
        $marriedDataTmp['tel'] = $input['married_tel'];
        $marriedDataTmp['companyaddr'] = $input['married_companyaddr'];
        $marriedDataTmp['job'] = $input['married_job'];
        $marriedDataTmp['companytel'] = $input['married_companytel'];
        $marriedDataTmp['child_status'] = $input['married_child_status'];
        $marriedDataTmp['child_number'] = $input['married_child_number'];
        $marriedDataTmp['boy_no'] = $input['married_boy_no'];
        $marriedDataTmp['boy_age'] = $input['married_boy_age'];
        $marriedDataTmp['daughter_no'] = $input['married_daughter_no'];
        $marriedDataTmp['daughter_age'] = $input['married_daughter_age'];

        MarriedData::create($marriedDataTmp);

        $militaryDataTmp = array();

        $militaryDataTmp['master_data_id'] = $masterId;
        $militaryDataTmp['status'] = $input['military_status'];
        if ($militaryDataTmp['status'] == 'ยังไม่ได้รับการเกณฑ์ทหาร') {
            $militaryDataTmp['year'] = $input['military1_year'];
        } elseif ($militaryDataTmp['status'] == 'เข้ารับการเกณฑ์ทหารแล้ว') {
            $militaryDataTmp['custom1'] = $input['military2_custom1'];
            $militaryDataTmp['duration'] = $input['military2_duration'];
            $militaryDataTmp['custom2'] = $input['military2_custom2'];
        } elseif ($militaryDataTmp['status'] == 'เรียนรักษาดินแดน(ร.ด.)') {
            $militaryDataTmp['year'] = $input['military3_year'];
        } elseif ($militaryDataTmp['status'] == 'อื่นๆ') {
            $militaryDataTmp['custom1'] = $input['military_custom1'];
        }

        MilitaryData::create($militaryDataTmp);

        $simpleDataTmp = array();

        $simpleDataTmp['master_data_id'] = $masterId;
        $simpleDataTmp['current_edu_level'] = $input['current_edu_level'];
        $simpleDataTmp['current_edu_dep'] = $input['current_edu_dep'];
        $simpleDataTmp['current_edu_year'] = $input['current_edu_year'];
        $simpleDataTmp['current_edu_school'] = $input['current_edu_school'];
        $simpleDataTmp['future_edu_level'] = $input['future_edu_level'];
        $simpleDataTmp['future_edu_dep'] = $input['future_edu_dep'];
        $simpleDataTmp['future_edu_year'] = $input['future_edu_year'];
        $simpleDataTmp['future_edu_school'] = $input['future_edu_school'];
        $simpleDataTmp['hobby_1'] = $input['hobby_1'];
        $simpleDataTmp['hobby_2'] = $input['hobby_2'];
        $simpleDataTmp['hobby_3'] = $input['hobby_3'];
        $simpleDataTmp['drunker'] = $input['drunker'];
        $simpleDataTmp['smoker'] = $input['smoker'];
        $simpleDataTmp['smoker_other'] = $input['smoker_other'];
        $simpleDataTmp['criminal'] = $input['criminal'];
        $simpleDataTmp['criminal_other'] = $input['criminal_other'];
        $simpleDataTmp['control'] = $input['control'];
        $simpleDataTmp['control_other'] = $input['control_other'];
        $simpleDataTmp['can_other_country'] = $input['can_other_country'];
        $simpleDataTmp['can_other_country_case'] = $input['can_other_country_case'];
        $simpleDataTmp['apply_job_no'] = $input['apply_job_no'];
        $simpleDataTmp['apply_job_name'] = $input['apply_job_name'];
        $simpleDataTmp['apply_job_company'] = $input['apply_job_company'];
        $simpleDataTmp['hear_from'] = $input['hear_from'];
        $simpleDataTmp['hear_from_other'] = $input['hear_from_other'];
        $simpleDataTmp['refer_name'] = $input['refer_name'];
        $simpleDataTmp['your_comments'] = $input['your_comments'];
        $simpleDataTmp['can_start_in'] = $input['can_start_in'];
        $simpleDataTmp['can_start_desc'] = $input['can_start_desc'];

        SimpleData::create($simpleDataTmp);

        $familyDataTmp = array();

        $familyDataTmp['master_data_id'] = $masterId;
        $familyDataTmp['father_name'] = $input['father_name'];
        $familyDataTmp['father_age'] = $input['father_age'];
        $familyDataTmp['father_occupation'] = $input['father_occupation'];
        $familyDataTmp['father_company'] = $input['father_company'];
        $familyDataTmp['father_job'] = $input['father_job'];
        $familyDataTmp['father_company_tel'] = $input['father_company_tel'];
        $familyDataTmp['father_addr'] = $input['father_addr'];
        $familyDataTmp['father_tel'] = $input['father_tel'];
        $familyDataTmp['mother_name'] = $input['mother_name'];
        $familyDataTmp['mother_age'] = $input['mother_age'];
        $familyDataTmp['mother_occupation'] = $input['mother_occupation'];
        $familyDataTmp['mother_company'] = $input['mother_company'];
        $familyDataTmp['mother_job'] = $input['mother_job'];
        $familyDataTmp['mother_company_tel'] = $input['mother_company_tel'];
        $familyDataTmp['mother_addr'] = $input['mother_addr'];
        $familyDataTmp['mother_tel'] = $input['mother_tel'];
        $familyDataTmp['total_nabor'] = $input['total_nabor'];
        $familyDataTmp['total_brother'] = $input['total_brother'];
        $familyDataTmp['total_sisther'] = $input['total_sisther'];
        $familyDataTmp['name_1'] = $input['nabor_name_1'];
        $familyDataTmp['age_1'] = $input['nabor_age_1'];
        $familyDataTmp['company_1'] = $input['nabor_company_1'];
        $familyDataTmp['level_1'] = $input['nabor_level_1'];
        $familyDataTmp['name_2'] = $input['nabor_name_2'];
        $familyDataTmp['age_2'] = $input['nabor_age_2'];
        $familyDataTmp['company_2'] = $input['nabor_company_2'];
        $familyDataTmp['level_2'] = $input['nabor_level_2'];
        $familyDataTmp['name_3'] = $input['nabor_name_3'];
        $familyDataTmp['age_3'] = $input['nabor_age_3'];
        $familyDataTmp['company_3'] = $input['nabor_company_3'];
        $familyDataTmp['level_3'] = $input['nabor_level_3'];
        // $familyDataTmp['name_4'] = $input['nabor_name_4'];
        // $familyDataTmp['age_4'] = $input['nabor_age_4'];
        // $familyDataTmp['company_4'] = $input['nabor_company_4'];
        // $familyDataTmp['level_4'] = $input['nabor_level_4'];
        // $familyDataTmp['name_5'] = $input['nabor_name_5'];
        // $familyDataTmp['age_5'] = $input['nabor_age_5'];
        // $familyDataTmp['company_5'] = $input['nabor_company_5'];
        // $familyDataTmp['level_5'] = $input['nabor_level_5'];

        FamilyData::create($familyDataTmp);

        $healthDataTmp = array();

        $healthDataTmp['master_data_id'] = $masterId;
        $healthDataTmp['congenital_disease'] = $input['congenital_disease'];
        $healthDataTmp['congenital_disease_other'] = $input['congenital_disease_other'];
        $healthDataTmp['contagious_disease'] = $input['contagious_disease'];
        $healthDataTmp['contagious_disease_other'] = $input['contagious_disease_other'];
        $healthDataTmp['allergy'] = $input['allergy'];
        $healthDataTmp['allergy_other'] = $input['allergy_other'];
        $healthDataTmp['back_pain'] = $input['back_pain'];
        $healthDataTmp['finger_pain'] = $input['finger_pain'];
        $healthDataTmp['thalassemia'] = $input['thalassemia'];
        $healthDataTmp['liver_virus'] = $input['liver_virus'];
        $healthDataTmp['food_allergy'] = $input['food_allergy'];
        $healthDataTmp['food_allergy_other'] = $input['food_allergy_other'];
        $healthDataTmp['drug_allergy'] = $input['drug_allergy'];
        $healthDataTmp['drug_allergy_other'] = $input['drug_allergy_other'];
        $healthDataTmp['other_disease'] = $input['other_disease'];
        $healthDataTmp['physical_exam_time'] = $input['physical_exam_time'];
        $healthDataTmp['physical_exam_part'] = $input['physical_exam_part'];
        $healthDataTmp['physical_exam_result'] = $input['physical_exam_result'];
        $healthDataTmp['physical_exam_result_other'] = $input['physical_exam_result_other'];
        $healthDataTmp['prev_social_sec_company'] = $input['prev_social_sec_company'];
        $healthDataTmp['prev_social_sec_hospital'] = $input['prev_social_sec_hospital'];

        HealthData::create($healthDataTmp);

        $refDataTmp = array();
        $refDataTmp['master_data_id'] = $masterId;
        $refDataTmp['name_1'] = $input['ref_name_1'];
        $refDataTmp['relation_1'] = $input['ref_relation_1'];
        $refDataTmp['company_job_1'] = $input['ref_company_job_1'];
        $refDataTmp['tel_1'] = $input['ref_tel_1'];
        $refDataTmp['name_2'] = $input['ref_name_2'];
        $refDataTmp['relation_2'] = $input['ref_relation_2'];
        $refDataTmp['company_job_2'] = $input['ref_company_job_2'];
        $refDataTmp['tel_2'] = $input['ref_tel_2'];

        RefData::create($refDataTmp);

        $uploadDataTmp = array();
        $uploadDataTmp['master_data_id'] = $masterId;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = md5($image->getClientOriginalName() . time()) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('files/' . $masterId);
            $image->move($destinationPath, $name);

            $uploadDataTmp['image'] = 'files/' . $masterId  . "/" . $name;
        }

        if ($request->hasFile('private_info')) {
            $private_info = $request->file('private_info');
            $name = md5($private_info->getClientOriginalName() . time()) . '.' . $private_info->getClientOriginalExtension();
            $destinationPath = public_path('files/' . $masterId);
            $private_info->move($destinationPath, $name);

            $uploadDataTmp['private_info'] = 'files/' . $masterId  . "/" . $name;
        }

        if ($request->hasFile('resume')) {
            $resume = $request->file('resume');
            $name = md5($resume->getClientOriginalName() . time()) . '.' . $resume->getClientOriginalExtension();
            $destinationPath = public_path('files/' . $masterId);
            $resume->move($destinationPath, $name);

            $uploadDataTmp['resume'] = 'files/' . $masterId  . "/" . $name;
        }

        $uploadDataTmp['map'] = $input['map'];
        UploadData::create($uploadDataTmp);


        // Mail::to('hr@lannaagro.com')->send(new ApplyComplete($masterObj));

        return redirect(url('/regcomplete'));
    }
}
