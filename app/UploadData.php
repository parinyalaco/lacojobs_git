<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadData extends Model
{
    protected $fillable = [
			'master_data_id',
			'image',
			'resume',
			'private_info',
			'map'];
}
