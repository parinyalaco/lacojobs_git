<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcceptData extends Model
{
    protected $fillable = ['master_data_id',
			'accept_1',
			'accept_2',
			'accept_3',
			'accept_4',
		'accept_5',
		'accept_6'];
}
