<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyDataExt extends Model
{
    protected $fillable = [
        'daily_data_id','marriedstatus','educationname',
        'exp_start_date_1','exp_end_date_1', 'exp_company_1','exp_province_1','exp_salary_1','exp_case_1', 'exp_job_1', 
        'exp_start_date_2','exp_end_date_2',  'exp_company_2','exp_province_2','exp_salary_2','exp_case_2', 'exp_job_2', 
        'refer_name', 'refer_relation', 
        'contact_name','refer_addr1','refer_addr2', 'contact_tumbon','contact_aumpher', 'contact_province','contact_tel','contact_relation',
        'criminal','criminal_other', 'congenital_disease','congenital_disease_other',
        'health_check_date','health_check_case','health_check_status','health_check_status_note',
        'social_security_hospital', 'contact_init', 'contact_lname', 'social_security_hospital_flag',
    ];
}
