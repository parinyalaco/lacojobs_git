<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpDetailData extends Model
{
    protected $fillable = ['master_data_id','expd_type_th','expd_type_en',
    'expd_type_note','expd_extra','expd_com_program1','expd_com_level1','expd_com_program2',
    'expd_com_program3','expd_com_program4','expd_com_program5','expd_com_program6','expd_com_level2',
    'expd_com_level3','expd_com_level4','expd_com_level5','expd_com_level6','expd_lang_ex','expd_lang_speak_th',
    'expd_lang_speak_en','expd_lang_speak_ch','expd_lang_speak_ex','expd_lang_read_th','expd_lang_read_en',
    'expd_lang_read_ch','expd_lang_read_ex','expd_lang_write_th','expd_lang_write_en','expd_lang_write_ch',
    'expd_lang_write_ex','expd_can_drive','expd_have_drive_licen','expd_drive_licen_type','expd_drive_licen_date',
    'expd_drive_licen_exp','expd_my_car_brand','expd_my_car_serie','expd_can_ride','expd_have_ride_licen',
    'expd_ride_licen_type','expd_ride_licen_date','expd_ride_licen_exp','expd_my_bike_brand',
    'expd_my_bike_serie','expd_others'];
}
