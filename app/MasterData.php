<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterData extends Model
{
    protected $fillable = ['job_name1',
			'job_name2',
			'job_salary1',
			'job_salary2',
			'init_th',
			'fname_th',
			'lname_th',
			'nickname_th',
			'init_en',
			'fname_en',
			'lname_en',
			'nickname_en',
			'birth_date',
			'age',
			'weight',
			'height',
			'blood',
			'nationality',
			'race',
			'religion',
			'addr1',
			'addr2',
			'subdistrict',
			'district',
			'province',
			'zipcode',
			'tel',
			'mobile',
			'addr_type',
			'addr_type_custom1',
			'addr_type_custom2',
			'citizenid',
			'citizenstartdate',
			'citizenexpdate',
			'email',
			'urgent_init',
			'urgent_name',
			'urgent_addr1',
			'urgent_addr2',
			'urgent_mobile',
			'urgent_relation',
			'addr2',
			'block',
			'road'
	];

	public function uploaddata()
	{
		return $this->hasOne('App\UploadData', 'master_data_id');
	}

	public function educationdata()
	{
		return $this->hasOne('App\EducationData', 'master_data_id');
	}

	public function trainingdata()
	{
		return $this->hasOne('App\TrainingData', 'master_data_id');
	}
	public function expmasterdata()
	{
		return $this->hasOne('App\ExpMasterData', 'master_data_id');
	}

	public function extrainingdata()
	{
		return $this->hasOne('App\ExTrainingData', 'master_data_id');
	}

	public function expdetaildata()
	{
		return $this->hasOne('App\ExpDetailData', 'master_data_id');
	}

	public function marrieddata()
	{
		return $this->hasOne('App\MarriedData', 'master_data_id');
	}
	
	public function militarydata()
	{
		return $this->hasOne('App\MilitaryData', 'master_data_id');
	}
	
	public function simpledata()
	{
		return $this->hasOne('App\SimpleData', 'master_data_id');
	}

	public function familydata()
	{
		return $this->hasOne('App\FamilyData', 'master_data_id');
	}
	
	public function healthdata()
	{
		return $this->hasOne('App\HealthData', 'master_data_id');
	}

	public function refdata()
	{
		return $this->hasOne('App\RefData', 'master_data_id');
	}
	
}
