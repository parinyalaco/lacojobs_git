<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefData extends Model
{
    protected $fillable = [
			'master_data_id',
			'name_1',
			'relation_1',
			'company_job_1',
			'tel_1',
			'name_2',
			'relation_2',
			'company_job_2',
			'tel_2',
			'name_3',
			'relation_3',
			'company_job_3',
			'tel_3'];
}
