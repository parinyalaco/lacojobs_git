<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarriedData extends Model
{
    protected $fillable = [
			'master_data_id',
			'status',
			'init',
			'name',
			'age',
			'career',
			'homeaddr',
			'tel',
			'companyaddr',
			'job',
			'companytel',
			'child_status',
			'child_number',
			'boy_no',
			'boy_age',
			'daughter_no',
			'daughter_age'];
}
