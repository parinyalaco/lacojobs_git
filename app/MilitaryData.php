<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MilitaryData extends Model
{
    protected $fillable = [
			'master_data_id',
			'status',
			'year',
			'duration',
			'custom1',
			'custom2',
			'custom3'];
}
