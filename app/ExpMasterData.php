<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpMasterData extends Model
{
    protected $fillable = [
			'master_data_id',
			'start_date_1',
			'end_date_1',
			'company_1',
			'province_1',
			'tel_1',
			'title_1',
			'salary_1',
			'case_1',
			'start_date_2',
			'end_date_2',
			'company_2',
			'province_2',
			'tel_2',
			'title_2',
			'salary_2',
			'case_2',
			'start_date_3',
			'end_date_3',
			'company_3',
			'province_3',
			'tel_3',
			'title_3',
			'salary_3',
			'case_3',
        'start_date_4',
			'end_date_4',
			'company_4',
			'province_4',
			'tel_4',
			'title_4',
			'salary_4',
			'case_4',
        'start_date_5',
			'end_date_5',
			'company_5',
			'province_5',
			'tel_5',
			'title_5',
			'salary_5',
			'case_5',
        ];
}
