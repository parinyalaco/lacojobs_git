<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingData extends Model
{
    protected $fillable = [
			'master_data_id',
			'start_date_1',
			'end_date_1',
			'title_1',
			'province_1',
			'level_1',
			'start_date_2',
			'end_date_2',
			'title_2',
			'province_2',
			'level_2',
			'start_date_3',
			'end_date_3',
			'title_3',
			'province_3',
			'level_3'];
}
