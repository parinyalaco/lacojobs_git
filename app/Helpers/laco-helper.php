<?php

if (!function_exists('tothaiyear')) {
    function tothaiyear($date)
    {
        $dateArr = explode('-', $date);
        if(sizeof($dateArr) == 3){
            $dateThai = $dateArr[2] . '/' . $dateArr[1] . '/' . ($dateArr['0'] + 543);
        }else{
            $dateThai = $date;
        }
        
        return $dateThai;
    }
}

if (!function_exists('chekempty')) {
    function chekempty($str,$sign = '-')
    {
        if(!empty($str)) {
            return $str;
        }else{
            return $sign;
        }
    }
}

if (!function_exists('filldot')) {
    function filldot($str, $len = 20, $sign = '.')
    {
        if (!empty($str)) {
            $data = $len-2-strlen($str);
            if($data < 0){
                $data = 0;
            }
            return "..". $str. str_repeat($sign,$data);
        } else {
            return str_repeat($sign,$len);
        }
    }
}
