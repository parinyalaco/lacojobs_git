<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimpleData extends Model
{
    protected $fillable = [
			'master_data_id',
			'current_edu_level',
			'current_edu_dep',
			'current_edu_year',
			'current_edu_school',
			'future_edu_level',
			'future_edu_dep',
			'future_edu_year',
			'future_edu_school',
			'hobby_1',
			'hobby_2',
			'hobby_3',
			'drunker',
			'smoker',
			'smoker_other',
			'criminal',
			'criminal_other',
			'control',
			'control_other',
			'can_other_country',
			'can_other_country_case',
			'apply_job_no',
			'apply_job_name',
			'apply_job_company',
			'hear_from',
			'hear_from_other',
			'refer_name',
			'your_comments',
			'can_start_in',
			'can_start_desc'];
}
