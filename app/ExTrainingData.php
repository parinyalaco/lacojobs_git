<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExTrainingData extends Model
{
    protected $fillable = [
			'master_data_id',
			'start_date_1',
			'end_date_1',
			'duration_1',
			'title_1',
			'by_1',
			'start_date_2',
			'end_date_2',
			'duration_2',
			'title_2',
			'by_2',
			'start_date_3',
			'end_date_3',
			'duration_3',
			'title_3',
			'by_3'
		];
}
