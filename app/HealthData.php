<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthData extends Model
{
    protected $fillable = ['master_data_id', 'congenital_disease', 'congenital_disease_other',
		 'contagious_disease', 'contagious_disease_other', 'allergy', 'allergy_other',
		  'back_pain', 'finger_pain', 'thalassemia', 'liver_virus', 'food_allergy',
		   'food_allergy_other', 'drug_allergy', 'drug_allergy_other', 'other_disease',
			'physical_exam_time', 'physical_exam_part', 'physical_exam_result',
			 'physical_exam_result_other', 'prev_social_sec_company', 'prev_social_sec_hospital'];
}
