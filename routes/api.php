<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\DailyData;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('dailyuser/{code}', function ($code) {
    $data = DailyData::where('citizenid',$code)->orderBy('id','desc')->first();
    $tmp = [];
    if(!empty($data)){
        $tmp['data'] = $data;
        $tmp['dataext'] = $data->dailydataext;
        return $tmp;
    }else{
        return 404;
    }
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
