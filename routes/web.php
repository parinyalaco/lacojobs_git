<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('month');
});

// Route::get('/', 'ApplyController@index');
Route::get('/monthform', 'ApplyController@index');

Route::get('/daily', function () {
    return view('daily');
});
// Route::get('/daily', 'ApplyDailysController@index');
Route::get('/dailyform', 'ApplyDailysController@index');

Route::get('tothaiyear', function () {
    echo tothaiyear('2020-09-08');
});

Route::get('/regcomplete', function () {
    return view('regcomplete');
});

Route::get('/regdailycomplete', function () {
    return view('regdailycomplete');
});

Route::get('masterdatas/delete/{id}', 'MasterDatasController@destroy')->name('masterdatas_delete');
Route::resource('masterdatas', 'MasterDatasController');

Route::get('dailydatas/changeStatus/{id}/{status}', 'DailyDatasController@changeStatus')->name('dailydatas.changestatus');
Route::resource('dailydatas', 'DailyDatasController');

Route::get('/applyjob', 'ApplyController@index');
Route::post('/applyjobAction', 'ApplyController@applyjobAction');

Route::get('/applyjobdaily', 'ApplyDailysController@index');
Route::post('/applyjobdailyAction', 'ApplyDailysController@applyAction');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/user', 'UserController');

Route::get('/addUser', function () {
    return view('auth/register');
});


