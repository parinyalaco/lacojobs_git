

<?php $__env->startSection('content'); ?>
<div style="max-width: 95%;margin-left:auto;margin-right:auto;">
        <div class="row">

            <div class="col-md-12">
                <div class="row" >
                    <div class="col-3">LACO<br/>รหัสเอกสาร F-HR-RC-001/1</div>
                    <div  class="col-6  text-center"><h2>บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด<br/>LANNA AGRO INDUSTRY CO., LTD.<br/>ใบสมัครงาน </h2></div>
                    <div class="col-3  text-right">เลขที่ <?php echo e($masterdata->id); ?><br/>ประกาศใช้วันที่ : 01/07/60<br/>ประกาศใช้ครั้งที่ : 05</div>
                </div>
                <div class="row" >
                    <div class="col-9">วันที่ <?php echo e($masterdata->created_at); ?><br/>
                        <b>โปรดกรอกใบสมัครนี้อย่างละเอียดและถูกต้องตามความเป็นจริง ด้วยตัวบรรจง เพื่อประโยชน์ของผู้สมัครเอง</b><br/>
                        ตำแหน่งงานที่ต้องการสมัคร <br/>
                        1. <span class='answer'><?php echo e($masterdata->job_name1); ?></span> อัตราเงินเดือนที่ต้องการ <span class='answer'><?php echo e($masterdata->job_salary1); ?></span> บาท<BR/> 
                        <?php if(!empty($masterdata->job_name2)): ?>
                            2. <span class='answer'><?php echo e($masterdata->job_name2); ?></span> อัตราเงินเดือนที่ต้องการ <span class='answer'><?php echo e($masterdata->job_salary2); ?></span> บาท<BR/> 
                        <?php endif; ?>
                        <br/><h5 class='newh5'>ส่วนที่ 1 : ข้อมูลส่วนตัว</h5>
                        <b>ชื่อ-นามสกุล ภาษาไทย </b><span class='answer'><?php echo e($masterdata->init_th); ?> <?php echo e($masterdata->fname_th); ?> <?php echo e($masterdata->lname_th); ?></span> <b>ชื่อเล่น</b> <span class='answer'><?php echo e($masterdata->nickname_th); ?></span><br/>
        	            <b>ชื่อ-นามสกุล ภาษาอังกฤษ </b><span class='answer'><?php echo e($masterdata->init_en); ?> <?php echo e($masterdata->fname_en); ?> <?php echo e($masterdata->lname_en); ?></span> <b>Nickname</b> <span class='answer'><?php echo e($masterdata->nickname_en); ?></span><br/>
                    <b>วัน/เดือน/ปี เกิด</b> <span class='answer'><?php echo e($masterdata->birth_date); ?></span> <b>อายุ</b> <span class='answer'>
                    <?php
                     $now = new DateTime('now');
                            $OldDate = new DateTime( $masterdata->birth_date);
                        $result = $OldDate->diff($now);
                        echo $result->y;
                    ?>
                    </span> 
                    <b>ปี น้ำหนัก</b> <span class='answer'><?php echo e($masterdata->weight); ?></span>
                    <b>กก. ส่วนสูง</b> <span class='answer'><?php echo e($masterdata->height); ?></span>
                    <b>ซม. หมู่เลือด</b> <span class='answer'><?php echo e($masterdata->blood); ?></span>
                    <b>เชื้อชาติ</b> <span class='answer'><?php echo e($masterdata->nationality); ?></span>
                    <b>สัญชาติ</b> <span class='answer'><?php echo e($masterdata->race); ?></span>
                    <b>ศาสนา</b> <span class='answer'><?php echo e($masterdata->religion); ?></span><br>
                    
                </div>
                    <div class="col-3  text-right">
                        <?php if(!empty($masterdata->uploaddata->image)): ?>
                            <img width="250px" src="<?php echo e(url($masterdata->uploaddata->image)); ?>" >   
                        <?php endif; ?>
                        
                    </div>
                </div>
                <div class="row" >
                     <div class="col-12">
                        <span><b>ที่อยู่ปัจจุบัน</b></span><br/>
                    <span class='answer'><?php echo e($masterdata->addr1); ?> </span>
                    ตำบล <span class='answer'><?php echo e($masterdata->subdistrict); ?></span> 
                    อำเภอ <span class='answer'><?php echo e($masterdata->district); ?></span> 
                    จังหวัด <span class='answer'><?php echo e($masterdata->province); ?></span> 
                    รหัสไปรษณีย์ <span class='answer'><?php echo e($masterdata->zipcode); ?></span> 
                    โทรศัพท์บ้าน <span class='answer'><?php echo e($masterdata->tel); ?></span> 
                    โทรศัพท์มือถือ <span class='answer'><?php echo e($masterdata->mobile); ?></span><br/>
                    <span><b>ซึ่งที่อยู่ปัจจุบันนี้เป็น</b></span> : <span class='answer'><?php echo e($masterdata->addr_type); ?></span>
                    <?php if($masterdata->addr_type == 'บ้านของญาติ'): ?>
                        <?php echo e($masterdata->addr_type_custom1); ?>

                    <?php endif; ?>
                    <?php if($masterdata->addr_type == 'หอพัก'): ?>
                        ชื่อ <?php echo e($masterdata->addr_type_custom1); ?> หมายเลขห้อง <?php echo e($masterdata->addr_type_custom1); ?>

                    <?php endif; ?>
                    <br/>
                    <b>บัตรประชาชน เลขที่</b> <?php echo e($masterdata->citizenid); ?> <b>วันออกบัตร</b> <?php echo e($masterdata->citizenstartdate); ?> <b>วันหมดอายุบัตร</b> <?php echo e($masterdata->citizenexpdate); ?> <br/>
                    <span style="text-decoration: underline" ><b>ในกรณีฉุกเฉิน สามารถติดต่อกับ</b></span><br/>
                    <?php echo e($masterdata->urgent_init); ?> <?php echo e($masterdata->urgent_name); ?> <b>ที่อยู่</b> <?php echo e($masterdata->urgent_addr1); ?> <b>โทรศัพท์</b> <?php echo e($masterdata->urgent_mobile); ?> <b>ความสัมพันธ์เป็น</b> <?php echo e($masterdata->urgent_relation); ?> 
                    <h5 class='newh5'>ส่วนที่ 2 : ประวัติการศึกษา</h5> 
                    <table class="table d-print-table mytable">
                            <tbody>
                                <tr>
                                    <td class="mycol_b_c">ระดับ</td>
                                    <td class="mycol_b_c">ชื่อสถานศึกษา</td>
                                    <td class="mycol_b_c">จังหวัด</td>
                                    <td class="mycol_b_c">สาขาวิชา/แผนก/คณะ</td>
                                    <td class="mycol_b_c">ปีที่จบการศึกษา</td>
                                    <td class="mycol_b_c">เกรดเฉลี่ย</td>
                                </tr>
                                <?php if(!empty($masterdata->educationdata->school_1)): ?>
                                <tr>
                                    <td class="mycol_b_l">มัธยมศึกษาตอนต้น</td>
                                    <td><?php echo e($masterdata->educationdata->school_1); ?></td>
                                    <td><?php echo e($masterdata->educationdata->province_1); ?></td>
                                    <td><?php echo e($masterdata->educationdata->title_1); ?></td>
                                    <td><?php echo e($masterdata->educationdata->year_1); ?></td>
                                    <td><?php echo e($masterdata->educationdata->grade_1); ?></td>
                                </tr>    
                                <?php endif; ?>
                                <?php if(!empty($masterdata->educationdata->school_2)): ?>
                                <tr>
                                    <td class="mycol_b_l">มัธยมศึกษาตอนปลาย</td>
                                    <td><?php echo e($masterdata->educationdata->school_2); ?></td>
                                    <td><?php echo e($masterdata->educationdata->province_2); ?></td>
                                    <td><?php echo e($masterdata->educationdata->title_2); ?></td>
                                    <td><?php echo e($masterdata->educationdata->year_2); ?></td>
                                    <td><?php echo e($masterdata->educationdata->grade_2); ?></td>
                                </tr>    
                                <?php endif; ?>
                                <?php if(!empty($masterdata->educationdata->school_3)): ?>
                                <tr>
                                    <td class="mycol_b_l">ปวช.</td>
                                    <td><?php echo e($masterdata->educationdata->school_3); ?></td>
                                    <td><?php echo e($masterdata->educationdata->province_3); ?></td>
                                    <td><?php echo e($masterdata->educationdata->title_3); ?></td>
                                    <td><?php echo e($masterdata->educationdata->year_3); ?></td>
                                    <td><?php echo e($masterdata->educationdata->grade_3); ?></td>
                                </tr>    
                                <?php endif; ?>
                                <?php if(!empty($masterdata->educationdata->school_4)): ?>
                                <tr>
                                    <td class="mycol_b_l">ปวส.</td>
                                    <td><?php echo e($masterdata->educationdata->school_4); ?></td>
                                    <td><?php echo e($masterdata->educationdata->province_1); ?></td>
                                    <td><?php echo e($masterdata->educationdata->title_4); ?></td>
                                    <td><?php echo e($masterdata->educationdata->year_4); ?></td>
                                    <td><?php echo e($masterdata->educationdata->grade_4); ?></td>
                                </tr>    
                                <?php endif; ?>
                                <?php if(!empty($masterdata->educationdata->school_5)): ?>
                                <tr>
                                    <td class="mycol_b_l">ปริญญาตรี</td>
                                    <td><?php echo e($masterdata->educationdata->school_5); ?></td>
                                    <td><?php echo e($masterdata->educationdata->province_5); ?></td>
                                    <td><?php echo e($masterdata->educationdata->title_5); ?></td>
                                    <td><?php echo e($masterdata->educationdata->year_5); ?></td>
                                    <td><?php echo e($masterdata->educationdata->grade_5); ?></td>
                                </tr>   
                                <?php endif; ?>
                                
                                <?php if(!empty($masterdata->educationdata->school_6)): ?>
                                <tr>
                                    <td class="mycol_b_l">ปริญญาโท</td>
                                    <td><?php echo e($masterdata->educationdata->school_6); ?></td>
                                    <td><?php echo e($masterdata->educationdata->province_6); ?></td>
                                    <td><?php echo e($masterdata->educationdata->title_6); ?></td>
                                    <td><?php echo e($masterdata->educationdata->year_6); ?></td>
                                    <td><?php echo e($masterdata->educationdata->grade_6); ?></td>
                                </tr>    
                                <?php endif; ?>
                                
                            </tbody>
                        </table>
                        <h5 class='newh5'>ส่วนที่ 3 : ประวัติการฝึกงานขณะกำลังศึกษาอยู่ (เรียงจากปัจจุบันตามลำดับ)</h5>
                        <table class="mytable table d-print-table ">
                            <tbody>
                                <tr>
                                    <td rowspan="2" class="mycol_b_c">ลำดับ</td>
                                    <td colspan="2" class="mycol_b_c">ระยะเวลาตั้งแต่ - ถึง</td>
                                    <td rowspan="2" class="mycol_b_c">ชื่อสถานที่ฝึกงาน</td>
                                    <td rowspan="2" class="mycol_b_c">จังหวัด</td>
                                    <td rowspan="2" class="mycol_b_c">ขณะที่ฝึกงานกำลังศึกษาอยู่ในระดับชั้น</td>
                                </tr>
                                <tr>
                                    <td class="mycol_b_c">วัน/เดือน/ปี</td>
                                    <td class="mycol_b_c">วัน/เดือน/ปี</td>
                                </tr>
                                <?php if(!empty($masterdata->trainingdata->title_1 )): ?>
                                <tr>
                                    <td> 1 </td>
                                    <td><?php echo e($masterdata->trainingdata->start_date_1); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->end_date_1); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->title_1); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->province_1); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->level_1); ?></td>
                                </tr>
                                <?php endif; ?>
                                <?php if(!empty($masterdata->trainingdata->title_2 )): ?>
                                <tr>
                                    <td> 2 </td>
                                    <td><?php echo e($masterdata->trainingdata->start_date_2); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->end_date_2); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->title_2); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->province_2); ?></td>
                                    <td><?php echo e($masterdata->trainingdata->level_2); ?></td>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>   
        <div class='row'>   
            <div class='col-12'>
                 <div class='row'>
                     <div class='col-12'>
                        <h5 class='newh5'>ส่วนที่ 4 : ประวัติการทำงาน(จากปัจจุบันตามลำดับ)</h5>
                        <table class="table d-print-table mytable" >
                            <tbody>
                                <?php if(!empty($masterdata->expmasterdata->title_1 )): ?>
                                <tr>
                                    <td  class="mycol_b_c" rowspan="3">ลำดับ<br/>1</td>
                                    <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
                                    <td class="mycol_b_c">ถึง ว/ด/ป</td>
                                    <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                                    <td class="mycol_b_c">จังหวัด</td>
                                    <td class="mycol_b_c">โทรศัพท์</td>
                                    <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
                                </tr>
                                <tr>
                                    <td><?php echo e($masterdata->expmasterdata->start_date_1); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->end_date_1); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->company_1); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->province_1); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->tel_1); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->title_1); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><b>เงินเดือน :</b> <?php echo e($masterdata->expmasterdata->salary_1); ?></td>
                                    <td colspan="4"><b>เหตุที่ลาออก :</b> <?php echo e($masterdata->expmasterdata->case_1); ?></td>
                                </tr>
                                <?php endif; ?>
                                <?php if(!empty($masterdata->expmasterdata->title_2 )): ?>
                                <tr>
                                    <td class="mycol_b_c" rowspan="3">ลำดับ<br/>2</td>
                                    <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
                                    <td class="mycol_b_c">ถึง ว/ด/ป</td>
                                    <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                                    <td class="mycol_b_c">จังหวัด</td>
                                    <td class="mycol_b_c">โทรศัพท์</td>
                                    <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
                                </tr>
                                <tr>
                                    <td><?php echo e($masterdata->expmasterdata->start_date_2); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->end_date_2); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->company_2); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->province_2); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->tel_2); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->title_2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><b>เงินเดือน :</b> <?php echo e($masterdata->expmasterdata->salary_2); ?></td>
                                    <td colspan="4"><b>เหตุที่ลาออก :</b> <?php echo e($masterdata->expmasterdata->case_2); ?></td>
                                </tr>
                                <?php endif; ?>
                                <?php if(!empty($masterdata->expmasterdata->title_3 )): ?>
                                <tr>
                                    <td  class="mycol_b_c" rowspan="3">ลำดับ<br/>3</td>
                                    <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
                                    <td class="mycol_b_c">ถึง ว/ด/ป</td>
                                    <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                                    <td class="mycol_b_c">จังหวัด</td>
                                    <td class="mycol_b_c">โทรศัพท์</td>
                                    <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
                                </tr>
                                <tr>
                                    <td><?php echo e($masterdata->expmasterdata->start_date_3); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->end_date_3); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->company_3); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->province_3); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->tel_3); ?></td>
                                    <td><?php echo e($masterdata->expmasterdata->title_3); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><b>เงินเดือน :</b> <?php echo e($masterdata->expmasterdata->salary_3); ?></td>
                                    <td colspan="4"><b>เหตุที่ลาออก :</b> <?php echo e($masterdata->expmasterdata->case_3); ?></td>
                                </tr>    
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <h5 class='newh5'>ส่วนที่ 5 : ประวัติการเข้ารับการฝึกอบรมพิเศษ</h5>
                        <table class="table d-print-table mytable" >
                            <tbody>
                                <tr>
                                    <td class="mycol_b_c"  rowspan="2">ลำดับ</td>
                                    <td class="mycol_b_c"  colspan="2">ระยะเวลาตั้งแต่ - ถึง</td>
                                    <td class="mycol_b_c" >รวมทั้งสิ้น</td>
                                    <td class="mycol_b_c"  rowspan="2">หลักสูตร / หัวข้อที่เข้ารับการอบรม</td>
                                    <td class="mycol_b_c"  rowspan="2">ชื่อหน่วยงานที่จัดฝึกอบรม</td>
                                </tr>
                                <tr>
                                    <td class="mycol_b_c" >วัน/เดือน/ปี</td>
                                    <td class="mycol_b_c" >วัน/เดือน/ปี</td>
                                    <td class="mycol_b_c" >วัน / ชั่วโมง</td>
                                </tr>
                                <?php if(!empty($masterdata->extrainingdata->title_1)): ?>
                                <tr>
                                    <td class="mycol_b_c" >1</td>
                                    <td><?php echo e($masterdata->extrainingdata->start_date_1); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->end_date_1); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->duration_1); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->title_1); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->by_1); ?></td>
                                </tr>    
                                <?php endif; ?>
                                <?php if(!empty($masterdata->extrainingdata->title_2)): ?>
                                <tr>
                                    <td class="mycol_b_c" >2</td>
                                    <td><?php echo e($masterdata->extrainingdata->start_date_2); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->end_date_2); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->duration_2); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->title_2); ?></td>
                                    <td><?php echo e($masterdata->extrainingdata->by_2); ?></td>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        
                     </div>
                     <div class="col-12">
                        <h5 class='newh5'>ส่วนที่ 6 : ความสามารถพิเศษ</h5>
                        <div class="row">
                            <div class="col-3">1. พิมพดีด ภาษาไทย <?php echo e($masterdata->expdetaildata->expd_type_th); ?> คำ/นาที</div>
                            <div class="col-3">ภาษาอังกฤษ <?php echo e($masterdata->expdetaildata->expd_type_en); ?> คำ/นาที</div>
                            <div class="col-3">2.ชวเลข <?php echo e($masterdata->expdetaildata->expd_type_note); ?> คำ/นาที</div>
                            <div class="col-3"></div>
                            <div class="col-12">3.  คอมพิวเตอร์ โปรแกรม :-</div>
                            <div class="col-3">3.1 <?php echo e($masterdata->expdetaildata->expd_com_program1); ?> (<?php echo e($masterdata->expdetaildata->expd_com_level1); ?>)</div>
                            <div class="col-3">3.2 <?php echo e($masterdata->expdetaildata->expd_com_program2); ?> (<?php echo e($masterdata->expdetaildata->expd_com_level2); ?>)</div>
                            <div class="col-3">3.3 <?php echo e($masterdata->expdetaildata->expd_com_program3); ?> (<?php echo e($masterdata->expdetaildata->expd_com_level3); ?>)</div>
                            <div class="col-3">3.4 <?php echo e($masterdata->expdetaildata->expd_com_program4); ?> (<?php echo e($masterdata->expdetaildata->expd_com_level4); ?>)</div>
                            <div class="col-3">3.5 <?php echo e($masterdata->expdetaildata->expd_com_program5); ?> (<?php echo e($masterdata->expdetaildata->expd_com_level5); ?>)</div>
                            <div class="col-3">3.6 <?php echo e($masterdata->expdetaildata->expd_com_program6); ?> (<?php echo e($masterdata->expdetaildata->expd_com_level6); ?>)</div>
                            <div class="col-12">4. ความสามารถด้านภาษา</div>
                            <div class="col-6">4.1 ภาษาไทย การพูด (<?php echo e($masterdata->expdetaildata->expd_lang_speak_th); ?>) / การอ่าน (<?php echo e($masterdata->expdetaildata->expd_lang_read_th); ?>)  / การเขียน (<?php echo e($masterdata->expdetaildata->expd_lang_write_th); ?>)</div>
                            <div class="col-6">4.2 ภาษาอังกฤษ การพูด (<?php echo e($masterdata->expdetaildata->expd_lang_speak_en); ?>) / การอ่าน (<?php echo e($masterdata->expdetaildata->expd_lang_read_en); ?>)  / การเขียน (<?php echo e($masterdata->expdetaildata->expd_lang_write_en); ?>)</div>
                            <div class="col-6">4.3 ภาษาจีน การพูด (<?php echo e($masterdata->expdetaildata->expd_lang_speak_ch); ?>) / การอ่าน (<?php echo e($masterdata->expdetaildata->expd_lang_read_ch); ?>)  / การเขียน (<?php echo e($masterdata->expdetaildata->expd_lang_write_ch); ?>)</div>
                            <div class="col-6">4.1 ภาษาอื่นๆ <?php echo e($masterdata->expdetaildata->expd_lang_ex); ?>  การพูด (<?php echo e($masterdata->expdetaildata->expd_lang_speak_ex); ?>) / การอ่าน (<?php echo e($masterdata->expdetaildata->expd_lang_read_ex); ?>)  / การเขียน (<?php echo e($masterdata->expdetaildata->expd_lang_write_ex); ?>)</div>
                            <div class="col-12">5. การขับขี่ยานพาหนะ</div>
                            <div class="col-12">ขับขี่รถยนต์ (<?php echo e($masterdata->expdetaildata->expd_can_drive); ?>) 
                            <?php if($masterdata->expdetaildata->expd_have_drive_licen == 'มี'): ?>
                                <?php echo e($masterdata->expdetaildata->expd_have_drive_licen); ?>ใบอนุญาตขับรถ ชนิด <?php echo e($masterdata->expdetaildata->expd_drive_licen_type); ?>  วันอนุญาต <?php echo e($masterdata->expdetaildata->expd_drive_licen_date); ?>  วันหมดอายุ <?php echo e($masterdata->expdetaildata->expd_drive_licen_exp); ?> 
                            <?php else: ?>
                                <?php echo e($masterdata->expdetaildata->expd_have_drive_licen); ?>ใบอนุญาตขับรถ 
                            <?php endif; ?>
                            รถยนต์ที่ใช้ปัจจุบันยี่ห้อ <?php echo e($masterdata->expdetaildata->expd_my_car_brand); ?> รุ่น <?php echo e($masterdata->expdetaildata->expd_my_car_serie); ?> 
                            </div>
                            <div class="col-12">ขับขี่รถจักรยานยนต์  (<?php echo e($masterdata->expdetaildata->expd_can_ride); ?>) 
                            <?php if($masterdata->expdetaildata->expd_have_ride_licen == 'มี'): ?>
                                <?php echo e($masterdata->expdetaildata->expd_have_ride_licen); ?>ใบอนุญาตขับรถ ชนิด <?php echo e($masterdata->expdetaildata->expd_ride_licen_type); ?>  วันอนุญาต <?php echo e($masterdata->expdetaildata->expd_ride_licen_date); ?>  วันหมดอายุ <?php echo e($masterdata->expdetaildata->expd_ride_licen_exp); ?> 
                            <?php else: ?>
                                <?php echo e($masterdata->expdetaildata->expd_have_ride_licen); ?>ใบอนุญาตขับรถ 
                            <?php endif; ?>
                            รถจักรยานยนต์ที่ใช้ปัจจุบันยี่ห้อ <?php echo e($masterdata->expdetaildata->expd_my_bike_brand); ?> รุ่น <?php echo e($masterdata->expdetaildata->expd_my_bike_serie); ?> 
                            </div>
                        </div>
                        <h5 class='newh5'>ส่วนที่ 7 : สถานภาพสมรส</h5>
                        <div class="row"> 
                            <div class="col-12"><?php echo e($masterdata->marrieddata->status); ?>  
                            <?php if($masterdata->marrieddata->status == 'โสด' || $masterdata->marrieddata->status == 'หม้าย (คู่สมรสถึงแก่กรรม)'): ?>
                                <br/>
                            <?php else: ?>
                                คู่สมรสชื่อ <?php echo e($masterdata->marrieddata->init); ?> <?php echo e($masterdata->marrieddata->name); ?> อายุ <?php echo e($masterdata->marrieddata->age); ?> ปี อาชีพ <?php echo e($masterdata->marrieddata->career); ?><br/>
                                ที่อยู่ปัจจุบัน <?php echo e($masterdata->marrieddata->homeaddr); ?> โทรศัพท์ <?php echo e($masterdata->marrieddata->tel); ?> ที่ตั้งสถานที่ทำงาน  <?php echo e($masterdata->marrieddata->companyaddr); ?> ตำแหน่ง <?php echo e($masterdata->marrieddata->job); ?> เบอร์โทรศัพท์ที่ทำงาน  <?php echo e($masterdata->marrieddata->companytel); ?> <br/>          
                            <?php endif; ?>
                                ปัจจุบัน <?php echo e($masterdata->marrieddata->child_status); ?>

                                <?php if($masterdata->marrieddata->child_status == 'มีบุตร'): ?>
                                    <?php echo e($masterdata->marrieddata->child_number); ?> คน เป็นชาย <?php echo e($masterdata->marrieddata->boy_no); ?> คน  อายุ <?php echo e($masterdata->marrieddata->boy_age); ?> ปี หญิง <?php echo e($masterdata->marrieddata->daughter_no); ?> คน  อายุ <?php echo e($masterdata->marrieddata->daughter_age); ?> ปี
                                <?php else: ?>
                                    
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if( !empty($masterdata->militarydata->status) ): ?>
                        <h5 class='newh5'>ส่วนที่ 8 :  กรณีผู้ชาย  การรับราชการทหาร</h5>
                        <div class="row">
                            <div class='col-12'>
                                <?php if($masterdata->militarydata->status == 'ยังไม่ได้รับการเกณฑ์ทหาร'): ?>
                                    ยังไม่ได้รับการเกณฑ์ทหาร โดยจะเข้ารับการเกณฑ์ทหารในปี พ.ศ <?php echo e($masterdata->militarydata->year); ?> 
                                <?php else: ?>    
                                    <?php if($masterdata->militarydata->status == 'เข้ารับการเกณฑ์ทหารแล้ว'): ?>
                                    เข้ารับการเกณฑ์ทหารแล้ว เคยอยู่สังกัด <?php echo e($masterdata->militarydata->custom1); ?> เป็นเวลา <?php echo e($masterdata->militarydata->duration); ?> ปี ปลดประจำการเมื่อวันที่ <?php echo e($masterdata->militarydata->custom2); ?> หรือ ได้รับการยกเว้น เนื่องจาก  □  จับได้ใบดำ □  เรียนรักษาดินแดน(ร.ด.)……ปี   □ อื่นๆ  ..................... 
                                    <?php else: ?>    
                                        <?php if($masterdata->militarydata->status == 'เรียนรักษาดินแดน(ร.ด.)'): ?>
                                        เรียนรักษาดินแดน(ร.ด.) <?php echo e($masterdata->militarydata->year); ?> ปี
                                        <?php else: ?>    
                                            <?php if($masterdata->militarydata->status == 'อื่นๆ'): ?>
                                            อื่นๆ  <?php echo e($masterdata->militarydata->custom1); ?> 
                                            <?php else: ?>    
                                            <?php echo e($masterdata->militarydata->status); ?> 
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>    
                        <?php endif; ?>
                        <h5 class='newh5'>ส่วนที่ 9 : ข้อมูลทั่วไป</h5>
                        <div class="row">
                            <div class='col-12'>
                                1.	ปัจจุบันกำลังศึกษาอยู่ในระดับ <?php echo e($masterdata->simpledata->current_edu_level); ?> สาขาวิชา  <?php echo e($masterdata->simpledata->current_edu_dep); ?> ชั้นปีที่  <?php echo e($masterdata->simpledata->current_edu_year); ?> ชื่อสถานศึกษา  <?php echo e($masterdata->simpledata->current_edu_school); ?> 
                                </div>
                            <div class='col-12'> 2.	มีแผนการศึกษาต่อในระดับ <?php echo e($masterdata->simpledata->future_edu_level); ?> สาขาวิชา <?php echo e($masterdata->simpledata->future_edu_dep); ?> ในปี พ.ศ <?php echo e($masterdata->simpledata->future_edu_year); ?> ชื่อสถานศึกษา <?php echo e($masterdata->simpledata->future_edu_school); ?> 

                            </div>
                            <div class='col-4'>3.	งานอดิเรก    1) <?php echo e($masterdata->simpledata->hobby_1); ?></div>
                            <div class='col-4'>2) <?php echo e($masterdata->simpledata->hobby_2); ?></div>
                            <div class='col-4'>3) <?php echo e($masterdata->simpledata->hobby_3); ?></div>
                            <div class='col-6'>4.	การดื่มสุรา <?php echo e($masterdata->simpledata->drunker); ?></div>
                            <div class='col-6'>5.	การสูบบุหรี่ <?php echo e($masterdata->simpledata->smoker); ?>

                            <?php if($masterdata->simpledata->smoker == 'สูบทุกวัน'): ?>
                                วันละ <?php echo e($masterdata->simpledata->smoker_other); ?> มวน
                            <?php endif; ?>
                                
                            </div>
                            <div class='col-12'>
                                6.	การต้องคดีอาญาถึงจำคุกหรือต้องคำพิพากษาถึงที่สุดให้จำคุก (<?php echo e($masterdata->simpledata->criminal); ?>) 
                                <?php if( $masterdata->simpledata->criminal == 'เคย' ): ?>
                                    ระบุรายละเอียด <?php echo e($masterdata->simpledata->criminal_other); ?> 
                                <?php endif; ?>    
                            </div>
                            <div class='col-12'>
                               7.	การถูกปลดออกจากหน้าที่เนื่องจากความประพฤติไม่เหมาะสม / การปฏิบัติงานบกพร่อง  (<?php echo e($masterdata->simpledata->control); ?>)  
                                <?php if( $masterdata->simpledata->control == 'เคย' ): ?>
                                    ระบุรายละเอียด <?php echo e($masterdata->simpledata->control_other); ?> 
                                <?php endif; ?>    
                            </div>
                            <div class='col-12'>
                               8.	การไปปฏิบัติงานต่างจังหวัด และ ต่างประเทศ  <span class="answer" >(<?php echo e($masterdata->simpledata->can_other_country); ?>)</span>
                                <?php if( $masterdata->simpledata->can_other_country == 'ไม่ได้' ): ?>
                                    เนื่องจาก <span class="answer" ><?php echo e($masterdata->simpledata->can_other_country_case); ?></span>
                                <?php endif; ?>    
                            </div>
                            <div class='col-12'>
                               9.	การสมัครงานกับบริษัทฯ ครั้งนี้เป็นครั้งที่ <span class="answer" ><?php echo e($masterdata->simpledata->apply_job_no); ?></span> เคยสอบข้อเขียน/สอบสัมภาษณ์กับบริษัทฯ ในตำแหน่ง <span class="answer" ><?php echo e($masterdata->simpledata->apply_job_name); ?></span>  เหตุผลที่สมัครเข้าทำงานกับบริษัทฯ <span class="answer" ><?php echo e($masterdata->simpledata->apply_job_company); ?></span>
                                <?php if( $masterdata->simpledata->can_other_country == 'ไม่ได้' ): ?>
                                    เนื่องจาก <span class="answer" ><?php echo e($masterdata->simpledata->can_other_country_case); ?></span>
                                <?php endif; ?>    
                            </div>
                            <div class='col-12'>
                            10.	ทราบข่าวการรับสมัครงาน   <span class="answer" ><?php echo e($masterdata->simpledata->hear_from); ?></span> 
                            <?php if($masterdata->simpledata->hear_from == 'สนง.จัดหางาน จังหวัด'): ?>
                                 จังหวัด <span class="answer" ><?php echo e($masterdata->simpledata->hear_from_other); ?></span>
                            <?php endif; ?>
                            <?php if($masterdata->simpledata->hear_from == 'สื่ออื่นๆ'): ?>
                                 ระบุ) <span class="answer" ><?php echo e($masterdata->simpledata->hear_from_other); ?></span>
                            <?php endif; ?>
                            </div>
                            <div class='col-12'>
                               11.	บุคคลที่แนะนำให้มาสมัครงาน หรือ ญาติพี่น้อง เพื่อน หรือคนรู้จัก ที่เป็นพนักงานของบริษัทฯ (ถ้ามี) <span class="answer" ><?php echo e($masterdata->simpledata->refer_name); ?></span>  
                            </div>
                            <div class='col-12'>
                               12.	ความรู้ความเข้าใจที่มีต่อ บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด มีดังนี้ <span class="answer" ><?php echo e($masterdata->simpledata->your_comments); ?></span>  
                            </div>
                            <div class='col-12'>
                               13.	หากบริษัทฯ รับเข้าทำงาน สามารถเริ่มงานได้ภายใน <span class="answer" ><?php echo e($masterdata->simpledata->can_start_in); ?></span> วัน เนื่องจาก <span class="answer" ><?php echo e($masterdata->simpledata->can_start_desc); ?></span>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>   
        <div class='row'>   
            <div class='col-12'>
                 <div class='row'>
                     <div class='col-12'>
                        <h5 class='newh5'>ส่วนที่ 10 : สมาชิกในครอบครัว</h5>
                        <div class="row">
                            <div class='col-12'>
                                บิดา ชื่อ <span class="answer" ><?php echo e($masterdata->familydata->father_name); ?></span> 
                                อายุ <span class="answer" ><?php echo e($masterdata->familydata->father_age); ?></span> ปี 
                                อาชีพ <span class="answer" ><?php echo e($masterdata->familydata->father_occupation); ?></span>
                                สถานที่ทำงาน <span class="answer" ><?php echo e($masterdata->familydata->father_company); ?></span> 
                                ตำแหน่ง <span class="answer" ><?php echo e($masterdata->familydata->father_job); ?></span> 
                                โทรศัพท์ <span class="answer" ><?php echo e($masterdata->familydata->father_company_tel); ?></span><br/>
                                ที่อยู่ปัจจุบัน <span class="answer" ><?php echo e($masterdata->familydata->father_addr); ?></span> 
                                โทรศัพท์ <span class="answer" ><?php echo e($masterdata->familydata->father_tel); ?></span><br/>


                                มารดา ชื่อ <span class="answer" ><?php echo e($masterdata->familydata->mother_name); ?></span> 
                                อายุ <span class="answer" ><?php echo e($masterdata->familydata->mother_age); ?></span> ปี 
                                อาชีพ <span class="answer" ><?php echo e($masterdata->familydata->mother_occupation); ?></span>
                                สถานที่ทำงาน <span class="answer" ><?php echo e($masterdata->familydata->mother_company); ?></span> 
                                ตำแหน่ง <span class="answer" ><?php echo e($masterdata->familydata->mother_job); ?></span> 
                                โทรศัพท์ <span class="answer" ><?php echo e($masterdata->familydata->mother_company_tel); ?></span><br/>
                                ที่อยู่ปัจจุบัน <span class="answer" ><?php echo e($masterdata->familydata->mother_addr); ?></span> 
                                โทรศัพท์ <span class="answer" ><?php echo e($masterdata->familydata->mother_tel); ?></span><br/>
                                มีพี่น้อง(รวมผู้สมัครด้วย)ทั้งสิ้น <span class="answer" ><?php echo e($masterdata->familydata->total_nabor); ?></span> คน 
                                เป็นชาย <span class="answer" ><?php echo e($masterdata->familydata->total_brother); ?></span> คน หญิง <span class="answer" ><?php echo e($masterdata->familydata->total_sisther); ?></span> คน 
                                เรียงลำดับตามอายุจากมากไปหาน้อย (รวมผู้สมัครด้วย) ดังนี้ :-
                            </div>
                            <div class='col-12'>
                                <table class="table d-print-table mytable" >
                                    <tbody>
                                        <tr>
                                            <td class="mycol_b_c" >ชื่อ-นามสกุล</td>
                                            <td class="mycol_b_c" >อายุ(ปี)</td>
                                            <td class="mycol_b_c" >ชื่อสถานที่ทำงาน/สถานศึกษา</td>
                                            <td class="mycol_b_c" >ตำแหน่ง/ชั้นปีที่กำลังศึกษา</td>
                                        </tr>
                                        <?php if(!empty( $masterdata->familydata->name_1)): ?>
                                            <tr>
                                                <td>1. <?php echo e($masterdata->familydata->name_1); ?></td>
                                                <td><?php echo e($masterdata->familydata->age_1); ?></td>
                                                <td><?php echo e($masterdata->familydata->company_1); ?></td>
                                                <td><?php echo e($masterdata->familydata->level_1); ?></td>
                                            </tr>
                                        <?php endif; ?>
                                         <?php if(!empty( $masterdata->familydata->name_2)): ?>
                                            <tr>
                                                <td>2. <?php echo e($masterdata->familydata->name_2); ?></td>
                                                <td><?php echo e($masterdata->familydata->age_2); ?></td>
                                                <td><?php echo e($masterdata->familydata->company_2); ?></td>
                                                <td><?php echo e($masterdata->familydata->level_2); ?></td>
                                            </tr>
                                        <?php endif; ?>
                                         <?php if(!empty( $masterdata->familydata->name_3)): ?>
                                            <tr>
                                                <td>3. <?php echo e($masterdata->familydata->name_3); ?></td>
                                                <td><?php echo e($masterdata->familydata->age_3); ?></td>
                                                <td><?php echo e($masterdata->familydata->company_3); ?></td>
                                                <td><?php echo e($masterdata->familydata->level_3); ?></td>
                                            </tr>
                                        <?php endif; ?>
                                         <?php if(!empty( $masterdata->familydata->name_4)): ?>
                                            <tr>
                                                <td>4. <?php echo e($masterdata->familydata->name_4); ?></td>
                                                <td><?php echo e($masterdata->familydata->age_4); ?></td>
                                                <td><?php echo e($masterdata->familydata->company_4); ?></td>
                                                <td><?php echo e($masterdata->familydata->level_4); ?></td>
                                            </tr>
                                        <?php endif; ?>
                                         <?php if(!empty( $masterdata->familydata->name_5)): ?>
                                            <tr>
                                                <td>5. <?php echo e($masterdata->familydata->name_5); ?></td>
                                                <td><?php echo e($masterdata->familydata->age_5); ?></td>
                                                <td><?php echo e($masterdata->familydata->company_5); ?></td>
                                                <td><?php echo e($masterdata->familydata->level_5); ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <h5 class='newh5'>ส่วนที่ 11 : สุขภาพและความพร้อมทางร่างกาย</h5>
                        <div class='row'>
                            <div class='col-4'>     
                                1.	โรคประจำตัว <span class="answer" ><?php echo e($masterdata->healthdata->congenital_disease); ?></span> 
                                <?php if($masterdata->healthdata->congenital_disease == 'เคยเป็น/กำลังเป็น'): ?>
                                    ระบุชื่อโรค <span class="answer" ><?php echo e($masterdata->healthdata->congenital_disease_other); ?></span>
                                <?php endif; ?>     
                            </div>
                             <div class='col-4'>     
                                2.	โรคติดต่อ/โรคเรื้อรัง <span class="answer" ><?php echo e($masterdata->healthdata->contagious_disease); ?></span> 
                                <?php if($masterdata->healthdata->contagious_disease == 'เคยเป็น/กำลังเป็น'): ?>
                                    ระบุชื่อโรค <span class="answer" ><?php echo e($masterdata->healthdata->contagious_disease_other); ?></span>
                                <?php endif; ?>     
                            </div>
                            <div class='col-4'>     
                                3.	โรคภูมิแพ้ <span class="answer" ><?php echo e($masterdata->healthdata->allergy); ?></span> 
                                <?php if($masterdata->healthdata->allergy == 'เคยเป็น/กำลังเป็น'): ?>
                                    ระบุชื่อโรค <span class="answer" ><?php echo e($masterdata->healthdata->allergy_other); ?></span>
                                <?php endif; ?>     
                            </div>
                            <div class='col-4'>     
                               4.	อาการปวดหลัง ปวดกล้ามเนื้อ  <span class="answer" ><?php echo e($masterdata->healthdata->back_pain); ?></span>                                
                            </div>
                            <div class='col-4'>     
                               5.	อาการชาตามกล้ามเนื้อ นิ้ว มือ  <span class="answer" ><?php echo e($masterdata->healthdata->finger_pain); ?></span>                                
                            </div>
                            <div class='col-4'>     
                               6.	โรคธาลัสซีเมีย	  <span class="answer" ><?php echo e($masterdata->healthdata->thalassemia); ?></span>                                
                            </div>
                            <div class='col-4'>     
                               7.	ไวรัสตับอักเสบ A B C  <span class="answer" ><?php echo e($masterdata->healthdata->liver_virus); ?></span>                                
                            </div>
                            <div class='col-4'>     
                               8.	แพ้อาหาร <span class="answer" ><?php echo e($masterdata->healthdata->food_allergy); ?></span> 
                                <?php if($masterdata->healthdata->food_allergy == 'เคยเป็น/กำลังเป็น'): ?>
                                    ระบุชื่ออาหารที่แพ้ <span class="answer" ><?php echo e($masterdata->healthdata->food_allergy_other); ?></span>
                                <?php endif; ?>     
                            </div>
                            <div class='col-4'>     
                               9.	แพ้ยา <span class="answer" ><?php echo e($masterdata->healthdata->drug_allergy); ?></span> 
                                <?php if($masterdata->healthdata->drug_allergy == 'เคยเป็น/กำลังเป็น'): ?>
                                    ระบุชื่อยาที่แพ้ <span class="answer" ><?php echo e($masterdata->healthdata->drug_allergy_other); ?></span>
                                <?php endif; ?>     
                            </div>
                            <div class='col-4'>     
                               10.	โรค/อาการอื่นๆ(ระบุ) <span class="answer" ><?php echo e($masterdata->healthdata->other_disease); ?></span>     
                            </div>
                            <div class='col-12'>     
                               11.	ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ <span class="answer" ><?php echo e($masterdata->healthdata->physical_exam_time); ?></span> ส่วนของร่างกายที่ได้รับการตรวจ <span class="answer" ><?php echo e($masterdata->healthdata->physical_exam_part); ?></span>
                                ผลการตรวจ <span class="answer" ><?php echo e($masterdata->healthdata->physical_exam_result); ?></span>
                                <?php if($masterdata->healthdata->physical_exam_result == 'ไม่ปกติ'): ?>
                                (ระบุรายละเอียด) <span class="answer" ><?php echo e($masterdata->healthdata->physical_exam_result_other); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class='col-12'>     
                               12.	ชื่อสถานที่ทำงานก่อนหน้านี้ซึ่งมีการทำประกันสังคม <span class="answer" ><?php echo e($masterdata->healthdata->prev_social_sec_company); ?></span>
                               โรงพยาบาลที่ใช้สิทธิรักษาพยาบาล <span class="answer" ><?php echo e($masterdata->healthdata->prev_social_sec_hospital); ?></span>
                            </div>
                        </div>
                    
                        <h5 class='newh5'>ส่วนที่ 12 : บุคคลอ้างอิงซึ่งสามารถรับรองความประพฤติ/ประวัติการทำงานได้ (ไม่ใช่ญาติพี่น้องหรือเพื่อน)</h5>
                        <table class="table d-print-table mytable" >
                            <tbody>
                                <tr>
                                    <td class="mycol_b_c" >ลำดับ</td>
                                    <td class="mycol_b_c" >ชื่อ-นามสกุล</td>
                                    <td class="mycol_b_c" >มีความสัมพันธ์เป็น</td>
                                    <td class="mycol_b_c" >ชื่อสถานที่ทำงาน/ตำแหน่ง</td>
                                    <td class="mycol_b_c" >โทรศัพท์</td>
                                </tr>
                                <?php if(!empty($masterdata->refdata->name_1)): ?>
                                    <tr>
                                        <td>1</td>
                                        <td><?php echo e($masterdata->refdata->name_1); ?></td>
                                        <td><?php echo e($masterdata->refdata->relation_1); ?></td>
                                        <td><?php echo e($masterdata->refdata->company_job_1); ?></td>
                                        <td><?php echo e($masterdata->refdata->tel_1); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <?php if(!empty($masterdata->refdata->name_2)): ?>
                                    <tr>
                                        <td>2</td>
                                        <td><?php echo e($masterdata->refdata->name_2); ?></td>
                                        <td><?php echo e($masterdata->refdata->relation_2); ?></td>
                                        <td><?php echo e($masterdata->refdata->company_job_2); ?></td>
                                        <td><?php echo e($masterdata->refdata->tel_2); ?></td>
                                    </tr>
                                <?php endif; ?>
                                <?php if(!empty($masterdata->refdata->name_3)): ?>
                                    <tr>
                                        <td>3</td>
                                        <td><?php echo e($masterdata->refdata->name_3); ?></td>
                                        <td><?php echo e($masterdata->refdata->relation_3); ?></td>
                                        <td><?php echo e($masterdata->refdata->company_job_3); ?></td>
                                        <td><?php echo e($masterdata->refdata->tel_3); ?></td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <h5 class='newh5'>ส่วนที่ 13 : ความรับผิดชอบต่อสังคม</h5>
                        <div class='row'>
                            <div class='col-12'>
                                ข้าพเจ้า<span class="answer" >ยินยอมให้บริษัทฯ ตรวจหาสารเสพติดได้ก่อนการเริ่มงานและ ตลอดเวลาการทำงานและขอรับรองว่าจะไม่ยุ่งเกี่ยวกับสารเสพติด</span> หากทางบริษัทฯ ตรวจพบสารเสพติดในตัว ข้าพเจ้ายินยอมถูกเลิกจ้างโดยไม่รับเงินชดเชยใดๆ ทั้งสิ้น
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>   
        <div class='row'>   
            <div class='col-12'>
                 <div class='row'>
                     <div class='col-12'>
                        <h5 class='newh5'>ส่วนที่ 14 : ผู้สมัครงานโปรดวาดแผนที่ ที่อยู่ปัจจุบันให้ชัดเจน</h5>
                        <div class='row'>
                            <div class='col-12'>
                                <div id="map" style="height: 400px"></div>
                            </div>
                            <div class='col-12'>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ขอรับรองว่าข้อความที่ได้กรอกไว้ข้างต้นทั้งหมดเป็นความจริงทุกประการ หากปรากฏภายหลังจากบริษัทฯ ว่าจ้างข้าพเจ้าเข้าทำงานแล้ว ว่าข้อความใดในใบสมัครงานนี้ไม่เป็นความจริง หรือข้าพเจ้าได้แนบเอกสารประกอบการสมัครงานอันเป็นเท็จ ข้าพเจ้ายินยอมให้บริษัทฯ เลิกจ้าง โดยไม่ขอเรียกร้องเพื่อรับค่าชดเชยใดๆ ทั้งสิ้น และหากข้าพเจ้าได้รับการว่าจ้างให้ทำงานกับบริษัทฯ ข้าพเจ้ายินดีจะปฏิบัติตามและรักษาไว้ซึ่งกฎ ระเบียบ คำสั่ง วินัย และนโยบายตามคู่มือพนักงานที่บริษัทฯ กำหนดไว้ทุกประการ
                            </div>
                            <div class="col-6">

                            </div>
                            <div class="col-6" style="text-align: center;">
                                ลงชื่อ...............................................................ผู้สมัคร<br/>
                                (..............................................................)<br/>
                                ................/.............../...............
                            </div>

                        </div>
                        <div class='row'>
                        <div class='col-12' >
                            <table class="table d-print-table mytable" >
                                    <tbody>
                                        <tr>
                                            <td class="mycol_b_c"><b>บันทึกของบริษัทฯ</b></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 400px"></td>
                                        </tr>
                                    </tbody>
                            </table>
                        </div>
                        <div class="col-6">

                            </div>
                            <div class="col-6" style="text-align: center;">
                                ลงชื่อ...............................................................ผู้สัมภาษณ์<br/>
                                (..............................................................)<br/>
                                ................/.............../...............
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if(!empty($masterdata->uploaddata->map)): ?>
    <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
        <?php
            $dataln = explode(',',$masterdata->uploaddata->map);
        ?>
        var uluru = {lat: <?php echo e($dataln[0]); ?>, lng: <?php echo e($dataln[1]); ?>};
    
  
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 14, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUxsUwOjdI8DXS0vDOneRaWo_s53WPg8k&callback=initMap">
    </script>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/master-datas/show.blade.php ENDPATH**/ ?>