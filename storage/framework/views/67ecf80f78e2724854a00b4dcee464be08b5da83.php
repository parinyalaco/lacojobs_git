<h3>ข้อมูลส่วนตัว</h3>
    <fieldset>
        <div class="row">
            <div class="col-12"><legend>ตำแหน่งงาน</legend>
                <div class="row">
                    <div class="form-check col-2">
                        <label for="job_name1">ตำแหน่ง1 *</label>
                    <input id="job_name1" name="job_name1" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "ตำแหน่ง1"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-2">
                        <label for="job_salary1">อัตราเงินเดือน1 *</label>
                        <input id="job_salary1" name="job_salary1" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "10000"
                    <?php endif; ?>>
                    </div>
                    
                    <div class="form-check col-2">
                        <label for="job_name2">ตำแหน่ง2</label>
                        <input id="job_name2" name="job_name2" type="text" class="form-control"
                    <?php if($testdata): ?>
                        value = "ตำแหน่ง2"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-2">
                        <label for="job_salary2">อัตราเงินเดือน2</label>
                        <input id="job_salary2" name="job_salary2" type="text" class="form-control"
                    <?php if($testdata): ?>
                        value = "20000"
                    <?php endif; ?>>
                    </div>
                </div>
                <legend>ที่อยู่ปัจจุบัน</legend>
                <div class="row">
                    <div class="form-check col-1">
                        <label for="init_th">คำนำหน้า *</label>
                    <input id="init_th" name="init_th" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "นาย"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-2">
                        <label for="fname_th">ชื่อ *</label>
                        <input id="fname_th" name="fname_th" type="text" class="form-control <?php echo e($flag); ?>"
                        <?php if($testdata): ?>
                        value = "ทดสอบ"
                    <?php endif; ?>
                    >
                    </div>
                    
                    <div class="form-check col-2">
                        <label for="lname_th">สกุล *</label>
                        <input id="lname_th" name="lname_th" type="text" class="form-control <?php echo e($flag); ?>"
                        <?php if($testdata): ?>
                        value = "ลองดู"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-1">
                        <label for="nickname_th">ชื่อเล่น *</label>
                        <input id="nickname_th" name="nickname_th" type="text" class="form-control <?php echo e($flag); ?>"
                        <?php if($testdata): ?>
                        value = "เต๊ด"
                    <?php endif; ?>
                    >
                    </div>
                     <div class="form-check col-1">
                        <label for="init_en">init *</label>
                    <input id="init_en" name="init_en" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "Mr."
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-2">
                        <label for="fname_en">Name *</label>
                        <input id="fname_en" name="fname_en" type="text" class="form-control <?php echo e($flag); ?>"
                        <?php if($testdata): ?>
                        value = "Todsorb"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-2">
                        <label for="lname_en">Surname *</label>
                        <input id="lname_en" name="lname_en" type="text" class="form-control <?php echo e($flag); ?>"<?php if($testdata): ?>
                        value = "Longdoo"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-1">
                        <label for="nickname_en">Nickname *</label>
                        <input id="nickname_en" name="nickname_en" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "Test"
                    <?php endif; ?>
                    >
                    </div>
                </div>
                <div class="row">
                   <div class="col-2">
                       <label for="birth_date">วันเกิด *</label>
                        <input id="birth_date" name="birth_date" type="date" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "2020-08-29"
                    <?php endif; ?>
                    >
                    </div>
                   <div class="col-1">
                       <label for="weight">น้ำหนัก(กก.) *</label>
                        <input id="weight" name="weight" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "55"
                    <?php endif; ?>
                    >
                    </div>
                   <div class="col-2">
                       <label for="height">ส่วนสูง(ซม.) *</label>
                        <input id="height" name="height" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "165"
                    <?php endif; ?>
                    >
                    </div>
                   <div class="col-1">
                       <label for="blood">หมู่เลือด *</label>
                        <input id="blood" name="blood" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "O"
                    <?php endif; ?>
                    >
                    </div>
                   <div class="col-2">
                       <label for="nationality">เชื้อชาติ *</label>
                        <input id="nationality" name="nationality" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "ไทย"
                    <?php endif; ?>
                    >
                    </div>
                   <div class="col-2">
                       <label for="race">สัญชาติ *</label>
                        <input id="race" name="race" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "ไทย"
                    <?php endif; ?>
                    >
                    </div>
                   <div class="col-2">
                       <label for="religion">ศาสนา *</label>
                        <input id="religion" name="religion" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "พุทธ"
                    <?php endif; ?>
                    >
                    </div>
                </div>

                <legend>ที่อยู่ปัจจุบัน</legend>
                <div class="row">
                   <div class="col-4">
                       <label for="addr1">ที่อยู่ *</label>
                        <input id="addr1" name="addr1" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "ที่อยู่."
                    <?php endif; ?>
                    >
                   </div>
                    <div class="col-2">
                       <label for="subdistrict">ตำบล *</label>
                        <input id="subdistrict" name="subdistrict" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "ตำบล."
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                       <label for="district">อำเภอ *</label>
                        <input id="district" name="district" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "อำเภอ."
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                       <label for="province">จังหวัด *</label>
                        <input id="province" name="province" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "จังหวัด."
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                       <label for="zipcode">รหัสไปรษณีย์ *</label>
                        <input id="zipcode" name="zipcode" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "50000"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                       <label for="tel">โทรศัพท์บ้าน</label>
                        <input id="tel" name="tel" type="text" class="form-control"
                    <?php if($testdata): ?>
                        value = "100000000"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                       <label for="mobile">โทรศัพท์มือถือ *</label>
                        <input id="mobile" name="mobile" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "100000000"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                       <label for="addr_type">ที่อยู่ปัจจุบันนี้เป็น *</label>
                        <select class="form-control <?php echo e($flag); ?>" id="addr_type" name="addr_type">
                            <option value="" >=== เลือก ===</option>
                            <option value="บ้านส่วนตัว" 
                    <?php if($testdata): ?>
                        selected
                    <?php endif; ?>
                    >บ้านส่วนตัว</option>
                            <option value="บ้านของญาติ" >บ้านของญาติ</option>
                            <option value="หอพัก" >หอพัก</option>
                        </select>
                    </div>
                    <div id="addr_type_div_1" class="col-2 d-none">
                        <label for="addr_type1_custom1">ระบุญาติ</label>
                        <input id="addr_type1_custom1" name="addr_type1_custom1" type="text" class="form-control">
                    </div>
                    <div id="addr_type_div_21" class="col-2 d-none">
                        <label for="addr_type2_custom1">หอพักชื่อ</label>
                        <input id="addr_type2_custom1" name="addr_type2_custom1" type="text" class="form-control">
                    </div>
                    <div id="addr_type_div_22" class="col-2  d-none">
                        <label for="addr_type2_custom2">หมายเลขห้อง</label>
                        <input id="addr_type2_custom2" name="addr_type2_custom2" type="text" class="form-control">
                    </div>
                   <div class="col-2">
                        <label for="citizenid">บัตรประชาชน เลขที่ *</label>
                        <input id="citizenid" name="citizenid" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "1234567890123"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                        <label for="citizenstartdate">วันออกบัตร *</label>
                        <input id="citizenstartdate" name="citizenstartdate" type="date" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "2020-08-09"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="col-2">
                        <label for="citizenexpdate">วันหมดอายุบัตร *</label>
                        <input id="citizenexpdate" name="citizenexpdate" type="date" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "2020-08-09"
                    <?php endif; ?>
                    >
                    </div>
                </div>
                <legend>ในกรณีฉุกเฉิน สามารถติดต่อกับ</legend>
                <div class="row">
                    <div class="form-check col-1">
                        <label for="urgent_init">คำนำหน้า *</label>
                    <input id="urgent_init" name="urgent_init" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "นาย"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-3">
                        <label for="urgent_name">ชื่อ-สกุล *</label>
                        <input id="urgent_name" name="urgent_name" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "ทดลอง"
                    <?php endif; ?>
                    >
                    </div>
                    
                    <div class="form-check col-4">
                        <label for="urgent_addr1">ที่อยู่ *</label>
                        <input id="urgent_addr1" name="urgent_addr1" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "ดหกหฟกดหกดหกดกหด"
                    <?php endif; ?>
                    >
                    </div>
                    <div class="form-check col-2">
                        <label for="urgent_mobile">โทรศัพท์ *</label>
                        <input id="urgent_mobile" name="urgent_mobile" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "123654987"
                    <?php endif; ?>
                    >
                    </div>
                     <div class="form-check col-2">
                        <label for="urgent_relation">ความสัมพันธ์เป็น *</label>
                    <input id="urgent_relation" name="urgent_relation" type="text" class="form-control <?php echo e($flag); ?>"
                    <?php if($testdata): ?>
                        value = "น้า"
                    <?php endif; ?>
                    >
                    </div>
                </div>
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset><?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/elements/step2.blade.php ENDPATH**/ ?>