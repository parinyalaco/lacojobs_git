<h3>สุขภาพและความพร้อมทางร่างกาย</h3>
    <fieldset>
        <div class="row">
            <div class="col-12">
                <legend>สุขภาพและความพร้อมทางร่างกาย</legend>

                <div class="row">
                    <div class="col-2">
                        <label for="congenital_disease">โรคประจำตัว</label>
                        <select class="form-control <?php echo e($flag); ?> " id="congenital_disease" name="congenital_disease">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2 congenital_disease d-none">
                        <label for="congenital_disease_other">ระบุชื่อโรคประจำตัว</label>
                        <input id="congenital_disease_other" name="congenital_disease_other" type="text" placeholder="ระบุโรคประจำตัว" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="contagious_disease">โรคติดต่อ/โรคเรื้อรัง</label>
                        <select class="form-control <?php echo e($flag); ?> " id="contagious_disease" name="contagious_disease">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2 contagious_disease d-none">
                        <label for="contagious_disease_other">ระบุชื่อโรคติดต่อ/โรคเรื้อรัง</label>
                        <input id="contagious_disease_other" name="contagious_disease_other" type="text" placeholder="ระบุชื่อโรคติดต่อ/โรคเรื้อรัง" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="allergy">โรคภูมิแพ้</label>
                        <select class="form-control <?php echo e($flag); ?> " id="allergy" name="allergy">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2 allergy d-none">
                        <label for="allergy_other">ระบุชื่อโรคภูมิแพ้</label>
                        <input id="allergy_other" name="allergy_other" type="text" placeholder="ระบุชื่อโรคภูมิแพ้" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="back_pain">อาการปวดหลัง ปวดกล้ามเนื้อ</label>
                        <select class="form-control <?php echo e($flag); ?> " id="back_pain" name="back_pain">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <label for="finger_pain">อาการชาตามกล้ามเนื้อ นิ้ว มือ</label>
                        <select class="form-control <?php echo e($flag); ?> " id="finger_pain" name="finger_pain">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <label for="thalassemia">โรคธาลัสซีเมีย</label>
                        <select class="form-control <?php echo e($flag); ?> " id="thalassemia" name="thalassemia">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <label for="liver_virus">ไวรัสตับอักเสบ A B C</label>
                        <select class="form-control <?php echo e($flag); ?> " id="liver_virus" name="liver_virus">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2">
                        <label for="food_allergy">โรคภูมิแพ้</label>
                        <select class="form-control <?php echo e($flag); ?> " id="food_allergy" name="food_allergy">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2 food_allergy d-none">
                        <label for="food_allergy_other">ระบุชื่ออาหารที่แพ้</label>
                        <input id="food_allergy_other" name="food_allergy_other" type="text" placeholder="ระบุชื่ออาหารที่แพ้" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="drug_allergy">แพ้ยา</label>
                        <select class="form-control <?php echo e($flag); ?> " id="drug_allergy" name="drug_allergy">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยเป็น" >ไม่เคยเป็น</option>
                            <option value="เคยเป็น/กำลังเป็น" >เคยเป็น/กำลังเป็น</option>
                        </select>
                    </div>
                    <div class="col-2 drug_allergy d-none">
                        <label for="drug_allergy_other">ระบุชื่อยาที่แพ้</label>
                        <input id="drug_allergy_other" name="drug_allergy_other" type="text" placeholder="ระบุชื่อยาที่แพ้" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="other_disease">โรค/อาการอื่นๆ</label>
                        <input id="other_disease" name="other_disease" type="text" placeholder="ระบุโรค/อาการอื่นๆ" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="physical_exam_time">ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ</label>
                        <input id="physical_exam_time" name="physical_exam_time" type="date" placeholder="ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="physical_exam_part">ส่วนของร่างกายที่ได้รับการตรวจ</label>
                        <input id="physical_exam_part" name="physical_exam_part" type="text" placeholder="ส่วนของร่างกายที่ได้รับการตรวจ" class="form-control">
                    </div>
                    <div class="col-2">
                        <label for="physical_exam_result">ผลการตรวจ</label>
                        <select class="form-control <?php echo e($flag); ?> " id="physical_exam_result" name="physical_exam_result">
                            <option value="" >=== เลือก ===</option>
                            <option value="ปรกติ" >ปรกติ</option>
                            <option value="ไม่ปรกติ" >ไม่ปรกติ</option>
                        </select>
                    </div>
                    <div class="col-2 physical_exam_result d-none">
                        <label for="physical_exam_result_other">ระบุรายละเอียด</label>
                        <input id="physical_exam_result_other" name="physical_exam_result_other" type="text" placeholder="ระบุรายละเอียด" class="form-control ">
                    </div>
                    <div class="col-3">
                        <label for="prev_social_sec_company">ชื่อสถานที่ทำงานก่อนหน้านี้ซึ่งมีการทำประกันสังคม</label>
                        <input id="prev_social_sec_company" name="prev_social_sec_company" type="text" placeholder="ชื่อสถานที่ทำงานก่อนหน้านี้ซึ่งมีการทำประกันสังคม" class="form-control">
                    </div>
                    <div class="col-3">
                        <label for="prev_social_sec_hospital">โรงพยาบาลที่ใช้สิทธิรักษาพยาบาล</label>
                        <input id="prev_social_sec_hospital" name="prev_social_sec_hospital" type="text" placeholder="โรงพยาบาลที่ใช้สิทธิรักษาพยาบาล" class="form-control">
                    </div>
                    <div class="col-12">
                        <legend>บุคคลอ้างอิงซึ่งสามารถรับรองความประพฤติ/ประวัติการทำงานได้ (ไม่ใช่ญาติพี่น้องหรือเพื่อน)</legend>
                        
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="5%">ลำดับ</th>
                                    <th width="25%">ชื่อ-นามสกุล</th>
                                    <th width="10%">มีความสัมพันธ์เป็น</th>
                                    <th>ชื่อสถานที่ทำงาน/ตำแหน่ง</th>
                                    <th width="15%">โทรศัพท์</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><input id="ref_name_1" name="ref_name_1" type="text" placeholder="ระบุชื่อ-สกุล" class="form-control "></td>
                                    <td><input id="ref_relation_1" name="ref_relation_1" type="text" placeholder="ระบุมีความสัมพันธ์เป็น" class="form-control "></td>
                                    <td><input id="ref_company_job_1" name="ref_company_job_1" type="text" placeholder="ระบุชื่อสถานที่ทำงาน/ตำแหน่ง" class="form-control "></td>
                                    <td><input id="ref_tel_1" name="ref_tel_1" type="text" placeholder="โทรศัพท์" class="form-control "></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><input id="ref_name_2" name="ref_name_2" type="text" placeholder="ระบุชื่อ-สกุล" class="form-control "></td>
                                    <td><input id="ref_relation_2" name="ref_relation_2" type="text" placeholder="ระบุมีความสัมพันธ์เป็น" class="form-control "></td>
                                    <td><input id="ref_company_job_2" name="ref_company_job_2" type="text" placeholder="ระบุชื่อสถานที่ทำงาน/ตำแหน่ง" class="form-control "></td>
                                    <td><input id="ref_tel_2" name="ref_tel_2" type="text" placeholder="โทรศัพท์" class="form-control "></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12">
                        <legend>ความรับผิดชอบต่อสังคม</legend>
                        ข้าพเจ้ายินยอมให้บริษัทฯ ตรวจหาสารเสพติดได้ก่อนการเริ่มงานและ ตลอดเวลาการทำงานและขอรับรองว่าจะไม่ยุ่งเกี่ยวกับสารเสพติด หากทางบริษัทฯ ตรวจพบสารเสพติดในตัว ข้าพเจ้ายินยอมถูกเลิกจ้างโดยไม่รับเงินชดเชยใดๆ ทั้งสิ้น<br/>
                        <input type="checkbox" name="accept6" id="accept6" class="form-check-input <?php echo e($flag); ?>" value="yes">
                        <label class="form-check-label" for="accept1">ข้าพเจ้ายินยอมให้บริษัทฯ ตรวจหาสารเสพติดได้ก่อนการเริ่มงานและ ตลอดเวลาการทำงานและขอรับรองว่าจะไม่ยุ่งเกี่ยวกับสารเสพติด</label>
                    </div>
                </div>
                
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset><?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/elements/step9.blade.php ENDPATH**/ ?>