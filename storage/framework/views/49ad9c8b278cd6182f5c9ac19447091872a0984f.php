<h3>สถานภาพสมรส รับราชการทหาร</h3>
    <fieldset>
        <div class="row">
            <div class="col-12"><legend>สถานภาพสมรส</legend>

                <div class="row">
                   <div class="col-2">
                       <label for="married_status">สถานะภาพ</label>
                       <select class="form-control <?php echo e($flag); ?> " id="married_status" name="married_status">
                            <option value="" >=== เลือก ===</option>
                            <option value="โสด" >โสด</option>
                            <option value="สมรส(จดทะเบียน)" >สมรส(จดทะเบียน)</option>
                            <option value="สมรสโดยไม่จดทะเบียน" >สมรสโดยไม่จดทะเบียน</option>
                            <option value="หย่าร้าง" >หย่าร้าง</option>
                            <option value="หม้าย (คู่สมรสถึงแก่กรรม)" >หม้าย (คู่สมรสถึงแก่กรรม)</option>
                            <option value="แยกกันอยู่" >แยกกันอยู่</option>
                        </select>
                   </div>
                   <div class="col-1 married d-none">
                       <label for="married_init">คำนำหน้า</label>
                        <input id="married_init" name="married_init" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2 married d-none">
                       <label for="married_name">ชื่อ-สกุล</label>
                        <input id="married_name" name="married_name" type="text" placeholder="ชื่อ-สกุลคู่สมรส"  class="form-control">
                   </div>
                   <div class="col-1 married d-none">
                       <label for="married_age">อายุ (ปี)</label>
                        <input id="married_age" name="married_age" type="text" placeholder="อายุคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2 married d-none">
                       <label for="married_career">อาชีพ</label>
                        <input id="married_career" name="married_career" type="text"  placeholder="อาชีพคู่สมรส"  class="form-control">
                   </div>
                   <div class="col-3 married d-none">
                       <label for="married_homeaddr">ที่อยู่ปัจจุบัน</label>
                        <input id="married_homeaddr" name="married_homeaddr" type="text" placeholder="ที่อยู่ปัจจุบันคู่สมรส"  class="form-control">
                   </div>
                   <div class="col-1 married d-none">
                       <label for="married_tel">โทรศัพท์</label>
                        <input id="married_tel" name="married_tel" type="text" placeholder="โทรศัพท์คู่สมรส" class="form-control">
                   </div>
                   <div class="col-3 married d-none">
                       <label for="married_companyaddr">ที่ตั้งสถานที่ทำงาน</label>
                        <input id="married_companyaddr" name="married_companyaddr" placeholder="ที่ตั้งสถานที่ทำงานคู่สมรส" type="text" class="form-control">
                   </div>
                   <div class="col-2 married d-none">
                       <label for="married_job">ตำแหน่ง</label>
                        <input id="married_job" name="married_job"  placeholder="ตำแหน่งคู่สมรส" type="text" class="form-control">
                   </div>
                   <div class="col-2 married d-none">
                       <label for="married_companytel">เบอร์โทรศัพท์ที่ทำงาน</label>
                        <input id="married_companytel" name="married_companytel" type="text"  placeholder="เบอร์โทรศัพท์ที่ทำงานคู่สมรส"  class="form-control">
                   </div> <div class="col-5  married d-none"></div>
                   <div class="col-2">
                       <label for="married_child_status">สถานะบุตร</label>
                       <select class="form-control <?php echo e($flag); ?>" id="married_child_status" name="married_child_status">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่มีบุตร" >ไม่มีบุตร</option>
                            <option value="มีบุตร" >มีบุตร</option>
                        </select>
                   </div>
                  
                   <div class="col-1 mychild d-none">
                       <label for="married_child_number">บุตร(คน)</label>
                        <input id="married_child_number" name="married_child_number" placeholder="ถ้ามีระบจำนวน" type="text" class="form-control">
                   </div>
                   <div class="col-1 mychild d-none">
                       <label for="married_boy_no">ชาย(คน)</label>
                        <input id="married_boy_no" name="married_boy_no" placeholder="ถ้ามีระบจำนวน" type="text" class="form-control">
                   </div>
                   <div class="col-2 mychild d-none">
                       <label for="married_boy_age">ชาย(อายุ)</label>
                        <input id="married_boy_age" name="married_boy_age" placeholder="ถ้ามีระบจำนวน" type="text" class="form-control">
                   </div>
                   <div class="col-1 mychild d-none">
                       <label for="married_daughter_no">หญิง(คน)</label>
                        <input id="married_daughter_no" name="married_daughter_no" placeholder="ถ้ามีระบจำนวน" type="text" class="form-control">
                   </div>
                   <div class="col-2 mychild d-none">
                       <label for="married_daughter_age">หญิง(อายุ)</label>
                        <input id="married_daughter_age" name="married_daughter_age" placeholder="ถ้ามีระบจำนวน" type="text" class="form-control">
                   
                    </div>
 <div class="col-12"><b>กรณีผู้ชาย  การรับราชการทหาร</b>
                   </div>

<div class="col-2">
                       <label for="military_status">สถานะการเกณฑ์ทหาร</label>
                       <select class="form-control" id="military_status" name="military_status">
                            <option value="" >=== เลือก ===</option>
                            <option value="ยังไม่ได้รับการเกณฑ์ทหาร" >ยังไม่ได้รับการเกณฑ์ทหาร</option>
                            <option value="เข้ารับการเกณฑ์ทหารแล้ว" >เข้ารับการเกณฑ์ทหารแล้ว</option>
                            <option value="จับได้ใบดำ" >จับได้ใบดำ</option>
                            <option value="เรียนรักษาดินแดน(ร.ด.)" >เรียนรักษาดินแดน(ร.ด.)</option>
                            <option value="อื่นๆ" >อื่นๆ</option>
                        </select>
                   </div>
                   <div class="col-2 military1  d-none">
                        <label for="military1_year">จะเข้ารับการเกณฑ์ทหารในปี</label>
                        <input id="military1_year" name="military1_year" placeholder="ระบุปีพ.ศ." type="text" class="form-control">
                    </div>
                   <div class="col-2 military2 d-none">
                        <label for="military2_custom1">เคยอยู่สังกัด</label>
                        <input id="military2_custom1" name="military2_custom1" placeholder="ระบุสังกัด" type="text" class="form-control">
                    </div>
                    <div class="col-2 military2 d-none">
                        <label for="military2_duration">เป็นเวลา (ปี)</label>
                        <input id="military2_duration" name="military2_duration" placeholder="ระบุปีพ.ศ." type="text" class="form-control">
                    </div>
                    <div class="col-2 military2 d-none">
                        <label for="military2_custom2">ปลดประจำการเมื่อวันที่</label>
                        <input id="military2_custom2" name="military2_custom2" placeholder="" type="date" class="form-control">
                    </div>
                    <div class="col-2 military3 d-none">
                        <label for="military3_year">เรียนรักษาดินแดน(ร.ด.) (ปี)</label>
                        <input id="military3_year" name="military3_year" placeholder="ระบุปีพ.ศ." type="text" class="form-control">
                    </div>
                    <div class="col-3  military4 d-none">
                        <label for="military4_custom1">สาเหตุ</label>
                        <input id="military4_custom1" name="military_custom1" placeholder="ระบุสาเหตุ" type="text" class="form-control">
                    </div>
                </div>
                
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset><?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/elements/step6.blade.php ENDPATH**/ ?>