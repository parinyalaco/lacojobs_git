<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title> 
   <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
                                <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>
 
   
     <link rel="stylesheet" href="<?php echo e(asset('css/main.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/jquery.steps.css')); ?>">


 <script src="<?php echo e(asset('js/jquery-1.9.1.min.js')); ?>" ></script>
 
                                <script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
 <script src="<?php echo e(asset('js/modernizr-2.6.2.min.js')); ?>" ></script>

 <script src="<?php echo e(asset('js/jquery.cookie-1.3.1.js')); ?>" ></script>
                                <script src="<?php echo e(asset('js/jquery.steps.js')); ?>" ></script>
                                <script src="<?php echo e(asset('js/jquery.validate.js')); ?>"></script>
                                <script src="<?php echo e(asset('js/stepapplyjobs.js')); ?>" defer></script>
</head>
<body>
    <?php echo $__env->yieldContent('content'); ?>
                            </body>
                        </html>
<?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/layouts/step.blade.php ENDPATH**/ ?>