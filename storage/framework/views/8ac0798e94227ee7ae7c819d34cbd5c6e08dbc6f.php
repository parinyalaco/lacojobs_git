<h3>ข้อมูลทั่วไป</h3>
    <fieldset>
        <div class="row">
            <div class="col-12"><legend>ข้อมูลทั่วไป</legend>

                <div class="row">
                    <div class="col-2">
                       <label for="current_edu_level">ปัจจุบันกำลังศึกษาอยู่ในระดับ</label>
                        <input id="current_edu_level" name="current_edu_level" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="current_edu_dep">สาขาวิชา</label>
                        <input id="current_edu_dep" name="current_edu_dep" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="current_edu_year">ชั้นปีที่</label>
                        <input id="current_edu_year" name="current_edu_year" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="current_edu_school">ชื่อสถานศึกษา</label>
                        <input id="current_edu_school" name="current_edu_school" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                    <div class="col-4">
                    </div>
                    <div class="col-2">
                       <label for="future_edu_level">มีแผนการศึกษาต่อในระดับ</label>
                        <input id="future_edu_level" name="future_edu_level" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="future_edu_dep">สาขาวิชา</label>
                        <input id="future_edu_dep" name="future_edu_dep" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="future_edu_year">ในปี พ.ศ</label>
                        <input id="future_edu_year" name="future_edu_year" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="future_edu_school">ชื่อสถานศึกษา</label>
                        <input id="future_edu_school" name="future_edu_school" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                    <div class="col-4">
                    </div>
                    <div  class="col-12" >
                        <div class="row" >
<div class="col-2">
                       <label for="hobby_1">งานอดิเรก 1</label>
                        <input id="hobby_1" name="hobby_1" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="hobby_2">งานอดิเรก 2</label>
                        <input id="hobby_2" name="hobby_2" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="hobby_3">งานอดิเรก 3</label>
                        <input id="hobby_3" name="hobby_3" type="text" placeholder="คำนำหน้าคู่สมรส" class="form-control">
                   </div>

                   <div class="col-2">
                    <label for="drunker">การดื่มสุรา</label>
                       <select class="form-control <?php echo e($flag); ?> " id="drunker" name="drunker">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยดื่ม" >ไม่เคยดื่ม</option>
                            <option value="เลิกดื่มแล้ว" >เลิกดื่มแล้ว</option>
                            <option value="ดื่มเป็นครั้งคราว" >ดื่มเป็นครั้งคราว</option>
                            <option value="ดื่มเป็นประจำ" >ดื่มเป็นประจำ</option>
                        </select>
                   </div>
                   <div class="col-2">
                    <label for="smoker">การสูบบุหรี่</label>
                       <select class="form-control <?php echo e($flag); ?> " id="smoker" name="smoker">
                            <option value="" >=== เลือก ===</option>
                            <option value="ไม่เคยสูบ" >ไม่เคยสูบ</option>
                            <option value="เลิกสูบแล้ว" >เลิกสูบแล้ว</option>
                            <option value="สูบเป็นครั้งคราว" >สูบเป็นครั้งคราว</option>
                            <option value="สูบทุกวัน" >สูบทุกวัน</option>
                        </select>
                   </div>
                   <div class="col-1 smoker d-none">
                       <label for="smoker_other">วันละ(มวน)</label>
                        <input id="smoker_other" name="smoker_other" type="text" placeholder="จำนวนมวน" class="form-control">
                   </div>

                        </div>
                        

                    </div>
                    <div class="col-12" >
                        <div class='row'>
                            <div class="col-2">
                    <label for="criminal">การต้องคดีอาญาถึงจำคุกหรือต้องคำพิพากษาถึงที่สุดให้จำคุก</label>
                       <select class="form-control <?php echo e($flag); ?> " id="criminal" name="criminal">
                            <option value="" >=== เลือก ===</option>
                            <option value="เคย" >เคย</option>
                            <option value="ไม่เคย" >ไม่เคย</option>
                        </select>
                   </div>
                   <div class="col-3 criminal d-none">
                       <label for="criminal_other">ระบุรายละเอียด</label>
                        <input id="criminal_other" name="criminal_other" type="text" placeholder="ระบุรายละเอียด" class="form-control">
                   </div>
                    <div class="col-2">
                    <label for="control">การถูกปลดออกจากหน้าที่เนื่องจากความประพฤติไม่เหมาะสม / การปฏิบัติงานบกพร่อง</label>
                       <select class="form-control <?php echo e($flag); ?> " id="control" name="control">
                            <option value="" >=== เลือก ===</option>
                            <option value="เคย" >เคย</option>
                            <option value="ไม่เคย" >ไม่เคย</option>
                        </select>
                   </div>
                   <div class="col-3 control d-none">
                       <label for="control_other">ระบุรายละเอียด</label>
                        <input id="control_other" name="control_other" type="text" placeholder="ระบุรายละเอียด" class="form-control">
                   </div>
                   <div class="col-2">
                    <label for="can_other_country">การไปปฏิบัติงานต่างจังหวัด และ ต่างประเทศ</label>
                       <select class="form-control <?php echo e($flag); ?> " id="can_other_country" name="can_other_country">
                            <option value="" >=== เลือก ===</option>
                            <option value="ได้" >ได้</option>
                            <option value="ไม่ได้" >ไม่ได้</option>
                        </select>
                   </div>
                   <div class="col-3 upcontry d-none">
                       <label for="can_other_country_case">เนื่องจาก</label>
                        <input id="can_other_country_case" name="can_other_country_case" type="text" placeholder="เนื่องจาก" class="form-control">
                   </div>



                        </div>
                    </div>
                    <div class="col-12" >
                        <div class='row'>
                            <div class="col-1">
                                <label for="apply_job_no">สมัครงานกับบริษัทฯ ครั้งนี้เป็นครั้งที่</label>
                                <input id="apply_job_no" name="apply_job_no" type="text" placeholder="ระบุครั้งที่" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="apply_job_name">เคยสอบข้อเขียน/สอบสัมภาษณ์กับบริษัทฯ ในตำแหน่ง</label>
                                <input id="apply_job_name" name="apply_job_name" type="text" placeholder="ระบุตำแหน่ง" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="apply_job_company">เหตุผลที่สมัครเข้าทำงานกับบริษัทฯ</label>
                                <input id="apply_job_company" name="apply_job_company" type="text" placeholder="ระบุตำแหน่ง" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="hear_from">ทราบข่าวการรับสมัครงาน</label>
                                <select class="form-control <?php echo e($flag); ?> " id="hear_from" name="hear_from">
                                    <option value="" >=== เลือก ===</option>
                                    <option value="สื่อทางอินเตอร์เน็ต" >สื่อทางอินเตอร์เน็ต</option>
                                    <option value="สนง.จัดหางาน จังหวัด" >สนง.จัดหางาน จังหวัด</option>
                                    <option value="เพื่อนหรือคนรู้จัก" >เพื่อนหรือคนรู้จัก</option>
                                    <option value="สื่ออื่นๆ" >สื่ออื่นๆ</option>
                                </select>
                            </div>
                            <div class="col-2 hear_from1 d-none">
                                <label for="hear_from_other">จังหวัด</label>
                                <input id="hear_from_other" name="hear_from_other" type="text" placeholder="จังหวัด" class="form-control">
                            </div>
                            <div class="col-2 hear_from2 d-none">
                                <label for="hear_from_other">ระบุ</label>
                                <input id="hear_from_other" name="hear_from_other" type="text" placeholder="ระบุ" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-12" >
                        <div class='row'>
                            <div class="col-3">
                                <label for="refer_name">บุคคลที่แนะนำให้มาสมัครงาน หรือ ญาติพี่น้อง เพื่อน หรือคนรู้จัก ที่เป็นพนักงานของบริษัทฯ (ถ้ามี) </label>
                                <input id="refer_name" name="refer_name" type="text" placeholder="ระบุชื่อ" class="form-control">
                            </div>
                            <div class="col-4">
                                <label for="your_comments">ความรู้ความเข้าใจที่มีต่อ บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด มีดังนี้</label>
                                <input id="your_comments" name="your_comments" type="text" placeholder="รายละเอียด" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="can_start_in">หากบริษัทฯ รับเข้าทำงาน สามารถเริ่มงานได้ภายใน (วัน)</label>
                                <input id="can_start_in" name="can_start_in" type="text" placeholder="ระบุวัน" class="form-control">
                            </div>
                            <div class="col-3">
                                <label for="can_start_desc">เนื่องจาก</label>
                                <input id="can_start_desc" name="can_start_desc" type="text" placeholder="ระบุ" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset><?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/elements/step7.blade.php ENDPATH**/ ?>