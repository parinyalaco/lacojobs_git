<h3>ความสามารถพิเศษ</h3>
    <fieldset>
        <div class="row">
            <div class="col-12"><legend>ความสามารถพิเศษ</legend>

                <div class="row">
                   <div class="col-2">
                       <label for="expd_type_th">พิมพดีด ภาษาไทย (คำ/นาที)</label>
                        <input id="expd_type_th" name="expd_type_th" type="text" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="expd_type_en">พิมพดีด ภาษาอังกฤษ (คำ/นาที)</label>
                        <input id="expd_type_en" name="expd_type_en" type="text" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="expd_type_note">ชวเลข (คำ/นาที)</label>
                        <input id="expd_type_note" name="expd_type_note" type="text" class="form-control">
                   </div>
                   <div class="col-6">
                       <label for="expd_extra">ความสามารถพิเศษอื่นๆ(ระบุ)</label>
                        <input id="expd_extra" name="expd_extra" type="text" class="form-control">
                   </div>
                   <div class="col-12"><b>คอมพิวเตอร์ โปรแกรม</b> 
                   </div>
                   <?php for($i = 1; $i < 7; $i++): ?>
                       <div class="col-2">
                       <input id="expd_com_program<?php echo e($i); ?>" name="expd_com_program<?php echo e($i); ?>" type="text" class="form-control">
                        <select class="form-control <?php echo e($flag); ?>" id="expd_com_level<?php echo e($i); ?>" name="expd_com_level<?php echo e($i); ?>">
                            <option value="" >=== เลือก ===</option>
                            <option value="ดีมาก" >ดีมาก</option>
                            <option value="ดี" >ดี</option>
                            <option value="พอใช้" >พอใช้</option>
                        </select>
                   </div>
                   <?php endfor; ?>
                    <div class="col-12"><b>ความสามารถด้านภาษา</b>
                   </div>
                   <div class="col-12">
                        <table class="table">
                            <thead>
                                <th width="20%" ></th>
                                <th width="20%" >
                                    ภาษาไทย
                                </th>
                                <th width="20%" >
                                    ภาษาอังกฤษ
                                </th width="20%" >
                                <th width="20%" >
                                    ภาษาจีน
                                </th>
                                <th width="20%" >
                                    <input id="expd_lang_ex" name="expd_lang_ex" type="text" placeholder="ระบุภาษา" class="form-control">
                                </th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>การพูด</td>
                                    <td><select class="form-control <?php echo e($flag); ?>" id="expd_lang_speak_th" name="expd_lang_speak_th">
                            <option value="" >=== เลือก ===</option>
                            <option value="ดีมาก" >ดีมาก</option>
                            <option value="ดี" >ดี</option>
                            <option value="พอใช้" >พอใช้</option>
                        </select></td>
                                    <td><select class="form-control <?php echo e($flag); ?>" id="expd_lang_speak_en" name="expd_lang_speak_en">
                            <option value="" >=== เลือก ===</option>
                            <option value="ดีมาก" >ดีมาก</option>
                            <option value="ดี" >ดี</option>
                            <option value="พอใช้" >พอใช้</option>
                        </select></td>
                                    <td><select class="form-control <?php echo e($flag); ?>" id="expd_lang_speak_ch" name="expd_lang_speak_ch">
                            <option value="" >=== เลือก ===</option>
                            <option value="ดีมาก" >ดีมาก</option>
                            <option value="ดี" >ดี</option>
                            <option value="พอใช้" >พอใช้</option>
                        </select></td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_speak_ex" name="expd_lang_speak_ex">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>การอ่าน </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_read_th" name="expd_lang_read_th">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_read_en" name="expd_lang_read_en">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_read_ch" name="expd_lang_read_ch">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_read_ex" name="expd_lang_read_ex">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>การเขียน </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_write_th" name="expd_lang_write_th">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_write_en" name="expd_lang_write_en">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_write_ch" name="expd_lang_write_ch">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control <?php echo e($flag); ?>" id="expd_lang_write_ex" name="expd_lang_write_ex">
                                            <option value="" >=== เลือก ===</option>
                                            <option value="ดีมาก" >ดีมาก</option>
                                            <option value="ดี" >ดี</option>
                                            <option value="พอใช้" >พอใช้</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                   </div>
                   
                    <div class="col-12"><b>การขับขี่ยานพาหนะ</b>
                   </div>
                   <div class="col-12">
                      <div class="row"> 
                    <div class="col-2">
                            <label for="expd_can_drive">สามารถขับรถยนต์</label> 
                            <select class="form-control <?php echo e($flag); ?>" id="expd_can_drive" name="expd_can_drive">
                                <option value="" >=== เลือก ===</option>
                                <option value="ไม่ได้" >ไม่ได้</option>
                                <option value="ได้" >ได้</option>
                            </select>
                    </div>
                    <div class="col-2">
                            <label for="expd_have_drive_licen">ใบอนุญาตรถยนต์</label> 
                            <select class="form-control <?php echo e($flag); ?>" id="expd_have_drive_licen" name="expd_have_drive_licen">
                                <option value="" >=== เลือก ===</option>
                                <option value="มี" >มี</option>
                                <option value="ไม่มี" >ไม่มี</option>
                            </select>
                    </div>
                    <div class="col-2">
                            <label for="expd_drive_licen_type">ใบอนุญาตรถยนต์แบบ</label> 
                            <input id="expd_drive_licen_type" name="expd_drive_licen_type" type="text" placeholder="ระบุรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_drive_licen_date">วันอนุญาต</label> 
                            <input id="expd_drive_licen_date" name="expd_drive_licen_date" type="date" placeholder="ระบรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_drive_licen_exp">วันหมดอายุ</label> 
                            <input id="expd_drive_licen_exp" name="expd_drive_licen_exp" type="date" placeholder="ระบรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_my_car_brand">รถยนต์ที่ใช้ปัจจุบันยี่ห้อ</label> 
                            <input id="expd_my_car_brand" name="expd_my_car_brand" type="text" placeholder="ระบุรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_my_car_serie">รถยนต์ที่ใช้ปัจจุบันรุ่น</label> 
                            <input id="expd_my_car_serie" name="expd_my_car_serie" type="text" placeholder="ระบุรูปแบบ" class="form-control">
                        </div>
                        </div>
                        </div>
                   <div class="col-12">
                      <div class="row"> 
                    <div class="col-2">
                            <label for="expd_can_ride">สามารถขับจักรยานยนต์</label> 
                            <select class="form-control <?php echo e($flag); ?>" id="expd_can_ride" name="expd_can_ride">
                                <option value="" >=== เลือก ===</option>
                                <option value="ไม่ได้" >ไม่ได้</option>
                                <option value="ได้" >ได้</option>
                            </select>
                    </div>
                    <div class="col-2">
                            <label for="expd_have_ride_licen">ใบอนุญาตจักรยานยนต์</label> 
                            <select class="form-control <?php echo e($flag); ?>" id="expd_have_ride_licen" name="expd_have_ride_licen">
                                <option value="" >=== เลือก ===</option>
                                <option value="มี" >มี</option>
                                <option value="ไม่มี" >ไม่มี</option>
                            </select>
                    </div>
                    <div class="col-2">
                            <label for="expd_ride_licen_type">ใบอนุญาตจักรยานยนต์แบบ</label> 
                            <input id="expd_ride_licen_type" name="expd_ride_licen_type" type="text" placeholder="ระบุรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_ride_licen_date">วันอนุญาต</label> 
                            <input id="expd_ride_licen_date" name="expd_ride_licen_date" type="date" placeholder="ระบรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_ride_licen_exp">วันหมดอายุ</label> 
                            <input id="expd_ride_licen_exp" name="expd_ride_licen_exp" type="date" placeholder="ระบรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_my_bike_brand">จักรยานยนต์ที่ใช้ปัจจุบันยี่ห้อ</label> 
                            <input id="expd_my_bike_brand" name="expd_my_bike_brand" type="text" placeholder="ระบุรูปแบบ" class="form-control">
                        </div>
                        <div class="col-2">
                            <label for="expd_my_bike_serie">จักรยานยนต์ที่ใช้ปัจจุบันรุ่น</label> 
                            <input id=" " name="expd_my_bike_serie" type="text" placeholder="ระบุรูปแบบ" class="form-control">
                        </div>
                        </div>
                        </div>
                </div>
                
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset><?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/elements/step5.blade.php ENDPATH**/ ?>