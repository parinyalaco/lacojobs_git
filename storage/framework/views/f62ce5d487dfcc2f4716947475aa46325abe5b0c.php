<h3>Upload เอกสาร</h3>
    <fieldset>
        <div class="row">
            <div class="col-12">
                <legend>Upload เอกสาร</legend>

                <div class="row">
                    <div class="col-3">
                        <label for="image">ภาพถ่าย</label>
                        <?php echo Form::file('image', $attributes = ['accept'=>'image/jpeg , image/jpg, image/gif, image/png']);; ?>

                    </div>
                    <div class="col-3">
                        <label for="private_info">สำเนา บัตรประจำตัวประชาชน,ทะเบียนบ้าน,หลักฐานแสดงวุฒิการศึกษา (รวมมาเป็น 1 pdf)</label>
                        <?php echo Form::file('private_info', $attributes = ['accept'=>'application/pdf']);; ?>

                    </div>
                    <div class="col-3">
                        <label for="resume">Resume และ เอกสารอื่นๆ  (รวมมาเป็น 1 pdf)</label>
                        <?php echo Form::file('resume', $attributes = ['accept'=>'application/pdf']);; ?>

                    </div>
                    <div class="col-3">
                        <label for="map">ตำแหน่งบ้าน (จุดบนแผนที่)</label>
                        <input id="map" name="map" type="text" placeholder="ระบุรายละเอียด" class="form-control <?php echo e($flag); ?>">
                    </div>
                </div>
                <div class="col-12">
                <div id="googleMap" style="width:100%;height:400px;"></div>
                </div>
                
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset>
    <script>
    var map;
    var lats;
    var lngs;
function error(msg) {
    alert('error in geolocation');
}

function success(position) {
    lats = position.coords.latitude;
    lngs = position.coords.longitude;
    map.setCenter(new google.maps.LatLng(lats,lngs));

};


function myMap() {

    var mapProp= {
        center:new google.maps.LatLng(18.67599842688653, 99.05243451279489),
        zoom:15,
    };
/*
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, error);
        alert(lats + " " + lngs);
    } else {
        alert('location not supported');
    }



    */


    map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

//copy and paste this in your script section.



google.maps.event.addListener(map, 'click', function(event) {
var r = confirm("GPS : " + event.latLng.lat() + ", " + event.latLng.lng() + " ?");
$('#map').val(event.latLng.lat() + ", " + event.latLng.lng());
});

}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUxsUwOjdI8DXS0vDOneRaWo_s53WPg8k&callback=myMap"></script><?php /**PATH D:\Projects\HR\lacojobs_git\resources\views/elements/step10.blade.php ENDPATH**/ ?>