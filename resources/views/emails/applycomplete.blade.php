<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FT Form Alert</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body>
    <p><strong>TO..HR</strong></p>
    <p><strong>Apply Job Alert System</strong></p>
    <p><strong>ตำแหน่งงานที่ต้องการสมัคร</strong></p>
    <p>1. {{ $masterdata->job_name1 }} </p> 
    @if (!empty($masterdata->job_name2))
       <p>1. {{ $masterdata->job_name2 }} </p> 
    @endif
    <p><strong>ชื่อ-นามสกุล ภาษาไทย </strong> {{ $masterdata->init_th }} {{ $masterdata->fname_th }} {{ $masterdata->lname_th }} <strong>ชื่อเล่น</strong> {{ $masterdata->nickname_th }}</p>
    <p><strong>ชื่อ-นามสกุล ภาษาอังกฤษ </strong>{{ $masterdata->init_en }} {{ $masterdata->fname_en }} {{ $masterdata->lname_en }} <strong>Nickname</strong> {{ $masterdata->nickname_en }}</p>                
    <p></p>
    <p><strong>System Alert</strong></p>
</body>
</html>