<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>มีผู้สมัครงานรายวัน</title>
    <style>
        table {
            border: solid 2px black;
        }
        thead tr th{
            border: solid 1px black;
            font-weight: bold;
            font-style: italic;
        }
        tbody tr td{
            border: solid 1px black;
        }
    </style>
</head>
<body>
    <p><strong>TO..HR</strong></p>
    <p><strong>Apply Job Alert System</strong></p>
    <p><strong>มีผู้สมัครงานรายวัน</strong></p>
    <p><strong>ชื่อ-นามสกุล</strong> {{ $dailydata->init }} {{ $dailydata->fname }} {{ $dailydata->lname }}</p>
    <p><strong>วันเกิดและอายุ</strong> {{ $dailydata->birth }} / 
    @php
        $now = new DateTime('now');
        $OldDate = new DateTime($dailydata->birth);
        $result = $OldDate->diff($now);
        echo $result->y;
    @endphp ปี</p>       
    <p><strong>เบอร์โทร</strong> {{ $dailydata->tel }}</p>             
    <p></p>
    <p><strong>System Alert</strong></p>
</body>
</html>