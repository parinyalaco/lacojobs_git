<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>สมัครงานลานนาเกษตร | LACO</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'>


    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.step2.css') }}">


    <script src="{{ asset('js/jquery-1.9.1.min.js') }}"></script>

    <script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>

    <script src="{{ asset('js/jquery.cookie-1.3.1.js') }}"></script>
    <script src="{{ asset('js/jquery.steps.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/dailystepapplyjobs.js?ver=20220425004') }}" defer></script>

    <style>
        .wrapper {
    overflow: auto;
    max-height: 400px;
    width: 100%;

    border: 2px solid black;
}
.term {
    overflow-y: auto;
}
    </style>
</head>

<body>
    <div id="app">
        <div style="max-width: 95%;margin-left:auto;margin-right:auto;">
            @yield('content')
        </div>
    </div>
</body>

</html>
