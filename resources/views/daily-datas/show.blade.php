@extends('layouts.app')

@section('content')
    <div style="max-width: 95%;margin-left:auto;margin-right:auto;">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-3 headerd">LACO</div>
                    <div class="col-6  text-center">
                        <h2 class="newh2">บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด<br />LANNA AGRO INDUSTRY CO., LTD.<br />เอกสารใบสมัครงาน(พนักงานรายวัน)</h2>
                    </div>
                    <div class="col-3  text-right headerd">ประกาศใช้วันที่ :
                        26/09/63</div>
                    <div class="col-3 headerd">รหัสเอกสาร F-HR-RC-001/2</div>
                    <div class="col-6  text-center">&nbsp;</div>
                    <div class="col-3  text-right headerd">ประกาศใช้ครั้งที่ : 06</div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-9"><b>วันที่สมัคร</b> {{ $dailydata->created_at }}<br />
                         <b>ตำแหน่งงานที่ต้องการสมัคร</b><br/>
                        1. <span class='answer'>{{ filldot($dailydata->job,20) }}</span> 2. <span class='answer'>{{ filldot($dailydata->job2,20)  }}</span>
                        <br /><br />
                        <h5 class='newh5'>ส่วนที่ 1 : ข้อมูลส่วนตัว</h5>
                        <b>1. ชื่อ-นามสกุล </b><span class='answer'>{{ filldot($dailydata->init,10) }}{{ filldot($dailydata->fname,10) }}{{ filldot($dailydata->lname,30) }}</span> <b>เพศ </b><span class='answer'>{{ filldot($dailydata->sex,15) }}</span> <br />
                        <b>วัน/เดือน/ปี เกิด</b> <span class='answer'>{{ filldot(tothaiyear($dailydata->birth),30) }}</span>
                        <b>อายุ</b> <span class='answer'>
                            @php
                                $now = new DateTime('now');
                                $OldDate = new DateTime($dailydata->birth);
                                $result = $OldDate->diff($now);
                                echo filldot($result->y,10);
                            @endphp
                        </span>
                        ปี <b>เลขที่บัตรประจำตัวประชาชน</b> <span class='answer'>{{ filldot($dailydata->citizenid,20) }}</span><br>
                        <b>น้ำหนัก</b> <span class='answer'>{{ filldot(number_format($dailydata->weight,1),10) }}</span>
                        กก. <b>ส่วนสูง</b> <span class='answer'>{{ filldot(number_format($dailydata->height,1),10) }}</span>
                        ซม. <b>โทรศัพท์มือถือ</b> <span class='answer'>{{ filldot($dailydata->tel,20) }}</span><br/>
                        <b>2. ที่อยู่</b><br/>
                        <b>เลขที่</b>  <span class='answer'>
                             {!! filldot($dailydata->addr,15) !!}
                            </span>
                         <b>หมู่ที่</b>  <span class='answer'>
                             {!! filldot($dailydata->addr2,10) !!}
                        </span>
                         <b>ตำบล</b>  <span class='answer'>
                             {!! filldot($dailydata->subcity,25) !!}
                            </span>
                         <b>อำเภอ</b>  <span class='answer'>
                             {!! filldot($dailydata->city,25) !!}
                            </span>
                         <b>จังหวัด</b>  <span class='answer'>
                             {!! filldot($dailydata->province,25) !!}
                            </span>
                            <b>รหัสไปรษณีย์</b>  <span class='answer'>
                             {!! filldot($dailydata->zipcode,10) !!}
                            </span><br/>
                       <b>3. ในกรณีฉุกเฉินทางบริษัทฯ สามารถติดต่อกับ</b> <span class='answer'>
                           {!! filldot($dailydata->dailydataext->contact_init,10) !!}{!! filldot($dailydata->dailydataext->contact_name,5) !!}{!! filldot($dailydata->dailydataext->contact_lname,25) !!}
                        </span>
                         <b>โทรศัพท์</b>  <span class='answer'>
                             {!! filldot($dailydata->dailydataext->contact_tel,20) !!}
                        </span>
                         <b>ซึ่งมีความสัมพันธ์เป็น</b>  <span class='answer'>
                             {!! filldot($dailydata->dailydataext->contact_relationm,20) !!}
                        </span><br>
                        <b>4. สิทธิประกันสังคม</b>  <span class='answer'>
                            {!! filldot($dailydata->dailydataext->social_security_hospital_flag,20) !!}
                        </span><b>(ถ้ามีระบุโรงพยาบาล)</b>  <span class='answer'>
                   {!! filldot($dailydata->dailydataext->social_security_hospital,30)!!}
                </span><br>
               <b>5. โรคประจำตัว</b> <span class='answer'>{{ filldot($dailydata->dailydataext->congenital_disease,20) }}</span> <b>(ถ้ามีระบุโรค)</b>  <span class='answer'>
                   {!! filldot($dailydata->dailydataext->congenital_disease_note,30)!!}
                </span>

                        <br><br>
                    </div>
                    <div class="col-3  text-right">
                        @if (!empty($dailydata->image))
                            <img width="250px" src="{{ url($dailydata->image) }}">
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">

                          <h5 class='newh5'>ส่วนที่ 2 : ประวัติการศึกษา</h5>
                        <b>จบการศึกษาสูงสุด ระดับ</b> <span class='answer'>{{ filldot($educationlist[$dailydata->education],20) }}</span> <b>ชื่อสถาบันการศึกษา</b> <span class='answer'>{{ filldot($dailydata->dailydataext->educationname,30) }}</span><br><br>
                         <h5 class='newh5'>ส่วนที่ 3 : ประวัติการทำงาน(จากปัจจุบันตามลำดับ)</h5>
                        <table>
                        <tr>
                            <td rowspan="3">ลำดับที่ 1</td>
                            <td>ตั้งแต่ ว/ด/ป - ถึง ว/ด/ป</td>
                            <td>ชื่อสถานที่ทำงาน</td>
                            <td>จังหวัด</td>
                            <td>ตำแหน่ง/ลักษณะงานที่ทำ</td>
                        </tr>
                        <tr>
                            <td >{{ $dailydata->dailydataext->exp_start_date_1 }} - {{ $dailydata->dailydataext->exp_end_date_1 }}</td>
                            <td >{{ $dailydata->dailydataext->exp_company_1 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_province_1 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_job_1 }}</td>
                        </tr>

                        <tr>
                            <td colspan="2">เงินเดือน :  {{ $dailydata->dailydataext->exp_salary_1 }}</td>
                            <td  colspan="2">สาเหตุที่ลาออก :  {{ $dailydata->dailydataext->exp_case_1 }}</td>
                        </tr>
                        <tr>
                            <td rowspan="3">ลำดับที่ 2</td>
                            <td>ตั้งแต่ ว/ด/ป - ถึง ว/ด/ป</td>
                            <td>ชื่อสถานที่ทำงาน</td>
                            <td>จังหวัด</td>
                            <td>ตำแหน่ง/ลักษณะงานที่ทำ</td>
                        </tr>
                        <tr>
                            <td >{{ $dailydata->dailydataext->exp_start_date_2 }} - {{ $dailydata->dailydataext->exp_end_date_2 }}</td>
                         <td >{{ $dailydata->dailydataext->exp_company_2 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_province_2 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_job_2 }}</td>
                        </tr>

                        <tr>
                            <td colspan="2">เงินเดือน :  {{ $dailydata->dailydataext->exp_salary_2 }}</td>
                            <td  colspan="2">สาเหตุที่ลาออก :  {{ $dailydata->dailydataext->exp_case_2 }}</td>
                        </tr>
                        </table><br/>  </div>

                </div>
                <div class='row'>
                    <div class='col-12'>
                        <div class='row'>
                            <div class="col-7">
                                <div class="row">
                                    <div class="col-9">
                                        “ข้าพเจ้าผู้สมัครงานขอให้ข้อมูลส่วนบุคคลต่อไปนี้ตามความจริงและยินยอมให้บริษัทฯ
                                        เก็บรวบรวม ใช้ หรือเปิดเผยตาม พรบ.คุ้มครองข้อมูลส่วนบุคคล
                                        ของบริษัทลานนาเกษตรที่ได้กำหนดไว้แล้ว ซึ่งข้าพเจ้าได้อ่านและตรวจสอบตาม QR CODE นี้
                                        และทำการเซ็นรับทราบและยินยอมแล้ว”</div>
                                    <div class="col-3"><img
                                            src="{{ asset('images/pdpa-laco-th-staff-applicant.png') }}" width="150px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-5" style="text-align: center;"><br /><br />
                                ลงชื่อ......................................................ผู้สมัคร<br />
                                (.....{{ $dailydata->init }} {{ $dailydata->fname }} {{ $dailydata->lname }}.....)<br />
                                ................/.............../...............
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
