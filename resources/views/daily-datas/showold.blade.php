@extends('layouts.app')

@section('content')
    <div style="max-width: 95%;margin-left:auto;margin-right:auto;">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-3">LACO<br />รหัสเอกสาร #</div>
                    <div class="col-6  text-center">
                        <h2>บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด<br />LANNA AGRO INDUSTRY CO., LTD.<br />ใบสมัครงาน </h2>
                    </div>
                    <div class="col-3  text-right">เลขที่ {{ $dailydata->id }}<br />ประกาศใช้วันที่ :
                        01/07/60<br />ประกาศใช้ครั้งที่ : 05</div>
                </div>
                <div class="row">
                    <div class="col-9">วันที่ {{ $dailydata->created_at }}<br />
                        <b>ตำแหน่งงานที่ต้องการสมัคร</b> <span class='answer'>{{ $dailydata->job }}</span>
                        <br /><br />
                        <h5 class='newh5'>ส่วนที่ 1 : ข้อมูลส่วนตัว</h5>
                        <b>ชื่อ-นามสกุล </b><span class='answer'>{{ $dailydata->init }} {{ $dailydata->fname }}
                            {{ $dailydata->lname }}</span> <b>ชื่อเล่น </b><span class='answer'>{{ $dailydata->nickname }}</span> <br />
                        <b>วัน/เดือน/ปี เกิด</b> <span class='answer'>{{ tothaiyear($dailydata->birth) }}</span>
                        <b>อายุ</b> <span class='answer'>
                            @php
                                $now = new DateTime('now');
                                $OldDate = new DateTime($dailydata->birth);
                                $result = $OldDate->diff($now);
                                echo $result->y;
                            @endphp
                        </span>
                        ปี <b>เลขที่บัตรประจำตัวประชาชน</b> <span class='answer'>{{ $dailydata->citizenid }}</span><br>
                        <b>น้ำหนัก</b> <span class='answer'>{{ $dailydata->weight }}</span>
                        กก. <b>ส่วนสูง</b> <span class='answer'>{{ $dailydata->height }}</span>
                        ซม. <b>โทรศัพท์มือถือ</b> <span class='answer'>{{ $dailydata->tel }}</span><br>
                        <b>ที่อยู่ปัจจุบัน </b> เลขที่ <span class='answer'> {{ $dailydata->addr }}</span> ตำบล <span class='answer'> {{ $dailydata->subcity }}</span> อำเภอ
                           <span class='answer'>  {{ $dailydata->city }}</span> จังหวัด <span class='answer'> {{ $dailydata->province }}</span>
                           รหัสไปรษณีย์ <span class='answer'> {{ $dailydata->zipcode }} </span><br><br>
                    </div>
                    <div class="col-3  text-right">
                        @if (!empty($dailydata->image))
                            <img width="250px" src="{{ url($dailydata->image) }}">
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">

                        <h5 class='newh5'>ส่วนที่ 2 : สถานะภาพสมรส</h5>
                        <b>สถานะภาพสมรส</b> <span class='answer'>{{ $dailydata->dailydataext->marriedstatus }}</span><br><br>
                        <h5 class='newh5'>ส่วนที่ 3 : ประวัติการศึกษา</h5>
                        <b>จบการศึกษาสูงสุด ระดับ</b> <span class='answer'>{{ $dailydata->education }}</span> <b>ชื่อสถาบันการศึกษา</b> <span class='answer'>{{ $dailydata->dailydataext->educationname }}</span><br><br>
                         <h5 class='newh5'>ส่วนที่ 4 : ประวัติการทำงาน(จากปัจจุบันตามลำดับ)</h5>
                        <table>
                        <tr>
                            <td rowspan="3">ลำดับที่ 1</td>
                            <td>ตั้งแต่ ว/ด/ป - ถึง ว/ด/ป</td>
                            <td>ชื่อสถานที่ทำงาน</td>
                            <td>จังหวัด</td>
                            <td>ตำแหน่ง/ลักษณะงานที่ทำ</td>
                        </tr>
                        <tr>
                            <td >{{ $dailydata->dailydataext->exp_start_date_1 }} - {{ $dailydata->dailydataext->exp_end_date_1 }}</td>
                            <td >{{ $dailydata->dailydataext->exp_company_1 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_province_1 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_job_1 }}</td>
                        </tr>
                        
                        <tr>
                            <td colspan="2">เงินเดือน :  {{ $dailydata->dailydataext->exp_salary_1 }}</td>
                            <td  colspan="2">สาเหตุที่ลาออก :  {{ $dailydata->dailydataext->exp_case_1 }}</td>
                        </tr>
                        <tr>
                            <td rowspan="3">ลำดับที่ 2</td>
                            <td>ตั้งแต่ ว/ด/ป - ถึง ว/ด/ป</td>
                            <td>ชื่อสถานที่ทำงาน</td>
                            <td>จังหวัด</td>
                            <td>ตำแหน่ง/ลักษณะงานที่ทำ</td>
                        </tr>
                        <tr>
                            <td >{{ $dailydata->dailydataext->exp_start_date_2 }} - {{ $dailydata->dailydataext->exp_end_date_2 }}</td>
                         <td >{{ $dailydata->dailydataext->exp_company_2 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_province_2 }}</td>
                            <td>{{ $dailydata->dailydataext->exp_job_2 }}</td>
                        </tr>
                        
                        <tr>
                            <td colspan="2">เงินเดือน :  {{ $dailydata->dailydataext->exp_salary_2 }}</td>
                            <td  colspan="2">สาเหตุที่ลาออก :  {{ $dailydata->dailydataext->exp_case_2 }}</td>
                        </tr>
                        </table><br/>
                        <h5 class='newh5'>ส่วนที่ 5 : ช้อมูลทั่วไป</h5>
                        <b>1. บุคคลที่แนะนำให้มาสมัครงานที่เป็นพนักงานในบริษัทฯ(ถ้ามี)</b> <span class='answer'>{{ $dailydata->dailydataext->refer_name }}</span> <b>มีความสัมพันธ์</b>  <span class='answer'>{{ $dailydata->dailydataext->refer_relation }}</span><br>
                         <b>2.ในกรณีฉุกเฉินทางบริษัทฯ สามารถติดต่อกับ</b> <span class='answer'>{{ $dailydata->dailydataext->contact_name }}</span> 
                         <b>เลขที่</b>  <span class='answer'>{{ $dailydata->dailydataext->refer_addr1 }}</span>
                         <b>หมู่ที่</b>  <span class='answer'>{{ $dailydata->dailydataext->refer_addr2 }}</span>
                         <b>ตำบล</b>  <span class='answer'>{{ $dailydata->dailydataext->contact_tumbon }}</span>
                         <b>อำเภอ</b>  <span class='answer'>{{ $dailydata->dailydataext->contact_aumpher }}</span>
                         <b>จังหวัด</b>  <span class='answer'>{{ $dailydata->dailydataext->contact_province }}</span>
                         <b>โทรศัพท์</b>  <span class='answer'>{{ $dailydata->dailydataext->contact_tel }}</span>
                         <b>ซึ่งมีความสัมพันธ์เป็น</b>  <span class='answer'>{{ $dailydata->dailydataext->contact_relation }}</span><br>
                         <b>3. การต้องคดีอาญาถึงจำคุกหรือต้องคำพิพากษาถึงที่สุดให้จำคุก</b> <span class='answer'>{{ $dailydata->dailydataext->criminal }}</span> <b>รายละเอียด</b>  <span class='answer'>{{ $dailydata->dailydataext->criminal_other }}</span><br><br/>
                         <h5 class='newh5'>ส่วนที่ 6 : สุขภาพความพร้อมทางร่างกาย</h5>
                         <b>1. โรคประจำตัว</b> <span class='answer'>{{ $dailydata->dailydataext->congenital_disease }}</span> <b>(ถ้ามีระบุโรค)</b>  <span class='answer'>{{ $dailydata->dailydataext->congenital_disease }}</span><br>
                          <b>2. ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ</b> <span class='answer'>{{ $dailydata->dailydataext->health_check_date }}</span> <b>ส่วนที่ได้รับการตรวจ</b>  <span class='answer'>{{ $dailydata->dailydataext->health_check_case }}</span>
                          <b>ผลการตรวจ</b>  <span class='answer'>{{ $dailydata->dailydataext->health_check_status }}</span> <b>(ระบุรายละเอียด)</b>  <span class='answer'>{{ $dailydata->dailydataext->health_check_status_note }}</span><br>
                        <b>3.โรงพยาบาลที่ขอใช้สิทธิรักษาพยาบาลประกันสังคม</b>  <span class='answer'>{{ $dailydata->dailydataext->social_security_hospital }}</span><br>
                    </div>

                </div>
                <div class='row'>
                    <div class='col-12'>
                        <div class='row'>
                            <div class="col-6">
                                &nbsp;
                            </div>
                            <div class="col-6" style="text-align: center; margin-top:30px;">
                                ลงชื่อ...............................................................ผู้สัมภาษณ์<br />
                                (.....{{ $dailydata->init }} {{ $dailydata->fname }} {{ $dailydata->lname }}.....)<br />
                                ................/.............../...............
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
