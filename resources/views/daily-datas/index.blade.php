@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">ข้อมูลผู้สมัครงานรายวัน</div>
                    <div class="card-body">

                        <form method="GET" action="{{ url('/dailydatas') }}" accept-charset="UTF-8"
                            class="form-inline my-2 my-lg-0 float-left" role="search">
                            <div class="input-group">
                                <input type="checkbox" class="form-check-input" id="weightcond" name="weightcond" value="1"
                                    @if (!empty(request('weightcond'))) checked @endif>
                                <label for="weightcond" class='form-check-label mr-3 ml-1'> น้ำหนัก <= 100 kgs </label>
                                        <input type="checkbox" class="form-check-input" id="heightcond" name="heightcond"
                                            value="1" @if (!empty(request('heightcond'))) checked @endif>
                                        <label for="heightcond" class='form-check-label mr-3 ml-1'> ความสูง >= 150 cm
                                        </label>
                                        <input type="checkbox" class="form-check-input" id="agecond" name="agecond"
                                            value="1" @if (!empty(request('agecond'))) checked @endif>
                                        <label for="agecond" class='form-check-label mr-3 ml-1'> อายุ <= 45 ปี </label>
                                                <input type="checkbox" class="form-check-input" id="educatecond"
                                                    name="educatecond" value="1"
                                                    @if (!empty(request('educatecond'))) checked @endif>
                                                <label for="educatecond" class='form-check-label mr-3 ml-1'>
                                                    การศึกษามากกว่าประถมศึกษาชั้นปีที่ 6 </label>
                                                </div>
                            <div class="input-group">
                                <label for="dtstart" class='form-check-label mr-1 ml-3'> ค้นหาชื่อ
                                        </label>
                            <input type="text" class="form-control" name="search"
                                    placeholder="Search..." value="{{ request('search') }}">
                                    <label for="dtstart" class='form-check-label mr-1 ml-3'> จากวันที่สมัคร
                                        </label>
                            <input type="date" class="form-control" name="dtstart"
                                    placeholder="Search..." value="{{ request('dtstart') }}">
                                    <label for="dtend" class='form-check-label mr-1 ml-3'> ถึงวันที่สมัคร
                                        </label>
                                    <input type="date" class="form-control" name="dtend"
                                    placeholder="Search..." value="{{ request('dtend') }}">
                                
                                    <button class="btn btn-secondary mr-3 ml-3" name="ftmtype" type="submit" value="submit">
                                        <i class="fa fa-search"></i> ค้นหา
                                    </button>
                                     <button class="btn btn-secondary mr-3 ml-3" name="ftmtype" type="submit" value="excel">
                                        <i class="fa fa-table"></i> Excel
                                    </button>
                            </div>
                        </form>

                        <br />
                        <br />
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>วันที่สมัคร</th>
                                        <th>ชื่อ</th>
                                        <th>น้ำหนัก / ความสูง</th>
                                        <th>อายุ / วุฒิการศึกษาสูงสุด</th>
                                        <th>สถานะ</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dailydatas as $item)
                                        @php
                                            $datage = strtotime(date('Y-m-d')) - strtotime($item->birth);
                                            $age = date('Y', $datage) - 1970;
                                        @endphp
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->init }} {{ $item->fname }} {{ $item->lname }}</td>
                                            <td>{{ number_format($item->weight,1) }} kgs / {{ number_format($item->height,1) }} cm</td>
                                            <td>{{ $age }} ปี / {{ $educationlist[$item->education] }}</td>
                                            <td>{{ $item->status }}</td>
                                            <td>
                                                <a href="{{ url('/dailydatas/' . $item->id) }}" title="View MasterData"
                                                    target="_blank">
                                                    <button class="btn btn-info btn-sm">
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                        ตรวจสอบข้อมูล
                                                    </button>
                                                </a>
                                                @if ($item->status == 'register')
                                                <br/>
                                                    <a href="{{ url('/dailydatas/changeStatus/' . $item->id . '/Approve') }}"
                                                        title="View MasterData">
                                                        <button class="btn btn-success btn-sm">
                                                            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                                            อนุมัติ
                                                        </button>
                                                    </a>
                                                    <a href="{{ url('/dailydatas/changeStatus/' . $item->id . '/Reject') }}"
                                                        title="View MasterData">
                                                        <button class="btn btn-warning btn-sm">
                                                            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                                            ไม่อนุมัติ
                                                        </button>
                                                    </a>
                                                @endif
                                                <br/>
                                                <form method="POST" action="{{ url('/dailydatas/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Job" onclick="return confirm(&quot;ยืนยันการลบข้อมูล?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> ลบข้อมูล</button>
                                            </form>

                                                   
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper">
                                {!! $dailydatas->appends([
                                    'search' => Request::get('search'),
                                    'weightcond' => Request::get('weightcond'),
                                    'heightcond' => Request::get('heightcond'),
                                    'agecond' => Request::get('agecond'),
                                    'educatecond' => Request::get('educatecond'),
                                ])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
