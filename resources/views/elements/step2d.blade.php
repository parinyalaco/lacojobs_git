<h3>ข้อมูลส่วนตัว</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ข้อมูลพื้นฐาน</legend>
            <div class="row">
                <div class="form-check col-12 col-xl-3">
                    <label for="job">ตำแหน่งงานที่ต้องการสมัคร *</label>
                    <input id="job" name="job" type="text" class="form-control {{ $flag }}"
                        value="{{ old('job') }}" @if ($testdata) value = "ทดสอบ" @endif>
                    @error('job')
                        <div class="alert alert-danger">ตำแหน่งงานที่ต้องการสมัคร</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-1">
                    <label for="init">คำนำหน้า *</label>
                    <select class="form-control  {{ $flag }}" id="init" name="init">
                        <option value="">=== เลือก ===</option>
                        <option value="นาย" @if ($testdata) selected @endif
                            {{ old('init') == 'นาย' ? 'selected' : '' }}>นาย</option>
                        <option value="นาง" {{ old('init') == 'นาง' ? 'selected' : '' }}>นาง</option>
                        <option value="นางสาว" {{ old('init') == 'นางสาว' ? 'selected' : '' }}>นางสาว</option>
                    </select>
                    @error('init')
                        <div class="alert alert-danger">กรุณาเลือกคำนำหน้า</div>
                    @enderror
                </div>
                <div class="form-check col-12 col-xl-3">
                    <label for="fname">ชื่อ *</label>
                    <input id="fname" name="fname" type="text" class="form-control {{ $flag }}"
                        value="{{ old('fname') }}" @if ($testdata) value = "ทดสอบ" @endif>
                    @error('fname')
                        <div class="alert alert-danger">กรุณาใส่ชื่อ</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-3">
                    <label for="lname">นามสกุล *</label>
                    <input id="lname" name="lname" type="text" class="form-control {{ $flag }}"
                        value="{{ old('lname') }}" @if ($testdata) value = "ลองดู" @endif>
                    @error('lname')
                        <div class="alert alert-danger">กรุณาใส่นามสกุล</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-2">
                    <label for="nickname">ชื่อเล่น *</label>
                    <input id="nickname" name="nickname" type="text" class="form-control {{ $flag }}"
                        value="{{ old('nickname') }}" @if ($testdata) value = "ลองดู" @endif>
                    @error('nickname')
                        <div class="alert alert-danger">กรุณาใส่ชื่อเล่น</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="sex">เพศ *</label>
                    <select class="form-control  {{ $flag }}" id="sex" name="sex">
                        <option value="">=== เลือก ===</option>
                        <option value="ชาย" {{ old('sex') == 'ชาย' ? 'selected' : '' }} @if ($testdata) selected @endif>ชาย</option>
                        <option value="หญิง" {{ old('sex') == 'หญิง' ? 'selected' : '' }} >หญิง</option>
                    </select>
                    @error('sex')
                        <div class="alert alert-danger">กรุณาเลือกเพศ</div>
                    @enderror
                </div>

                <div class="form-check col-6  col-xl-2">
                    <label for="birth_date">วันเกิด *</label>
                    <input id="birth_date" name="birth_date" type="date" class="form-control {{ $flag }}"
                        value="{{ old('birth_date') }}"
                        @if ($testdata) value = "2020-08-29" @endif>
                    @error('birth_date')
                        <div class="alert alert-danger">กรุณาเลือกวันเกิด</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="weight">น้ำหนัก(กก.) *</label>
                    <input id="weight" name="weight" type="number" class="form-control {{ $flag }}"
                        value="{{ old('weight') }}" @if ($testdata) value = "55" @endif>
                    @error('weight')
                        <div class="alert alert-danger">กรุณาใส่น้ำหนัก</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="height">ส่วนสูง(ซม.) *</label>
                    <input id="height" name="height" type="number" class="form-control {{ $flag }}"
                        value="{{ old('height') }}" @if ($testdata) value = "165" @endif>
                    @error('height')
                        <div class="alert alert-danger">กรุณาใส่ส่วนสูง</div>
                    @enderror
                </div>
                <div class="form-check col-12  col-xl-4">
                    <label for="citizenid">เลขที่บัตรประชาชน *</label>
                    <input id="citizenid" name="citizenid" type="number" maxlength="13"
                        value="{{ old('citizenid') }}" class="form-control {{ $flag }}"
                        @if ($testdata) value = "1234567890123" @endif
                        oninvalid="this.setCustomValidity('กรุณาใส่เลขที่บัตรประชาชน 13 หลัก')"
                        onvalid="this.setCustomValidity('')">
                    @error('citizenid')
                        <div class="alert alert-danger">กรุณาใส่เลขที่บัตรประชาชน 13 หลัก</div>
                    @enderror
                </div>
                <div class="form-check col-12  col-xl-4">
                    <label for="marriedstatus">สถานะภาพสมรส *</label>
                    <select class="form-control  {{ $flag }}" id="marriedstatus" name="marriedstatus">
                        <option value="">=== เลือก ===</option>
                        <option value="โสด" {{ old('marriedstatus') == 'โสด' ? 'selected' : '' }} @if ($testdata) selected @endif>โสด
                        </option>
                        <option value="สมรส(จดทะเบียน)" {{ old('marriedstatus') == 'สมรส(จดทะเบียน)' ? 'selected' : '' }}>สมรส(จดทะเบียน)</option>
                        <option value="อยู่ด้วยกันโดยไม่จดทะเบียน" {{ old('marriedstatus') == 'อยู่ด้วยกันโดยไม่จดทะเบียน' ? 'selected' : '' }}>อยู่ด้วยกันโดยไม่จดทะเบียน</option>
                        <option value="หย่าร้าง" {{ old('marriedstatus') == 'หย่าร้าง' ? 'selected' : '' }}>หย่าร้าง</option>
                        <option value="หม้าย(คู่สมรสถึงแก่กรรม)" {{ old('marriedstatus') == 'หม้าย(คู่สมรสถึงแก่กรรม)' ? 'selected' : '' }}>หม้าย(คู่สมรสถึงแก่กรรม)</option>
                        <option value="แยกกันอยู่" {{ old('marriedstatus') == 'แยกกันอยู่' ? 'selected' : '' }}>แยกกันอยู่</option>
                    </select>
                    @error('marriedstatus')
                        <div class="alert alert-danger">กรุณาเลือกสถานะภาพสมรส</div>
                    @enderror
                </div>
                <div class="form-check col-12  col-xl-4">
                    <label for="education">วุฒิการศึกษาสูงสุด *</label>
                    <select class="form-control  {{ $flag }}" id="education" name="education">
                        <option value="">=== เลือก ===</option>
                        <option value="1" {{ old('education') == '1' ? 'selected' : '' }} @if ($testdata) selected @endif>ประถมศึกษาชั้นปีที่6
                        </option>
                        <option value="2" {{ old('education') == '2' ? 'selected' : '' }}>มัธยมศึกษาชั้นปีที่3</option>
                        <option value="3" {{ old('education') == '3' ? 'selected' : '' }}>มัธยมศึกษาชั้นปีที่6 / ปวช.</option>
                        <option value="4" {{ old('education') == '4' ? 'selected' : '' }}>ปวส.</option>
                        <option value="4" {{ old('education') == '5' ? 'selected' : '' }}>ปริญญาตรี</option>
                    </select>
                    @error('education')
                        <div class="alert alert-danger">กรุณาเลือกวุฒิการศึกษาสูงสุด</div>
                    @enderror
                </div>
                <div class="form-check col-12  col-xl-4">
                    <label for="educationname">ชื่อสถานศึกษา *</label>
                    <input id="educationname" name="educationname" type="text"
                        value="{{ old('educationname') }}" class="form-control {{ $flag }}"
                        @if ($testdata) value = "1234567890123" @endif ">
                    @error('educationname')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานศึกษา</div>
                    @enderror
                </div>
            </div>
            <legend>ที่อยู่ปัจจุบัน</legend>
            <div class="row">
                <div class="form-check col-12  col-xl-2">
                    <label for="addr">ที่อยู่ *</label>
                    <input id="addr" name="addr" type="text" class="form-control {{ $flag }}"
                        value="{{ old('addr') }}" @if ($testdata) value = "ที่อยู่." @endif>
                    @error('addr')
                        <div class="alert alert-danger">กรุณาใส่ที่อยู่</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="subcity">ตำบล *</label>
                    <input id="subcity" name="subcity" type="text" class="form-control {{ $flag }}"
                        value="{{ old('subcity') }}" @if ($testdata) value = "ตำบล." @endif>
                    @error('subcity')
                        <div class="alert alert-danger">กรุณาใส่ตำบล</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="city">อำเภอ *</label>
                    <input id="city" name="city" type="text" class="form-control {{ $flag }}"
                        value="{{ old('city') }}" @if ($testdata) value = "อำเภอ." @endif>
                    @error('city')
                        <div class="alert alert-danger">กรุณาใส่อำเภอ</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="province">จังหวัด *</label>
                    <input id="province" name="province" type="text" class="form-control {{ $flag }}"
                        value="{{ old('province') }}" @if ($testdata) value = "จังหวัด." @endif>
                    @error('province')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="zipcode">รหัสไปรษณีย์ *</label>
                    <input id="zipcode" name="zipcode" type="number" maxlength="5" value="{{ old('zipcode') }}"
                        class="form-control {{ $flag }}"
                        @if ($testdata) value = "50000" @endif>
                    @error('zipcode')
                        <div class="alert alert-danger">กรุณาใส่รหัสไปรษณีย์ 5 หลัก</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="tel">โทรศัพท์ *</label>
                    <input id="tel" name="tel" type="number" maxlength="10" class="form-control {{ $flag }}"
                        value="{{ old('tel') }}" @if ($testdata) value = "100000000" @endif>
                    @error('tel')
                        <div class="alert alert-danger">กรุณาใส่เบอร์โทรศัพท์ 10 หลัก</div>
                    @enderror
                </div>
            </div>
            <legend>ภาพถ่าย</legend>

            <div class="row">
                <div class="col-12 col-xl-4">
                    <label for="image">ภาพถ่ายผู้สมัคร รูปถ่ายหน้าตรงเท่านั้น *</label>
                    <label for="image" class="btn btn-success">แนบไฟล์ หรือถ่ายรูป </label>
                    <input id="image" name="image" style="visibility:hidden;" accept="image/*" capture="camera"
                        class=" {{ $flag }} " type="file">
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
