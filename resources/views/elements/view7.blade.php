<h5 class='newh5'>ส่วนที่ 7 : สถานภาพสมรส</h5>
@if (isset($masterdata->marrieddata))
<div class="row"> 
    <div class="col-12"><span class='answer'>{{ $masterdata->marrieddata->status }}</span>  
    @if ($masterdata->marrieddata->status == 'โสด' || $masterdata->marrieddata->status == 'หม้าย (คู่สมรสถึงแก่กรรม)')
        <br/>
    @else
        <b>คู่สมรสชื่อ</b> <span class='answer'>{{ $masterdata->marrieddata->init }} {{ $masterdata->marrieddata->name }}</span> 
        <b>อายุ</b> <span class='answer'>{{ $masterdata->marrieddata->age }}</span> 
        <b>ปี อาชีพ</b> <span class='answer'>{{ $masterdata->marrieddata->career }}</span><br/>
        <b>ที่อยู่ปัจจุบัน</b> <span class='answer'>{{ $masterdata->marrieddata->homeaddr }}</span> 
        <b>โทรศัพท์</b> <span class='answer'>{{ $masterdata->marrieddata->tel }}</span> 
        <b>ที่ตั้งสถานที่ทำงาน</b>  <span class='answer'>{{ $masterdata->marrieddata->companyaddr }}</span> 
        <b>ตำแหน่ง</b> <span class='answer'>{{ $masterdata->marrieddata->job }}</span> 
        <b>เบอร์โทรศัพท์ที่ทำงาน</b>  <span class='answer'>{{ $masterdata->marrieddata->companytel }}</span> <br/>          
    @endif
        <b>ปัจจุบัน</b> <span class='answer'>{{ $masterdata->marrieddata->child_status }}</span>
        @if ($masterdata->marrieddata->child_status == 'มีบุตร')
            <span class='answer'>{{ $masterdata->marrieddata->child_number }}</span> 
            <b>คน เป็นชาย</b> 
            <span class='answer'>{{ $masterdata->marrieddata->boy_no }}</span> <b>คน  อายุ</b> 
            <span class='answer'>{{ $masterdata->marrieddata->boy_age }}</span> <b>ปี หญิง</b> 
            <span class='answer'>{{ $masterdata->marrieddata->daughter_no }}</span> <b> คน  อายุ</b>  
            <span class='answer'>{{ $masterdata->marrieddata->daughter_age }}</span> <b> ปี</b> 
        @else
            
        @endif
    </div>
</div>
@endif