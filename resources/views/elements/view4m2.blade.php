<h5 class='newh5'>ส่วนที่ 4 : ประวัติการทำงาน(เรียงลำดับจากล่าสุดตามลำดับ)</h5>
<table class="table d-print-table mytable">
    <tbody>
        {{-- @if (!empty($masterdata->expmasterdata->title_1)) --}}
            <tr>
                <td class="mycol_b_c" style="width: 5%" rowspan="3">ลำดับ<br />1</td>
                <td class="mycol_b_c" style="width: 12%">ตั้งแต่ ว/ด/ป</td>
                <td class="mycol_b_c" style="width: 12%">ถึง ว/ด/ป</td>
                <td class="mycol_b_c" style="width: 25%">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                <td class="mycol_b_c" style="width: 10%">จังหวัด</td>
                <td class="mycol_b_c" style="width: 10%">โทรศัพท์</td>
                <td class="mycol_b_c" style="width: 26%">ตำแหน่ง/ลักษณะงานที่ทำ</td>
            </tr>
            <tr style="height: 2rem">
                {{-- <td>{{ $masterdata->expmasterdata->start_date_1 ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->start_date_1))
                    @if(date('Y',strtotime($masterdata->expmasterdata->start_date_1))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_1)).(date('Y',strtotime($masterdata->expmasterdata->start_date_1))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_1)).date('Y',strtotime($masterdata->expmasterdata->start_date_1)) }}
                    @endif
                @endif</td>
                {{-- <td>{{ $masterdata->expmasterdata->end_date_1 ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->end_date_1))
                    @if(date('Y',strtotime($masterdata->expmasterdata->end_date_1))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_1)).(date('Y',strtotime($masterdata->expmasterdata->end_date_1))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_1)).date('Y',strtotime($masterdata->expmasterdata->end_date_1)) }}
                    @endif
                @endif</td>
                <td>{{ $masterdata->expmasterdata->company_1 ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->province_1 ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->tel_1 ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->title_1 ?? ''}}</td>
            </tr>
            <tr>
                <td colspan="2"><b>เงินเดือน :</b>@if(!empty($masterdata->expmasterdata->salary_1)){{ number_format(trim($masterdata->expmasterdata->salary_1)) }}@endif</td>
                <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_1 ?? ''}}</td>
            </tr>
        {{-- @endif
        @if (!empty($masterdata->expmasterdata->title_2)) --}}
            <tr>
                <td class="mycol_b_c" rowspan="3">ลำดับ<br />2</td>
                <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
                <td class="mycol_b_c">ถึง ว/ด/ป</td>
                <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                <td class="mycol_b_c">จังหวัด</td>
                <td class="mycol_b_c">โทรศัพท์</td>
                <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
            </tr>
            <tr style="height: 2rem">
                {{-- <td>{{ $masterdata->expmasterdata->start_date_2  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->start_date_2))
                    @if(date('Y',strtotime($masterdata->expmasterdata->start_date_2))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_2)).(date('Y',strtotime($masterdata->expmasterdata->start_date_2))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_2)).date('Y',strtotime($masterdata->expmasterdata->start_date_2)) }}
                    @endif
                @endif</td>
                {{-- <td>{{ $masterdata->expmasterdata->end_date_2  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->end_date_2))
                    @if(date('Y',strtotime($masterdata->expmasterdata->end_date_2))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_2)).(date('Y',strtotime($masterdata->expmasterdata->end_date_2))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_2)).date('Y',strtotime($masterdata->expmasterdata->end_date_2)) }}
                    @endif
                @endif</td>
                <td>{{ $masterdata->expmasterdata->company_2  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->province_2  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->tel_2  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->title_2  ?? ''}}</td>
            </tr>
            <tr>
                <td colspan="2"><b>เงินเดือน :</b>@if(!empty($masterdata->expmasterdata->salary_2)){{ number_format(trim($masterdata->expmasterdata->salary_2)) }}@endif</td>
                <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_2  ?? ''}}</td>
            </tr>
        {{-- @endif --}}
         {{--@if (!empty($masterdata->expmasterdata->title_3)) --}}
            <tr>
                <td class="mycol_b_c" rowspan="3">ลำดับ<br />3</td>
                <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
                <td class="mycol_b_c">ถึง ว/ด/ป</td>
                <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                <td class="mycol_b_c">จังหวัด</td>
                <td class="mycol_b_c">โทรศัพท์</td>
                <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
            </tr>
            <tr style="height: 2rem">
                {{-- <td>{{ $masterdata->expmasterdata->start_date_3  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->start_date_3))
                    @if(date('Y',strtotime($masterdata->expmasterdata->start_date_3))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_3)).(date('Y',strtotime($masterdata->expmasterdata->start_date_3))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_3)).date('Y',strtotime($masterdata->expmasterdata->start_date_3)) }}
                    @endif
                @endif</td>
                {{-- <td>{{ $masterdata->expmasterdata->end_date_3  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->end_date_3))
                    @if(date('Y',strtotime($masterdata->expmasterdata->end_date_3))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_3)).(date('Y',strtotime($masterdata->expmasterdata->end_date_3))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_3)).date('Y',strtotime($masterdata->expmasterdata->end_date_3)) }}
                    @endif
                @endif</td>
                <td>{{ $masterdata->expmasterdata->company_3  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->province_3  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->tel_3  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->title_3  ?? ''}}</td>
            </tr>
            <tr>
                <td colspan="2"><b>เงินเดือน :</b>@if(!empty($masterdata->expmasterdata->salary_3)){{ number_format(trim($masterdata->expmasterdata->salary_3)) }}@endif</td>
                <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_3  ?? ''}}</td>
            </tr>
        {{-- @endif --}}
        {{-- @if (!empty($masterdata->expmasterdata->title_4)) --}}
            <tr>
                <td class="mycol_b_c" rowspan="3">ลำดับ<br />4</td>
                <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
                <td class="mycol_b_c">ถึง ว/ด/ป</td>
                <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                <td class="mycol_b_c">จังหวัด</td>
                <td class="mycol_b_c">โทรศัพท์</td>
                <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
            </tr>
            <tr style="height: 2rem">
                {{-- <td>{{ $masterdata->expmasterdata->start_date_4  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->start_date_4))
                    @if(date('Y',strtotime($masterdata->expmasterdata->start_date_4))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_4)).(date('Y',strtotime($masterdata->expmasterdata->start_date_4))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_4)).date('Y',strtotime($masterdata->expmasterdata->start_date_4)) }}
                    @endif
                @endif</td>
                {{-- <td>{{ $masterdata->expmasterdata->end_date_4  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->end_date_4))
                    @if(date('Y',strtotime($masterdata->expmasterdata->end_date_4))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_4)).(date('Y',strtotime($masterdata->expmasterdata->end_date_4))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_4)).date('Y',strtotime($masterdata->expmasterdata->end_date_4)) }}
                    @endif
                @endif</td>
                <td>{{ $masterdata->expmasterdata->company_4  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->province_4  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->tel_4  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->title_4  ?? ''}}</td>
            </tr>
            <tr>
                <td colspan="2"><b>เงินเดือน :</b>@if(!empty($masterdata->expmasterdata->salary_4)){{ number_format(trim($masterdata->expmasterdata->salary_4)) }}@endif</td>
                <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_4  ?? ''}}</td>
            </tr>
        {{-- @endif
        @if (!empty($masterdata->expmasterdata->title_5)) --}}
            <tr>
                <td class="mycol_b_c" rowspan="3">ลำดับ<br />5</td>
                <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
                <td class="mycol_b_c">ถึง ว/ด/ป</td>
                <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
                <td class="mycol_b_c">จังหวัด</td>
                <td class="mycol_b_c">โทรศัพท์</td>
                <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
            </tr>
            <tr style="height: 2rem">
                {{-- <td>{{ $masterdata->expmasterdata->start_date_5  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->start_date_5))
                    @if(date('Y',strtotime($masterdata->expmasterdata->start_date_5))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_5)).(date('Y',strtotime($masterdata->expmasterdata->start_date_5))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->start_date_5)).date('Y',strtotime($masterdata->expmasterdata->start_date_5)) }}
                    @endif
                @endif</td>
                {{-- <td>{{ $masterdata->expmasterdata->end_date_5  ?? ''}}</td> --}}
                <td>@if (!empty($masterdata->expmasterdata->end_date_5))
                    @if(date('Y',strtotime($masterdata->expmasterdata->end_date_5))<2500)
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_5)).(date('Y',strtotime($masterdata->expmasterdata->end_date_5))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->expmasterdata->end_date_5)).date('Y',strtotime($masterdata->expmasterdata->end_date_5)) }}
                    @endif
                @endif</td>
                <td>{{ $masterdata->expmasterdata->company_5  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->province_5  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->tel_5  ?? ''}}</td>
                <td>{{ $masterdata->expmasterdata->title_5  ?? ''}}</td>
            </tr>
            <tr>
                <td colspan="2"><b>เงินเดือน :</b>@if(!empty($masterdata->expmasterdata->salary_5)){{ number_format(trim($masterdata->expmasterdata->salary_5)) }}@endif</td>
                <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_5  ?? ''}}</td>
            </tr>
        {{-- @endif --}}
    </tbody>
</table>
