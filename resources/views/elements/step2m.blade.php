<h3>ข้อมูลส่วนตัว</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ตำแหน่งงาน</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="job_name1">ตำแหน่ง1 *</label>
                    <input id="job_name1" name="job_name1" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ตำแหน่ง1" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="job_salary1">อัตราเงินเดือน1 *</label>
                    <input id="job_salary1" name="job_salary1" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "10000" @endif>
                </div>

                <div class="form-check  col-xl-2 col-6">
                    <label for="job_name2">ตำแหน่ง2</label>
                    <input id="job_name2" name="job_name2" type="text" class="form-control"
                        @if ($testdata) value = "ตำแหน่ง2" @endif>
                </div>
                <div class="form-check  col-xl-2 col-6">
                    <label for="job_salary2">อัตราเงินเดือน2</label>
                    <input id="job_salary2" name="job_salary2" type="text" class="form-control"
                        @if ($testdata) value = "20000" @endif>
                </div>
            </div>
            <legend>ที่อยู่ปัจจุบัน</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="init_th">คำนำหน้า *</label>
                    <input id="init_th" name="init_th" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "นาย" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="fname_th">ชื่อ *</label>
                    <input id="fname_th" name="fname_th" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ทดสอบ" @endif>
                </div>

                <div class="form-check col-xl-3 col-6">
                    <label for="lname_th">สกุล *</label>
                    <input id="lname_th" name="lname_th" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ลองดู" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="nickname_th">ชื่อเล่น</label>
                    <input id="nickname_th" name="nickname_th" type="text" class="form-control"
                        @if ($testdata) value = "เต๊ด" @endif>
                </div>
            </div>
            <div class="row">
                <div class="form-check col-xl-3 col-6">
                    <label for="birth_date">วันเกิด *</label>
                    <input id="birth_date" name="birth_date" type="date" class="form-control {{ $flag }}"
                        @if ($testdata) value = "2020-08-29" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="weight">น้ำหนัก(กก.)</label>
                    <input id="weight" name="weight" type="text" class="form-control"
                        @if ($testdata) value = "55" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="height">ส่วนสูง(ซม.)</label>
                    <input id="height" name="height" type="text" class="form-control"
                        @if ($testdata) value = "165" @endif>
                </div>
            </div>

            <legend>ที่อยู่ปัจจุบัน</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="addr1">เลขที่ *</label>
                    <input id="addr1" name="addr1" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ที่อยู่." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="addr2">หมู่ที่ *</label>
                    <input id="addr2" name="addr2" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ที่อยู่." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="block">ตรอก/ซอย</label>
                    <input id="block" name="block" type="text" class="form-control"
                        @if ($testdata) value = "ตรอก/ซอย." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="road">ถนน</label>
                    <input id="road" name="road" type="text" class="form-control"
                        @if ($testdata) value = "ที่อยู่." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="subdistrict">ตำบล *</label>
                    <input id="subdistrict" name="subdistrict" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ตำบล." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="district">อำเภอ *</label>
                    <input id="district" name="district" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "อำเภอ." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="province">จังหวัด *</label>
                    <input id="province" name="province" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "จังหวัด." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="zipcode">รหัสไปรษณีย์ *</label>
                    <input id="zipcode" name="zipcode" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "50000" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="tel">โทรศัพท์บ้าน</label>
                    <input id="tel" name="tel" type="text" class="form-control"
                        @if ($testdata) value = "100000000" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="mobile">โทรศัพท์มือถือ *</label>
                    <input id="mobile" name="mobile" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "100000000" @endif>
                </div>
            </div>
            <legend>ในกรณีฉุกเฉิน สามารถติดต่อกับ</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="urgent_init">คำนำหน้า</label>
                    <input id="urgent_init" name="urgent_init" type="text" class="form-control"
                        @if ($testdata) value = "นาย" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="urgent_name">ชื่อ-สกุล</label>
                    <input id="urgent_name" name="urgent_name" type="text" class="form-control"
                        @if ($testdata) value = "ทดลอง" @endif>
                </div>

                <div class="form-check col-xl-3 col-6">
                    <label for="urgent_addr1">ที่อยู่</label>
                    <input id="urgent_addr1" name="urgent_addr1" type="text" class="form-control"
                        @if ($testdata) value = "ดหกหฟกดหกดหกดกหด" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="urgent_mobile">โทรศัพท์</label>
                    <input id="urgent_mobile" name="urgent_mobile" type="text" class="form-control"
                        @if ($testdata) value = "123654987" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="urgent_relation">ความสัมพันธ์เป็น</label>
                    <input id="urgent_relation" name="urgent_relation" type="text"
                        class="form-control"
                        @if ($testdata) value = "น้า" @endif>
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
