@if ( isset($masterdata->simpledata) )
<h5 class='newh5'>ส่วนที่ 9 : ข้อมูลทั่วไป</h5>
                        <div class="row">
                            <div class='col-12'>
                                1.	ปัจจุบันกำลังศึกษาอยู่ในระดับ 
                                <span class='answer'>
                                    @if (!empty($masterdata->simpledata->current_edu_level))
                                        {{ $masterdata->simpledata->current_edu_level  }}
                                    @else
                                        -
                                    @endif
                                </span> สาขาวิชา  <span class='answer'>
                                    @if (!empty($masterdata->simpledata->current_edu_dep))
                                        {{ $masterdata->simpledata->current_edu_dep  }}
                                    @else
                                        -
                                    @endif
                                </span>
                                 ชั้นปีที่  <span class='answer'>
                                     @if (!empty($masterdata->simpledata->current_edu_year))
                                        {{ $masterdata->simpledata->current_edu_year  }}
                                    @else
                                        -
                                    @endif
                                    </span> ชื่อสถานศึกษา  
                                    <span class='answer'>                                        
                                        {{ chekempty($masterdata->simpledata->current_edu_school)  }}
                                    </span> 
                                </div>
                            <div class='col-12'> 2.	มีแผนการศึกษาต่อในระดับ <span class='answer'>{{ chekempty($masterdata->simpledata->future_edu_level)  }}</span> 
                                สาขาวิชา <span class='answer'>{{ chekempty($masterdata->simpledata->future_edu_dep)  }}</span> 
                                ในปี พ.ศ <span class='answer'>{{ chekempty($masterdata->simpledata->future_edu_year)  }}</span> 
                                ชื่อสถานศึกษา <span class='answer'>{{ chekempty($masterdata->simpledata->future_edu_school)  }}</span> 
                            </div>
                            <div class='col-4'>3.	งานอดิเรก    1) <span class='answer'>{{ chekempty($masterdata->simpledata->hobby_1)  }}</span></div>
                            <div class='col-4'>2) <span class='answer'>{{ chekempty($masterdata->simpledata->hobby_2)  }}</span></div>
                            <div class='col-4'>3) <span class='answer'>{{ chekempty($masterdata->simpledata->hobby_3)  }}</span></div>
                            <div class='col-6'>4.	การดื่มสุรา <span class='answer'>{{ $masterdata->simpledata->drunker  }}</span></div>
                            <div class='col-6'>5.	การสูบบุหรี่ <span class='answer'>{{ $masterdata->simpledata->smoker  }}</span>
                            @if ($masterdata->simpledata->smoker == 'สูบทุกวัน')
                                วันละ <span class='answer'>{{ $masterdata->simpledata->smoker_other  }}</span> มวน
                            @endif
                                
                            </div>
                            <div class='col-12'>
                                6.	การต้องคดีอาญาถึงจำคุกหรือต้องคำพิพากษาถึงที่สุดให้จำคุก <span class='answer'>({{ $masterdata->simpledata->criminal  }})</span> 
                                @if ( $masterdata->simpledata->criminal == 'เคย' )
                                    ระบุรายละเอียด <span class='answer'>{{ $masterdata->simpledata->criminal_other  }}</span> 
                                @endif    
                            </div>
                            <div class='col-12'>
                               7.	การถูกปลดออกจากหน้าที่เนื่องจากความประพฤติไม่เหมาะสม / การปฏิบัติงานบกพร่อง  <span class='answer'>({{ $masterdata->simpledata->control  }})</span>  
                                @if ( $masterdata->simpledata->control == 'เคย' )
                                    ระบุรายละเอียด <span class='answer'>{{ $masterdata->simpledata->control_other  }}</span> 
                                @endif    
                            </div>
                            <div class='col-12'>
                               8.	การไปปฏิบัติงานต่างจังหวัด และ ต่างประเทศ  <span class="answer" >({{ $masterdata->simpledata->can_other_country  }})</span>
                                @if ( $masterdata->simpledata->can_other_country == 'ไม่ได้' )
                                    เนื่องจาก <span class="answer" >{{ $masterdata->simpledata->can_other_country_case  }}</span>
                                @endif    
                            </div>
                            <div class='col-12'>
                               9.	การสมัครงานกับบริษัทฯ ครั้งนี้เป็นครั้งที่ <span class="answer" >{{ chekempty($masterdata->simpledata->apply_job_no)  }}</span> 
                               เคยสอบข้อเขียน/สอบสัมภาษณ์กับบริษัทฯ ในตำแหน่ง <span class="answer" >{{ chekempty($masterdata->simpledata->apply_job_name)  }}</span>  
                               เหตุผลที่สมัครเข้าทำงานกับบริษัทฯ <span class="answer" >{{ chekempty($masterdata->simpledata->apply_job_company)  }}</span>
                                @if ( $masterdata->simpledata->can_other_country == 'ไม่ได้' )
                                    เนื่องจาก <span class="answer" >{{ $masterdata->simpledata->can_other_country_case  }}</span>
                                @endif    
                            </div>
                            <div class='col-12'>
                            10.	ทราบข่าวการรับสมัครงาน   <span class="answer" >{{ $masterdata->simpledata->hear_from  }}</span> 
                            @if ($masterdata->simpledata->hear_from == 'สนง.จัดหางาน จังหวัด')
                                 จังหวัด <span class="answer" >{{ $masterdata->simpledata->hear_from_other  }}</span>
                            @endif
                            @if ($masterdata->simpledata->hear_from == 'สื่ออื่นๆ')
                                 ระบุ) <span class="answer" >{{ $masterdata->simpledata->hear_from_other  }}</span>
                            @endif
                            </div>
                            <div class='col-12'>
                               11.	บุคคลที่แนะนำให้มาสมัครงาน หรือ ญาติพี่น้อง เพื่อน หรือคนรู้จัก ที่เป็นพนักงานของบริษัทฯ (ถ้ามี) <span class="answer" >{{ chekempty($masterdata->simpledata->refer_name)  }}</span>  
                            </div>
                            <div class='col-12'>
                               12.	ความรู้ความเข้าใจที่มีต่อ บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด มีดังนี้ <span class="answer" >{{ $masterdata->simpledata->your_comments  }}</span>  
                            </div>
                            <div class='col-12'>
                               13.	หากบริษัทฯ รับเข้าทำงาน สามารถเริ่มงานได้ภายใน <span class="answer" >{{ $masterdata->simpledata->can_start_in  }}</span> วัน เนื่องจาก <span class="answer" >{{ $masterdata->simpledata->can_start_desc  }}</span>  
                            </div>
                        </div>
@endif                        