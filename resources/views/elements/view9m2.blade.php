@if (isset($masterdata->simpledata))
    <h5 class='newh5'>ส่วนที่ 9 : ข้อมูลทั่วไป</h5>
    <div class="row">
        <div class='col-12'>
            1. ปัจจุบันกำลังศึกษาอยู่ในระดับ
            <span class='answer'>
                {{ filldot($masterdata->simpledata->current_edu_level,20) }}
            </span> สาขาวิชา <span class='answer'>
                    {{ filldot($masterdata->simpledata->current_edu_dep,20) }}
            </span>
            ชั้นปีที่ <span class='answer'>
                    {{ filldot($masterdata->simpledata->current_edu_year,20) }}
            </span> ชื่อสถานศึกษา
            <span class='answer'>
                {{ filldot(chekempty($masterdata->simpledata->current_edu_school),20) }}
            </span>
        </div>
        <div class='col-12'> 2. มีแผนการศึกษาต่อในระดับ <span
                class='answer'>{{ filldot(chekempty($masterdata->simpledata->future_edu_level),20) }}</span>
            สาขาวิชา <span class='answer'>{{ filldot(chekempty($masterdata->simpledata->future_edu_dep),20) }}</span>
            ในปี พ.ศ <span class='answer'>{{ filldot(chekempty($masterdata->simpledata->future_edu_year),20) }}</span>
            ชื่อสถานศึกษา <span class='answer'>{{ filldot(chekempty($masterdata->simpledata->future_edu_school),20) }}</span>
        </div>
        <div class='col-4'>3. งานอดิเรก 1) <span
                class='answer'>{{ filldot(chekempty($masterdata->simpledata->hobby_1),20) }}</span></div>
        <div class='col-4'>2) <span class='answer'>{{ filldot(chekempty($masterdata->simpledata->hobby_2),20) }}</span></div>
        <div class='col-4'>3) <span class='answer'>{{ filldot(chekempty($masterdata->simpledata->hobby_3),20) }}</span></div>
        <div class='col-6'>4. การดื่มสุรา <span class='answer'>{{ filldot($masterdata->simpledata->drunker,50) }}</span></div>
        <div class='col-6'>5. การสูบบุหรี่ <span class='answer'>{{ filldot($masterdata->simpledata->smoker,50) }}</span>
            @if ($masterdata->simpledata->smoker == 'สูบทุกวัน')
                วันละ <span class='answer'>{{ filldot($masterdata->simpledata->smoker_other,20) }}</span> มวน
            @endif

        </div>
        <div class='col-12'>
            6. การต้องคดีอาญาถึงจำคุกหรือต้องคำพิพากษาถึงที่สุดให้จำคุก <span
                class='answer'>({{ filldot($masterdata->simpledata->criminal,20) }})</span>
            @if ($masterdata->simpledata->criminal == 'เคย')
                ระบุรายละเอียด <span class='answer'>{{ filldot($masterdata->simpledata->criminal_other,200) }}</span>
            @endif
        </div>
        <div class='col-12'>
            7. การถูกปลดออกจากหน้าที่เนื่องจากความประพฤติไม่เหมาะสม / การปฏิบัติงานบกพร่อง <span
                class='answer'>({{ filldot($masterdata->simpledata->control,20) }})</span>
            @if ($masterdata->simpledata->control == 'เคย')
                ระบุรายละเอียด <span class='answer'>{{ filldot($masterdata->simpledata->control_other,200) }}</span>
            @endif
        </div>
        <div class='col-12'>
            8. การไปปฏิบัติงานต่างจังหวัด และ ต่างประเทศ <span
                class="answer">({{ filldot($masterdata->simpledata->can_other_country,20) }})</span>
            @if ($masterdata->simpledata->can_other_country == 'ไม่ได้')
                เนื่องจาก <span class="answer">{{ filldot($masterdata->simpledata->can_other_country_case,200) }} }}</span>
            @endif
        </div>
        <div class='col-12'>
            9. การสมัครงานกับบริษัทฯ ครั้งนี้เป็นครั้งที่ <span
                class="answer">{{ filldot(chekempty($masterdata->simpledata->apply_job_no),20) }}</span>
            เคยสอบข้อเขียน/สอบสัมภาษณ์กับบริษัทฯ ในตำแหน่ง <span
                class="answer">{{ filldot(chekempty($masterdata->simpledata->apply_job_name),30) }}</span>
            เหตุผลที่สมัครเข้าทำงานกับบริษัทฯ <span
                class="answer">{{ filldot(chekempty($masterdata->simpledata->apply_job_company),30) }}</span>
            @if ($masterdata->simpledata->can_other_country == 'ไม่ได้')
                เนื่องจาก <span class="answer">{{ filldot($masterdata->simpledata->can_other_country_case,200) }}</span>
            @endif
        </div>
        <div class='col-12'>
            10. ทราบข่าวการรับสมัครงาน <span class="answer">{{ filldot($masterdata->simpledata->hear_from,50) }}</span>
            @if ($masterdata->simpledata->hear_from == 'สนง.จัดหางาน จังหวัด')
                จังหวัด <span class="answer">{{ filldot($masterdata->simpledata->hear_from_other,50) }}</span>
            @endif
            @if ($masterdata->simpledata->hear_from == 'สื่ออื่นๆ')
                ระบุ) <span class="answer">{{ filldot($masterdata->simpledata->hear_from_other,200) }}</span>
            @endif
        </div>
        <div class='col-12'>
            11. บุคคลที่แนะนำให้มาสมัครงาน หรือ ญาติพี่น้อง เพื่อน หรือคนรู้จัก ที่เป็นพนักงานของบริษัทฯ (ถ้ามี) <span
                class="answer">{{ filldot(chekempty($masterdata->simpledata->refer_name),50) }}</span>
        </div>
        <div class='col-12'>
            12. ความรู้ความเข้าใจที่มีต่อ บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด มีดังนี้ <span
                class="answer">{{ filldot($masterdata->simpledata->your_comments,50) }}</span>
        </div>
        <div class='col-12'>
            13. หากบริษัทฯ รับเข้าทำงาน สามารถเริ่มงานได้ภายใน <span
                class="answer">{{ filldot($masterdata->simpledata->can_start_in,20) }}</span> วัน เนื่องจาก <span
                class="answer">{{ filldot($masterdata->simpledata->can_start_desc,50) }}</span>
        </div>
    </div>
@endif
