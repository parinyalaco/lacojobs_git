<h5 class='newh5'>ส่วนที่ 6 : ความสามารถพิเศษ</h5>
@if (isset($masterdata->expdetaildata))
<div class="row">
    <div class="col-3"><b>1. พิมพดีด ภาษาไทย</b> <span class="answer" >
    @if (!empty($masterdata->expdetaildata->expd_type_th))
        {{ $masterdata->expdetaildata->expd_type_th }}
    @else
        -
    @endif
    </span> <b>คำ/นาที</b></div>
    <div class="col-3"><b>ภาษาอังกฤษ</b> <span class="answer" >
    @if (!empty($masterdata->expdetaildata->expd_type_en))
        {{ $masterdata->expdetaildata->expd_type_en }}
    @else
        -
    @endif
    </span> 
    <b>คำ/นาที</b></div>
    <div class="col-3"><b>2.ชวเลข</b> <span class="answer" >
    @if (!empty($masterdata->expdetaildata->expd_type_note))
        {{ $masterdata->expdetaildata->expd_type_note }}
    @else
        -
    @endif</span> <b>คำ/นาที</b></div>
    <div class="col-3"></div>
    <div class="col-12"><b>3.  คอมพิวเตอร์ โปรแกรม :-</b></div>
    @if (!empty($masterdata->expdetaildata->expd_com_program1))
        <div class="col-3"><b>3.1 {{ $masterdata->expdetaildata->expd_com_program1 }}</b> <span class="answer" >({{ $masterdata->expdetaildata->expd_com_level1 }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program2))
        <div class="col-3"><b>3.2 {{ $masterdata->expdetaildata->expd_com_program2 }}</b> <span class="answer" >({{ $masterdata->expdetaildata->expd_com_level2 }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program3))
        <div class="col-3"><b>3.3 {{ $masterdata->expdetaildata->expd_com_program3 }}</b> <span class="answer" >({{ $masterdata->expdetaildata->expd_com_level3 }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program4))
        <div class="col-3"><b>3.4 {{ $masterdata->expdetaildata->expd_com_program4 }}</b> <span class="answer" >({{ $masterdata->expdetaildata->expd_com_level4 }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program5))
        <div class="col-3"><b>3.5 {{ $masterdata->expdetaildata->expd_com_program5 }}</b> <span class="answer" >({{ $masterdata->expdetaildata->expd_com_level5 }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program6))
        <div class="col-3"><b>3.6 {{ $masterdata->expdetaildata->expd_com_program6 }}</b> <span class="answer" >({{ $masterdata->expdetaildata->expd_com_level6 }})</span></div>
    @endif
    
    <div class="col-12"><b>4. ความสามารถด้านภาษา</b></div>
    <div class="col-6"><b>4.1 ภาษาไทย</b> 
        การพูด <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_speak_th }})</span> / 
        การอ่าน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_read_th }})</span>  / 
        การเขียน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_write_th }})</span>
    </div>
    <div class="col-6"><b>4.2 ภาษาอังกฤษ</b> 
        การพูด <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_speak_en }})</span> / 
        การอ่าน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_read_en }})</span>  / 
        การเขียน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_write_en }})</span>
    </div>
    <div class="col-6"><b>4.3 ภาษาจีน</b> 
        การพูด <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_speak_ch }})</span> / 
        การอ่าน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_read_ch }})</span>  / 
        การเขียน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_write_ch }})</span>
    </div>
    <div class="col-6"><b>4.1 ภาษาอื่นๆ {{ $masterdata->expdetaildata->expd_lang_ex }}</b>  
        การพูด <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_speak_ex }})</span> / 
        การอ่าน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_read_ex }})</span>  / 
        การเขียน <span class="answer" >({{ $masterdata->expdetaildata->expd_lang_write_ex }})</span>
    </div>
    <div class="col-12"><b>5. การขับขี่ยานพาหนะ</b></div>
    <div class="col-12"><b>ขับขี่รถยนต์</b> <span class='answer'>({{ $masterdata->expdetaildata->expd_can_drive }}) </span>
    @if ($masterdata->expdetaildata->expd_have_drive_licen == 'มี')
        <span class='answer'>{{ $masterdata->expdetaildata->expd_have_drive_licen }}ใบอนุญาตขับรถ</span>  ชนิด <span class='answer'>{{ $masterdata->expdetaildata->expd_drive_licen_type }}</span>  วันอนุญาต <span class='answer'>{{ tothaiyear($masterdata->expdetaildata->expd_drive_licen_date) }}</span>  วันหมดอายุ <span class='answer'>{{ tothaiyear($masterdata->expdetaildata->expd_drive_licen_exp) }}</span> 
    @else
        <span class='answer'>{{ $masterdata->expdetaildata->expd_have_drive_licen }}ใบอนุญาตขับรถ</span> 
    @endif
    <b>รถยนต์ที่ใช้ปัจจุบันยี่ห้อ</b> <span class='answer'>
    @if (!empty($masterdata->expdetaildata->expd_my_car_brand))
        {{ $masterdata->expdetaildata->expd_my_car_brand }}
    @else
        -
    @endif 
    </span>
        <b>รุ่น</b> <span class='answer'>
    @if (!empty($masterdata->expdetaildata->expd_my_car_serie))
        {{ $masterdata->expdetaildata->expd_my_car_serie }}
    @else
        -
    @endif 
        </span>
    </div>
    <div class="col-12"><b>ขับขี่รถจักรยานยนต์</b>  <span class='answer'>({{ $masterdata->expdetaildata->expd_can_ride }})</span> 
    @if ($masterdata->expdetaildata->expd_have_ride_licen == 'มี')
        <span class='answer'>{{ $masterdata->expdetaildata->expd_have_ride_licen }}ใบอนุญาตขับรถ</span> 
        ชนิด <span class='answer'>{{ $masterdata->expdetaildata->expd_ride_licen_type }}</span>  
            วันอนุญาต <span class='answer'>{{ tothaiyear($masterdata->expdetaildata->expd_ride_licen_date) }}</span>  
            วันหมดอายุ <span class='answer'>{{ tothaiyear($masterdata->expdetaildata->expd_ride_licen_exp) }}</span> 
    @else
        <span class='answer'>{{ $masterdata->expdetaildata->expd_have_ride_licen }}ใบอนุญาตขับรถ</span> 
    @endif
    <b>รถจักรยานยนต์ที่ใช้ปัจจุบันยี่ห้อ</b> 
    <span class='answer'>
        @if (!empty($masterdata->expdetaildata->expd_my_bike_brand))
            {{ $masterdata->expdetaildata->expd_my_bike_brand }}
        @else
            -
        @endif
    </span> 
    <b>รุ่น</b> <span class='answer'>
        @if (!empty($masterdata->expdetaildata->expd_my_bike_serie))
            {{ $masterdata->expdetaildata->expd_my_bike_serie }}
        @else
            -
        @endif
    </span> 
    </div>
</div>
@endif