<h5 class='newh5'>ส่วนที่ 4 : ประวัติการเข้ารับการฝึกอบรมพิเศษ</h5>
<table class="table d-print-table mytable">
    <tbody>
        <tr>
            <td class="mycol_b_c" rowspan="2">ลำดับ</td>
            <td class="mycol_b_c" colspan="2">ระยะเวลาตั้งแต่ - ถึง</td>
            <td class="mycol_b_c">รวมทั้งสิ้น</td>
            <td class="mycol_b_c" rowspan="2">หลักสูตร / หัวข้อที่เข้ารับการอบรม</td>
            <td class="mycol_b_c" rowspan="2">ชื่อหน่วยงานที่จัดฝึกอบรม</td>
        </tr>
        <tr>
            <td class="mycol_b_c">วัน/เดือน/ปี</td>
            <td class="mycol_b_c">วัน/เดือน/ปี</td>
            <td class="mycol_b_c">วัน / ชั่วโมง</td>
        </tr>
        @if (!empty($masterdata->extrainingdata->title_1))
            <tr>
                <td class="mycol_b_c">1</td>
                <td>{{ $masterdata->extrainingdata->start_date_1 }}</td>
                <td>{{ $masterdata->extrainingdata->end_date_1 }}</td>
                <td>{{ $masterdata->extrainingdata->duration_1 }}</td>
                <td>{{ $masterdata->extrainingdata->title_1 }}</td>
                <td>{{ $masterdata->extrainingdata->by_1 }}</td>
            </tr>
        @endif
        @if (!empty($masterdata->extrainingdata->title_2))
            <tr>
                <td class="mycol_b_c">2</td>
                <td>{{ $masterdata->extrainingdata->start_date_2 }}</td>
                <td>{{ $masterdata->extrainingdata->end_date_2 }}</td>
                <td>{{ $masterdata->extrainingdata->duration_2 }}</td>
                <td>{{ $masterdata->extrainingdata->title_2 }}</td>
                <td>{{ $masterdata->extrainingdata->by_2 }}</td>
            </tr>
        @endif
    </tbody>
</table>
