<h3>สุขภาพ</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>สุขภาพและความพร้อมทางร่างกาย</legend>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="congenital_disease">โรคประจำตัว</label>
                    <select class="form-control {{ $flag }} " id="congenital_disease" name="congenital_disease">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('congenital_disease') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น" @if (old('congenital_disease') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>
                            เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2 congenital_disease d-none">
                    <label for="congenital_disease_other">ระบุชื่อโรคประจำตัว</label>
                    <input id="congenital_disease_other" name="congenital_disease_other" type="text"
                        placeholder="ระบุโรคประจำตัว" class="form-control"
                        value="{{ old('congenital_disease_other') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="contagious_disease">โรคติดต่อ/โรคเรื้อรัง</label>
                    <select class="form-control {{ $flag }} " id="contagious_disease" name="contagious_disease">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('contagious_disease') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น" @if (old('contagious_disease') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>
                            เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2 contagious_disease d-none">
                    <label for="contagious_disease_other">ระบุชื่อโรคติดต่อ/โรคเรื้อรัง</label>
                    <input id="contagious_disease_other" name="contagious_disease_other" type="text"
                        placeholder="ระบุชื่อโรคติดต่อ/โรคเรื้อรัง" class="form-control"
                        value="{{ old('contagious_disease_other') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="allergy">โรคภูมิแพ้</label>
                    <select class="form-control {{ $flag }} " id="allergy" name="allergy">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('allergy') == 'ไม่ได้') {{ 'ไม่เคยเป็น' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น"
                            @if (old('allergy') == 'ไม่ได้') {{ 'เคยเป็น/กำลังเป็น' }} @endif>เคยเป็น/กำลังเป็น
                        </option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2 allergy d-none">
                    <label for="allergy_other">ระบุชื่อโรคภูมิแพ้</label>
                    <input id="allergy_other" name="allergy_other" type="text" placeholder="ระบุชื่อโรคภูมิแพ้"
                        class="form-control" value="{{ old('allergy_other') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="back_pain">อาการปวดหลัง ปวดกล้ามเนื้อ</label>
                    <select class="form-control {{ $flag }} " id="back_pain" name="back_pain">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('back_pain') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น" @if (old('back_pain') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>
                            เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="finger_pain">อาการชาตามกล้ามเนื้อ นิ้ว มือ</label>
                    <select class="form-control {{ $flag }} " id="finger_pain" name="finger_pain">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('finger_pain') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น" @if (old('finger_pain') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>
                            เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="thalassemia">โรคธาลัสซีเมีย</label>
                    <select class="form-control {{ $flag }} " id="thalassemia" name="thalassemia">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('thalassemia') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น"
                            @if (old('thalassemia') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="liver_virus">ไวรัสตับอักเสบ A B C</label>
                    <select class="form-control {{ $flag }} " id="liver_virus" name="liver_virus">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('liver_virus') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น"
                            @if (old('liver_virus') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="food_allergy">แพ้อาหาร</label>
                    <select class="form-control {{ $flag }} " id="food_allergy" name="food_allergy">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('food_allergy') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น"
                            @if (old('food_allergy') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2 food_allergy d-none">
                    <label for="food_allergy_other">ระบุชื่ออาหารที่แพ้</label>
                    <input id="food_allergy_other" name="food_allergy_other" type="text"
                        placeholder="ระบุชื่ออาหารที่แพ้" class="form-control"
                        value="{{ old('food_allergy_other') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="drug_allergy">แพ้ยา</label>
                    <select class="form-control {{ $flag }} " id="drug_allergy" name="drug_allergy">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่เคยเป็น" @if (old('drug_allergy') == 'ไม่เคยเป็น') {{ 'selected' }} @endif>
                            ไม่เคยเป็น</option>
                        <option value="เคยเป็น/กำลังเป็น"
                            @if (old('drug_allergy') == 'เคยเป็น/กำลังเป็น') {{ 'selected' }} @endif>เคยเป็น/กำลังเป็น</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2 drug_allergy d-none">
                    <label for="drug_allergy_other">ระบุชื่อยาที่แพ้</label>
                    <input id="drug_allergy_other" name="drug_allergy_other" type="text"
                        placeholder="ระบุชื่อยาที่แพ้" class="form-control"
                        value="{{ old('drug_allergy_other') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="other_disease">โรค/อาการอื่นๆ</label>
                    <input id="other_disease" name="other_disease" type="text" placeholder="ระบุโรค/อาการอื่นๆ"
                        class="form-control" value="{{ old('other_disease') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="physical_exam_time">ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ</label>
                    <input id="physical_exam_time" name="physical_exam_time" type="date"
                        placeholder="ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ" class="form-control"
                        value="{{ old('physical_exam_time') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="physical_exam_part">ส่วนของร่างกายที่ได้รับการตรวจ</label>
                    <input id="physical_exam_part" name="physical_exam_part" type="text"
                        placeholder="ส่วนของร่างกายที่ได้รับการตรวจ" class="form-control"
                        value="{{ old('physical_exam_part') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="physical_exam_result">ผลการตรวจ</label>
                    <select class="form-control {{ $flag }} " id="physical_exam_result"
                        name="physical_exam_result">
                        <option value="">=== เลือก ===</option>
                        <option value="ปรกติ" @if (old('physical_exam_result') == 'ปรกติ') {{ 'selected' }} @endif>ปรกติ
                        </option>
                        <option value="ไม่ปรกติ" @if (old('physical_exam_result') == 'ไม่ปรกติ') {{ 'selected' }} @endif>
                            ไม่ปรกติ</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2 physical_exam_result d-none">
                    <label for="physical_exam_result_other">ระบุรายละเอียด</label>
                    <input id="physical_exam_result_other" name="physical_exam_result_other" type="text"
                        placeholder="ระบุรายละเอียด" class="form-control"
                        value="{{ old('physical_exam_result_other') }}">
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="prev_social_sec_company">ชื่อสถานที่ทำงานก่อนหน้านี้ซึ่งมีการทำประกันสังคม</label>
                    <input id="prev_social_sec_company" name="prev_social_sec_company" type="text"
                        placeholder="ชื่อสถานที่ทำงานก่อนหน้านี้ซึ่งมีการทำประกันสังคม" class="form-control"
                        value="{{ old('prev_social_sec_company') }}">
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="prev_social_sec_hospital">โรงพยาบาลที่ใช้สิทธิรักษาพยาบาล</label>
                    <input id="prev_social_sec_hospital" name="prev_social_sec_hospital" type="text"
                        placeholder="โรงพยาบาลที่ใช้สิทธิรักษาพยาบาล" class="form-control"
                        value="{{ old('prev_social_sec_hospital') }}">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <legend>บุคคลอ้างอิงซึ่งสามารถรับรองความประพฤติ/ประวัติการทำงานได้ (ไม่ใช่ญาติพี่น้องหรือเพื่อน)
                    </legend>
                    @for ($i = 1; $i <= 2; $i++)
                        <div class="row">
                            <div class="form-check col-6 col-xl-1">
                                <b>ลำดับ {{ $i }}</b>
                            </div>
                            <div class="form-check col-6 col-xl-2">
                                <label for="ref_name_{{ $i }}">ชื่อ-นามสกุล</label>
                                <input id="ref_name_{{ $i }}" name="ref_name_{{ $i }}"
                                    type="text" placeholder="ระบุชื่อ-สกุล" class="form-control"
                                    value="{{ old('ref_name_' . $i) }}">
                            </div>
                            <div class="form-check col-6 col-xl-2">
                                <label for="ref_relation_{{ $i }}">มีความสัมพันธ์เป็น</label>
                                <input id="ref_relation_{{ $i }}" name="ref_relation_{{ $i }}"
                                    type="text" placeholder="ระบุมีความสัมพันธ์เป็น" class="form-control"
                                    value="{{ old('ref_relation_' . $i) }}">
                            </div>
                            <div class="form-check col-6 col-xl-2">
                                <label for="ref_company_job_{{ $i }}">ชื่อสถานที่ทำงาน/ตำแหน่ง</label>
                                <input id="ref_company_job_{{ $i }}"
                                    name="ref_company_job_{{ $i }}" type="text"
                                    placeholder="ระบุชื่อสถานที่ทำงาน/ตำแหน่ง" class="form-control"
                                    value="{{ old('ref_company_job_' . $i) }}">
                            </div>
                            <div class="form-check col-6 col-xl-2">
                                <label for="ref_tel_{{ $i }}">โทรศัพท์</label>
                                <input id="ref_tel_{{ $i }}" name="ref_tel_{{ $i }}"
                                    type="text" placeholder="โทรศัพท์" class="form-control"
                                    value="{{ old('ref_tel_' . $i) }}">
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
        <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
    </div>
</fieldset>
