<span><b>ที่อยู่ปัจจุบัน</b></span><br/>
                    <span class='answer'>{{ $masterdata->addr1 }} </span>
                    ตำบล <span class='answer'>{{ $masterdata->subdistrict }}</span> 
                    อำเภอ <span class='answer'>{{ $masterdata->district }}</span> 
                    จังหวัด <span class='answer'>{{ $masterdata->province }}</span> 
                    รหัสไปรษณีย์ <span class='answer'>{{ $masterdata->zipcode }}</span> 
                    โทรศัพท์บ้าน <span class='answer'>{{ $masterdata->tel }}</span> 
                    โทรศัพท์มือถือ <span class='answer'>{{ $masterdata->mobile }}</span><br/>
                    <span><b>ซึ่งที่อยู่ปัจจุบันนี้เป็น</b></span> : <span class='answer'>{{ $masterdata->addr_type }}</span>
                    @if ($masterdata->addr_type == 'บ้านของญาติ')
                        {{ $masterdata->addr_type_custom1 }}
                    @endif
                    @if ($masterdata->addr_type == 'หอพัก')
                        ชื่อ {{ $masterdata->addr_type_custom1 }} หมายเลขห้อง {{ $masterdata->addr_type_custom1 }}
                    @endif
                    <br/>
                    <b>บัตรประชาชน เลขที่</b> <span class='answer'>{{ $masterdata->citizenid }}</span> 
                    <b>วันออกบัตร</b> <span class='answer'>{{ tothaiyear($masterdata->citizenstartdate) }}</span> 
                    <b>วันหมดอายุบัตร</b> <span class='answer'>{{ tothaiyear($masterdata->citizenexpdate) }}</span> <br/>
                    <span style="text-decoration: underline" ><b>ในกรณีฉุกเฉิน สามารถติดต่อกับ</b></span><br/>
                    <span class='answer'>{{ $masterdata->urgent_init }} {{ $masterdata->urgent_name }}</span> 
                    <b>ที่อยู่</b> <span class='answer'>{{ $masterdata->urgent_addr1 }}</span> 
                    <b>โทรศัพท์</b> <span class='answer'>{{ $masterdata->urgent_mobile }}</span> 
                    <b>ความสัมพันธ์เป็น</b> <span class='answer'>{{ $masterdata->urgent_relation }}</span> 