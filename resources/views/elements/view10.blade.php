
<h5 class='newh5'>ส่วนที่ 10 : สมาชิกในครอบครัว</h5>
@if (isset($masterdata->familydata))
<div class="row">
    <div class='col-12'>
        บิดา ชื่อ <span class="answer" >{{ $masterdata->familydata->father_name  }}</span> 
        อายุ <span class="answer" >{{ $masterdata->familydata->father_age  }}</span> ปี 
        อาชีพ <span class="answer" >{{ $masterdata->familydata->father_occupation  }}</span>
        สถานที่ทำงาน <span class="answer" >{{ $masterdata->familydata->father_company  }}</span> 
        ตำแหน่ง <span class="answer" >{{ $masterdata->familydata->father_job  }}</span> 
        โทรศัพท์ <span class="answer" >{{ $masterdata->familydata->father_company_tel  }}</span><br/>
        ที่อยู่ปัจจุบัน <span class="answer" >{{ $masterdata->familydata->father_addr  }}</span> 
        โทรศัพท์ <span class="answer" >{{ $masterdata->familydata->father_tel  }}</span><br/>


        มารดา ชื่อ <span class="answer" >{{ $masterdata->familydata->mother_name  }}</span> 
        อายุ <span class="answer" >{{ $masterdata->familydata->mother_age  }}</span> ปี 
        อาชีพ <span class="answer" >{{ $masterdata->familydata->mother_occupation  }}</span>
        สถานที่ทำงาน <span class="answer" >{{ $masterdata->familydata->mother_company  }}</span> 
        ตำแหน่ง <span class="answer" >{{ $masterdata->familydata->mother_job  }}</span> 
        โทรศัพท์ <span class="answer" >{{ $masterdata->familydata->mother_company_tel  }}</span><br/>
        ที่อยู่ปัจจุบัน <span class="answer" >{{ $masterdata->familydata->mother_addr  }}</span> 
        โทรศัพท์ <span class="answer" >{{ $masterdata->familydata->mother_tel  }}</span><br/>
        มีพี่น้อง(รวมผู้สมัครด้วย)ทั้งสิ้น <span class="answer" >{{ $masterdata->familydata->total_nabor  }}</span> คน 
        เป็นชาย <span class="answer" >{{ $masterdata->familydata->total_brother  }}</span> คน หญิง <span class="answer" >{{ $masterdata->familydata->total_sisther  }}</span> คน 
        เรียงลำดับตามอายุจากมากไปหาน้อย (รวมผู้สมัครด้วย) ดังนี้ :-
    </div>
    <div class='col-12'>
        <table class="table d-print-table mytable" >
            <tbody>
                <tr>
                    <td class="mycol_b_c" >ชื่อ-นามสกุล</td>
                    <td class="mycol_b_c" >อายุ(ปี)</td>
                    <td class="mycol_b_c" >ชื่อสถานที่ทำงาน/สถานศึกษา</td>
                    <td class="mycol_b_c" >ตำแหน่ง/ชั้นปีที่กำลังศึกษา</td>
                </tr>
                @if (!empty( $masterdata->familydata->name_1))
                    <tr>
                        <td>1. {{ $masterdata->familydata->name_1 }}</td>
                        <td>{{ $masterdata->familydata->age_1 }}</td>
                        <td>{{ $masterdata->familydata->company_1 }}</td>
                        <td>{{ $masterdata->familydata->level_1 }}</td>
                    </tr>
                @endif
                    @if (!empty( $masterdata->familydata->name_2))
                    <tr>
                        <td>2. {{ $masterdata->familydata->name_2 }}</td>
                        <td>{{ $masterdata->familydata->age_2 }}</td>
                        <td>{{ $masterdata->familydata->company_2 }}</td>
                        <td>{{ $masterdata->familydata->level_2 }}</td>
                    </tr>
                @endif
                    @if (!empty( $masterdata->familydata->name_3))
                    <tr>
                        <td>3. {{ $masterdata->familydata->name_3 }}</td>
                        <td>{{ $masterdata->familydata->age_3 }}</td>
                        <td>{{ $masterdata->familydata->company_3 }}</td>
                        <td>{{ $masterdata->familydata->level_3 }}</td>
                    </tr>
                @endif
                    @if (!empty( $masterdata->familydata->name_4))
                    <tr>
                        <td>4. {{ $masterdata->familydata->name_4 }}</td>
                        <td>{{ $masterdata->familydata->age_4 }}</td>
                        <td>{{ $masterdata->familydata->company_4 }}</td>
                        <td>{{ $masterdata->familydata->level_4 }}</td>
                    </tr>
                @endif
                    @if (!empty( $masterdata->familydata->name_5))
                    <tr>
                        <td>5. {{ $masterdata->familydata->name_5 }}</td>
                        <td>{{ $masterdata->familydata->age_5 }}</td>
                        <td>{{ $masterdata->familydata->company_5 }}</td>
                        <td>{{ $masterdata->familydata->level_5 }}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@endif