<h5 class='newh5'>ส่วนที่ 4 : ประวัติการทำงาน(จากปัจจุบันตามลำดับ)</h5>
<table class="table d-print-table mytable" >
    <tbody>
        @if (!empty($masterdata->expmasterdata->title_1 ))
        <tr>
            <td  class="mycol_b_c" rowspan="3">ลำดับ<br/>1</td>
            <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
            <td class="mycol_b_c">ถึง ว/ด/ป</td>
            <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
            <td class="mycol_b_c">จังหวัด</td>
            <td class="mycol_b_c">โทรศัพท์</td>
            <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
        </tr>
        <tr>
            <td>{{ $masterdata->expmasterdata->start_date_1 }}</td>
            <td>{{ $masterdata->expmasterdata->end_date_1 }}</td>
            <td>{{ $masterdata->expmasterdata->company_1 }}</td>
            <td>{{ $masterdata->expmasterdata->province_1 }}</td>
            <td>{{ $masterdata->expmasterdata->tel_1 }}</td>
            <td>{{ $masterdata->expmasterdata->title_1 }}</td>
        </tr>
        <tr>
            <td colspan="2"><b>เงินเดือน :</b> {{ $masterdata->expmasterdata->salary_1 }}</td>
            <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_1 }}</td>
        </tr>
        @endif
        @if (!empty($masterdata->expmasterdata->title_2 ))
        <tr>
            <td class="mycol_b_c" rowspan="3">ลำดับ<br/>2</td>
            <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
            <td class="mycol_b_c">ถึง ว/ด/ป</td>
            <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
            <td class="mycol_b_c">จังหวัด</td>
            <td class="mycol_b_c">โทรศัพท์</td>
            <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
        </tr>
        <tr>
            <td>{{ $masterdata->expmasterdata->start_date_2 }}</td>
            <td>{{ $masterdata->expmasterdata->end_date_2 }}</td>
            <td>{{ $masterdata->expmasterdata->company_2 }}</td>
            <td>{{ $masterdata->expmasterdata->province_2 }}</td>
            <td>{{ $masterdata->expmasterdata->tel_2 }}</td>
            <td>{{ $masterdata->expmasterdata->title_2 }}</td>
        </tr>
        <tr>
            <td colspan="2"><b>เงินเดือน :</b> {{ $masterdata->expmasterdata->salary_2 }}</td>
            <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_2 }}</td>
        </tr>
        @endif
        @if (!empty($masterdata->expmasterdata->title_3 ))
        <tr>
            <td  class="mycol_b_c" rowspan="3">ลำดับ<br/>3</td>
            <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
            <td class="mycol_b_c">ถึง ว/ด/ป</td>
            <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
            <td class="mycol_b_c">จังหวัด</td>
            <td class="mycol_b_c">โทรศัพท์</td>
            <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
        </tr>
        <tr>
            <td>{{ $masterdata->expmasterdata->start_date_3 }}</td>
            <td>{{ $masterdata->expmasterdata->end_date_3 }}</td>
            <td>{{ $masterdata->expmasterdata->company_3 }}</td>
            <td>{{ $masterdata->expmasterdata->province_3 }}</td>
            <td>{{ $masterdata->expmasterdata->tel_3 }}</td>
            <td>{{ $masterdata->expmasterdata->title_3 }}</td>
        </tr>
        <tr>
            <td colspan="2"><b>เงินเดือน :</b> {{ $masterdata->expmasterdata->salary_3 }}</td>
            <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_3 }}</td>
        </tr>    
        @endif
        @if (!empty($masterdata->expmasterdata->title_4 ))
        <tr>
            <td  class="mycol_b_c" rowspan="3">ลำดับ<br/>4</td>
            <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
            <td class="mycol_b_c">ถึง ว/ด/ป</td>
            <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
            <td class="mycol_b_c">จังหวัด</td>
            <td class="mycol_b_c">โทรศัพท์</td>
            <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
        </tr>
        <tr>
            <td>{{ $masterdata->expmasterdata->start_date_4 }}</td>
            <td>{{ $masterdata->expmasterdata->end_date_4 }}</td>
            <td>{{ $masterdata->expmasterdata->company_4 }}</td>
            <td>{{ $masterdata->expmasterdata->province_4 }}</td>
            <td>{{ $masterdata->expmasterdata->tel_4 }}</td>
            <td>{{ $masterdata->expmasterdata->title_4 }}</td>
        </tr>
        <tr>
            <td colspan="2"><b>เงินเดือน :</b> {{ $masterdata->expmasterdata->salary_4 }}</td>
            <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_4 }}</td>
        </tr>    
        @endif
        @if (!empty($masterdata->expmasterdata->title_5 ))
        <tr>
            <td  class="mycol_b_c" rowspan="3">ลำดับ<br/>5</td>
            <td class="mycol_b_c">ตั้งแต่ ว/ด/ป</td>
            <td class="mycol_b_c">ถึง ว/ด/ป</td>
            <td class="mycol_b_c">ชื่อสถานที่ทำงาน และที่ตั้ง</td>
            <td class="mycol_b_c">จังหวัด</td>
            <td class="mycol_b_c">โทรศัพท์</td>
            <td class="mycol_b_c">ตำแหน่ง/ลักษณะงานที่ทำ</td>
        </tr>
        <tr>
            <td>{{ $masterdata->expmasterdata->start_date_5 }}</td>
            <td>{{ $masterdata->expmasterdata->end_date_5 }}</td>
            <td>{{ $masterdata->expmasterdata->company_5 }}</td>
            <td>{{ $masterdata->expmasterdata->province_5 }}</td>
            <td>{{ $masterdata->expmasterdata->tel_5 }}</td>
            <td>{{ $masterdata->expmasterdata->title_5 }}</td>
        </tr>
        <tr>
            <td colspan="2"><b>เงินเดือน :</b> {{ $masterdata->expmasterdata->salary_5 }}</td>
            <td colspan="4"><b>เหตุที่ลาออก :</b> {{ $masterdata->expmasterdata->case_5 }}</td>
        </tr>    
        @endif
    </tbody>
</table>