<h5 class='newh5'>ส่วนที่ 11 : สุขภาพและความพร้อมทางร่างกาย</h5>
@if (isset($masterdata->healthdata))
<div class='row'>
    <div class='col-4'>     
        1.	โรคประจำตัว <span class="answer" >{{ filldot($masterdata->healthdata->congenital_disease,30) }}</span> 
        @if ($masterdata->healthdata->congenital_disease == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อโรค <span class="answer" >{{ filldot($masterdata->healthdata->congenital_disease_other,50) }}</span>
        @endif     
    </div>
        <div class='col-4'>     
        2.	โรคติดต่อ/โรคเรื้อรัง <span class="answer" >{{ filldot($masterdata->healthdata->contagious_disease,30) }}</span> 
        @if ($masterdata->healthdata->contagious_disease == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อโรค <span class="answer" >{{ filldot($masterdata->healthdata->contagious_disease_other,50) }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        3.	โรคภูมิแพ้ <span class="answer" >{{ filldot($masterdata->healthdata->allergy,30) }}</span> 
        @if ($masterdata->healthdata->allergy == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อโรค <span class="answer" >{{ filldot($masterdata->healthdata->allergy_other,50) }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        4.	อาการปวดหลัง ปวดกล้ามเนื้อ  <span class="answer" >{{ filldot($masterdata->healthdata->back_pain,30) }}</span>                                
    </div>
    <div class='col-4'>     
        5.	อาการชาตามกล้ามเนื้อ นิ้ว มือ  <span class="answer" >{{ filldot($masterdata->healthdata->finger_pain,30) }}</span>                                
    </div>
    <div class='col-4'>     
        6.	โรคธาลัสซีเมีย	  <span class="answer" >{{ filldot($masterdata->healthdata->thalassemia,30) }}</span>                                
    </div>
    <div class='col-4'>     
        7.	ไวรัสตับอักเสบ A B C  <span class="answer" >{{ filldot($masterdata->healthdata->liver_virus,30) }}</span>                                
    </div>
    <div class='col-4'>     
        8.	แพ้อาหาร <span class="answer" >{{ filldot($masterdata->healthdata->food_allergy,30) }}</span> 
        @if ($masterdata->healthdata->food_allergy == 'เคยเป็น/กำลังเป็น')
            ระบุชื่ออาหารที่แพ้ <span class="answer" >{{ filldot($masterdata->healthdata->food_allergy_other,50) }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        9.	แพ้ยา <span class="answer" >{{ filldot($masterdata->healthdata->drug_allergy,30) }}</span> 
        @if ($masterdata->healthdata->drug_allergy == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อยาที่แพ้ <span class="answer" >{{ filldot($masterdata->healthdata->drug_allergy_other,50) }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        10.	โรค/อาการอื่นๆ(ระบุ) <span class="answer" >{{ filldot($masterdata->healthdata->other_disease,50) }}</span>     
    </div>
    <div class='col-12'>     
        11.	ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ <span class="answer" >{{ filldot($masterdata->healthdata->physical_exam_time,30) }}</span> 
        ส่วนของร่างกายที่ได้รับการตรวจ <span class="answer" >{{ filldot($masterdata->healthdata->physical_exam_part,50) }}</span>
        ผลการตรวจ <span class="answer" >{{ filldot($masterdata->healthdata->physical_exam_result,50) }}</span>
        @if ($masterdata->healthdata->physical_exam_result == 'ไม่ปกติ')
        (ระบุรายละเอียด) <span class="answer" >{{ filldot($masterdata->healthdata->physical_exam_result_other,100) }}</span>
        @endif
    </div>
    <div class='col-12'>     
        12.	ชื่อสถานที่ทำงานก่อนหน้านี้ซึ่งมีการทำประกันสังคม <span class="answer" >{{ filldot($masterdata->healthdata->prev_social_sec_company,50) }}</span>
        โรงพยาบาลที่ใช้สิทธิรักษาพยาบาล <span class="answer" >{{ filldot($masterdata->healthdata->prev_social_sec_hospital,50) }}</span>
    </div>
</div>
@endif
<div class="pagebreak"></div>