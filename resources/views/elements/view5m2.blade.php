<h5 class='newh5'>ส่วนที่ 5 : ประวัติการเข้ารับการฝึกอบรม</h5>
<table class="table d-print-table mytable">
    <tbody>
        <tr>
            <td class="mycol_b_c" style="width: 4%" rowspan="2">ลำดับ</td>
            <td class="mycol_b_c" style="width: 24%" colspan="2">ระยะเวลาตั้งแต่ - ถึง</td>
            <td class="mycol_b_c" style="width: 12%">รวมทั้งสิ้น</td>
            <td class="mycol_b_c" style="width: 30%" rowspan="2">หลักสูตร / หัวข้อที่เข้ารับการอบรม</td>
            <td class="mycol_b_c" style="width: 30%" rowspan="2">ชื่อหน่วยงานที่จัดฝึกอบรม</td>
        </tr>
        <tr>
            <td class="mycol_b_c" style="width: 12%">วัน/เดือน/ปี</td>
            <td class="mycol_b_c" style="width: 12%">วัน/เดือน/ปี</td>
            <td class="mycol_b_c" style="width: 12%">วัน / ชั่วโมง</td>
        </tr>
        {{-- @if (!empty($masterdata->extrainingdata->title_1)) --}}
            <tr>
                <td class="mycol_b_c">1</td>
                {{-- <td>@if (!empty($masterdata->extrainingdata->start_date_1)){{ $masterdata->extrainingdata->start_date_1 }}@endif</td> --}}
                <td>@if (!empty($masterdata->extrainingdata->start_date_1))
                    @if(date('Y',strtotime($masterdata->extrainingdata->start_date_1))<2500)
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->start_date_1)).(date('Y',strtotime($masterdata->extrainingdata->start_date_1))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->start_date_1)).date('Y',strtotime($masterdata->extrainingdata->start_date_1)) }}
                    @endif
                @endif</td>
                {{-- <td>@if (!empty($masterdata->extrainingdata->end_date_1)){{ $masterdata->extrainingdata->end_date_1 }}@endif</td> --}}
                <td>@if (!empty($masterdata->extrainingdata->end_date_1))
                    @if(date('Y',strtotime($masterdata->extrainingdata->end_date_1))<2500)
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->end_date_1)).(date('Y',strtotime($masterdata->extrainingdata->end_date_1))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->end_date_1)).date('Y',strtotime($masterdata->extrainingdata->end_date_1)) }}
                    @endif
                @endif</td>
                <td>@if (!empty($masterdata->extrainingdata->duration_1)){{ $masterdata->extrainingdata->duration_1 }}@endif</td>
                <td>@if (!empty($masterdata->extrainingdata->title_1)){{ $masterdata->extrainingdata->title_1 }}@endif</td>
                <td>@if (!empty($masterdata->extrainingdata->by_1)){{ $masterdata->extrainingdata->by_1 }}@endif</td>
            </tr>
        {{-- @endif
        @if (!empty($masterdata->extrainingdata->title_2)) --}}
            <tr>
                <td class="mycol_b_c">2</td>
                {{-- <td>@if (!empty($masterdata->extrainingdata->start_date_2)){{ $masterdata->extrainingdata->start_date_2 }}@endif</td> --}}
                <td>@if (!empty($masterdata->extrainingdata->start_date_2))
                    @if(date('Y',strtotime($masterdata->extrainingdata->start_date_2))<2500)
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->start_date_2)).(date('Y',strtotime($masterdata->extrainingdata->start_date_2))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->start_date_2)).date('Y',strtotime($masterdata->extrainingdata->start_date_2)) }}
                    @endif
                @endif</td>
                {{-- <td>@if (!empty($masterdata->extrainingdata->end_date_2)){{ $masterdata->extrainingdata->end_date_2 }}@endif</td> --}}
                <td>@if (!empty($masterdata->extrainingdata->end_date_2))
                    @if(date('Y',strtotime($masterdata->extrainingdata->end_date_2))<2500)
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->end_date_2)).(date('Y',strtotime($masterdata->extrainingdata->end_date_2))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->extrainingdata->end_date_2)).date('Y',strtotime($masterdata->extrainingdata->end_date_2)) }}
                    @endif
                @endif</td>
                <td>@if (!empty($masterdata->extrainingdata->duration_2)){{ $masterdata->extrainingdata->duration_2 }}@endif</td>
                <td>@if (!empty($masterdata->extrainingdata->title_2)){{ $masterdata->extrainingdata->title_2 }}@endif</td>
                <td>@if (!empty($masterdata->extrainingdata->by_2)){{ $masterdata->extrainingdata->by_2 }}@endif</td>
            </tr>
        {{-- @endif --}}
    </tbody>
</table>
