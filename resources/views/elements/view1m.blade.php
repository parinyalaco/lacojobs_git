<span><b>ที่อยู่ปัจจุบัน</b></span><br />
เลขที่ <span class='answer'>{{ filldot($masterdata->addr1, 10) }}</span>
หมู่ <span class='answer'>{{ filldot($masterdata->addr2, 15) }}</span>
ตรอก/ซอย <span class='answer'>{{ filldot($masterdata->block, 20) }}</span>
ถนน <span class='answer'>{{ filldot($masterdata->road, 20) }}</span>
ตำบล <span class='answer'>{{ filldot($masterdata->subdistrict, 20) }}</span>
อำเภอ <span class='answer'>{{ filldot($masterdata->district, 30) }}</span><br />
จังหวัด <span class='answer'>{{ filldot($masterdata->province, 30) }}</span>
รหัสไปรษณีย์ <span class='answer'>{{ filldot($masterdata->zipcode, 10) }}</span>
โทรศัพท์บ้าน <span class='answer'>{{ filldot($masterdata->tel, 15) }}</span>
โทรศัพท์มือถือ <span class='answer'>{{ filldot($masterdata->mobile, 15) }}</span><br />

<span style="text-decoration: underline"><b>ในกรณีฉุกเฉิน สามารถติดต่อกับ</b></span><br />
<span class='answer'>{{ filldot($masterdata->urgent_init, 10) }}..{{ filldot($masterdata->urgent_name, 40) }}</span>
<b>ที่อยู่</b> <span class='answer'>{{ filldot($masterdata->urgent_addr1, 50) }}</span>
<b>โทรศัพท์</b> <span class='answer'>{{ filldot($masterdata->urgent_mobile, 15) }}</span>
<b>ความสัมพันธ์เป็น</b> <span class='answer'>{{ filldot($masterdata->urgent_relation, 30) }}</span>
