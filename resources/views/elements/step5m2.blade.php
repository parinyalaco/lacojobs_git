<h3>ความสามารถพิเศษ</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ความสามารถพิเศษ</legend>
            <div class="row">
            <div class="form-check col-6 col-xl-3">
                       <label for="expd_type_th">พิมพดีด ภาษาไทย (คำ/นาที)</label>
                        <input id="expd_type_th" name="expd_type_th" type="text" class="form-control" value="{{ old('expd_type_th') }}">
                   </div>
                   <div class="form-check col-6 col-xl-3">
                       <label for="expd_type_en">พิมพดีด ภาษาอังกฤษ (คำ/นาที)</label>
                        <input id="expd_type_en" name="expd_type_en" type="text" class="form-control" value="{{ old('expd_type_en') }}">
                   </div>
             </div>
            <div class="row">
                <div class="form-check col-12"><b>คอมพิวเตอร์ โปรแกรม</b>
                </div>
                @for ($i = 1; $i < 4; $i++)
                    <div class="form-check col-6 col-xl-4">
                        <input id="expd_com_program{{ $i }}" name="expd_com_program{{ $i }}"
                            type="text" class="form-control" value="{{ old('expd_com_program'.$i) }}">
                        <select class="form-control" id="expd_com_level{{ $i }}"
                            name="expd_com_level{{ $i }}">
                            <option value="">=== เลือก ===</option>
                            <option value="ดีมาก" @if (old('expd_com_level'.$i) == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                            <option value="ดี" @if (old('expd_com_level'.$i) == "ดี") {{ 'selected' }} @endif>ดี</option>
                            <option value="พอใช้" @if (old('expd_com_level'.$i) == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                        </select>
                    </div>
                @endfor
            </div>
            <div class="row">
                <div class="form-check col-12"><b>ความสามารถด้านภาษา</b></div>
                <div class="form-check col-6 col-xl-3 text-center align-middle">
                    <label for="expd_lang_speak_th">ภาษาไทย</label>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_th">การพูด</label>
                    <select class="form-control" id="expd_lang_speak_th" name="expd_lang_speak_th">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_speak_th') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_speak_th') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_speak_th') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_th">การอ่าน</label>
                    <select class="form-control" id="expd_lang_read_th" name="expd_lang_read_th">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_read_th') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_read_th') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_read_th') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_th">การเขียน</label>
                    <select class="form-control " id="expd_lang_write_th" name="expd_lang_write_th">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_write_th') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_write_th') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_write_th') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3 text-center align-items-center">
                    <label for="expd_lang_speak_en">ภาษาอังกฤษ</label>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_en">การพูด</label>
                    <select class="form-control" id="expd_lang_speak_en" name="expd_lang_speak_en">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_speak_en') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_speak_en') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_speak_en') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_en">การอ่าน</label>
                    <select class="form-control " id="expd_lang_read_en" name="expd_lang_read_en">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_read_en') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_read_en') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_read_en') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_en">การเขียน</label>
                    <select class="form-control " id="expd_lang_write_en" name="expd_lang_write_en">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_write_en') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_write_en') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_write_en') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3 text-center align-items-center">
                    <label for="expd_lang_speak_ch">ภาษาจีน</label>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_ch">การพูด</label>
                    <select class="form-control " id="expd_lang_speak_ch" name="expd_lang_speak_ch">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_speak_ch') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_speak_ch') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_speak_ch') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_ch">การอ่าน</label>
                    <select class="form-control  " id="expd_lang_read_ch" name="expd_lang_read_ch">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_read_ch') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_read_ch') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_read_ch') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_ch">การเขียน</label>
                    <select class="form-control  " id="expd_lang_write_ch" name="expd_lang_write_ch">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_write_ch') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_write_ch') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_write_ch') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>

                <div class="form-check col-6 col-xl-3 text-center align-items-center">
                    <label for="expd_lang_ex">ภาษาอื่น</label>
                    <input id="expd_lang_ex" name="expd_lang_ex" type="text" placeholder="ระบุภาษา"
                        class="form-control"  value="{{ old('expd_lang_ex') }}">
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_ex">การพูด</label>
                    <select class="form-control " id="expd_lang_speak_ex" name="expd_lang_speak_ex">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_speak_ex') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_speak_ex') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_speak_ex') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_ex">การอ่าน</label>
                    <select class="form-control " id="expd_lang_read_ex" name="expd_lang_read_ex">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_read_ex') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_read_ex') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_read_ex') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_ex">การเขียน</label>
                    <select class="form-control " id="expd_lang_write_ex" name="expd_lang_write_ex">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก" @if (old('expd_lang_write_ex') == "ดีมาก") {{ 'selected' }} @endif>ดีมาก</option>
                        <option value="ดี" @if (old('expd_lang_write_ex') == "ดี") {{ 'selected' }} @endif>ดี</option>
                        <option value="พอใช้" @if (old('expd_lang_write_ex') == "พอใช้") {{ 'selected' }} @endif>พอใช้</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12"><b>การขับขี่ยานพาหนะ</b>
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_can_drive">สามารถขับรถยนต์</label>
                    <select class="form-control" id="expd_can_drive" name="expd_can_drive">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่ได้" @if (old('expd_can_drive') == "ไม่ได้") {{ 'selected' }} @endif>ไม่ได้</option>
                        <option value="ได้" @if (old('expd_can_drive') == "ได้") {{ 'selected' }} @endif>ได้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_have_drive_licen">ใบอนุญาตรถยนต์</label>
                    <select class="form-control" id="expd_have_drive_licen"
                        name="expd_have_drive_licen">
                        <option value="">=== เลือก ===</option>
                        <option value="มี" @if (old('expd_have_drive_licen') == "มี") {{ 'selected' }} @endif>มี</option>
                        <option value="ไม่มี" @if (old('expd_have_drive_licen') == "ไม่มี") {{ 'selected' }} @endif>ไม่มี</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_drive_licen_type">ใบอนุญาตรถยนต์แบบ</label>
                    <input id="expd_drive_licen_type" name="expd_drive_licen_type" type="text"
                        placeholder="ระบุรูปแบบ" class="form-control" value="{{ old('expd_drive_licen_type') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_drive_licen_date">วันอนุญาต</label>
                    <input id="expd_drive_licen_date" name="expd_drive_licen_date" type="date"
                        placeholder="ระบรูปแบบ" class="form-control" value="{{ old('expd_drive_licen_date') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_drive_licen_exp">วันหมดอายุ</label>
                    <input id="expd_drive_licen_exp" name="expd_drive_licen_exp" type="date"
                        placeholder="ระบรูปแบบ" class="form-control" value="{{ old('expd_drive_licen_exp') }}">
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_can_ride">สามารถขับจักรยานยนต์</label>
                    <select class="form-control" id="expd_can_ride" name="expd_can_ride">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่ได้" @if (old('expd_can_ride') == "ไม่ได้") {{ 'selected' }} @endif>ไม่ได้</option>
                        <option value="ได้" @if (old('expd_can_ride') == "ได้") {{ 'selected' }} @endif>ได้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_have_ride_licen">ใบอนุญาตจักรยานยนต์</label>
                    <select class="form-control" id="expd_have_ride_licen"
                        name="expd_have_ride_licen">
                        <option value="">=== เลือก ===</option>
                        <option value="มี" @if (old('expd_have_ride_licen') == "มี") {{ 'selected' }} @endif>มี</option>
                        <option value="ไม่มี" @if (old('expd_have_ride_licen') == "ไม่มี") {{ 'selected' }} @endif>ไม่มี</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_ride_licen_type">ใบอนุญาตจักรยานยนต์แบบ</label>
                    <input id="expd_ride_licen_type" name="expd_ride_licen_type" type="text"
                        placeholder="ระบุรูปแบบ" class="form-control"  value="{{ old('expd_ride_licen_type') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_ride_licen_date">วันอนุญาต</label>
                    <input id="expd_ride_licen_date" name="expd_ride_licen_date" type="date"
                        placeholder="ระบรูปแบบ" class="form-control"   value="{{ old('expd_ride_licen_date') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="expd_ride_licen_exp">วันหมดอายุ</label>
                    <input id="expd_ride_licen_exp" name="expd_ride_licen_exp" type="date"
                        placeholder="ระบรูปแบบ" class="form-control" value="{{ old('expd_ride_licen_exp') }}">
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12"><b>ความสามารถพิเศษอื่นๆ</b>
                </div>
                <div class="form-check col-12 col-xl-12">
                    <input id="expd_others" name="expd_others" type="text"
                        placeholder="ระบุความสามารถพิเศษอื่นๆ" class="form-control" value="{{ old('expd_others') }}">
                </div>
            </div>
        </div>
    <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
    </div>


</fieldset>
