<h3>ข้อมูลส่วนตัว</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ตำแหน่งงาน</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="job_name1">ตำแหน่ง1 *</label>
                    <input id="job_name1" name="job_name1" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ตำแหน่ง1" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="job_salary1">อัตราเงินเดือน1 *</label>
                    <input id="job_salary1" name="job_salary1" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "10000" @endif>
                </div>

                <div class="form-check  col-xl-2 col-6">
                    <label for="job_name2">ตำแหน่ง2</label>
                    <input id="job_name2" name="job_name2" type="text" class="form-control"
                        @if ($testdata) value = "ตำแหน่ง2" @endif>
                </div>
                <div class="form-check  col-xl-2 col-6">
                    <label for="job_salary2">อัตราเงินเดือน2</label>
                    <input id="job_salary2" name="job_salary2" type="text" class="form-control"
                        @if ($testdata) value = "20000" @endif>
                </div>
            </div>
            <legend>ข้อมูลส่วนตัว</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="init_th">คำนำหน้า *</label>
                    <input id="init_th" name="init_th" type="text" class="form-control {{ $flag }}" value="{{ old('init_th') }}"
                        @if ($testdata) value = "นาย" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="fname_th">ชื่อ *</label>
                    <input id="fname_th" name="fname_th" type="text" class="form-control {{ $flag }}" value="{{ old('fname_th') }}"
                        @if ($testdata) value = "ทดสอบ" @endif>
                </div>

                <div class="form-check col-xl-3 col-6">
                    <label for="lname_th">สกุล *</label>
                    <input id="lname_th" name="lname_th" type="text" class="form-control {{ $flag }}" value="{{ old('lname_th') }}"
                        @if ($testdata) value = "ลองดู" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="nickname_th">ชื่อเล่น</label>
                    <input id="nickname_th" name="nickname_th" type="text" class="form-control" value="{{ old('nickname_th') }}"
                        @if ($testdata) value = "เต๊ด" @endif>
                </div>
            </div>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="init_en">Init *</label>
                    <input id="init_en" name="init_en" type="text" class="form-control {{ $flag }}" value="{{ old('init_en') }}"
                        @if ($testdata) value = "นาย" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="fname_en">First Name *</label>
                    <input id="fname_en" name="fname_en" type="text" class="form-control {{ $flag }}" value="{{ old('fname_en') }}"
                        @if ($testdata) value = "ทดสอบ" @endif>
                </div>

                <div class="form-check col-xl-3 col-6">
                    <label for="lname_en">Last Name *</label>
                    <input id="lname_en" name="lname_en" type="text" class="form-control {{ $flag }}" value="{{ old('lname_en') }}"
                        @if ($testdata) value = "ลองดู" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="nickname_en">Nickname</label>
                    <input id="nickname_en" name="nickname_en" type="text" class="form-control"  value="{{ old('nickname_en') }}"
                        @if ($testdata) value = "เต๊ด" @endif>
                </div>
            </div>
            <div class="row">
                <div class="form-check col-xl-3 col-6">
                    <label for="birth_date">วันเกิด *</label>
                    <input id="birth_date" name="birth_date" type="date" class="form-control {{ $flag }}" value="{{ old('birth_date') }}"
                        @if ($testdata) value = "2020-08-29" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="weight">น้ำหนัก(กก.)</label>
                    <input id="weight" name="weight" type="text" class="form-control"  value="{{ old('weight') }}"
                        @if ($testdata) value = "55" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="height">ส่วนสูง(ซม.)</label>
                    <input id="height" name="height" type="text" class="form-control"  value="{{ old('height') }}"
                        @if ($testdata) value = "165" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                       <label for="blood">หมู่เลือด</label>
                        <input id="blood" name="blood" type="text" class="form-control" value="{{ old('blood') }}"
                    @if ($testdata)
                        value = "O"
                    @endif
                    >
                </div>
                <div class="form-check col-xl-3 col-6">
                       <label for="nationality">เชื้อชาติ</label>
                        <input id="nationality" name="nationality" type="text" class="form-control" value="{{ old('nationality') }}"
                    @if ($testdata)
                        value = "ไทย"
                    @endif
                    >
                    </div>
                   <div class="form-check col-xl-3 col-6">
                       <label for="race">สัญชาติ</label>
                        <input id="race" name="race" type="text" class="form-control" value="{{ old('race') }}"

                    @if ($testdata)
                        value = "ไทย"
                    @endif
                    >
                    </div>
                   <div class="form-check col-xl-3 col-6">
                       <label for="religion">ศาสนา</label>
                        <input id="religion" name="religion" type="text" class="form-control" value="{{ old('religion') }}"

                    @if ($testdata)
                        value = "พุทธ"
                    @endif
                    >
                    </div>

            </div>
             <div class="row">
            <div class="form-check col-xl-3 col-6">
                        <label for="citizenid">บัตรประชาชน เลขที่ *</label>
                        <input id="citizenid" name="citizenid" type="text" class="form-control {{ $flag }}"  value="{{ old('citizenid') }}" 
                        maxlength="13" minlength="13" oninput="this.value = this.value.replace(/[^0-9.]/g, '');"
                    @if ($testdata)
                        value = "1234567890123"
                    @endif
                    >
                    </div>
                    <div class="form-check col-xl-2 col-6">
                        <label for="citizenstartdate">วันออกบัตร *</label>
                        <input id="citizenstartdate" name="citizenstartdate" type="date" class="form-control {{ $flag }}"  value="{{ old('citizenstartdate') }}"
                    @if ($testdata)
                        value = "2020-08-09"
                    @endif
                    >
                    </div>
                    <div class="form-check col-xl-2 col-6">
                        <label for="citizenexpdate">วันหมดอายุบัตร *</label>
                        <input id="citizenexpdate" name="citizenexpdate" type="date" class="form-control {{ $flag }}"  value="{{ old('citizenexpdate') }}"
                    @if ($testdata)
                        value = "2020-08-09"
                    @endif
                    >
                    </div>
                    <div class="form-check col-xl-3 col-6">
                        <label for="email">อีเมลล์ *</label>
                        <input id="email" name="email" type="email" class="form-control {{ $flag }}" value="{{ old('email') }}"
                    @if ($testdata)
                        value = "your@email.com"
                    @endif
                    >
                    </div>
        </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
