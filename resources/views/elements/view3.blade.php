<h5 class='newh5'>ส่วนที่ 3 : ประวัติการฝึกงานขณะกำลังศึกษาอยู่ (เรียงจากปัจจุบันตามลำดับ)</h5>
<table class="mytable table d-print-table ">
    <tbody>
        <tr>
            <td rowspan="2" class="mycol_b_c">ลำดับ</td>
            <td colspan="2" class="mycol_b_c">ระยะเวลาตั้งแต่ - ถึง</td>
            <td rowspan="2" class="mycol_b_c">ชื่อสถานที่ฝึกงาน</td>
            <td rowspan="2" class="mycol_b_c">จังหวัด</td>
            <td rowspan="2" class="mycol_b_c">ขณะที่ฝึกงานกำลังศึกษาอยู่ในระดับชั้น</td>
        </tr>
        <tr>
            <td class="mycol_b_c">วัน/เดือน/ปี</td>
            <td class="mycol_b_c">วัน/เดือน/ปี</td>
        </tr>
        @if (!empty($masterdata->trainingdata->title_1 ))
        <tr>
            <td> 1 </td>
            <td>{{ $masterdata->trainingdata->start_date_1 }}</td>
            <td>{{ $masterdata->trainingdata->end_date_1 }}</td>
            <td>{{ $masterdata->trainingdata->title_1 }}</td>
            <td>{{ $masterdata->trainingdata->province_1 }}</td>
            <td>{{ $masterdata->trainingdata->level_1 }}</td>
        </tr>
        @endif
        @if (!empty($masterdata->trainingdata->title_2 ))
        <tr>
            <td> 2 </td>
            <td>{{ $masterdata->trainingdata->start_date_2 }}</td>
            <td>{{ $masterdata->trainingdata->end_date_2 }}</td>
            <td>{{ $masterdata->trainingdata->title_2 }}</td>
            <td>{{ $masterdata->trainingdata->province_2 }}</td>
            <td>{{ $masterdata->trainingdata->level_2 }}</td>
        </tr>
        @endif
    </tbody>
</table>