<h5 class='newh5'>ส่วนที่ 12 : บุคคลอ้างอิงซึ่งสามารถรับรองความประพฤติ/ประวัติการทำงานได้ (ไม่ใช่ญาติพี่น้องหรือเพื่อน)</h5>
@if (isset($masterdata->refdata))
<table class="table d-print-table mytable" >
<tbody>
<tr>
<td class="mycol_b_c" >ลำดับ</td>
<td class="mycol_b_c" >ชื่อ-นามสกุล</td>
<td class="mycol_b_c" >มีความสัมพันธ์เป็น</td>
<td class="mycol_b_c" >ชื่อสถานที่ทำงาน/ตำแหน่ง</td>
<td class="mycol_b_c" >โทรศัพท์</td>
</tr>
@if (!empty($masterdata->refdata->name_1))
<tr>
<td>1</td>
<td>{{ $masterdata->refdata->name_1 }}</td>
<td>{{ $masterdata->refdata->relation_1 }}</td>
<td>{{ $masterdata->refdata->company_job_1 }}</td>
<td>{{ $masterdata->refdata->tel_1 }}</td>
</tr>
@endif
@if (!empty($masterdata->refdata->name_2))
<tr>
<td>2</td>
<td>{{ $masterdata->refdata->name_2 }}</td>
<td>{{ $masterdata->refdata->relation_2 }}</td>
<td>{{ $masterdata->refdata->company_job_2 }}</td>
<td>{{ $masterdata->refdata->tel_2 }}</td>
</tr>
@endif
@if (!empty($masterdata->refdata->name_3))
<tr>
<td>3</td>
<td>{{ $masterdata->refdata->name_3 }}</td>
<td>{{ $masterdata->refdata->relation_3 }}</td>
<td>{{ $masterdata->refdata->company_job_3 }}</td>
<td>{{ $masterdata->refdata->tel_3 }}</td>
</tr>
@endif
</tbody>
</table>
@endif