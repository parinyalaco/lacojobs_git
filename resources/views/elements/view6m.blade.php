<h5 class='newh5'>ส่วนที่ 5 : ความสามารถพิเศษ</h5>
@if (isset($masterdata->expdetaildata))
<div class="row">
    <div class="col-12">
        <b>1.  คอมพิวเตอร์ โปรแกรม :-</b>
    </div>
    @if (!empty($masterdata->expdetaildata->expd_com_program1))
        <div class="col-4"><b>1) {{ filldot($masterdata->expdetaildata->expd_com_program1,30) }}</b> 
            <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_com_level1,20) }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program2))
        <div class="col-4"><b>2) {{ filldot($masterdata->expdetaildata->expd_com_program2,30) }}</b> 
            <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_com_level2,20) }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program3))
        <div class="col-4"><b>3) {{ filldot($masterdata->expdetaildata->expd_com_program3,30) }}</b> 
            <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_com_level3,20) }})</span></div>
    @endif
    
    <div class="col-12"><b>4. ความสามารถด้านภาษา</b></div>
    <div class="col-6"><b>4.1 ภาษาไทย</b> 
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_th,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_th,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_th,20) }})</span>
    </div>
    <div class="col-6"><b>4.2 ภาษาอังกฤษ</b> 
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_en,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_en,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_en,20) }})</span>
    </div>
    <div class="col-6"><b>4.3 ภาษาจีน</b> 
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_ch,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_ch,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_ch,20) }})</span>
    </div>
    <div class="col-6"><b>4.1 ภาษาอื่นๆ {{ filldot($masterdata->expdetaildata->expd_lang_ex,20) }}</b>  
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_ex,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_ex,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_ex,20) }})</span>
    </div>
</div>
@endif