<h3>ครอบครัว</h3>
    <fieldset>
        <div class="row">
            <div class="col-12">
                <legend>สมาชิกในครอบครัว</legend>

                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-2">
                                <label for="father_name">บิดา ชื่อ-สกุล</label>
                                <input id="father_name" name="father_name" type="text" placeholder="ชื่อ-สกุลบิดา" class="form-control">
                            </div>
                            <div class="col-1">
                                <label for="father_age">อายุ</label>
                                    <input id="father_age" name="father_age" type="text" placeholder="อายุบิดา" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="father_occupation">อาชีพ</label>
                                    <input id="father_occupation" name="father_occupation" type="text" placeholder="อาชีพบิดา" class="form-control">
                            </div>
                            <div class="col-3">
                                <label for="father_company">สถานที่ทำงาน</label>
                                    <input id="father_company" name="father_company" type="text" placeholder="สถานที่ทำงานบิดา" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="father_job">ตำแหน่ง</label>
                                    <input id="father_job" name="father_job" type="text" placeholder="ตำแหน่งงานบิดา" class="form-control">
                            </div>
                            <div class="col-2">
                                    <label for="father_company_tel">โทรศัพท์</label>
                                    <input id="father_company_tel" name="father_company_tel" type="text" placeholder="โทรศัพท์สถานที่ทำงานบิดา" class="form-control">
                            </div>
                            <div class="col-5">
                                <label for="father_addr">ที่อยู่ปัจจุบัน</label>
                                    <input id="father_addr" name="father_addr" type="text" placeholder="ที่อยู่ปัจจุบันบิดา" class="form-control">
                            </div>
                            <div class="col-2">
                                    <label for="father_tel">โทรศัพท์</label>
                                    <input id="father_tel" name="father_tel" type="text" placeholder="โทรศัพท์บิดา" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-2">
                                <label for="mother_name">มารดา ชื่อ-สกุล</label>
                                <input id="mother_name" name="mother_name" type="text" placeholder="ชื่อ-สกุลมารดา" class="form-control">
                            </div>
                            <div class="col-1">
                                <label for="mother_age">อายุ</label>
                                    <input id="mother_age" name="mother_age" type="text" placeholder="อายุมารดา" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="mother_occupation">อาชีพ</label>
                                    <input id="mother_occupation" name="mother_occupation" type="text" placeholder="อาชีพมารดา" class="form-control">
                            </div>
                            <div class="col-3">
                                <label for="mother_company">สถานที่ทำงาน</label>
                                    <input id="mother_company" name="mother_company" type="text" placeholder="สถานที่ทำงานมารดา" class="form-control">
                            </div>
                            <div class="col-2">
                                <label for="mother_job">ตำแหน่ง</label>
                                    <input id="mother_job" name="mother_job" type="text" placeholder="ตำแหน่งงานมารดา" class="form-control">
                            </div>
                            <div class="col-2">
                                    <label for="mother_company_tel">โทรศัพท์</label>
                                    <input id="mother_company_tel" name="mother_company_tel" type="text" placeholder="โทรศัพท์สถานที่ทำงานมารดา" class="form-control">
                            </div>
                            <div class="col-5">
                                <label for="mother_addr">ที่อยู่ปัจจุบัน</label>
                                    <input id="mother_addr" name="mother_addr" type="text" placeholder="ที่อยู่ปัจจุบันมารดา" class="form-control">
                            </div>
                            <div class="col-2">
                                    <label for="mother_tel">โทรศัพท์</label>
                                    <input id="mother_tel" name="mother_tel" type="text" placeholder="โทรศัพท์มารดา" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                       <label for="total_nabor">มีพี่น้อง(รวมผู้สมัครด้วย)ทั้งสิ้น (คน)</label>
                        <input id="total_nabor" name="total_nabor" type="text" placeholder="ระบุจำนวน" class="form-control">
                   </div>
                   <div class="col-1">
                       <label for="total_brother">ชาย (คน)</label>
                        <input id="total_brother" name="total_brother" type="text" placeholder="ระบุจำนวน" class="form-control">
                   </div>
                   <div class="col-2">
                       <label for="total_sisther">หญิง (คน)</label>
                        <input id="total_sisther" name="total_sisther" type="text" placeholder="ระบุจำนวน" class="form-control">
                   </div>
                    <div class="col-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="5">เรียงลำดับตามอายุจากมากไปน้อย (รวมผู้สมัครด้วย) ดังนี้</th>
                                </tr>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="20%">ชื่อ-นามสกุล</th>
                                    <th width="10%">อายุ(ปี)</th>
                                    <th>ชื่อสถานที่ทำงาน/สถานศึกษา</th>
                                    <th>ตำแหน่ง/ชั้นปีที่กำลังศึกษา</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td><input id="nabor_name_1" name="nabor_name_1" type="text" placeholder="ระบุชื่อ-สกุล" class="form-control"></td>
                                    <td><input id="nabor_age_1" name="nabor_age_1" type="text" placeholder="ระบุปี" class="form-control"></td>
                                    <td><input id="nabor_company_1" name="nabor_company_1" type="text" placeholder="ระบุชื่อสถานที่ทำงาน/สถานศึกษา" class="form-control"></td>
                                    <td><input id="nabor_level_1" name="nabor_level_1" type="text" placeholder="ระบุตำแหน่ง/ชั้นปีที่กำลังศึกษา" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><input id="nabor_name_2" name="nabor_name_2" type="text" placeholder="ระบุชื่อ-สกุล" class="form-control"></td>
                                    <td><input id="nabor_age_2" name="nabor_age_2" type="text" placeholder="ระบุปี" class="form-control"></td>
                                    <td><input id="nabor_company_2" name="nabor_company_2" type="text" placeholder="ระบุชื่อสถานที่ทำงาน/สถานศึกษา" class="form-control"></td>
                                    <td><input id="nabor_level_2" name="nabor_level_2" type="text" placeholder="ระบุตำแหน่ง/ชั้นปีที่กำลังศึกษา" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td><input id="nabor_name_3" name="nabor_name_3" type="text" placeholder="ระบุชื่อ-สกุล" class="form-control"></td>
                                    <td><input id="nabor_age_3" name="nabor_age_3" type="text" placeholder="ระบุปี" class="form-control"></td>
                                    <td><input id="nabor_company_3" name="nabor_company_3" type="text" placeholder="ระบุชื่อสถานที่ทำงาน/สถานศึกษา" class="form-control"></td>
                                    <td><input id="nabor_level_3" name="nabor_level_3" type="text" placeholder="ระบุตำแหน่ง/ชั้นปีที่กำลังศึกษา" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td><input id="nabor_name_4" name="nabor_name_4" type="text" placeholder="ระบุชื่อ-สกุล" class="form-control"></td>
                                    <td><input id="nabor_age_4" name="nabor_age_4" type="text" placeholder="ระบุปี" class="form-control"></td>
                                    <td><input id="nabor_company_4" name="nabor_company_4" type="text" placeholder="ระบุชื่อสถานที่ทำงาน/สถานศึกษา" class="form-control"></td>
                                    <td><input id="nabor_level_4" name="nabor_level_4" type="text" placeholder="ระบุตำแหน่ง/ชั้นปีที่กำลังศึกษา" class="form-control"></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td><input id="nabor_name_5" name="nabor_name_5" type="text" placeholder="ระบุชื่อ-สกุล" class="form-control"></td>
                                    <td><input id="nabor_age_5" name="nabor_age_5" type="text" placeholder="ระบุปี" class="form-control"></td>
                                    <td><input id="nabor_company_5" name="nabor_company_5" type="text" placeholder="ระบุชื่อสถานที่ทำงาน/สถานศึกษา" class="form-control"></td>
                                    <td><input id="nabor_level_5" name="nabor_level_5" type="text" placeholder="ระบุตำแหน่ง/ชั้นปีที่กำลังศึกษา" class="form-control"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset>