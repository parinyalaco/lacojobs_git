<h3>สุขภาพ / ความพร้อมทางร่างกาย</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>สุขภาพ / ความพร้อมทางร่างกาย</legend>

            <div class="row">
                <div class="form-check col-12  col-xl-2">
                    <label for="congenital_disease">1. โรคประจำตัว *</label>
                    <select class="form-control {{ $flag }} " id="congenital_disease" name="congenital_disease">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่มี" {{ old('congenital_disease') == 'ไม่มี' ? 'selected' : '' }}>ไม่มี</option>
                        <option value="มี" {{ old('congenital_disease') == 'มี' ? 'selected' : '' }}>มี</option>
                    </select>
                </div>
                <div class="form-check col-12 col-xl-6 congenital_disease d-none">
                    <label for="congenital_disease_other">ระบุชื่อโรคประจำตัว</label>
                    <input id="congenital_disease_other" name="congenital_disease_other" type="text"
                        placeholder="ระบุโรคประจำตัว" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-3">
                    <label for="health_check_date">2. ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ</label>
                    <input id="health_check_date" name="health_check_date" type="date"
                    value="{{ old('health_check_date') }}"
                        placeholder="ระบุโรคประจำตัว" class="form-control ">
                </div>
                <div class="form-check col-12 col-xl-3">
                    <label for="health_check_case">ส่วนที่ได้รับการตรวจ</label>
                    <input id="health_check_case" name="health_check_case" type="text"
                    value="{{ old('health_check_case') }}"
                        placeholder="ระบุการตรวจ" class="form-control ">
                </div>
                <div class="form-check col-12  col-xl-2">
                    <label for="health_check_status">ผลการตรวจ</label>
                    <select class="form-control {{ $flag }} " id="health_check_status" name="health_check_status">
                        <option value="">=== เลือก ===</option>
                        <option value="ปรกติ" {{ old('heal_check_status') == 'ปรกติ' ? 'selected' : '' }}>ปรกติ</option>
                        <option value="ไม่ปรกติ" {{ old('heal_check_status') == 'ไม่ปรกติ' ? 'selected' : '' }}>ไม่ปรกติ</option>
                    </select>
                </div>
                <div class="form-check col-12 col-xl-4 health_check_status d-none">
                    <label for="health_check_status_note">ระบุรายละเอียด</label>
                    <input id="health_check_status_note" name="health_check_status_note" type="text"
                    value="{{ old('health_check_status_note') }}"
                        placeholder="ระบุรายละเอียด" class="form-control ">
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-6">
                    <label for="social_security_hospital">3.โรงพยาบาลที่ขอใช้สิทธิรักษาพยาบาลประกันสังคม *</label>
                    <input id="social_security_hospital" name="social_security_hospital" type="text"
                        placeholder="ระบุโรงพยาบาล" class="form-control {{ $flag }}" value="{{ old('social_security_hospital') }}">
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>


</fieldset>
