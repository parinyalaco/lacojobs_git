<h3>ครอบครัว</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>สมาชิกในครอบครัว</legend>

            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="form-check col-6 col-xl-2">
                            <label for="father_name">บิดา ชื่อ-สกุล</label>
                            <input id="father_name" name="father_name" type="text" placeholder="ชื่อ-สกุลบิดา"
                                class="form-control" value="{{ old('father_name') }}">
                        </div>
                        <div class="form-check col-6 col-xl-1">
                            <label for="father_age">อายุ</label>
                            <input id="father_age" name="father_age" type="text" placeholder="อายุบิดา"
                                class="form-control" value="{{ old('father_age') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="father_occupation">อาชีพ</label>
                            <input id="father_occupation" name="father_occupation" type="text"
                                placeholder="อาชีพบิดา" class="form-control" value="{{ old('father_occupation') }}">
                        </div>
                        <div class="col-3">
                            <label for="father_company">สถานที่ทำงาน</label>
                            <input id="father_company" name="father_company" type="text"
                                placeholder="สถานที่ทำงานบิดา" class="form-control"
                                value="{{ old('father_company') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="father_job">ตำแหน่ง</label>
                            <input id="father_job" name="father_job" type="text" placeholder="ตำแหน่งงานบิดา"
                                class="form-control" value="{{ old('father_job') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="father_company_tel">โทรศัพท์</label>
                            <input id="father_company_tel" name="father_company_tel" type="text"
                                placeholder="โทรศัพท์สถานที่ทำงานบิดา" class="form-control"
                                value="{{ old('father_company_tel') }}">
                        </div>
                        <div class="form-check col-6 col-xl-5">
                            <label for="father_addr">ที่อยู่ปัจจุบัน</label>
                            <input id="father_addr" name="father_addr" type="text" placeholder="ที่อยู่ปัจจุบันบิดา"
                                class="form-control" value="{{ old('father_addr') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="father_tel">โทรศัพท์</label>
                            <input id="father_tel" name="father_tel" type="text" placeholder="โทรศัพท์บิดา"
                                class="form-control" value="{{ old('father_tel') }}">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="form-check col-6 col-xl-2">
                            <label for="mother_name">มารดา ชื่อ-สกุล</label>
                            <input id="mother_name" name="mother_name" type="text" placeholder="ชื่อ-สกุลมารดา"
                                class="form-control" value="{{ old('mother_name') }}">
                        </div>
                        <div class="form-check col-6 col-xl-1">
                            <label for="mother_age">อายุ</label>
                            <input id="mother_age" name="mother_age" type="text" placeholder="อายุมารดา"
                                class="form-control" value="{{ old('mother_age') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="mother_occupation">อาชีพ</label>
                            <input id="mother_occupation" name="mother_occupation" type="text"
                                placeholder="อาชีพมารดา" class="form-control" value="{{ old('mother_occupation') }}">
                        </div>
                        <div class="col-3">
                            <label for="mother_company">สถานที่ทำงาน</label>
                            <input id="mother_company" name="mother_company" type="text"
                                placeholder="สถานที่ทำงานมารดา" class="form-control"
                                value="{{ old('mother_company') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="mother_job">ตำแหน่ง</label>
                            <input id="mother_job" name="mother_job" type="text" placeholder="ตำแหน่งงานมารดา"
                                class="form-control" value="{{ old('mother_job') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="mother_company_tel">โทรศัพท์</label>
                            <input id="mother_company_tel" name="mother_company_tel" type="text"
                                placeholder="โทรศัพท์สถานที่ทำงานมารดา" class="form-control"
                                value="{{ old('mother_company_tel') }}">
                        </div>
                        <div class="form-check col-6 col-xl-5">
                            <label for="mother_addr">ที่อยู่ปัจจุบัน</label>
                            <input id="mother_addr" name="mother_addr" type="text"
                                placeholder="ที่อยู่ปัจจุบันมารดา" class="form-control"
                                value="{{ old('mother_addr') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="mother_tel">โทรศัพท์</label>
                            <input id="mother_tel" name="mother_tel" type="text" placeholder="โทรศัพท์มารดา"
                                class="form-control" value="{{ old('mother_tel') }}">
                        </div>
                    </div>
                </div>
                 <div class="col-12">
                    <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="total_nabor">มีพี่น้อง(รวมผู้สมัครด้วย)ทั้งสิ้น (คน)</label>
                    <input id="total_nabor" name="total_nabor" type="text" placeholder="ระบุจำนวน"
                        class="form-control" value="{{ old('total_nabor') }}">
                </div>
                <div class="form-check col-6 col-xl-1">
                    <label for="total_brother">ชาย (คน)</label>
                    <input id="total_brother" name="total_brother" type="text" placeholder="ระบุจำนวน"
                        class="form-control" value="{{ old('total_brother') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="total_sisther">หญิง (คน)</label>
                    <input id="total_sisther" name="total_sisther" type="text" placeholder="ระบุจำนวน"
                        class="form-control" value="{{ old('total_sisther') }}">
                </div>
                    </div>
                <div class="col-12">
                        <div class="row">
                            <div class="col-12"><b>เรียงลำดับตามอายุจากมากไปน้อย (รวมผู้สมัครด้วย) ดังนี้</b></div>
                        </div>
                    @for ($i = 1; $i <= 3; $i++)
                        <div class="row">
                            <div class="form-check col-6 col-xl-1">
                                <b>ลำดับที่ {{ $i }}</b>
                            </div>
                            <div class="form-check col-6 col-xl-3">
                                <label for="nabor_name_{{ $i }}">ชื่อ-นามสกุล</label>
                                <input id="nabor_name_{{ $i }}" name="nabor_name_{{ $i }}"
                                    type="text" placeholder="ระบุชื่อ-สกุล" class="form-control"  value="{{ old('nabor_name_'.$i) }}">
                            </div>
                            <div class="form-check col-6 col-xl-2">
                                <label for="nabor_age_{{ $i }}">อายุ(ปี)</label>
                                <input id="nabor_age_{{ $i }}" name="nabor_age_{{ $i }}"
                                    type="text" placeholder="ระบุปี" class="form-control"  value="{{ old('nabor_age_'.$i) }}">
                            </div>
                            <div class="form-check col-6 col-xl-3">
                                <label for="nabor_company_{{ $i }}">ชื่อสถานที่ทำงาน/สถานศึกษา</label>
                                <input id="nabor_company_{{ $i }}"
                                    name="nabor_company_{{ $i }}" type="text"
                                    placeholder="ระบุชื่อสถานที่ทำงาน/สถานศึกษา" class="form-control"  value="{{ old('nabor_company_'.$i) }}">
                            </div>
                            <div class="form-check col-6 col-xl-3">
                                <label for="nabor_level_{{ $i }}">ตำแหน่ง/ชั้นปีที่กำลังศึกษา</label>
                                <input id="nabor_level_{{ $i }}" name="nabor_level_{{ $i }}"
                                    type="text" placeholder="ระบุตำแหน่ง/ชั้นปีที่กำลังศึกษา"
                                    class="form-control"  value="{{ old('nabor_level_'.$i) }}">
                            </div>
                        </div>
                    @endfor


                </div>
            </div>

            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>


</fieldset>
