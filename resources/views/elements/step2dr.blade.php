<h3>ข้อมูลส่วนตัว</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>1. ตำแหน่งงาน</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="job1">ตำแหน่ง1 *</label>
                    <input id="job1" name="job1" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ตำแหน่ง1" @endif>
                </div>
                <div class="form-check  col-xl-2 col-6">
                    <label for="job2">ตำแหน่ง2</label>
                    <input id="job2" name="job2" type="text" class="form-control"
                        @if ($testdata) value = "ตำแหน่ง2" @endif>
                </div>
            </div>
            <legend>2 ข้อมูลส่วนตัว</legend>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="init">คำนำหน้า *</label>
                    <select class="form-control  {{ $flag }}" id="init" name="init">
                        <option value="">=== เลือก ===</option>
                        <option value="นาย" @if ($testdata) selected @endif
                            {{ old('init') == 'นาย' ? 'selected' : '' }}>นาย</option>
                        <option value="นาง" {{ old('init') == 'นาง' ? 'selected' : '' }}>นาง</option>
                        <option value="นางสาว" {{ old('init') == 'นางสาว' ? 'selected' : '' }}>นางสาว</option>
                    </select>
                    @error('init')
                        <div class="alert alert-danger">กรุณาเลือกคำนำหน้า</div>
                    @enderror
                </div>
                <div class="form-check col-12 col-xl-3">
                    <label for="fname">ชื่อ *</label>
                    <input id="fname" name="fname" type="text" class="form-control {{ $flag }}"
                        value="{{ old('fname') }}" @if ($testdata) value = "ทดสอบ" @endif>
                    @error('fname')
                        <div class="alert alert-danger">กรุณาใส่ชื่อ</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-3">
                    <label for="lname">นามสกุล *</label>
                    <input id="lname" name="lname" type="text" class="form-control {{ $flag }}"
                        value="{{ old('lname') }}" @if ($testdata) value = "ลองดู" @endif>
                    @error('lname')
                        <div class="alert alert-danger">กรุณาใส่นามสกุล</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="sex">เพศ *</label>
                    <select class="form-control  {{ $flag }}" id="sex" name="sex">
                        <option value="">=== เลือก ===</option>
                        <option value="ชาย" {{ old('sex') == 'ชาย' ? 'selected' : '' }}
                            @if ($testdata) selected @endif>ชาย</option>
                        <option value="หญิง" {{ old('sex') == 'หญิง' ? 'selected' : '' }}>หญิง</option>
                    </select>
                    @error('sex')
                        <div class="alert alert-danger">กรุณาเลือกเพศ</div>
                    @enderror
                </div>

            </div>
            <div class="row">

                <div class="form-check col-6  col-xl-2">
                    <label for="birth_date">วันเกิด *</label>
                    <input id="birth_date" name="birth_date" type="date" class="form-control {{ $flag }}"
                        value="{{ old('birth_date') }}"
                        @if ($testdata) value = "2020-08-29" @endif>
                    @error('birth_date')
                        <div class="alert alert-danger">กรุณาเลือกวันเกิด</div>
                    @enderror
                </div>
                <div class="form-check col-12  col-xl-4">
                    <label for="citizenid">เลขที่บัตรประชาชน *</label>
                    <input id="citizenid" name="citizenid" type="number" maxlength="13"
                        value="{{ old('citizenid') }}" class="form-control {{ $flag }}"
                        @if ($testdata) value = "1234567890123" @endif
                        oninvalid="this.setCustomValidity('กรุณาใส่เลขที่บัตรประชาชน 13 หลัก')"
                        onvalid="this.setCustomValidity('')">
                    @error('citizenid')
                        <div class="alert alert-danger">กรุณาใส่เลขที่บัตรประชาชน 13 หลัก</div>
                    @enderror
                </div>

                <div class="form-check col-6  col-xl-2">
                    <label for="weight">น้ำหนัก(กก.) *</label>
                    <input id="weight" name="weight" type="number" class="form-control {{ $flag }}"
                        value="{{ old('weight') }}" @if ($testdata) value = "55" @endif>
                    @error('weight')
                        <div class="alert alert-danger">กรุณาใส่น้ำหนัก</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="height">ส่วนสูง(ซม.) *</label>
                    <input id="height" name="height" type="number" class="form-control {{ $flag }}"
                        value="{{ old('height') }}" @if ($testdata) value = "165" @endif>
                    @error('height')
                        <div class="alert alert-danger">กรุณาใส่ส่วนสูง</div>
                    @enderror
                </div>

            </div>
            <legend>3. ที่อยู่</legend>
            <div class="row">
                <div class="form-check col-6  col-xl-1">
                    <label for="addr">เลขที่ *</label>
                    <input id="addr" name="addr" type="text" class="form-control {{ $flag }}"
                        value="{{ old('addr') }}" @if ($testdata) value = "ที่อยู่." @endif>
                    @error('addr')
                        <div class="alert alert-danger">กรุณาใส่เลขที่</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-1">
                    <label for="addr2">หมู่ที่ *</label>
                    <input id="addr2" name="addr2" type="text" class="form-control {{ $flag }}"
                        value="{{ old('addr2') }}" @if ($testdata) value = "ที่อยู่." @endif>
                    @error('addr2')
                        <div class="alert alert-danger">กรุณาใส่หมู่ที่</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="subcity">ตำบล *</label>
                    <input id="subcity" name="subcity" type="text" class="form-control {{ $flag }}"
                        value="{{ old('subcity') }}" @if ($testdata) value = "ตำบล." @endif>
                    @error('subcity')
                        <div class="alert alert-danger">กรุณาใส่ตำบล</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="city">อำเภอ *</label>
                    <input id="city" name="city" type="text" class="form-control {{ $flag }}"
                        value="{{ old('city') }}" @if ($testdata) value = "อำเภอ." @endif>
                    @error('city')
                        <div class="alert alert-danger">กรุณาใส่อำเภอ</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="province">จังหวัด *</label>
                    <input id="province" name="province" type="text" class="form-control {{ $flag }}"
                        value="{{ old('province') }}" @if ($testdata) value = "จังหวัด." @endif>
                    @error('province')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="zipcode">รหัสไปรษณีย์ *</label>
                    <input id="zipcode" name="zipcode" type="number" maxlength="5" value="{{ old('zipcode') }}"
                        class="form-control {{ $flag }}"
                        @if ($testdata) value = "50000" @endif>
                    @error('zipcode')
                        <div class="alert alert-danger">กรุณาใส่รหัสไปรษณีย์ 5 หลัก</div>
                    @enderror
                </div>
                <div class="form-check col-6  col-xl-2">
                    <label for="tel">โทรศัพท์ *</label>
                    <input id="tel" name="tel" type="number" maxlength="10" class="form-control {{ $flag }}"
                        value="{{ old('tel') }}" @if ($testdata) value = "100000000" @endif>
                    @error('tel')
                        <div class="alert alert-danger">กรุณาใส่เบอร์โทรศัพท์ 10 หลัก</div>
                    @enderror
                </div>
            </div>
            <legend>4. ข้อมูลบุคคลที่สามารถติดต่อได้กรณีฉุกเฉิน</legend>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="contact_init">คำนำหน้า</label>
                    <select class="form-control" id="contact_init" name="contact_init">
                        <option value="">=== เลือก ===</option>
                        <option value="นาย" @if ($testdata) selected @endif
                            {{ old('contact_init') == 'นาย' ? 'selected' : '' }}>นาย</option>
                        <option value="นาง" {{ old('contact_init') == 'นาง' ? 'selected' : '' }}>นาง</option>
                        <option value="นางสาว" {{ old('contact_init') == 'นางสาว' ? 'selected' : '' }}>นางสาว
                        </option>
                    </select>
                    @error('contact_init')
                        <div class="alert alert-danger">กรุณาเลือกคำนำหน้า</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="contact_name">ชื่อ</label>
                    <input id="contact_name" name="contact_name" type="text" placeholder="ระบุชื่อ"
                        class="form-control" value="{{ old('contact_name') }}">
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="contact_lname">นามสกุล</label>
                    <input id="contact_lname" name="contact_lname" type="text" placeholder="ระบุชื่อ"
                        class="form-control" value="{{ old('contact_lname') }}">
                </div>

                <div class="form-check col-6  col-xl-2">
                    <label for="contact_tel">โทรศัพท์</label>
                    <input id="contact_tel" name="contact_tel" type="number" maxlength="10" class="form-control"
                        value="{{ old('contact_tel') }}"
                        @if ($testdata) value = "100000000" @endif>
                    @error('tel')
                        <div class="alert alert-danger">กรุณาใส่เบอร์โทรศัพท์ 10 หลัก</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="contact_relation">มีความสัมพันธ์</label>
                    <input id="contact_relation" name="contact_relation" type="text" placeholder="ความสัมพันธ์"
                        class="form-control" value="{{ old('contact_relation') }}">
                </div>
            </div>


            <legend>4 สุขภาพ / ความพร้อมทางร่างกาย</legend>
            <div class="row">
                <div class="form-check col-12 col-xl-3">
                    <label for="social_security_hospital_flag">ท่านเคยมีบัตรรับรองสิทธิประกันสังคมมาก่อนหรือไม่</label>
                    <select class="form-control {{ $flag }} " id="social_security_hospital_flag"
                        name="social_security_hospital_flag">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่มี"
                            {{ old('social_security_hospital_flag') == 'ไม่มี' ? 'selected' : '' }}>ไม่มี</option>
                        <option value="มี" {{ old('social_security_hospital_flag') == 'มี' ? 'selected' : '' }}>มี
                        </option>
                    </select>
                </div>
                <div class="form-check col-12 col-xl-3 social_security_hospital_flag d-none">
                    <label for="social_security_hospital">ระบุชื่อโรงพยาบาล'</label>
                    <input id="social_security_hospital" name="social_security_hospital" type="text"
                        placeholder="ระบุโรงพยาบาล" class="form-control"
                        value="{{ old('social_security_hospital') }}">
                </div>
                <div class="form-check col-12  col-xl-3">
                    <label for="congenital_disease">โรคประจำตัว *</label>
                    <select class="form-control {{ $flag }} " id="congenital_disease"
                        name="congenital_disease">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่มี" {{ old('congenital_disease') == 'ไม่มี' ? 'selected' : '' }}>ไม่มี
                        </option>
                        <option value="มี" {{ old('congenital_disease') == 'มี' ? 'selected' : '' }}>มี</option>
                    </select>
                </div>
                <div class="form-check col-12 col-xl-3 congenital_disease d-none">
                    <label for="congenital_disease_other">ระบุชื่อโรคประจำตัว</label>
                    <input id="congenital_disease_other" name="congenital_disease_other" type="text"
                        placeholder="ระบุโรคประจำตัว" class="form-control">
                </div>
            </div>

            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
