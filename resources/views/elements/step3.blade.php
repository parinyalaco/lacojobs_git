<h3>การศึกษา</h3>
    <fieldset>
        <div class="row">
            <div class="col-12"><legend>ประวัติการศึกษา</legend>
                <table class="table">
                    <thead>
                        <tr>
                            <th>ระดับ</th>
                            <th>ชื่อสถานศึกษา</th>
                            <th>จังหวัด</th>
                            <th>สาขาวิชา/แผนก/คณะ</th>
                            <th>ปีที่จบการศึกษา</th>
                            <th>เกรดเฉลี่ย</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>มัธยมศึกษาตอนต้น</td>
                            <td><input id="school_1" name="school_1" type="text" class="form-control"></td>
                            <td><input id="province_1" name="province_1" type="text" class="form-control"></td>
                            <td><input id="title_1" name="title_1" type="text" class="form-control"></td>
                            <td><input id="year_1" name="year_1" type="text" class="form-control"></td>
                            <td><input id="grade_1" name="grade_1" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>มัธยมศึกษาตอนปลาย</td>
                            <td><input id="school_2" name="school_2" type="text" class="form-control"></td>
                            <td><input id="province_2" name="province_2" type="text" class="form-control"></td>
                            <td><input id="title_2" name="title_2" type="text" class="form-control"></td>
                            <td><input id="year_2" name="year_2" type="text" class="form-control"></td>
                            <td><input id="grade_2" name="grade_2" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>ปวช.</td>
                            <td><input id="school_3" name="school_3" type="text" class="form-control"></td>
                            <td><input id="province_3" name="province_3" type="text" class="form-control"></td>
                            <td><input id="title_3" name="title_3" type="text" class="form-control"></td>
                            <td><input id="year_3" name="year_3" type="text" class="form-control"></td>
                            <td><input id="grade_3" name="grade_3" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>ปวส.</td>
                            <td><input id="school_4" name="school_4" type="text" class="form-control"></td>
                            <td><input id="province_4" name="province_4" type="text" class="form-control"></td>
                            <td><input id="title_4" name="title_4" type="text" class="form-control"></td>
                            <td><input id="year_4" name="year_4" type="text" class="form-control"></td>
                            <td><input id="grade_4" name="grade_4" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>ปริญญาตรี</td>
                            <td><input id="school_5" name="school_5" type="text" class="form-control"></td>
                            <td><input id="province_5" name="province_5" type="text" class="form-control"></td>
                            <td><input id="title_5" name="title_5" type="text" class="form-control"></td>
                            <td><input id="year_5" name="year_5" type="text" class="form-control"></td>
                            <td><input id="grade_5" name="grade_5" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>ปริญญาโท</td>
                            <td><input id="school_6" name="school_6" type="text" class="form-control"></td>
                            <td><input id="province_6" name="province_6" type="text" class="form-control"></td>
                            <td><input id="title_6" name="title_6" type="text" class="form-control"></td>
                            <td><input id="year_6" name="year_6" type="text" class="form-control"></td>
                            <td><input id="grade_6" name="grade_6" type="text" class="form-control"></td>
                        </tr>
                    </tbody>
                </table>
               
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset>