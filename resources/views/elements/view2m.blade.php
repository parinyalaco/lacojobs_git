<h5 class='newh5'>ส่วนที่ 2 : ประวัติการศึกษา</h5>
<table class="table d-print-table mytable">
    <tbody>
        <tr>
            <td class="mycol_b_c">ระดับ</td>
            <td class="mycol_b_c">ชื่อสถานศึกษา</td>
            <td class="mycol_b_c">จังหวัด</td>
            <td class="mycol_b_c">สาขาวิชา/แผนก/คณะ</td>
            <td class="mycol_b_c">ปีที่จบการศึกษา</td>
            <td class="mycol_b_c">เกรดเฉลี่ย</td>
        </tr>
        @if (!empty($masterdata->educationdata->school_2))
            <tr>
                <td class="mycol_b_l">มัธยมศึกษาตอนปลาย/ปวช.</td>
                <td>{{ $masterdata->educationdata->school_2 }}</td>
                <td>{{ $masterdata->educationdata->province_2 }}</td>
                <td>{{ $masterdata->educationdata->title_2 }}</td>
                <td>{{ $masterdata->educationdata->year_2 }}</td>
                <td>{{ $masterdata->educationdata->grade_2 }}</td>
            </tr>
        @endif
        @if (!empty($masterdata->educationdata->school_4))
            <tr>
                <td class="mycol_b_l">ปวส.</td>
                <td>{{ $masterdata->educationdata->school_4 }}</td>
                <td>{{ $masterdata->educationdata->province_1 }}</td>
                <td>{{ $masterdata->educationdata->title_4 }}</td>
                <td>{{ $masterdata->educationdata->year_4 }}</td>
                <td>{{ $masterdata->educationdata->grade_4 }}</td>
            </tr>
        @endif
        @if (!empty($masterdata->educationdata->school_5))
            <tr>
                <td class="mycol_b_l">ปริญญาตรี</td>
                <td>{{ $masterdata->educationdata->school_5 }}</td>
                <td>{{ $masterdata->educationdata->province_5 }}</td>
                <td>{{ $masterdata->educationdata->title_5 }}</td>
                <td>{{ $masterdata->educationdata->year_5 }}</td>
                <td>{{ $masterdata->educationdata->grade_5 }}</td>
            </tr>
        @endif

        @if (!empty($masterdata->educationdata->school_6))
            <tr>
                <td class="mycol_b_l">ปริญญาโท</td>
                <td>{{ $masterdata->educationdata->school_6 }}</td>
                <td>{{ $masterdata->educationdata->province_6 }}</td>
                <td>{{ $masterdata->educationdata->title_6 }}</td>
                <td>{{ $masterdata->educationdata->year_6 }}</td>
                <td>{{ $masterdata->educationdata->grade_6 }}</td>
            </tr>
        @endif

        @if (!empty($masterdata->educationdata->school_1))
            <tr>
                <td class="mycol_b_l">การศึกษาอื่นๆ</td>
                <td>{{ $masterdata->educationdata->school_1 }}</td>
                <td>{{ $masterdata->educationdata->province_1 }}</td>
                <td>{{ $masterdata->educationdata->title_1 }}</td>
                <td>{{ $masterdata->educationdata->year_1 }}</td>
                <td>{{ $masterdata->educationdata->grade_1 }}</td>
            </tr>
        @endif
    </tbody>
</table>
