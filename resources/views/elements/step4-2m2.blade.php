<h3>ฝึกงานและอบรม</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ประวัติการฝึกงานขณะกำลังศึกษาอยู่ (เรียงจากปัจจุบันตามลำดับ)</legend>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 1</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_start_date_1">ตั้งแต่ ว/ด/ป </label>
                    <input id="train_start_date_1" name="train_start_date_1" type="date"
                        class="form-control" value="{{ old('train_start_date_1') }}">
                    @error('train_start_date_1')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_end_date_1">ถึง ว/ด/ป</label>
                    <input id="train_end_date_1" name="train_end_date_1"  type="date"
                        class="form-control" value="{{ old('train_end_date_1') }}">
                    @error('train_end_date_1')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_title_1">ชื่อสถานที่ฝึกงาน</label>
                    <input id="train_title_1" name="train_title_1"  type="text" class="form-control"
                        value="{{ old('train_title_1') }}">
                    @error('train_title_1')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_province_1">จังหวัด</label>
                    <input id="train_province_1" name="train_province_1" type="text"
                        class="form-control" value="{{ old('train_province_1') }}">
                    @error('train_province_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-3">
                    <label for="train_level_1">ขณะที่ฝึกงานกำลังศึกษาอยู่ในระดับชั้น</label>
                    <input id="train_level_1" name="train_level_1" type="text"
                        class="form-control" value="{{ old('train_level_1') }}">
                    @error('train_level_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 2</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_start_date_2">ตั้งแต่ ว/ด/ป </label>
                    <input id="train_start_date_2" name="train_start_date_2" type="date"
                        class="form-control" value="{{ old('train_start_date_2') }}">
                    @error('train_start_date_2')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_end_date_2">ถึง ว/ด/ป</label>
                    <input id="train_end_date_2" name="train_end_date_2"  type="date"
                        class="form-control" value="{{ old('train_end_date_2') }}">
                    @error('train_end_date_2')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_title_2">ชื่อสถานที่ฝึกงาน</label>
                    <input id="train_title_2" name="train_title_2"  type="text" class="form-control"
                        value="{{ old('train_title_2') }}">
                    @error('train_title_2')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_province_2">จังหวัด</label>
                    <input id="train_province_2" name="train_province_2" type="text"
                        class="form-control" value="{{ old('train_province_2') }}">
                    @error('train_province_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-3">
                    <label for="train_level_2">ขณะที่ฝึกงานกำลังศึกษาอยู่ในระดับชั้น</label>
                    <input id="train_level_2" name="train_level_2" type="text"
                        class="form-control" value="{{ old('train_level_2') }}">
                    @error('train_level_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
            </div>



            <legend>ประวัติการเข้ารับการฝึกอบรมพิเศษ</legend>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 1</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_start_date_1">ตั้งแต่ ว/ด/ป </label>
                    <input id="train_ex_start_date_1" name="train_ex_start_date_1" type="date"
                        class="form-control" value="{{ old('train_ex_start_date_1') }}">
                    @error('train_ex_start_date_1')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_end_date_1">ถึง ว/ด/ป</label>
                    <input id="train_ex_end_date_1" name="train_ex_end_date_1" type="date"
                        class="form-control" value="{{ old('train_ex_end_date_1') }}">
                    @error('train_ex_end_date_1')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="train_ex_duration_1">รวมทั้งสิ้น (วัน / ชั่วโมง)</label>
                    <input id="train_ex_duration_1" name="train_ex_duration_1" type="text" class="form-control"
                        value="{{ old('train_ex_duration_1') }}">
                    @error('train_ex_duration_1')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_title_1">หลักสูตร / หัวข้อที่เข้ารับการอบรม</label>
                    <input id="train_ex_title_1" name="train_ex_title_1" type="text"
                        class="form-control" value="{{ old('train_ex_title_1') }}">
                    @error('train_ex_title_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_by_1">ชื่อหน่วยงานที่จัดฝึกอบรม</label>
                    <input id="train_ex_by_1" name="train_ex_by_1" type="text"
                        class="form-control" value="{{ old('train_ex_by_1') }}">
                    @error('train_ex_by_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 2</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_start_date_2">ตั้งแต่ ว/ด/ป </label>
                    <input id="train_ex_start_date_2" name="train_ex_start_date_2" type="date"
                        class="form-control" value="{{ old('train_ex_start_date_2') }}">
                    @error('train_ex_start_date_2')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_end_date_2">ถึง ว/ด/ป</label>
                    <input id="train_ex_end_date_2" name="train_ex_end_date_2" type="date"
                        class="form-control" value="{{ old('train_ex_end_date_2') }}">
                    @error('train_ex_end_date_2')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="train_ex_duration_2">รวมทั้งสิ้น (วัน / ชั่วโมง)</label>
                    <input id="train_ex_duration_2" name="train_ex_duration_2" type="text" class="form-control"
                        value="{{ old('train_ex_duration_2') }}">
                    @error('train_ex_duration_2')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_title_2">หลักสูตร / หัวข้อที่เข้ารับการอบรม</label>
                    <input id="train_ex_title_2" name="train_ex_title_2" type="text"
                        class="form-control" value="{{ old('train_ex_title_2') }}">
                    @error('train_ex_title_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="train_ex_by_2">ชื่อหน่วยงานที่จัดฝึกอบรม</label>
                    <input id="train_ex_by_2" name="train_ex_by_2" type="text"
                        class="form-control" value="{{ old('train_ex_by_2') }}">
                    @error('train_ex_by_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>


</fieldset>
