<h5 class='newh5'>ส่วนที่ 7 : สุขภาพและความพร้อมทางร่างกาย</h5>
@if (isset($masterdata->healthdata))
<div class='row'>
    <div class='col-12'>     
        โรคประจำตัว <span class="answer" >{{ filldot($masterdata->healthdata->congenital_disease,30) }}</span> 
        @if ($masterdata->healthdata->congenital_disease == 'มี')
            ระบุชื่อโรค <span class="answer" >{{ $masterdata->healthdata->congenital_disease_other }}</span>
        @endif     
    </div>
</div>
@endif