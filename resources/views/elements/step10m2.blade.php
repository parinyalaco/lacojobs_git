<h3>Upload เอกสาร</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>Upload เอกสาร</legend>

            <div class="row">
                <div class="form-check col-6 col-xl-3">
                    <label for="image">ภาพถ่าย</label>
                    {!! Form::file('image', $attributes = ['accept' => 'image/jpeg , image/jpg, image/gif, image/png', 'class' => 'form-control']) !!}
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="private_info">สำเนา บัตรประจำตัวประชาชน,ทะเบียนบ้าน,หลักฐานแสดงวุฒิการศึกษา (รวมมาเป็น 1
                        pdf)</label>
                    {!! Form::file('private_info', $attributes = ['accept' => 'application/pdf', 'class' => 'form-control']) !!}
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="resume">Resume และ เอกสารอื่นๆ (รวมมาเป็น 1 pdf)</label>
                    {!! Form::file('resume', $attributes = ['accept' => 'application/pdf', 'class' => 'form-control']) !!}
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="map">ตำแหน่งบ้าน (จุดบนแผนที่)</label>
                    <input id="map" name="map" type="text" readonly placeholder="ระบุรายละเอียด"
                        class="form-control {{ $flag }}">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <input id="pac-input" class="controls form-control" type="text"
                        placeholder="พิมพ์เพื่อค้นหาที่บ้าน" />
                    <div id="googleMap" style="width:100%;height:400px;"></div>
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
            <div class="row">
                <div class="form-check col-6 col-xl-9">
                    “ข้าพเจ้าผู้สมัครงานขอให้ข้อมูลส่วนบุคคลต่อไปนี้ตามความจริงและยินยอมให้บริษัทฯ
                    เก็บรวบรวม ใช้ หรือเปิดเผยตาม พรบ.คุ้มครองข้อมูลส่วนบุคคล
                    {{-- ของบริษัทลานนาเกษตรที่ได้กำหนดไว้แล้ว ซึ่งข้าพเจ้าได้อ่านและตรวจสอบตาม QR CODE นี้
                    และทำการเซ็นรับทราบและยินยอมแล้ว”<br/> --}}
                    ของบริษัท ลานนาเกษตรอุตสาหกรรม จำกัด ที่ได้กำหนดไวแล้ว ซึ่งข้าพเจ้าได้อ่านและตรวจสอบตาม QR CODE นี้ 
                    และทำการเซ็นรับทราบและยินยอมแล้ว”<br/>
                <label for="job_name1">พิมพ์ชื่อ-นามสกุล เพื่อยืนยัน*</label>
                    <input id="confirm-data" name="confirm-data" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ตำแหน่ง1" @endif>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <img src="{{ asset('images/pdpa-laco-th-staff-applicant.png') }}" width="150px">
                </div>
            </div>


        </div>
    </div>
</fieldset>
<script>
    var map;
    var lats;
    var lngs;

    function error(msg) {
        alert('error in geolocation');
    }

    function success(position) {
        lats = position.coords.latitude;
        lngs = position.coords.longitude;
        map.setCenter(new google.maps.LatLng(lats, lngs));

    };


    function myMap() {
        var mapProp = {
            center: new google.maps.LatLng(18.67599842688653, 99.05243451279489),
            zoom: 15,
        };
        const input = document.getElementById("pac-input");
        const searchBox = new google.maps.places.SearchBox(input);
        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener("bounds_changed", () => {
            searchBox.setBounds(map.getBounds());
        });
        let markers = [];

        searchBox.addListener("places_changed", () => {
            const places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            // Clear out the old markers.
            markers.forEach(marker => {
                marker.setMap(null);
            });
            markers = [];
            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();
            places.forEach(place => {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                const icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                // Create a marker for each place.
                markers.push(
                    new google.maps.Marker({
                        map,
                        icon,
                        title: place.name,
                        position: place.geometry.location
                    })
                );

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });

        google.maps.event.addListener(map, 'click', function(event) {
            var r = confirm("กดเพื่อยืนยันตำแหน่ง GPS : " + event.latLng.lat() + ", " + event.latLng.lng() +
                " ?");
            $('#map').val(event.latLng.lat() + ", " + event.latLng.lng());
        });

    }
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUxsUwOjdI8DXS0vDOneRaWo_s53WPg8k&libraries=places&callback=myMap">
</script>
