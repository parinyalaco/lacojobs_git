<h5 class='newh5'>ส่วนที่ 3 : ประวัติการฝึกงานขณะกำลังศึกษาอยู่ (เรียงจากปัจจุบันตามลำดับ)</h5>
<table class="mytable table d-print-table ">
    <tbody>
        <tr>
            <td rowspan="2" class="mycol_b_c" style="width: 5%">ลำดับ</td>
            <td colspan="2" class="mycol_b_c" style="width: 24%">ระยะเวลาตั้งแต่ - ถึง</td>
            <td rowspan="2" class="mycol_b_c" style="width: 40%">ชื่อสถานที่ฝึกงาน</td>
            <td rowspan="2" class="mycol_b_c" style="width: 10%">จังหวัด</td>
            <td rowspan="2" class="mycol_b_c" style="width: 21%">ขณะที่ฝึกงานกำลังศึกษาอยู่ในระดับชั้น</td>
        </tr>
        <tr>
            <td class="mycol_b_c" style="width: 12%">วัน/เดือน/ปี</td>
            <td class="mycol_b_c" style="width: 12%">วัน/เดือน/ปี</td>
        </tr>
        {{-- @if (!empty($masterdata->trainingdata->title_1 )) --}}
        <tr>
            <td> 1 </td>
            <td>@if (!empty($masterdata->trainingdata->start_date_1 ))
                    @if(date('Y',strtotime($masterdata->trainingdata->start_date_1))<2500)
                        {{ date('d/m/',strtotime($masterdata->trainingdata->start_date_1)).(date('Y',strtotime($masterdata->trainingdata->start_date_1))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->trainingdata->start_date_1)).date('Y',strtotime($masterdata->trainingdata->start_date_1)) }}
                    @endif
                @endif</td>
            <td>@if (!empty($masterdata->trainingdata->end_date_1 ))
                    @if(date('Y',strtotime($masterdata->trainingdata->end_date_1))<2500)
                        {{ date('d/m/',strtotime($masterdata->trainingdata->end_date_1)).(date('Y',strtotime($masterdata->trainingdata->end_date_1))+543) }}
                    @else
                        {{ date('d/m/',strtotime($masterdata->trainingdata->end_date_1)).date('Y',strtotime($masterdata->trainingdata->end_date_1)) }}
                    @endif
                @endif</td>
            <td>@if (!empty($masterdata->trainingdata->title_1 )){{ $masterdata->trainingdata->title_1 }}@endif</td>
            <td>@if (!empty($masterdata->trainingdata->province_1 )){{ $masterdata->trainingdata->province_1 }}@endif</td>
            <td>@if (!empty($masterdata->trainingdata->level_1 )){{ $masterdata->trainingdata->level_1 }}@endif</td>
        </tr>
        {{-- @endif
        @if (!empty($masterdata->trainingdata->title_2 )) --}}
        <tr>
            <td> 2 </td>
            <td>@if (!empty($masterdata->trainingdata->start_date_2 ))             
                @if(date('Y',strtotime($masterdata->trainingdata->start_date_2))<2500)
                    {{ date('d/m/',strtotime($masterdata->trainingdata->start_date_2)).(date('Y',strtotime($masterdata->trainingdata->start_date_2))+543) }}
                @else
                    {{ date('d/m/',strtotime($masterdata->trainingdata->start_date_2)).date('Y',strtotime($masterdata->trainingdata->start_date_2)) }}
                @endif
                @endif</td>
            <td>@if (!empty($masterdata->trainingdata->end_date_2 ))
                @if(date('Y',strtotime($masterdata->trainingdata->end_date_2))<2500)
                    {{ date('d/m/',strtotime($masterdata->trainingdata->end_date_2)).(date('Y',strtotime($masterdata->trainingdata->end_date_2))+543) }}
                @else
                    {{ date('d/m/',strtotime($masterdata->trainingdata->end_date_2)).date('Y',strtotime($masterdata->trainingdata->end_date_2)) }}
                @endif
                @endif</td>
            <td>@if (!empty($masterdata->trainingdata->title_2 )){{ $masterdata->trainingdata->title_2 }}@endif</td>
            <td>@if (!empty($masterdata->trainingdata->province_2 )){{ $masterdata->trainingdata->province_2 }}@endif</td>
            <td>@if (!empty($masterdata->trainingdata->level_2 )){{ $masterdata->trainingdata->level_2 }}@endif</td>
        </tr>
        {{-- @endif --}}
    </tbody>
</table>
<div class="pagebreak"></div>