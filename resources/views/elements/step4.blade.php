<h3>ฝึกงานและทำงาน</h3>
    <fieldset>
        <div class="row">
            <div class="col-12"><legend>ประวัติการฝึกงานขณะกำลังศึกษาอยู่ (เรียงจากปัจจุบันตามลำดับ)</legend>
                <table class="table">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>ตั้งแต่ วัน/เดือน/ปี</th>
                            <th>ถึง วัน/เดือน/ปี</th>
                            <th>ชื่อสถานที่ฝึกงาน</th>
                            <th>จังหวัด</th>
                            <th>ขณะที่ฝึกงานกำลังศึกษาอยู่ในระดับชั้น</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td><input id="train_start_date_1" name="train_start_date_1" type="date" class="form-control"></td>
                            <td><input id="train_end_date_1" name="train_end_date_1" type="date" class="form-control"></td>
                            <td><input id="train_title_1" name="train_title_1" type="text" class="form-control"></td>
                            <td><input id="train_province_1" name="train_province_1" type="text" class="form-control"></td>
                            <td><input id="train_level_1" name="train_level_1" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><input id="train_start_date_2" name="train_start_date_2" type="date" class="form-control"></td>
                            <td><input id="train_end_date_2" name="train_end_date_2" type="date" class="form-control"></td>
                            <td><input id="train_title_2" name="train_title_2" type="text" class="form-control"></td>
                            <td><input id="train_province_2" name="train_province_2" type="text" class="form-control"></td>
                            <td><input id="train_level_2" name="train_level_2" type="text" class="form-control"></td>
                        </tr>
                    </tbody>
                </table>
<legend>
                ประวัติการทำงาน(จากปัจจุบันตามลำดับ)</legend>
               <table class="table">
                    
                        <thead>
                        <tr>
                            <th>ลำดับ </th>
                            <th>ตั้งแต่ ว/ด/ป</th>
                            <th>ถึง ว/ด/ป</th>
                            <th>ชื่อสถานที่ทำงาน และที่ตั้ง</th>
                            <th>จังหวัด</th>
                            <th>โทรศัพท์</th>
                            <th>ตำแหน่ง/ลักษณะงานที่ทำ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td><input id="exp_start_date_1" name="exp_start_date_1" type="date" class="form-control"></td>
                            <td><input id="exp_end_date_1" name="exp_end_date_1" type="date" class="form-control"></td>
                            <td><input id="exp_company_1" name="exp_company_1" type="text" class="form-control"></td>
                            <td><input id="exp_province_1" name="exp_province_1" type="text" class="form-control"></td>
                            <td><input id="exp_tel_1" name="exp_tel_1" type="text" class="form-control"></td>
                            <td><input id="exp_title_1" name="exp_title_1" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td colspan="2">เงินเดือน<input id="exp_salary_1" name="exp_salary_1" type="text" class="form-control"></td>
                            <td colspan="5">เหตุที่ลาออก<input id="exp_case_1" name="exp_case_1" type="text" class="form-control"></td>
                        </tr>
                        
                        <tr>
                            <td>2</td>
                            <td><input id="exp_start_date_2" name="exp_start_date_2" type="date" class="form-control"></td>
                            <td><input id="exp_end_date_2" name="exp_end_date_2" type="date" class="form-control"></td>
                            <td><input id="exp_company_2" name="exp_company_2" type="text" class="form-control"></td>
                            <td><input id="exp_province_2" name="exp_province_2" type="text" class="form-control"></td>
                            <td><input id="exp_tel_2" name="exp_tel_2" type="text" class="form-control"></td>
                            <td><input id="exp_title_2" name="exp_title_2" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td colspan="2">เงินเดือน<input id="exp_salary_2" name="exp_salary_2" type="text" class="form-control"></td>
                            <td colspan="5">เหตุที่ลาออก<input id="exp_case_2" name="exp_case_2" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td><input id="exp_start_date_3" name="exp_start_date_3" type="date" class="form-control"></td>
                            <td><input id="exp_end_date_3" name="exp_end_date_3" type="date" class="form-control"></td>
                            <td><input id="exp_company_3" name="exp_company_3" type="text" class="form-control"></td>
                            <td><input id="exp_province_3" name="exp_province_3" type="text" class="form-control"></td>
                            <td><input id="exp_tel_3" name="exp_tel_3" type="text" class="form-control"></td>
                            <td><input id="exp_title_3" name="exp_title_3" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td colspan="2">เงินเดือน<input id="exp_salary_3" name="exp_salary_3" type="text" class="form-control"></td>
                            <td colspan="5">เหตุที่ลาออก<input id="exp_case_3" name="exp_case_3" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td><input id="exp_start_date_4" name="exp_start_date_4" type="date" class="form-control"></td>
                            <td><input id="exp_end_date_4" name="exp_end_date_4" type="date" class="form-control"></td>
                            <td><input id="exp_company_4" name="exp_company_4" type="text" class="form-control"></td>
                            <td><input id="exp_province_4" name="exp_province_4" type="text" class="form-control"></td>
                            <td><input id="exp_tel_4" name="exp_tel_4" type="text" class="form-control"></td>
                            <td><input id="exp_title_4" name="exp_title_4" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td colspan="2">เงินเดือน<input id="exp_salary_4" name="exp_salary_4" type="text" class="form-control"></td>
                            <td colspan="5">เหตุที่ลาออก<input id="exp_case_4" name="exp_case_4" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td><input id="exp_start_date_5" name="exp_start_date_5" type="date" class="form-control"></td>
                            <td><input id="exp_end_date_5" name="exp_end_date_5" type="date" class="form-control"></td>
                            <td><input id="exp_company_5" name="exp_company_5" type="text" class="form-control"></td>
                            <td><input id="exp_province_5" name="exp_province_5" type="text" class="form-control"></td>
                            <td><input id="exp_tel_5" name="exp_tel_5" type="text" class="form-control"></td>
                            <td><input id="exp_title_5" name="exp_title_5" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td colspan="2">เงินเดือน<input id="exp_salary_5" name="exp_salary_5" type="text" class="form-control"></td>
                            <td colspan="5">เหตุที่ลาออก<input id="exp_case_5" name="exp_case_5" type="text" class="form-control"></td>
                        </tr>
                    </tbody>
               </table>
               <legend>ประวัติการเข้ารับการฝึกอบรมพิเศษ</legend>
                <table class="table">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>ตั้งแต่ วัน/เดือน/ปี</th>
                            <th>ถึง วัน/เดือน/ปี</th>
                            <th>รวมทั้งสิ้น (วัน / ชั่วโมง)</th>
                            <th>หลักสูตร / หัวข้อที่เข้ารับการอบรม</th>
                            <th>ชื่อหน่วยงานที่จัดฝึกอบรม</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td><input id="train_ex_start_date_1" name="train_ex_start_date_1" type="date" class="form-control"></td>
                            <td><input id="train_ex_end_date_1" name="train_ex_end_date_1" type="date" class="form-control"></td>
                            <td><input id="train_ex_duration_1" name="train_ex_duration_1" type="text" class="form-control"></td>
                            <td><input id="train_ex_title_1" name="train_ex_title_1" type="text" class="form-control"></td>
                            <td><input id="train_ex_by_1" name="train_ex_by_1" type="text" class="form-control"></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><input id="train_ex_start_date_2" name="train_ex_start_date_2" type="date" class="form-control"></td>
                            <td><input id="train_ex_end_date_2" name="train_ex_end_date_2" type="date" class="form-control"></td>
                            <td><input id="train_ex_duration_2" name="train_ex_duration_2" type="text" class="form-control"></td>
                            <td><input id="train_ex_title_2" name="train_ex_title_2" type="text" class="form-control"></td>
                            <td><input id="train_ex_by_2" name="train_ex_by_2" type="text" class="form-control"></td>
                        </tr>
                    </tbody>
                </table>
        <p>(*) Mandatory</p>
            </div>
        </div>
        
        
    </fieldset>