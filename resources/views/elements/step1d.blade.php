<h3>ความยินยอม</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ความยินยอมของผู้สมัครงาน</legend>
            <p>ความยินยอมของผู้สมัครงาน/ลูกจ้าง
                ผู้สมัครงาน ทราบว่า ข้อมูลส่วนบุคคลตามที่ปรากฏในแบบฟอร์มใบสมัคร
                และตามเอกสารซึ่งข้าพเจ้าได้ส่งมอบประกอบการสมัครงานให้กับบริษัทนั้น
                มีวัตถุประสงค์เพื่อเป็นหลักฐานและข้อมูลประกอบการพิจารณารับสมัครงานของบริษัท
                และ/หรือเป็นข้อมูลเอกสารหลักฐาน อันจำเป็นในการทำสัญญาว่าจ้าง
                และการอื่นใดอันจำเป็นในการปฏิบัติอันเกี่ยวกับการสัญญาว่าจ้าง ระหว่างบริษัท กับข้าพเจ้า
                (กรณีผ่านการพิจารณาและทำสัญญาเป็นลูกจ้างของบริษัท) โดยข้าพเจ้าตกลงให้บริษัทดำเนินการดังนี้
            </p>
            <p>1.การเก็บบันทึกและใช้ข้อมูล (ในส่วนของรายละเอียดส่วนตัวผู้สมัคร)
                มีความจำเป็นต้องบันทึกข้อมูลเกี่ยวกับศาสนา (ก) การเก็บบันทึกและใช้ข้อมูลเกี่ยวกับศาสนา : ข้าพเจ้าทราบว่า
                บริษัทมีความจำเป็นต้องบันทึกข้อมูลเกี่ยวกับศาสนา เพื่อเป็นการอันจำเป็นในการจัดสวัสดิการ สถานที่
                หรือวิธีการทำงาน ในกรณีที่หากข้าพเจ้าได้พิจารณาทำสัญญาจ้าง โดยหากข้าพเจ้าไม่ให้ข้อมูลดังกล่าวแล้วนั้น
                บริษัทอาจไม่สามารถดำเนินการดังกล่าวได้</p>
            <p>2.การเปิดเผยข้อมูลส่วนบุคคล ข้าพเจ้าทราบว่า
                เพื่อประโยชน์ของข้าพเจ้าตามวัตถุประสงค์ในการสมัครงานตามที่กล่าวข้างต้น บริษัท
                อาจเปิดเผยข้อมูลตามแบบฟอร์มใบสมัครและเอกสารอื่นใด ซึ่งข้าพเจ้าได้ส่งมอบประกอบการสมัครงานให้กับบริษัท
                ลานนาเกษตรอุตสาหกรรม จำกัด ในการนี้ข้าพเจ้าตกลงให้ดำเนินการ ดังนี้</p>

            <p>3.เกี่ยวกับข้อมูลชีวภาพ (Biometrics) ข้าพเจ้าทราบว่า หากข้าพเจ้าผ่านการพิจารณารับเข้าทำงาน
                บริษัทมีการเก็บบันทึกและใช้ข้อมูลชีวภาพ (Biometrics) ของพนักงาน เช่น ข้อมูลจำลองลายนิ้วมือ
                ข้อมูลจำลองใบหน้า ข้อมูลจำลองม่านตา น้ำหนัก ส่วนสูง เป็นต้นเพื่อใช้เป็นการบันทึกเวลาการทำงาน
                บันทึกเวลาเข้า-ออกงาน การเข้า-ออกสถานที่ทำงาน หรือ สถานที่อื่นใดของบริษัทฯ
                ใช้เพื่อการพัฒนาเพิ่มพูนความรู้ความสามารถเพิ่มทักษะ และ ศักยภาพ ในการทำงาน ตามระเบียบข้อบังคับของบริษัท
            </p>

            <p>4. ประวัติอาชญากรรม ข้าพเจ้าทราบว่า หากข้าพเจ้าผ่านการพิจารณา บริษัทมีการดำเนินการตรวจประวัติอาชญากรรม
                ทั้งนี้ บริษัทจำเป็นต้องเปิดเผยข้อมูลส่วนบุคคล เช่น ชื่อ-นามสกุล ที่อยู่ เบอร์โทรศัพท์
                เลขหรือสำเนาบัตรประจำตัวประชาชน รวมถึง สำเนาบัตรประจำตัวประชาชน เป็นต้น แก่กองทะเบียนประวัติอาชญากร
                สำนักงานตำรวจแห่งชาติ เพื่อดำเนินการตรวจประวัติอาชญากรรม และตกลงยินยอมให้ สำนักงานตำรวจแห่งชาติ
                ส่งข้อมูลผลการตรวจประวัติอาชญากรรมให้กับบริษัท เพื่อเก็บบันทึกและประกอบการจ้างงาน</p>

            <p>5.การเก็บรวบรวมข้อมูลส่วนบุคคลจากแหล่งอื่น ข้าพเจ้าทราบว่า
                ในการพิจารณาคุณสมบัติของข้าพเจ้าเพื่อการพิจารณารับเข้าทำงานของบริษัท
                บริษัทอาจมีการตรวจสอบและเก็บรวบรวมข้อมูลส่วนบุคคลจากแหล่งอื่น เช่น สื่อสังคมออนไลน์ เว็บไซต์หางาน
                สถาบันการศึกษา บุคคลอ้างอิง หรือจากบริษัทที่เคยร่วมงาน ตามข้อมูลซึ่งได้ให้ไว้</p>
            <div class="form-check">
                <input type="checkbox" name="accept1" id="accept1" class="form-check-input chkstep1 {{ $flag }}"
                    value="yes" @if ($testdata) checked @endif
                    @if (old('accept1'))
                        checked 
                    @endif
                    >
                <label class="form-check-label" for="accept1">ข้าพเจ้าอ่านข้อความแล้วและรับทราบตามข้อความดังกล่าว</label>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
