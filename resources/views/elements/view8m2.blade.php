{{-- @if (!empty($masterdata->militarydata->status)) --}}
    <h5 class='newh5'>ส่วนที่ 8 : กรณีผู้ชาย การรับราชการทหาร</h5>
    <div class="row">
        <div class='col-12'>
            @if (empty($masterdata->militarydata->status))
                ไม่ระบุ
            @else
                @if ($masterdata->militarydata->status == 'ยังไม่ได้รับการเกณฑ์ทหาร')
                    ยังไม่ได้รับการเกณฑ์ทหาร โดยจะเข้ารับการเกณฑ์ทหารในปี พ.ศ
                    <span class='answer'>{{ filldot($masterdata->militarydata->year, 20) }}</span>
                @else
                    @if ($masterdata->militarydata->status == 'เข้ารับการเกณฑ์ทหารแล้ว')
                        เข้ารับการเกณฑ์ทหารแล้ว เคยอยู่สังกัด <span
                            class='answer'>{{ filldot($masterdata->militarydata->custom1, 20) }}</span> เป็นเวลา <span
                            class='answer'>{{ filldot($masterdata->militarydata->duration, 20) }}</span> ปี ปลดประจำการเมื่อวันที่ <span
                            class='answer'>{{ filldot($masterdata->militarydata->custom2, 20) }}</span>
                    @else
                        @if ($masterdata->militarydata->status == 'เรียนรักษาดินแดน(ร.ด.)')
                            เรียนรักษาดินแดน(ร.ด.) <span
                                class='answer'>{{ filldot($masterdata->militarydata->year, 20) }}</span> ปี
                        @else
                            @if ($masterdata->militarydata->status == 'อื่นๆ')
                                อื่นๆ <span class='answer'>{{ filldot($masterdata->militarydata->custom1, 20) }}</span>
                            @else
                                <span class='answer'>{{ filldot($masterdata->militarydata->status, 20) }}</span>
                            @endif
                        @endif
                    @endif
                @endif
            @endif
        </div>
    </div>
{{-- @endif --}}
