<h3>ข้อมูลทั่วไป</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ข้อมูลทั่วไป</legend>

            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="current_edu_level">ปัจจุบันกำลังศึกษาอยู่ในระดับ</label>
                    <input id="current_edu_level" name="current_edu_level" type="text"
                        placeholder="ปัจจุบันกำลังศึกษาอยู่ในระดับ" class="form-control"
                        value="{{ old('current_edu_level') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="current_edu_dep">สาขาวิชา</label>
                    <input id="current_edu_dep" name="current_edu_dep" type="text" placeholder="สาขาวิชา"
                        class="form-control" value="{{ old('current_edu_dep') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="current_edu_year">ชั้นปีที่</label>
                    <input id="current_edu_year" name="current_edu_year" type="text" placeholder="ชั้นปีที่"
                        class="form-control" value="{{ old('current_edu_year') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="current_edu_school">ชื่อสถานศึกษา</label>
                    <input id="current_edu_school" name="current_edu_school" type="text" placeholder="ชื่อสถานศึกษา"
                        class="form-control" value="{{ old('current_edu_school') }}">
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <label for="future_edu_level">มีแผนการศึกษาต่อในระดับ</label>
                    <input id="future_edu_level" name="future_edu_level" type="text"
                        placeholder="มีแผนการศึกษาต่อในระดับ" class="form-control"
                        value="{{ old('future_edu_level') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="future_edu_dep">สาขาวิชา</label>
                    <input id="future_edu_dep" name="future_edu_dep" type="text" placeholder="สาขาวิชา"
                        class="form-control" value="{{ old('future_edu_dep') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="future_edu_year">ในปี พ.ศ</label>
                    <input id="future_edu_year" name="future_edu_year" type="text" placeholder="ในปี พ.ศ"
                        class="form-control" value="{{ old('future_edu_year') }}">
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="future_edu_school">ชื่อสถานศึกษา</label>
                    <input id="future_edu_school" name="future_edu_school" type="text" placeholder="ชื่อสถานศึกษา"
                        class="form-control" value="{{ old('future_edu_school') }}">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="form-check col-6 col-xl-2">
                            <label for="hobby_1">งานอดิเรก 1</label>
                            <input id="hobby_1" name="hobby_1" type="text" placeholder="งานอดิเรก"
                                class="form-control" value="{{ old('hobby_1') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="hobby_2">งานอดิเรก 2</label>
                            <input id="hobby_2" name="hobby_2" type="text" placeholder="งานอดิเรก"
                                class="form-control" value="{{ old('hobby_2') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="hobby_3">งานอดิเรก 3</label>
                            <input id="hobby_3" name="hobby_3" type="text" placeholder="งานอดิเรก"
                                class="form-control" value="{{ old('hobby_3') }}">
                        </div>

                        <div class="form-check col-6 col-xl-2">
                            <label for="drunker">การดื่มสุรา</label>
                            <select class="form-control" id="drunker" name="drunker">
                                <option value="">=== เลือก ===</option>
                                <option value="ไม่เคยดื่ม"
                                    @if (old('drunker') == 'ไม่เคยดื่ม') {{ 'selected' }} @endif>ไม่เคยดื่ม</option>
                                <option value="เลิกดื่มแล้ว"
                                    @if (old('drunker') == 'เลิกดื่มแล้ว') {{ 'selected' }} @endif>เลิกดื่มแล้ว</option>
                                <option value="ดื่มเป็นครั้งคราว"
                                    @if (old('drunker') == 'ดื่มเป็นครั้งคราว') {{ 'selected' }} @endif>ดื่มเป็นครั้งคราว
                                </option>
                                <option value="ดื่มเป็นประจำ"
                                    @if (old('drunker') == 'ดื่มเป็นประจำ') {{ 'selected' }} @endif>ดื่มเป็นประจำ</option>
                            </select>
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="smoker">การสูบบุหรี่</label>
                            <select class="form-control" id="smoker" name="smoker">
                                <option value="">=== เลือก ===</option>
                                <option value="ไม่เคยสูบ"
                                    @if (old('smoker') == 'ไม่เคยสูบ') {{ 'selected' }} @endif>ไม่เคยสูบ</option>
                                <option value="เลิกสูบแล้ว"
                                    @if (old('smoker') == 'เลิกสูบแล้ว') {{ 'selected' }} @endif>เลิกสูบแล้ว</option>
                                <option value="สูบเป็นครั้งคราว"
                                    @if (old('smoker') == 'สูบเป็นครั้งคราว') {{ 'selected' }} @endif>สูบเป็นครั้งคราว
                                </option>
                                <option value="สูบทุกวัน"
                                    @if (old('smoker') == 'สูบทุกวัน') {{ 'selected' }} @endif>สูบทุกวัน</option>
                            </select>
                        </div>
                        <div class="form-check col-6 col-xl-1 smoker d-none">
                            <label for="smoker_other">วันละ(มวน)</label>
                            <input id="smoker_other" name="smoker_other" type="text" placeholder="จำนวนมวน"
                                class="form-control" value="{{ old('smoker_other') }}">
                        </div>

                    </div>


                </div>
                <div class="col-12">
                    <div class='row'>
                        <div class="form-check col-6 col-xl-2">
                            <label for="criminal">การต้องคดีอาญาถึงจำคุกหรือต้องคำพิพากษาถึงที่สุดให้จำคุก</label>
                            <select class="form-control" id="criminal" name="criminal">
                                <option value="">=== เลือก ===</option>
                                <option value="เคย" @if (old('criminal') == 'เคย') {{ 'selected' }} @endif>
                                    เคย</option>
                                <option value="ไม่เคย" @if (old('criminal') == 'ไม่เคย') {{ 'selected' }} @endif>
                                    ไม่เคย</option>
                            </select>
                        </div>
                        <div class="form-check col-6 col-xl-3 criminal d-none">
                            <label for="criminal_other">ระบุรายละเอียด</label>
                            <input id="criminal_other" name="criminal_other" type="text"
                                placeholder="ระบุรายละเอียด" class="form-control"
                                value="{{ old('criminal_other') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="control">การถูกปลดออกจากหน้าที่เนื่องจากความประพฤติไม่เหมาะสม /
                                การปฏิบัติงานบกพร่อง</label>
                            <select class="form-control" id="control" name="control">
                                <option value="">=== เลือก ===</option>
                                <option value="เคย" @if (old('control') == 'เคย') {{ 'selected' }} @endif>
                                    เคย</option>
                                <option value="ไม่เคย" @if (old('control') == 'ไม่เคย') {{ 'selected' }} @endif>
                                    ไม่เคย</option>
                            </select>
                        </div>
                        <div class="form-check col-6 col-xl-3 control d-none">
                            <label for="control_other">ระบุรายละเอียด</label>
                            <input id="control_other" name="control_other" type="text"
                                placeholder="ระบุรายละเอียด" class="form-control"
                                value="{{ old('control_other') }}">
                        </div>
                        <div class="form-check col-6 col-xl-2">
                            <label for="can_other_country">การไปปฏิบัติงานต่างจังหวัด และ ต่างประเทศ</label>
                            <select class="form-control " id="can_other_country"
                                name="can_other_country">
                                <option value="">=== เลือก ===</option>
                                <option value="ได้" @if (old('can_other_country') == 'ได้') {{ 'selected' }} @endif>
                                    ได้</option>
                                <option value="ไม่ได้" @if (old('can_other_country') == 'ไม่ได้') {{ 'selected' }} @endif>
                                    ไม่ได้</option>
                            </select>
                        </div>
                        <div class="form-check col-6 col-xl-3 upcontry d-none">
                            <label for="can_other_country_case">เนื่องจาก</label>
                            <input id="can_other_country_case" name="can_other_country_case" type="text"
                                placeholder="เนื่องจาก" class="form-control"
                                value="{{ old('can_other_country_case') }}">
                        </div>



                    </div>
                </div>
                <div class="col-12">
                    <div class='row'>
                        <div class="col-1">
                            <label for="apply_job_no">สมัครงานกับบริษัทฯ ครั้งนี้เป็นครั้งที่</label>
                            <input id="apply_job_no" name="apply_job_no" type="text" placeholder="ระบุครั้งที่"
                                class="form-control" value="{{ old('apply_job_no') }}">
                        </div>
                        <div class="col-2">
                            <label for="apply_job_name">เคยสอบข้อเขียน/สอบสัมภาษณ์กับบริษัทฯ ในตำแหน่ง</label>
                            <input id="apply_job_name" name="apply_job_name" type="text"
                                placeholder="ระบุตำแหน่ง" class="form-control"
                                value="{{ old('apply_job_name') }}">
                        </div>
                        <div class="col-2">
                            <label for="apply_job_company">เหตุผลที่สมัครเข้าทำงานกับบริษัทฯ</label>
                            <input id="apply_job_company" name="apply_job_company" type="text"
                                placeholder="ระบุตำแหน่ง" class="form-control"
                                value="{{ old('apply_job_company') }}">
                        </div>
                        <div class="col-2">
                            <label for="hear_from">ทราบข่าวการรับสมัครงาน</label>
                            <select class="form-control " id="hear_from" name="hear_from">
                                <option value="">=== เลือก ===</option>
                                <option value="สื่อทางอินเตอร์เน็ต"
                                    @if (old('hear_from') == 'สื่อทางอินเตอร์เน็ต') {{ 'selected' }} @endif>สื่อทางอินเตอร์เน็ต
                                </option>
                                <option value="สนง.จัดหางาน จังหวัด"
                                    @if (old('hear_from') == 'สนง.จัดหางาน จังหวัด') {{ 'selected' }} @endif>สนง.จัดหางาน จังหวัด
                                </option>
                                <option value="เพื่อนหรือคนรู้จัก"
                                    @if (old('hear_from') == 'เพื่อนหรือคนรู้จัก') {{ 'selected' }} @endif>เพื่อนหรือคนรู้จัก
                                </option>
                                <option value="สื่ออื่นๆ"
                                    @if (old('hear_from') == 'สื่ออื่นๆ') {{ 'selected' }} @endif>สื่ออื่นๆ</option>
                            </select>
                        </div>
                        <div class="col-2 hear_from1 d-none">
                            <label for="hear_from_other">จังหวัด</label>
                            <input id="hear_from_other" name="hear_from_other" type="text" placeholder="จังหวัด"
                                class="form-control" value="{{ old('hear_from_other') }}">
                        </div>
                        <div class="col-2 hear_from2 d-none">
                            <label for="hear_from_other">ระบุ</label>
                            <input id="hear_from_other" name="hear_from_other" type="text" placeholder="ระบุ"
                                class="form-control" value="{{ old('hear_from_other') }}">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class='row'>
                        <div class="col-3">
                            <label for="refer_name">บุคคลที่แนะนำให้มาสมัครงาน หรือ ญาติพี่น้อง เพื่อน หรือคนรู้จัก
                                ที่เป็นพนักงานของบริษัทฯ (ถ้ามี) </label>
                            <input id="refer_name" name="refer_name" type="text" placeholder="ระบุชื่อ"
                                class="form-control" value="{{ old('refer_name') }}">
                        </div>
                        <div class="col-4">
                            <label for="your_comments">ความรู้ความเข้าใจที่มีต่อ บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด
                                มีดังนี้</label>
                            <input id="your_comments" name="your_comments" type="text" placeholder="รายละเอียด"
                                class="form-control" value="{{ old('your_comments') }}">
                        </div>
                        <div class="col-2">
                            <label for="can_start_in">หากบริษัทฯ รับเข้าทำงาน สามารถเริ่มงานได้ภายใน (วัน)</label>
                            <input id="can_start_in" name="can_start_in" type="text" placeholder="ระบุวัน"
                                class="form-control" value="{{ old('can_start_in') }}">
                        </div>
                        <div class="col-3">
                            <label for="can_start_desc">เนื่องจาก</label>
                            <input id="can_start_desc" name="can_start_desc" type="text" placeholder="ระบุ"
                                class="form-control" value="{{ old('can_start_desc') }}">
                        </div>
                    </div>
                </div>
            </div>

            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>


</fieldset>
