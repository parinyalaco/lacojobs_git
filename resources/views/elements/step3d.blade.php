<h3>ประวัติการทำงาน</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>
                ประวัติการทำงาน(จากปัจจุบันตามลำดับ)</legend>
            <div class="row">
                <div class="form-check col-6 col-xl-1">
                    <b>ลำดับที่ 1</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_start_date_1">ตั้งแต่ ว/ด/ป </label>
                    <input id="exp_start_date_1" name="exp_start_date_1" id="exp_start_date_1" type="date"
                        class="form-control" value="{{ old('exp_start_date_1') }}">
                    @error('exp_start_date_1')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_end_date_1">ถึง ว/ด/ป</label>
                    <input id="exp_end_date_1" name="exp_end_date_1" id="exp_end_date_1" type="date"
                        class="form-control" value="{{ old('exp_end_date_1') }}">
                    @error('exp_end_date_1')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="exp_company_1">ชื่อสถานที่ทำงาน</label>
                    <input id="exp_company_1" name="exp_company_1" id="exp_company_1" type="text" class="form-control"
                        value="{{ old('exp_company_1') }}">
                    @error('exp_company_1')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_province_1">จังหวัด</label>
                    <input id="exp_province_1" name="exp_province_1" type="text"
                        class="form-control" value="{{ old('exp_province_1') }}">
                    @error('exp_province_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_job_1">ตำแหน่ง/ลักษณะงานที่ทำ</label>
                    <input id="exp_job_1" name="exp_job_1" type="text"
                        class="form-control" value="{{ old('exp_job_1') }}">
                    @error('exp_job_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_salary_1">เงินเดือน</label>
                    <input id="exp_salary_1" name="exp_salary_1" type="number" class="form-control"
                        value="{{ old('exp_salary_1') }}">
                    @error('exp_salary_1')
                        <div class="alert alert-danger">กรุณาใส่เงินเดือน</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-10">
                    <label for="exp_case_1">เหตุที่ลาออก</label>
                    <input name="exp_case_1" id="exp_case_1" type="text" class="form-control"
                        value="{{ old('exp_case_1') }}">
                    @error('exp_case_1')
                        <div class="alert alert-danger">กรุณาใส่เหตุที่ลาออก</div>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-check col-6 col-xl-1">
                    <b>ลำดับที่ 2</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_start_date_2">ตั้งแต่ ว/ด/ป </label>
                    <input id="exp_start_date_2" name="exp_start_date_2" id="exp_start_date_2" type="date"
                        class="form-control" value="{{ old('exp_start_date_2') }}">
                    @error('exp_start_date_2')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_end_date_2">ถึง ว/ด/ป</label>
                    <input id="exp_end_date_2" name="exp_end_date_2" id="exp_end_date_2" type="date"
                        class="form-control" value="{{ old('exp_end_date_2') }}">
                    @error('exp_end_date_2')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="exp_company_2">ชื่อสถานที่ทำงาน</label>
                    <input id="exp_company_2" name="exp_company_2" id="exp_company_2" type="text" class="form-control"
                        value="{{ old('exp_company_2') }}">
                    @error('exp_company_2')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_province_2">จังหวัด</label>
                    <input id="exp_province_2" name="exp_province_2" type="text"
                        class="form-control" value="{{ old('exp_province_2') }}">
                    @error('exp_province_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_job_2">ตำแหน่ง/ลักษณะงานที่ทำ</label>
                    <input id="exp_job_2" name="exp_job_2" type="text"
                        class="form-control" value="{{ old('exp_job_2') }}">
                    @error('exp_job_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_salary_2">เงินเดือน</label>
                    <input id="exp_salary_2" name="exp_salary_2" type="number" class="form-control"
                        value="{{ old('exp_salary_2') }}">
                    @error('exp_salary_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-10">
                    <label for="exp_case_2">เหตุที่ลาออก</label>
                    <input name="exp_case_2" id="exp_case_2" type="text" class="form-control"
                        value="{{ old('exp_case_2') }}">
                    @error('exp_case_2')
                        <div class="alert alert-danger">กรุณาใส่เหตุที่ลาออก</div>
                    @enderror
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
