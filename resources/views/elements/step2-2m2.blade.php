<h3>ที่อยู่</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ที่อยู่ปัจจุบัน</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="addr1">เลขที่ *</label>
                    <input id="addr1" name="addr1" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ที่อยู่." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="addr2">หมู่ที่ *</label>
                    <input id="addr2" name="addr2" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ที่อยู่." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="block">ตรอก/ซอย</label>
                    <input id="block" name="block" type="text" class="form-control"
                        @if ($testdata) value = "ตรอก/ซอย." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="road">ถนน</label>
                    <input id="road" name="road" type="text" class="form-control"
                        @if ($testdata) value = "ที่อยู่." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="subdistrict">ตำบล *</label>
                    <input id="subdistrict" name="subdistrict" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "ตำบล." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="district">อำเภอ *</label>
                    <input id="district" name="district" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "อำเภอ." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="province">จังหวัด *</label>
                    <input id="province" name="province" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "จังหวัด." @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="zipcode">รหัสไปรษณีย์ *</label>
                    <input id="zipcode" name="zipcode" type="text" class="form-control {{ $flag }}"
                        @if ($testdata) value = "50000" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="tel">โทรศัพท์บ้าน</label>
                    <input id="tel" name="tel" type="text" class="form-control"
                        @if ($testdata) value = "100000000" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="mobile">โทรศัพท์มือถือ *</label>
                    <input id="mobile" name="mobile" type="text" class="form-control {{ $flag }}" 
                    maxlength="10" minlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, '');"
                        @if ($testdata) value = "100000000" @endif>
                </div>
            </div>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                       <label for="addr_type">ที่อยู่ปัจจุบันนี้เป็น *</label>
                        <select class="form-control {{ $flag }}" id="addr_type" name="addr_type">
                            <option value="" >=== เลือก ===</option>
                            <option value="บ้านส่วนตัว"
                    @if ($testdata)
                        selected
                    @endif
                    >บ้านส่วนตัว</option>
                            <option value="บ้านของญาติ" >บ้านของญาติ</option>
                            <option value="หอพัก" >หอพัก</option>
                        </select>
                    </div>
                    <div id="addr_type_div_1" class="form-check col-xl-2 col-6 d-none">
                        <label for="addr_type1_custom1">ระบุญาติ</label>
                        <input id="addr_type1_custom1" name="addr_type1_custom1" type="text" class="form-control">
                    </div>
                    <div id="addr_type_div_21" class="form-check col-xl-2 col-6 d-none">
                        <label for="addr_type2_custom1">หอพักชื่อ</label>
                        <input id="addr_type2_custom1" name="addr_type2_custom1" type="text" class="form-control">
                    </div>
                    <div id="addr_type_div_22" class="form-check col-xl-2 col-6 d-none">
                        <label for="addr_type2_custom2">หมายเลขห้อง</label>
                        <input id="addr_type2_custom2" name="addr_type2_custom2" type="text" class="form-control">
                    </div>

            </div>
            <legend>ในกรณีฉุกเฉิน สามารถติดต่อกับ</legend>
            <div class="row">
                <div class="form-check col-xl-2 col-6">
                    <label for="urgent_init">คำนำหน้า</label>
                    <input id="urgent_init" name="urgent_init" type="text" class="form-control"
                        @if ($testdata) value = "นาย" @endif>
                </div>
                <div class="form-check col-xl-3 col-6">
                    <label for="urgent_name">ชื่อ-สกุล</label>
                    <input id="urgent_name" name="urgent_name" type="text" class="form-control"
                        @if ($testdata) value = "ทดลอง" @endif>
                </div>

                <div class="form-check col-xl-3 col-6">
                    <label for="urgent_addr1">ที่อยู่</label>
                    <input id="urgent_addr1" name="urgent_addr1" type="text" class="form-control"
                        @if ($testdata) value = "ดหกหฟกดหกดหกดกหด" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="urgent_mobile">โทรศัพท์</label>
                    <input id="urgent_mobile" name="urgent_mobile" type="text" class="form-control"
                        @if ($testdata) value = "123654987" @endif>
                </div>
                <div class="form-check col-xl-2 col-6">
                    <label for="urgent_relation">ความสัมพันธ์เป็น</label>
                    <input id="urgent_relation" name="urgent_relation" type="text"
                        class="form-control"
                        @if ($testdata) value = "น้า" @endif>
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
