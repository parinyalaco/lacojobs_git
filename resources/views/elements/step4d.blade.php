<h3>ข้อมูลทั่วไป</h3>
<fieldset>
    <div class="row">
    <div class="col-12">
        <legend>ข้อมูลทั่วไป</legend>
        <div class="row">
            <div class="form-check col-12 col-xl-6">
                <label for="refer_name">1. บุคคลที่แนะนำให้มาสมัครงานที่เป็นพนักงานในบริษัทฯ(ถ้ามี)</label>
                <input id="refer_name" name="refer_name" type="text" placeholder="ระบุชื่อ" class="form-control"
                    value="{{ old('refer_name') }}">
            </div>
            
            <div class="form-check col-12 col-xl-6">
                <label for="refer_relation">มีความสัมพันธ์</label>
                <input id="refer_relation" name="refer_relation" type="text" placeholder="ความสัมพันธ์" class="form-control"
                    value="{{ old('refer_relation') }}">
            </div>

            <div class="form-check col-12 col-xl-4">
                <label for="contact_name">2.ในกรณีฉุกเฉินทางบริษัทฯ สามารถติดต่อกับ *</label>
                <input id="contact_name" name="contact_name" type="text" placeholder="ระบุชื่อ" class="form-control  {{ $flag }}"
                    value="{{ old('contact_name') }}">
            </div>
            <div class="form-check col-6 col-xl-2">
                <label for=refer_addr1">เลขที่ *</label>
                <input id="refer_addr1" name="refer_addr1" type="text" placeholder="เลขที่" class="form-control  {{ $flag }}"
                    value="{{ old('refer_addr1') }}">
            </div>
            <div class="form-check col-6 col-xl-2">
                <label for="refer_addr2">หมู่ที่ *</label>
                <input id="refer_addr2" name="refer_addr2" type="text" placeholder="หมู่ที่" class="form-control  {{ $flag }}"
                    value="{{ old('refer_addr2') }}">
            </div>
            <div class="form-check col-6 col-xl-2">
                <label for="contact_tumbon">ตำบล *</label>
                <input id="contact_tumbon" name="contact_tumbon" type="text" placeholder="ตำบล" class="form-control  {{ $flag }}"
                    value="{{ old('contact_tumbon') }}">
            </div>
            <div class="form-check col-6 col-xl-2">
                <label for="contact_aumpher">อำเภอ *</label>
                <input id="contact_aumpher" name="contact_aumpher" type="text" placeholder="อำเภอ" class="form-control  {{ $flag }}"
                    value="{{ old('contact_aumpher') }}">
            </div>
            
            <div class="form-check col-6 col-xl-2">
                <label for="contact_province">จังหวัด *</label>
                <input id="contact_province" name="contact_province" type="text" placeholder="จังหวัด" class="form-control  {{ $flag }}"
                    value="{{ old('contact_province') }}">
            </div>
            
                <div class="form-check col-6  col-xl-2">
                    <label for="contact_tel">โทรศัพท์ *</label>
                    <input id="contact_tel" name="contact_tel" type="number" maxlength="10" class="form-control {{ $flag }}"
                        value="{{ old('contact_tel') }}" @if ($testdata) value = "100000000" @endif>
                    @error('tel')
                        <div class="alert alert-danger">กรุณาใส่เบอร์โทรศัพท์ 10 หลัก</div>
                    @enderror
                </div>
                <div class="form-check col-12 col-xl-6">
                <label for="contact_relation">มีความสัมพันธ์ *</label>
                <input id="contact_relation" name="contact_relation" type="text" placeholder="ความสัมพันธ์" class="form-control  {{ $flag }}"
                    value="{{ old('contact_relation') }}">
            </div>
            <div class="form-check col-12 col-xl-6">
                    <label for="criminal">3. การต้องคดีอาญาถึงจำคุกหรือต้องคำพิพากษาถึงที่สุดให้จำคุก *</label>
                       <select class="form-control {{ $flag }} " id="criminal" name="criminal">
                            <option value="" >=== เลือก ===</option>
                            <option value="เคย" {{ old('criminal') == 'เคย' ? 'selected' : '' }}>เคย</option>
                            <option value="ไม่เคย" {{ old('criminal') == 'ไม่เคย' ? 'selected' : '' }}>ไม่เคย</option>
                        </select>
                   </div>
                   <div class="form-check col-12 col-xl-6 criminal d-none">
                       <label for="criminal_other">ระบุรายละเอียด</label>
                        <input id="criminal_other" name="criminal_other"  value="{{ old('criminal_other') }}" type="text" placeholder="ระบุรายละเอียด" class="form-control">
                   </div>
        </div>
        <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
    </div>
    </div>
</fieldset>
