<h5 class='newh5'>ส่วนที่ 11 : สุขภาพและความพร้อมทางร่างกาย</h5>
@if (isset($masterdata->healthdata))
<div class='row'>
    <div class='col-4'>     
        1.	โรคประจำตัว <span class="answer" >{{ $masterdata->healthdata->congenital_disease }}</span> 
        @if ($masterdata->healthdata->congenital_disease == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อโรค <span class="answer" >{{ $masterdata->healthdata->congenital_disease_other }}</span>
        @endif     
    </div>
        <div class='col-4'>     
        2.	โรคติดต่อ/โรคเรื้อรัง <span class="answer" >{{ $masterdata->healthdata->contagious_disease }}</span> 
        @if ($masterdata->healthdata->contagious_disease == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อโรค <span class="answer" >{{ $masterdata->healthdata->contagious_disease_other }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        3.	โรคภูมิแพ้ <span class="answer" >{{ $masterdata->healthdata->allergy }}</span> 
        @if ($masterdata->healthdata->allergy == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อโรค <span class="answer" >{{ $masterdata->healthdata->allergy_other }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        4.	อาการปวดหลัง ปวดกล้ามเนื้อ  <span class="answer" >{{ $masterdata->healthdata->back_pain }}</span>                                
    </div>
    <div class='col-4'>     
        5.	อาการชาตามกล้ามเนื้อ นิ้ว มือ  <span class="answer" >{{ $masterdata->healthdata->finger_pain }}</span>                                
    </div>
    <div class='col-4'>     
        6.	โรคธาลัสซีเมีย	  <span class="answer" >{{ $masterdata->healthdata->thalassemia }}</span>                                
    </div>
    <div class='col-4'>     
        7.	ไวรัสตับอักเสบ A B C  <span class="answer" >{{ $masterdata->healthdata->liver_virus }}</span>                                
    </div>
    <div class='col-4'>     
        8.	แพ้อาหาร <span class="answer" >{{ $masterdata->healthdata->food_allergy }}</span> 
        @if ($masterdata->healthdata->food_allergy == 'เคยเป็น/กำลังเป็น')
            ระบุชื่ออาหารที่แพ้ <span class="answer" >{{ $masterdata->healthdata->food_allergy_other }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        9.	แพ้ยา <span class="answer" >{{ $masterdata->healthdata->drug_allergy }}</span> 
        @if ($masterdata->healthdata->drug_allergy == 'เคยเป็น/กำลังเป็น')
            ระบุชื่อยาที่แพ้ <span class="answer" >{{ $masterdata->healthdata->drug_allergy_other }}</span>
        @endif     
    </div>
    <div class='col-4'>     
        10.	โรค/อาการอื่นๆ(ระบุ) <span class="answer" >{{ $masterdata->healthdata->other_disease }}</span>     
    </div>
    <div class='col-12'>     
        11.	ได้รับการตรวจสุขภาพครั้งสุดท้ายเมื่อ <span class="answer" >{{ $masterdata->healthdata->physical_exam_time }}</span> ส่วนของร่างกายที่ได้รับการตรวจ <span class="answer" >{{ $masterdata->healthdata->physical_exam_part }}</span>
        ผลการตรวจ <span class="answer" >{{ $masterdata->healthdata->physical_exam_result }}</span>
        @if ($masterdata->healthdata->physical_exam_result == 'ไม่ปกติ')
        (ระบุรายละเอียด) <span class="answer" >{{ $masterdata->healthdata->physical_exam_result_other }}</span>
        @endif
    </div>
    <div class='col-12'>     
        12.	ชื่อสถานที่ทำงานก่อนหน้านี้ซึ่งมีการทำประกันสังคม <span class="answer" >{{ $masterdata->healthdata->prev_social_sec_company }}</span>
        โรงพยาบาลที่ใช้สิทธิรักษาพยาบาล <span class="answer" >{{ $masterdata->healthdata->prev_social_sec_hospital }}</span>
    </div>
</div>
@endif