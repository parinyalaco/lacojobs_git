<h3>ความสามารถพิเศษ</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ความสามารถพิเศษ</legend>
            <div class="row">
                <div class="form-check col-12"><b>คอมพิวเตอร์ โปรแกรม</b>
                </div>
                @for ($i = 1; $i < 4; $i++)
                    <div class="form-check col-6 col-xl-4">
                        <input id="expd_com_program{{ $i }}" name="expd_com_program{{ $i }}"
                            type="text" class="form-control">
                        <select class="form-control" id="expd_com_level{{ $i }}"
                            name="expd_com_level{{ $i }}">
                            <option value="">=== เลือก ===</option>
                            <option value="ดีมาก">ดีมาก</option>
                            <option value="ดี">ดี</option>
                            <option value="พอใช้">พอใช้</option>
                        </select>
                    </div>
                @endfor
            </div>
            <div class="row">
                <div class="form-check col-12"><b>ความสามารถด้านภาษา</b></div>
                <div class="form-check col-6 col-xl-3 text-center align-middle">
                    <label for="expd_lang_speak_th">ภาษาไทย</label>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_th">การพูด</label>
                    <select class="form-control" id="expd_lang_speak_th" name="expd_lang_speak_th">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_th">การอ่าน</label>
                    <select class="form-control" id="expd_lang_read_th" name="expd_lang_read_th">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_th">การเขียน</label>
                    <select class="form-control " id="expd_lang_write_th" name="expd_lang_write_th">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3 text-center align-items-center">
                    <label for="expd_lang_speak_en">ภาษาอังกฤษ</label>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_en">การพูด</label>
                    <select class="form-control" id="expd_lang_speak_en" name="expd_lang_speak_en">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_en">การอ่าน</label>
                    <select class="form-control " id="expd_lang_read_en" name="expd_lang_read_en">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_en">การเขียน</label>
                    <select class="form-control " id="expd_lang_write_en" name="expd_lang_write_en">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3 text-center align-items-center">
                    <label for="expd_lang_speak_ch">ภาษาจีน</label>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_ch">การพูด</label>
                    <select class="form-control " id="expd_lang_speak_ch" name="expd_lang_speak_ch">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_ch">การอ่าน</label>
                    <select class="form-control  " id="expd_lang_read_ch" name="expd_lang_read_ch">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_ch">การเขียน</label>
                    <select class="form-control  " id="expd_lang_write_ch" name="expd_lang_write_ch">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>

                <div class="form-check col-6 col-xl-3 text-center align-items-center">
                    <label for="expd_lang_ex">ภาษาอื่น</label>
                    <input id="expd_lang_ex" name="expd_lang_ex" type="text" placeholder="ระบุภาษา"
                        class="form-control">
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_speak_ex">การพูด</label>
                    <select class="form-control " id="expd_lang_speak_ex" name="expd_lang_speak_ex">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_read_ex">การอ่าน</label>
                    <select class="form-control " id="expd_lang_read_ex" name="expd_lang_read_ex">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="expd_lang_write_ex">การเขียน</label>
                    <select class="form-control " id="expd_lang_write_ex" name="expd_lang_write_ex">
                        <option value="">=== เลือก ===</option>
                        <option value="ดีมาก">ดีมาก</option>
                        <option value="ดี">ดี</option>
                        <option value="พอใช้">พอใช้</option>
                    </select>
                </div>
            </div>
        </div>
        <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
    </div>


</fieldset>
