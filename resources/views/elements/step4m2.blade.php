<h3>ประวัติการทำงาน</h3>
<fieldset>
    <div class="row">
        <div class="col-12">

            <legend>ประวัติการทำงาน(เรียงลำดับจากล่าสุดตามลำดับ)</legend>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 1</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_start_date_1">ตั้งแต่ ว/ด/ป </label>
                    <input id="exp_start_date_1" name="exp_start_date_1" type="date"
                        class="form-control" value="{{ old('exp_start_date_1') }}">
                    @error('exp_start_date_1')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_end_date_1">ถึง ว/ด/ป</label>
                    <input id="exp_end_date_1" name="exp_end_date_1"  type="date"
                        class="form-control" value="{{ old('exp_end_date_1') }}">
                    @error('exp_end_date_1')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="exp_company_1">ชื่อสถานที่ทำงาน</label>
                    <input id="exp_company_1" name="exp_company_1"  type="text" class="form-control"
                        value="{{ old('exp_company_1') }}">
                    @error('exp_company_1')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_province_1">จังหวัด</label>
                    <input id="exp_province_1" name="exp_province_1" type="text"
                        class="form-control" value="{{ old('exp_province_1') }}">
                    @error('exp_province_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="exp_tel_1">เบอร์ติดต่อ</label>
                    <input id="exp_tel_1" name="exp_tel_1" type="text"
                        class="form-control" value="{{ old('exp_tel_1') }}">
                    @error('exp_tel_1')
                        <div class="alert alert-danger">เบอร์ติดต่อ</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="exp_job_1">ตำแหน่ง/ลักษณะงานที่ทำ</label>
                    <input id="exp_job_1" name="exp_job_1" type="text"
                        class="form-control" value="{{ old('exp_job_1') }}">
                    @error('exp_job_1')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_salary_1">เงินเดือน</label>
                    <input id="exp_salary_1" name="exp_salary_1" type="number" class="form-control"
                        value="{{ old('exp_salary_1') }}">
                    @error('exp_salary_1')
                        <div class="alert alert-danger">กรุณาใส่เงินเดือน</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-8">
                    <label for="exp_case_1">เหตุที่ลาออก</label>
                    <input name="exp_case_1" id="exp_case_1" type="text" class="form-control"
                        value="{{ old('exp_case_1') }}">
                    @error('exp_case_1')
                        <div class="alert alert-danger">กรุณาใส่เหตุที่ลาออก</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 2</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_start_date_2">ตั้งแต่ ว/ด/ป </label>
                    <input id="exp_start_date_2" name="exp_start_date_2" type="date"
                        class="form-control" value="{{ old('exp_start_date_2') }}">
                    @error('exp_start_date_2')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_end_date_2">ถึง ว/ด/ป</label>
                    <input id="exp_end_date_2" name="exp_end_date_2" type="date"
                        class="form-control" value="{{ old('exp_end_date_2') }}">
                    @error('exp_end_date_2')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="exp_company_2">ชื่อสถานที่ทำงาน</label>
                    <input id="exp_company_2" name="exp_company_2" type="text" class="form-control"
                        value="{{ old('exp_company_2') }}">
                    @error('exp_company_2')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_province_2">จังหวัด</label>
                    <input id="exp_province_2" name="exp_province_2" type="text"
                        class="form-control" value="{{ old('exp_province_2') }}">
                    @error('exp_province_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>


                <div class="form-check col-6 col-xl-2">
                    <label for="exp_tel_2">เบอร์ติดต่อ</label>
                    <input id="exp_tel_2" name="exp_tel_2" type="text"
                        class="form-control" value="{{ old('exp_tel_2') }}">
                    @error('exp_tel_2')
                        <div class="alert alert-danger">เบอร์ติดต่อ</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_job_2">ตำแหน่ง/ลักษณะงานที่ทำ</label>
                    <input id="exp_job_2" name="exp_job_2" type="text"
                        class="form-control" value="{{ old('exp_job_2') }}">
                    @error('exp_job_2')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_salary_2">เงินเดือน</label>
                    <input id="exp_salary_2" name="exp_salary_2" type="number" class="form-control"
                        value="{{ old('exp_salary_2') }}">
                    @error('exp_salary_2')
                        <div class="alert alert-danger">กรุณาใส่เงินเดือน</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-8">
                    <label for="exp_case_2">เหตุที่ลาออก</label>
                    <input name="exp_case_2" id="exp_case_2" type="text" class="form-control"
                        value="{{ old('exp_case_2') }}">
                    @error('exp_case_2')
                        <div class="alert alert-danger">กรุณาใส่เหตุที่ลาออก</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 3</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_start_date_3">ตั้งแต่ ว/ด/ป </label>
                    <input  name="exp_start_date_3" id="exp_start_date_3" type="date"
                        class="form-control" value="{{ old('exp_start_date_3') }}">
                    @error('exp_start_date_3')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_end_date_3">ถึง ว/ด/ป</label>
                    <input name="exp_end_date_3" id="exp_end_date_3" type="date"
                        class="form-control" value="{{ old('exp_end_date_3') }}">
                    @error('exp_end_date_3')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="exp_company_3">ชื่อสถานที่ทำงาน</label>
                    <input  name="exp_company_3" id="exp_company_3" type="text" class="form-control"
                        value="{{ old('exp_company_3') }}">
                    @error('exp_company_3')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_province_3">จังหวัด</label>
                    <input id="exp_province_3" name="exp_province_3" type="text"
                        class="form-control" value="{{ old('exp_province_3') }}">
                    @error('exp_province_3')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>


                <div class="form-check col-6 col-xl-2">
                    <label for="exp_tel_3">เบอร์ติดต่อ</label>
                    <input id="exp_tel_3" name="exp_tel_3" type="text"
                        class="form-control" value="{{ old('exp_tel_3') }}">
                    @error('exp_tel_3')
                        <div class="alert alert-danger">เบอร์ติดต่อ</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_job_3">ตำแหน่ง/ลักษณะงานที่ทำ</label>
                    <input id="exp_job_3" name="exp_job_3" type="text"
                        class="form-control" value="{{ old('exp_job_3') }}">
                    @error('exp_job_3')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_salary_3">เงินเดือน</label>
                    <input id="exp_salary_3" name="exp_salary_3" type="number" class="form-control"
                        value="{{ old('exp_salary_3') }}">
                    @error('exp_salary_3')
                        <div class="alert alert-danger">กรุณาใส่เงินเดือน</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-8">
                    <label for="exp_case_3">เหตุที่ลาออก</label>
                    <input name="exp_case_3" id="exp_case_3" type="text" class="form-control"
                        value="{{ old('exp_case_3') }}">
                    @error('exp_case_3')
                        <div class="alert alert-danger">กรุณาใส่เหตุที่ลาออก</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 4</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_start_date_4">ตั้งแต่ ว/ด/ป </label>
                    <input id="exp_start_date_4" name="exp_start_date_4" type="date"
                        class="form-control" value="{{ old('exp_start_date_4') }}">
                    @error('exp_start_date_4')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_end_date_4">ถึง ว/ด/ป</label>
                    <input id="exp_end_date_4" name="exp_end_date_4" type="date"
                        class="form-control" value="{{ old('exp_end_date_4') }}">
                    @error('exp_end_date_4')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="exp_company_4">ชื่อสถานที่ทำงาน</label>
                    <input id="exp_company_4" name="exp_company_4" type="text" class="form-control"
                        value="{{ old('exp_company_4') }}">
                    @error('exp_company_4')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_province_4">จังหวัด</label>
                    <input id="exp_province_4" name="exp_province_4" type="text"
                        class="form-control" value="{{ old('exp_province_4') }}">
                    @error('exp_province_4')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>


                <div class="form-check col-6 col-xl-2">
                    <label for="exp_tel_4">เบอร์ติดต่อ</label>
                    <input id="exp_tel_4" name="exp_tel_4" type="text"
                        class="form-control" value="{{ old('exp_tel_4') }}">
                    @error('exp_tel_4')
                        <div class="alert alert-danger">เบอร์ติดต่อ</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_job_4">ตำแหน่ง/ลักษณะงานที่ทำ</label>
                    <input id="exp_job_4" name="exp_job_4" type="text"
                        class="form-control" value="{{ old('exp_job_4') }}">
                    @error('exp_job_4')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_salary_4">เงินเดือน</label>
                    <input id="exp_salary_4" name="exp_salary_4" type="number" class="form-control"
                        value="{{ old('exp_salary_4') }}">
                    @error('exp_salary_4')
                        <div class="alert alert-danger">กรุณาใส่เงินเดือน</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-8">
                    <label for="exp_case_4">เหตุที่ลาออก</label>
                    <input name="exp_case_4" id="exp_case_4" type="text" class="form-control"
                        value="{{ old('exp_case_4') }}">
                    @error('exp_case_4')
                        <div class="alert alert-danger">กรุณาใส่เหตุที่ลาออก</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12 col-xl-1">
                    <b>ลำดับที่ 5</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_start_date_5">ตั้งแต่ ว/ด/ป </label>
                    <input id="exp_start_date_5" name="exp_start_date_5" type="date"
                        class="form-control" value="{{ old('exp_start_date_5') }}">
                    @error('exp_start_date_5')
                        <div class="alert alert-danger">กรุณาเลือกตั้งแต่ ว/ด/ป </div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_end_date_5">ถึง ว/ด/ป</label>
                    <input id="exp_end_date_5" name="exp_end_date_5" type="date"
                        class="form-control" value="{{ old('exp_end_date_5') }}">
                    @error('exp_end_date_5')
                        <div class="alert alert-danger">กรุณาเลือกถึง ว/ด/ป</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="exp_company_5">ชื่อสถานที่ทำงาน</label>
                    <input id="exp_company_5" name="exp_company_5" type="text" class="form-control"
                        value="{{ old('exp_company_5') }}">
                    @error('exp_company_5')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานที่ทำงาน</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_province_5">จังหวัด</label>
                    <input id="exp_province_5" name="exp_province_5" type="text"
                        class="form-control" value="{{ old('exp_province_5') }}">
                    @error('exp_province_5')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>


                <div class="form-check col-6 col-xl-2">
                    <label for="exp_tel_5">เบอร์ติดต่อ</label>
                    <input id="exp_tel_5" name="exp_tel_5" type="text"
                        class="form-control" value="{{ old('exp_tel_5') }}">
                    @error('exp_tel_5')
                        <div class="alert alert-danger">เบอร์ติดต่อ</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_job_5">ตำแหน่ง/ลักษณะงานที่ทำ</label>
                    <input id="exp_job_5" name="exp_job_5" type="text"
                        class="form-control" value="{{ old('exp_job_5') }}">
                    @error('exp_job_5')
                        <div class="alert alert-danger">กรุณาใส่จังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="exp_salary_5">เงินเดือน</label>
                    <input id="exp_salary_5" name="exp_salary_5" type="number" class="form-control"
                        value="{{ old('exp_salary_5') }}">
                    @error('exp_salary_5')
                        <div class="alert alert-danger">กรุณาใส่เงินเดือน</div>
                    @enderror
                </div>

                <div class="form-check col-12 col-xl-8">
                    <label for="exp_case_5">เหตุที่ลาออก</label>
                    <input name="exp_case_5" id="exp_case_5" type="text" class="form-control"
                        value="{{ old('exp_case_5') }}">
                    @error('exp_case_5')
                        <div class="alert alert-danger">กรุณาใส่เหตุที่ลาออก</div>
                    @enderror
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>


</fieldset>
