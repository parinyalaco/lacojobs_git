<h3>การศึกษา</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>ประวัติการศึกษา</legend>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <b>มัธยมศึกษาตอนต้น *</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="school_1">ชื่อสถานศึกษา *</label>
                    <input id="school_1" name="school_1" type="text" class="form-control  {{ $flag }}"
                        value="{{ old('school_1') }}">
                    @error('school_1')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานศึกษา</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="province_1">จังหวัด *</label>
                    <input id="province_1" name="province_1" type="text" class="form-control {{ $flag }}"
                        value="{{ old('school_1') }}">
                    @error('province_1')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="title_1">สาขาวิชา/แผนก/คณะ *</label>
                    <input id="title_1" name="title_1" type="text" class="form-control {{ $flag }}"
                        value="{{ old('title_1') }}">
                    @error('title_1')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสาขาวิชา/แผนก/คณะ</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="year_1">ปีที่จบการศึกษา *</label>
                    <input id="year_1" name="year_1" type="text" class="form-control {{ $flag }}" value="{{ old('year_2') }}">
                    @error('year_1')
                        <div class="alert alert-danger">กรุณาใส่ปีที่จบการศึกษา</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-1">
                    <label for="grade_1">เกรดเฉลี่ย *</label>
                    <input id="grade_1" name="grade_1" type="text" class="form-control {{ $flag }}"
                        value="{{ old('grade_1') }}">
                    @error('grade_1')
                        <div class="alert alert-danger">กรุณาใส่เกรดเฉลี่ย</div>
                    @enderror
                </div>
</div>
                <div class="row">

                <div class="form-check col-6 col-xl-2">
                    <b>มัธยมศึกษาตอนปลาย *</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="school_2">ชื่อสถานศึกษา *</label>
                    <input id="school_2" name="school_2" type="text" class="form-control  {{ $flag }}"
                        value="{{ old('school_2') }}">
                    @error('school_2')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานศึกษา</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="province_2">จังหวัด *</label>
                    <input id="province_2" name="province_2" type="text" class="form-control {{ $flag }}"
                        value="{{ old('school_2') }}">
                    @error('province_2')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="title_2">สาขาวิชา/แผนก/คณะ *</label>
                    <input id="title_2" name="title_2" type="text" class="form-control {{ $flag }}"
                        value="{{ old('title_2') }}">
                    @error('title_2')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสาขาวิชา/แผนก/คณะ</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="year_2">ปีที่จบการศึกษา *</label>
                    <input id="year_2" name="year_2" type="text" class="form-control {{ $flag }}" value="{{ old('year_2') }}">
                    @error('year_2')
                        <div class="alert alert-danger">กรุณาใส่ปีที่จบการศึกษา</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-1">
                    <label for="grade_2">เกรดเฉลี่ย *</label>
                    <input id="grade_2" name="grade_2" type="text" class="form-control {{ $flag }}"
                        value="{{ old('grade_2') }}">
                    @error('grade_2')
                        <div class="alert alert-danger">กรุณาใส่เกรดเฉลี่ย</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <b>ปวช.</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="school_3">ชื่อสถานศึกษา</label>
                    <input id="school_3" name="school_3" type="text" class="form-control"
                        value="{{ old('school_3') }}">
                    @error('school_3')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานศึกษา</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="province_3">จังหวัด</label>
                    <input id="province_3" name="province_3" type="text" class="form-control"
                        value="{{ old('school_3') }}">
                    @error('province_3')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="title_3">สาขาวิชา/แผนก/คณะ</label>
                    <input id="title_3" name="title_3" type="text" class="form-control"
                        value="{{ old('title_3') }}">
                    @error('title_3')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสาขาวิชา/แผนก/คณะ</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="year_3">ปีที่จบการศึกษา</label>
                    <input id="year_3" name="year_3" type="text" class="form-control" value="{{ old('year_3') }}">
                    @error('year_3')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-1">
                    <label for="grade_3">เกรดเฉลี่ย</label>
                    <input id="grade_3" name="grade_3" type="text" class="form-control"
                        value="{{ old('grade_3') }}">
                    @error('grade_3')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <b>ปวส.</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="school_4">ชื่อสถานศึกษา</label>
                    <input id="school_4" name="school_4" type="text" class="form-control"
                        value="{{ old('school_4') }}">
                    @error('school_4')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานศึกษา</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="province_4">จังหวัด</label>
                    <input id="province_4" name="province_4" type="text" class="form-control"
                        value="{{ old('school_4') }}">
                    @error('province_4')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="title_4">สาขาวิชา/แผนก/คณะ</label>
                    <input id="title_4" name="title_4" type="text" class="form-control"
                        value="{{ old('title_4') }}">
                    @error('title_4')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสาขาวิชา/แผนก/คณะ</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="year_4">ปีที่จบการศึกษา</label>
                    <input id="year_4" name="year_4" type="text" class="form-control" value="{{ old('year_4') }}">
                    @error('year_4')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-1">
                    <label for="grade_4">เกรดเฉลี่ย</label>
                    <input id="grade_4" name="grade_4" type="text" class="form-control"
                        value="{{ old('grade_4') }}">
                    @error('grade_4')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <b>ปริญญาตรี</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="school_5">ชื่อสถานศึกษา</label>
                    <input id="school_5" name="school_5" type="text" class="form-control"
                        value="{{ old('school_5') }}">
                    @error('school_5')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานศึกษา</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="province_5">จังหวัด</label>
                    <input id="province_5" name="province_5" type="text" class="form-control"
                        value="{{ old('school_5') }}">
                    @error('province_5')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="title_5">สาขาวิชา/แผนก/คณะ</label>
                    <input id="title_5" name="title_5" type="text" class="form-control"
                        value="{{ old('title_5') }}">
                    @error('title_5')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสาขาวิชา/แผนก/คณะ</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="year_5">ปีที่จบการศึกษา</label>
                    <input id="year_5" name="year_5" type="text" class="form-control" value="{{ old('year_5') }}">
                    @error('year_5')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-1">
                    <label for="grade_5">เกรดเฉลี่ย</label>
                    <input id="grade_5" name="grade_5" type="text" class="form-control"
                        value="{{ old('grade_5') }}">
                    @error('grade_5')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="form-check col-6 col-xl-2">
                    <b>ปริญญาโท / อื่นๆ</b>
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="school_6">ชื่อสถานศึกษา</label>
                    <input id="school_6" name="school_6" type="text" class="form-control"
                        value="{{ old('school_6') }}">
                    @error('school_6')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสถานศึกษา</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-2">
                    <label for="province_6">จังหวัด</label>
                    <input id="province_6" name="province_6" type="text" class="form-control"
                        value="{{ old('school_6') }}">
                    @error('province_6')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
                <div class="form-check col-6 col-xl-3">
                    <label for="title_6">สาขาวิชา/แผนก/คณะ</label>
                    <input id="title_6" name="title_6" type="text" class="form-control"
                        value="{{ old('title_6') }}">
                    @error('title_6')
                        <div class="alert alert-danger">กรุณาใส่ชื่อสาขาวิชา/แผนก/คณะ</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-2">
                    <label for="year_6">ปีที่จบการศึกษา</label>
                    <input id="year_6" name="year_6" type="text" class="form-control" value="{{ old('year_6') }}">
                    @error('year_6')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>

                <div class="form-check col-6 col-xl-1">
                    <label for="grade_6">เกรดเฉลี่ย</label>
                    <input id="grade_6" name="grade_6" type="text" class="form-control"
                        value="{{ old('grade_6') }}">
                    @error('grade_6')
                        <div class="alert alert-danger">กรุณาใส่ชื่อจังหวัด</div>
                    @enderror
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
