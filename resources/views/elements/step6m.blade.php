<h3>สถานภาพ / สุขภาพ</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>สถานภาพสมรส</legend>
            <div class="row">
                <div class="form-check col-6 col-xl-4">
                    <label for="married_status">สถานะภาพ</label>
                    <select class="form-control {{ $flag }} " id="married_status" name="married_status">
                        <option value="">=== เลือก ===</option>
                        <option value="โสด">โสด</option>
                        <option value="สมรส(จดทะเบียน)">สมรส(จดทะเบียน)</option>
                        <option value="สมรสโดยไม่จดทะเบียน">สมรสโดยไม่จดทะเบียน</option>
                    </select>
                </div>

            </div>
            <legend>สุขภาพและความพร้อมทางร่างกาย</legend>

            <div class="row">
                <div class="form-check col-6 col-xl-4">
                    <label for="congenital_disease">โรคประจำตัว</label>
                    <select class="form-control {{ $flag }} " id="congenital_disease" name="congenital_disease">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่มี">ไม่มี</option>
                        <option value="มี">มี</option>
                    </select>
                </div>
                <div class="form-check col-6 col-xl-4 congenital_disease d-none">
                    <label for="congenital_disease_other">ระบุชื่อโรคประจำตัว</label>
                    <input id="congenital_disease_other" name="congenital_disease_other" type="text"
                        placeholder="ระบุโรคประจำตัว" class="form-control">
                </div>
            </div>
            <legend>Upload เอกสาร</legend>

            <div class="row">
                <div class="form-check col-12 col-xl-4">
                    <label for="image">ภาพถ่ายผู้สมัคร รูปถ่ายหน้าตรงเท่านั้น *</label>
                    <label for="image" class="btn btn-success">แนบไฟล์ หรือถ่ายรูป </label>
                    <input id="image" name="image" style="visibility:hidden;" accept="image/*" capture="camera"
                        class=" {{ $flag }} " type="file">
                </div>
                <div class="form-check col-12 col-xl-4">
                    <label for="private_info">สำเนา บัตรประจำตัวประชาชน,ทะเบียนบ้าน,หลักฐานแสดงวุฒิการศึกษา (รวมมาเป็น 1
                        pdf)</label>
                    {!! Form::file('private_info', $attributes = ['accept' => 'application/pdf']) !!}
                </div>
                <div class="form-check col-12 col-xl-4">
                    <label for="resume">Resume และ เอกสารอื่นๆ (รวมมาเป็น 1 pdf)</label>
                    {!! Form::file('resume', $attributes = ['accept' => 'application/pdf']) !!}
                </div>
            </div>
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>
</fieldset>
