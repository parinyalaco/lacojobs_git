<h5 class='newh5'>ส่วนที่ 6 : ความสามารถพิเศษ</h5>
@if (isset($masterdata->expdetaildata))
<div class="row">
    <div class="col-12"><b>1. พิมพดีด ภาษาไทย</b> <span class="answer" >
    {{-- @if (!empty($masterdata->expdetaildata->expd_type_th)) --}}
        {{ filldot($masterdata->expdetaildata->expd_type_th,20) }}
    {{-- @else --}}
        {{-- - --}}
    {{-- @endif --}}
    </span> <b>คำ/นาที</b> <b>ภาษาอังกฤษ</b> <span class="answer" >
    {{-- @if (!empty($masterdata->expdetaildata->expd_type_en)) --}}
        {{ filldot($masterdata->expdetaildata->expd_type_en,20) }}
    {{-- @else
        -
    @endif --}}
    </span> 
    <b>คำ/นาที</b></div>
    <div class="col-12">
        <b>2.  คอมพิวเตอร์ โปรแกรม :-</b>
    </div>
    @if (!empty($masterdata->expdetaildata->expd_com_program1))
        <div class="col-4"><b>1) {{ filldot($masterdata->expdetaildata->expd_com_program1,30) }}</b> 
            <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_com_level1,20) }})</span></div>
    @else
        <div class="col-4"><b>1) {{ filldot('.',30) }}</b> 
            <span class="answer" >({{ filldot('.',20) }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program2))
        <div class="col-4"><b>2) {{ filldot($masterdata->expdetaildata->expd_com_program2,30) }}</b> 
            <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_com_level2,20) }})</span></div>
    @else
        <div class="col-4"><b>2) {{ filldot('.',30) }}</b> 
            <span class="answer" >({{ filldot('.',20) }})</span></div>
    @endif
    @if (!empty($masterdata->expdetaildata->expd_com_program3))
        <div class="col-4"><b>3) {{ filldot($masterdata->expdetaildata->expd_com_program3,30) }}</b> 
            <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_com_level3,20) }})</span></div>
    @else
        <div class="col-4"><b>3) {{ filldot('.',30) }}</b> 
            <span class="answer" >({{ filldot('.',20) }})</span></div>
    @endif
    
    <div class="col-12"><b>3. ความสามารถด้านภาษา</b></div>
    <div class="col-6"><b>3.1 ภาษาไทย</b> 
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_th,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_th,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_th,20) }})</span>
    </div>
    <div class="col-6"><b>3.2 ภาษาอังกฤษ</b> 
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_en,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_en,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_en,20) }})</span>
    </div>
    <div class="col-6"><b>3.3 ภาษาจีน</b> 
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_ch,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_ch,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_ch,20) }})</span>
    </div>
    <div class="col-6"><b>3.4 ภาษาอื่นๆ {{ filldot($masterdata->expdetaildata->expd_lang_ex,20) }}</b>  
        การพูด <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_speak_ex,20) }})</span> / 
        การอ่าน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_read_ex,20) }})</span>  / 
        การเขียน <span class="answer" >({{ filldot($masterdata->expdetaildata->expd_lang_write_ex,20) }})</span>
    </div>
    <div class="col-12"><b>4. การขับขี่ยานพาหนะ</b></div>
    <div class="col-12"><b>ขับขี่รถยนต์ </b> <span class='answer'>({{ filldot($masterdata->expdetaildata->expd_can_drive,20) }}) </span>
    @if ($masterdata->expdetaildata->expd_have_drive_licen == 'มี')
        <span class='answer'>{{ filldot($masterdata->expdetaildata->expd_have_drive_licen,20) }} ใบอนุญาตขับรถ</span> 
         ชนิด <span class='answer'>{{ filldot($masterdata->expdetaildata->expd_drive_licen_type,20) }}</span>
           วันอนุญาต <span class='answer'>{{ filldot(tothaiyear($masterdata->expdetaildata->expd_drive_licen_date),20) }}</span>
             วันหมดอายุ <span class='answer'>{{ filldot(tothaiyear($masterdata->expdetaildata->expd_drive_licen_exp),20) }}</span> 
    @else
        <span class='answer'>{{ filldot($masterdata->expdetaildata->expd_have_drive_licen,20) }} ใบอนุญาตขับรถ</span> 
    @endif
    </div>
    <div class="col-12"><b>ขับขี่รถจักรยานยนต์</b>  <span class='answer'>({{ filldot($masterdata->expdetaildata->expd_can_ride,20) }})</span> 
    @if ($masterdata->expdetaildata->expd_have_ride_licen == 'มี')
        <span class='answer'>{{ filldot($masterdata->expdetaildata->expd_have_ride_licen,20) }}ใบอนุญาตขับรถ</span> 
        ชนิด <span class='answer'>{{ filldot($masterdata->expdetaildata->expd_ride_licen_type,20) }}</span>  
            วันอนุญาต <span class='answer'>{{ filldot(tothaiyear($masterdata->expdetaildata->expd_ride_licen_date),20) }}</span>  
            วันหมดอายุ <span class='answer'>{{ filldot(tothaiyear($masterdata->expdetaildata->expd_ride_licen_exp),20) }}</span> 
    @else
        <span class='answer'>{{ filldot($masterdata->expdetaildata->expd_have_ride_licen,20) }}ใบอนุญาตขับรถ</span> 
    @endif
    </div>
    <div class="col-12"><b>5. ความสามารถพิเศษอื่นๆ</b> <span class='answer'>{{ filldot($masterdata->expdetaildata->expd_others,50) }}</span> </div>
</div>
@endif