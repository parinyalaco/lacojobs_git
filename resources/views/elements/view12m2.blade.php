<h5 class='newh5'>ส่วนที่ 12 : บุคคลอ้างอิงซึ่งสามารถรับรองความประพฤติ/ประวัติการทำงานได้ (ไม่ใช่ญาติพี่น้องหรือเพื่อน)</h5>
{{-- @if (isset($masterdata->refdata)) --}}
<table class="table d-print-table mytable" >
<tbody>
<tr>
<td class="mycol_b_c" >ลำดับ</td>
<td class="mycol_b_c" >ชื่อ-นามสกุล</td>
<td class="mycol_b_c" >มีความสัมพันธ์เป็น</td>
<td class="mycol_b_c" >ชื่อสถานที่ทำงาน/ตำแหน่ง</td>
<td class="mycol_b_c" >โทรศัพท์</td>
</tr>
{{-- @if (!empty($masterdata->refdata->name_1)) --}}
<tr>
<td>1</td>
<td>@if (!empty($masterdata->refdata->name_1)){{ $masterdata->refdata->name_1 }}@endif</td>
<td>@if (!empty($masterdata->refdata->relation_1)){{ $masterdata->refdata->relation_1 }}@endif</td>
<td>@if (!empty($masterdata->refdata->company_job_1)){{ $masterdata->refdata->company_job_1 }}@endif</td>
<td>@if (!empty($masterdata->refdata->tel_1)){{ $masterdata->refdata->tel_1 }}@endif</td>
</tr>
{{-- @endif
@if (!empty($masterdata->refdata->name_2)) --}}
<tr>
<td>2</td>
<td>@if (!empty($masterdata->refdata->name_2)){{ $masterdata->refdata->name_2 }}@endif</td>
<td>@if (!empty($masterdata->refdata->relation_2)){{ $masterdata->refdata->relation_2 }}@endif</td>
<td>@if (!empty($masterdata->refdata->company_job_2)){{ $masterdata->refdata->company_job_2 }}@endif</td>
<td>@if (!empty($masterdata->refdata->tel_2)){{ $masterdata->refdata->tel_2 }}@endif</td>
</tr>
{{-- @endif
@if (!empty($masterdata->refdata->name_3)) --}}
<tr>
<td>3</td>
<td>@if (!empty($masterdata->refdata->name_3)){{ $masterdata->refdata->name_3 }}@endif</td>
<td>@if (!empty($masterdata->refdata->relation_3)){{ $masterdata->refdata->relation_3 }}@endif</td>
<td>@if (!empty($masterdata->refdata->company_job_3)){{ $masterdata->refdata->company_job_3 }}@endif</td>
<td>@if (!empty($masterdata->refdata->tel_3)){{ $masterdata->refdata->tel_3 }}@endif</td>
</tr>
{{-- @endif --}}
</tbody>
</table>
{{-- @endif --}}