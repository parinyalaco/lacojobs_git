<h5 class='newh5'>ส่วนที่ 7  : สถานภาพสมรส</h5>
@if (isset($masterdata->marrieddata))
<div class="row"> 
    <div class="col-12"><span class='answer'>{{ filldot($masterdata->marrieddata->status,20) }}</span>  
    {{-- @if ($masterdata->marrieddata->status == 'โสด' || $masterdata->marrieddata->status == 'หม้าย (คู่สมรสถึงแก่กรรม)')
        <br/>
    @else --}}
        <b>คู่สมรสชื่อ</b> <span class='answer'>{{ filldot($masterdata->marrieddata->init,10) }}{{ filldot($masterdata->marrieddata->name,50) }}</span> 
        <b>อายุ</b> <span class='answer'>{{ filldot($masterdata->marrieddata->age,20) }}</span> 
        <b>ปี อาชีพ</b> <span class='answer'>{{ filldot($masterdata->marrieddata->career,20) }}</span><br/>
        <b>ที่อยู่ปัจจุบัน</b> <span class='answer'>{{ filldot($masterdata->marrieddata->homeaddr,20) }}</span> 
        <b>โทรศัพท์</b> <span class='answer'>{{ filldot($masterdata->marrieddata->tel,20) }}</span> 
        <b>ที่ตั้งสถานที่ทำงาน</b>  <span class='answer'>{{ filldot($masterdata->marrieddata->companyaddr,20) }}</span> 
        <b>ตำแหน่ง</b> <span class='answer'>{{ filldot($masterdata->marrieddata->job,20) }}</span> 
        <b>เบอร์โทรศัพท์ที่ทำงาน</b>  <span class='answer'>{{ filldot($masterdata->marrieddata->companytel,20) }}</span> <br/>          
    {{-- @endif --}}
        <b>ปัจจุบัน</b> <span class='answer'>{{ filldot($masterdata->marrieddata->child_status,20) }}</span>
        {{-- @if ($masterdata->marrieddata->child_status == 'มีบุตร') --}}
            <span class='answer'>{{ filldot($masterdata->marrieddata->child_number,20) }}</span> 
            <b>คน เป็นชาย</b> 
            <span class='answer'>{{ filldot($masterdata->marrieddata->boy_no,20) }}</span> <b>คน  อายุ</b> 
            <span class='answer'>{{ filldot($masterdata->marrieddata->boy_age,20) }}</span> <b>ปี หญิง</b> 
            <span class='answer'>{{ filldot($masterdata->marrieddata->daughter_no,20) }}</span> <b> คน  อายุ</b>  
            <span class='answer'>{{ filldot($masterdata->marrieddata->daughter_age,20) }}</span> <b> ปี</b> 
        {{-- @else
            
        @endif --}}
    </div>
</div>
@endif
<div class="pagebreak"></div>