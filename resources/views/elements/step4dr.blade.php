<h3>สุขภาพ / ความพร้อมทางร่างกาย</h3>
<fieldset>
    <div class="row">
        <div class="col-12">
            <legend>สุขภาพ / ความพร้อมทางร่างกาย</legend>
            <div class="row">
                <div class="form-check col-12 col-xl-6">
                    <label for="social_security_hospital">1.โรงพยาบาลที่ขอใช้สิทธิรักษาพยาบาลประกันสังคม</label>
                    <input id="social_security_hospital" name="social_security_hospital" type="text"
                        placeholder="ระบุโรงพยาบาล" class="form-control" value="{{ old('social_security_hospital') }}">
                </div>
            </div>
            <div class="row">
                <div class="form-check col-12  col-xl-2">
                    <label for="congenital_disease">1. โรคประจำตัว *</label>
                    <select class="form-control {{ $flag }} " id="congenital_disease" name="congenital_disease">
                        <option value="">=== เลือก ===</option>
                        <option value="ไม่มี" {{ old('congenital_disease') == 'ไม่มี' ? 'selected' : '' }}>ไม่มี</option>
                        <option value="มี" {{ old('congenital_disease') == 'มี' ? 'selected' : '' }}>มี</option>
                    </select>
                </div>
                <div class="form-check col-12 col-xl-6 congenital_disease d-none">
                    <label for="congenital_disease_other">ระบุชื่อโรคประจำตัว</label>
                    <input id="congenital_disease_other" name="congenital_disease_other" type="text"
                        placeholder="ระบุโรคประจำตัว" class="form-control">
                </div>
            </div>
            
            <p>(*) ข้อมูลที่จำเป็นต้องใส่</p>
        </div>
    </div>


</fieldset>
