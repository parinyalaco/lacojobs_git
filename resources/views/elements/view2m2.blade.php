<h5 class='newh5'>ส่วนที่ 2 : ประวัติการศึกษา</h5>
<table class="table d-print-table mytable">
    <tbody>
        <tr>
            <td class="mycol_b_c" style="width: 22%">ระดับ</td>
            <td class="mycol_b_c" style="width: 25%">ชื่อสถานศึกษา</td>
            <td class="mycol_b_c" style="width: 13%">จังหวัด</td>
            <td class="mycol_b_c" style="width: 25%">สาขาวิชา/แผนก/คณะ</td>
            <td class="mycol_b_c" style="width: 10%">ปีที่จบการศึกษา</td>
            <td class="mycol_b_c" style="width: 5%">เกรดเฉลี่ย</td>
        </tr>
        @if (!empty($masterdata->educationdata->school_1))
            <tr>
                <td class="mycol_b_l">มัธยมศึกษาตอนต้น.</td>
                <td>@if (!empty($masterdata->educationdata->school_1)){{ $masterdata->educationdata->school_1 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->province_1)){{ $masterdata->educationdata->province_1 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->title_1)){{ $masterdata->educationdata->title_1 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->year_1)){{ $masterdata->educationdata->year_1 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->grade_1)){{ $masterdata->educationdata->grade_1 }}@endif</td>
            </tr>
        @endif
        @if (!empty($masterdata->educationdata->school_2))
            <tr>
                <td class="mycol_b_l">มัธยมศึกษาตอนปลาย.</td>
                <td>@if (!empty($masterdata->educationdata->school_2)){{ $masterdata->educationdata->school_2 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->province_2)){{ $masterdata->educationdata->province_2 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->title_2)){{ $masterdata->educationdata->title_2 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->year_2)){{ $masterdata->educationdata->year_2 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->grade_2)){{ $masterdata->educationdata->grade_2 }}@endif</td>
            </tr>
        @endif        
        @if (!empty($masterdata->educationdata->school_3))
            <tr>
                <td class="mycol_b_l">ปวช.</td>
                <td>@if (!empty($masterdata->educationdata->school_3)){{ $masterdata->educationdata->school_3 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->province_3)){{ $masterdata->educationdata->province_3 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->title_3)){{ $masterdata->educationdata->title_3 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->year_3)){{ $masterdata->educationdata->year_3 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->grade_3)){{ $masterdata->educationdata->grade_3 }}@endif</td>
            </tr>
        @endif
        @if (!empty($masterdata->educationdata->school_4))
            <tr>
                <td class="mycol_b_l">ปวส.</td>
                <td>@if (!empty($masterdata->educationdata->school_4)){{ $masterdata->educationdata->school_4 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->province_4)){{ $masterdata->educationdata->province_4 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->title_4)){{ $masterdata->educationdata->title_4 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->year_4)){{ $masterdata->educationdata->year_4 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->grade_4)){{ $masterdata->educationdata->grade_4 }}@endif</td>
            </tr>
        @endif
        {{-- @if (!empty($masterdata->educationdata->school_5)) --}}
            <tr>
                <td class="mycol_b_l">ปริญญาตรี</td>
                <td>@if (!empty($masterdata->educationdata->school_5)){{ $masterdata->educationdata->school_5 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->province_5)){{ $masterdata->educationdata->province_5 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->title_5)){{ $masterdata->educationdata->title_5 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->year_5)){{ $masterdata->educationdata->year_5 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->grade_5)){{ $masterdata->educationdata->grade_5 }}@endif</td>
            </tr>
        {{-- @endif
        @if (!empty($masterdata->educationdata->school_6)) --}}
            <tr>
                <td class="mycol_b_l">ปริญญาโท/อื่นๆ</td>
                <td>@if (!empty($masterdata->educationdata->school_6)){{ $masterdata->educationdata->school_6 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->province_6)){{ $masterdata->educationdata->province_6 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->title_6)){{ $masterdata->educationdata->title_6 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->year_6)){{ $masterdata->educationdata->year_6 }}@endif</td>
                <td>@if (!empty($masterdata->educationdata->grade_6)){{ $masterdata->educationdata->grade_6 }}@endif</td>
            </tr>
        {{-- @endif --}}
    </tbody>
</table>
