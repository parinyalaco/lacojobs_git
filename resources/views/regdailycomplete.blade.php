<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css'>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body style='background-color:#ffffff !important'>
    <main class="py-4">
        <div class='text-center'><img src="{{ asset('/images/logo.png') }}" height="150px"></div>
        <h4 class='text-center'>ทางบริษัทได้รับข้อมูลของท่านแล้ว และจะมีการติดต่อกลับไปยังเบอร์โทรที่ท่านได้ให้ไว้ หากไม่มีการติดต่อภายใน 15 วัน ให้ถือว่าคุณสมบัติของท่านยังไม่ได้รับการพิจารณา</h4>
        <h4 class='text-center'>หากผ่านการคัดเลือกทางบริษัทจะติดต่อกลับไปให้เข้ามาอบรม โดยต้องเตรียมเอกสารดังต่อไปนี้มาในวันที่อบรม หากไม่เตรียมมาจะไม่ได้การอนุมัติเข้าอบรม</h4>
        <h4 class='text-center'>1.สำเนาบัตรประชาชน</h4>
        <h4 class='text-center'>2.สำเนาทะเบียนบ้าน</h4>
        <h4 class='text-center'>3.สำเนาวุฒิการศึกษา</h4>
        <h4 class='text-center'>4.สำเนาเอกสารฉีดวัคซีน</h4>
        <h4 class='text-center'>5.สำเนาบัตรผู้พิการ(กรณีเป็นผู้พิการ)</h4>
        <h4 class='text-center'>6.สำเนาเอกสารการเกณฑ์ทหาร(ถ้ามี)</h4>
        <h4 class='text-center'>7.รูปถ่ายขนาด 1 นิ้ว 1 ใบ</h4>
    </main>
    </div>
</body>

</html>
