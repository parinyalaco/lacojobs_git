@extends('layouts.stepdaily')

@section('content')
    <div class="row">
        <div class="col-12 text-center">
            <img src="{{ asset('/images/logo-addr.png') }}" width="400px" alt="HR LACO">
            <img src="{{ asset('/images/haccp.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/gmp.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/fda.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/islamic.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/brcgs.png') }}" width="100px" alt="HR LACO">
            <h1>สมัครงานลานนาเกษตร</h1>
            @if ($errors->any())
                <div class="alert alert-danger">มีข้อมูลบางอย่างไม่ถูกต้อง</div>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            @endif
        </div>
        <div class="col-12">
            {!! Form::open(['url' => url('applyjobdailyAction'), 'class' => 'form-horizontal', 'files' => true, 'id' => 'apply-job-form']) !!}
                @include('elements.step1dr')
                @include('elements.step2dr')
                @include('elements.step3dr')
            </form>
        </div>
    </div>
@endsection
