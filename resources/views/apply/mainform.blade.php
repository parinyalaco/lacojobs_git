@extends('layouts.apply')

@section('content')
<div class="row" >
    <div class="col-12">
       <h1>สมัครงาน LACO</h1>
    </div>
</div>
<div class="wrapper">
    <form action="">
        <div id="wizard">
            <!-- SECTION 1 -->
            <h4></h4>
            <section>
                <p>ความยินยอมของผู้สมัครงาน/ลูกจ้าง
ผู้สมัครงาน ทราบว่า ข้อมูลส่วนบุคคลตามที่ปรากฏในแบบฟอร์มใบสมัคร และตามเอกสารซึ่งข้าพเจ้าได้ส่งมอบประกอบการสมัครงานให้กับบริษัทนั้น  มีวัตถุประสงค์เพื่อเป็นหลักฐานและข้อมูลประกอบการพิจารณารับสมัครงานของบริษัท และ/หรือเป็นข้อมูลเอกสารหลักฐาน อันจำเป็นในการทำสัญญาว่าจ้าง และการอื่นใดอันจำเป็นในการปฏิบัติอันเกี่ยวกับการสัญญาว่าจ้าง ระหว่างบริษัท กับข้าพเจ้า (กรณีผ่านการพิจารณาและทำสัญญาเป็นลูกจ้างของบริษัท) โดยข้าพเจ้าตกลงให้บริษัทดำเนินการดังนี้
</p>
<p>1.การเก็บบันทึกและใช้ข้อมูล (ในส่วนของรายละเอียดส่วนตัวผู้สมัคร) มีความจำเป็นต้องบันทึกข้อมูลเกี่ยวกับศาสนา (ก) การเก็บบันทึกและใช้ข้อมูลเกี่ยวกับศาสนา : ข้าพเจ้าทราบว่า บริษัทมีความจำเป็นต้องบันทึกข้อมูลเกี่ยวกับศาสนา เพื่อเป็นการอันจำเป็นในการจัดสวัสดิการ สถานที่ หรือวิธีการทำงาน ในกรณีที่หากข้าพเจ้าได้พิจารณาทำสัญญาจ้าง โดยหากข้าพเจ้าไม่ให้ข้อมูลดังกล่าวแล้วนั้น บริษัทอาจไม่สามารถดำเนินการดังกล่าวได้</p>
<div class="form-check">
    <input type="checkbox" name="accept1" id="accept1" class="form-check-input chkstep1 required" value="yes">
    <label class="form-check-label" for="accept1">ข้าพเจ้าตกลงยินยอมให้บันทึกและใช้ข้อมูลดังกล่าว</label>
</div>
<p>2.การเปิดเผยข้อมูลส่วนบุคคล ข้าพเจ้าทราบว่า เพื่อประโยชน์ของข้าพเจ้าตามวัตถุประสงค์ในการสมัครงานตามที่กล่าวข้างต้น บริษัท อาจเปิดเผยข้อมูลตามแบบฟอร์มใบสมัครและเอกสารอื่นใด ซึ่งข้าพเจ้าได้ส่งมอบประกอบการสมัครงานให้กับบริษัท ลานนาเกษตรอุตสาหกรรม จำกัด ในการนี้ข้าพเจ้าตกลงให้ดำเนินการ ดังนี้</p>
 <div class="form-check">
     <input type="checkbox" name="accept2" id="accept2" class="form-check-input chkstep1" value="yes">
     <label class="form-check-label" for="accept2">ข้าพเจ้าตกลงยินยอมให้เปิดเผยข้อมูลส่วนบุคคลต่อบุคคลดังกล่าว</label>
</div>
<p>3.เกี่ยวกับข้อมูลชีวภาพ (Biometrics) ข้าพเจ้าทราบว่า หากข้าพเจ้าผ่านการพิจารณารับเข้าทำงาน บริษัทมีการเก็บบันทึกและใช้ข้อมูลชีวภาพ (Biometrics) ของพนักงาน เช่น ข้อมูลจำลองลายนิ้วมือ ข้อมูลจำลองใบหน้า ข้อมูลจำลองม่านตา น้ำหนัก ส่วนสูง เป็นต้นเพื่อใช้เป็นการบันทึกเวลาการทำงาน บันทึกเวลาเข้า-ออกงาน การเข้า-ออกสถานที่ทำงาน หรือ สถานที่อื่นใดของบริษัทฯ ใช้เพื่อการพัฒนาเพิ่มพูนความรู้ความสามารถเพิ่มทักษะ และ ศักยภาพ ในการทำงาน ตามระเบียบข้อบังคับของบริษัท</p>
 <div class="form-check">
     <input type="checkbox" name="accept3" id="accept3" class="form-check-input chkstep1" value="yes">
     <label class="form-check-label" for="accept3">ข้าพเจ้าตกลงยินยอมให้บันทึกและใช้ข้อมูลดังกล่าว</label>
</div>
<p>4. ประวัติอาชญากรรม ข้าพเจ้าทราบว่า หากข้าพเจ้าผ่านการพิจารณา บริษัทมีการดำเนินการตรวจประวัติอาชญากรรม ทั้งนี้ บริษัทจำเป็นต้องเปิดเผยข้อมูลส่วนบุคคล เช่น ชื่อ-นามสกุล ที่อยู่ เบอร์โทรศัพท์ เลขหรือสำเนาบัตรประจำตัวประชาชน รวมถึง สำเนาบัตรประจำตัวประชาชน เป็นต้น แก่กองทะเบียนประวัติอาชญากร สำนักงานตำรวจแห่งชาติ เพื่อดำเนินการตรวจประวัติอาชญากรรม และตกลงยินยอมให้ สำนักงานตำรวจแห่งชาติ ส่งข้อมูลผลการตรวจประวัติอาชญากรรมให้กับบริษัท เพื่อเก็บบันทึกและประกอบการจ้างงาน</p>
 <div class="form-check">
     <input type="checkbox" name="accept4" id="accept4" class="form-check-input chkstep1" value="yes">
     <label class="form-check-label" for="accept4">ข้าพเจ้าตกลงยินยอมให้เก็บรวบรวม ใช้ และดำเนินการดังกล่าว</label>
</div>
<p>5.การเก็บรวบรวมข้อมูลส่วนบุคคลจากแหล่งอื่น ข้าพเจ้าทราบว่า ในการพิจารณาคุณสมบัติของข้าพเจ้าเพื่อการพิจารณารับเข้าทำงานของบริษัท บริษัทอาจมีการตรวจสอบและเก็บรวบรวมข้อมูลส่วนบุคคลจากแหล่งอื่น เช่น สื่อสังคมออนไลน์ เว็บไซต์หางาน สถาบันการศึกษา บุคคลอ้างอิง หรือจากบริษัทที่เคยร่วมงาน ตามข้อมูลซึ่งได้ให้ไว้</p>
 <div class="form-check">
     <input type="checkbox" name="accept5" id="accept5" class="form-check-input chkstep1" value="yes">
     <label class="form-check-label" for="accept5">ข้าพเจ้าตกลงยินยอมให้เก็บรวบรวม ใช้ และดำเนินการดังกล่าว</label>
</div>
            </section> <!-- SECTION 2 -->
            <h4></h4>
            <section>
                <div class="form-row"> <input type="text" class="form-control" placeholder="country"> </div>
                <div class="form-row"> <input type="text" class="form-control" placeholder="zip code"> </div>
                <div class="form-row" style="margin-bottom: 18px"> <textarea name="" id="" class="form-control" placeholder="Any order note about delivery or special offer" style="height: 108px"></textarea> </div>
            </section> <!-- SECTION 3 -->
            <h4></h4>
            <section>
                <div class="form-row"> <input type="text" class="form-control" placeholder="country"> </div>
                <div class="form-row"> <input type="text" class="form-control" placeholder="zip code"> </div>
                <div class="form-row" style="margin-bottom: 18px"> <textarea name="" id="" class="form-control" placeholder="Any order note about delivery or special offer" style="height: 108px"></textarea> </div>
            </section> 
            <h4></h4>
            <section>
                <div class="product">
                    <div class="item">
                        <div class="left"> <a href="#" class="thumb"> <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1564246609/Ecommerce/img-2.jpg" alt=""> </a>
                            <div class="purchase">
                                <h6> <a href="#">Macbook Air Laptop</a> </h6> <span>x1</span>
                            </div>
                        </div> <span class="price">$290</span>
                    </div>
                    <div class="item">
                        <div class="left"> <a href="#" class="thumb"> <img src="https://www.businessinsider.in/thumb/msid-70101317,width-600,resizemode-4,imgsize-87580/tech/ways-to-increase-mobile-phone-speed/13d0e0722dbca5aa91e16a8ae2a744e5.jpg" alt=""> </a>
                            <div class="purchase">
                                <h6> <a href="#">Mobile Phone Mi</a> </h6> <span>x1</span>
                            </div>
                        </div> <span class="price">$124</span>
                    </div>
                </div>
                <div class="checkout">
                    <div class="subtotal"> <span class="heading">Subtotal</span> <span class="total">$364</span> </div>
                </div>
            </section> <!-- SECTION 4 -->
            <h4></h4>
            <section>
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                    <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                    <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
                </svg>
                <p class="success">Order placed successfully. Your order will be dispacted soon. meanwhile you can track your order in my order section.</p>
            </section>
        </div>
    </form>
</div>
@endsection
