@extends('layouts.stepmonth')

@section('content')
    <div class="row">
        <div class="col-12 text-center">
            <img src="{{ asset('/images/logo-addr.png') }}" width="400px" alt="HR LACO">
            <img src="{{ asset('/images/haccp.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/gmp.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/fda.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/islamic.png') }}" width="100px" alt="HR LACO">
            <img src="{{ asset('/images/brcgs.png') }}" width="100px" alt="HR LACO">
            <h1>สมัครงานลานนาเกษตร</h1>
            @if ($errors->any())
                <div class="alert alert-danger">มีข้อมูลบางอย่างไม่ถูกต้อง</div>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @endif
        </div>
        <div class="col-12">

            {!! Form::open(['url' => url('applyjobAction'), 'class' => 'form-horizontal', 'files' => true, 'id' => 'apply-job-form']) !!}

            {{-- confirm infor --}}
            @include('elements.step1m2')
            {{-- Salary Basic info --}}
            @include('elements.step2m2')
            {{-- Address --}}
            @include('elements.step2-2m2')
            {{-- Education --}}
            @include('elements.step3m2')
            {{-- Prev Jobs --}}
            @include('elements.step4m2')
            {{-- Training ang Intern part --}}
            @include('elements.step4-2m2')
            {{-- Extra Exp --}}
            @include('elements.step5m2')
            {{-- Engage and military status --}}
            @include('elements.step6m2')
            {{-- personal data 2 --}}
            @include('elements.step7m2')
            {{-- family  --}}
            @include('elements.step8m2')
            {{-- health info --}}
            @include('elements.step9m2')
            {{-- upload documents and map --}}
            @include('elements.step10m2')
            </form>
        </div>
    </div>

@endsection
