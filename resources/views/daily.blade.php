
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css2?family=Kanit" rel="stylesheet">
<link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'>
<style>
body, html {
  height: 100%;
  margin: 0;
  font: 400 15px/1.8 "Kanit", sans-serif;
  color: #777;

  background: url("{{ asset('/images/bg2.jpg') }}") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}

.bgimg-1, .bgimg-2, .bgimg-3 {
  position: relative;
  opacity: 0.65;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

}


.caption {
  position: absolute;
  left: 0;
  top: 15%;
  width: 100%;
  text-align: center;
  color: #000;
}

.caption span.border1 {
  color: rgb(20, 2, 2);
  padding: 18px;
  font-size: 2em;
  letter-spacing: 0.25em;
}

h3 {
  letter-spacing: 5px;
  text-transform: uppercase;
  font: 20px ;
  color: #111;
}
.mylogo {
    background-color: rgba(255, 255, 255, 0.75);
}
.myfont{
  font-size: 1.25rem;
}
</style>
</head>
<body>

  <div class="caption text-center">
    <span class="border1">ยินดีต้อนรับเข้าสู่การสมัครงาน</span><br>
    <span class="border1">บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด</span><br>
    <span class="border1"><img src="{{ asset('/images/logo.png') }}" class="mylogo img-fluid" alt="HR LACO"></span><br>
    <span class="border1"><a href="{{ url('/dailyform') }}" class="btn btn-success btn-xl myfont">สมัครงานรายวัน</a></span>
  </div>

</body>
</html>