@extends('layouts.app')

@section('content')
    <div style="max-width: 95%;margin-left:auto;margin-right:auto;">
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-3">LACO<br />รหัสเอกสาร F-HR-RC-001/1</div>
                    <div class="col-6  text-center">
                        <h2>บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด<br />LANNA AGRO INDUSTRY CO., LTD.<br />ใบสมัครงาน </h2>
                    </div>
                    <div class="col-3  text-right">เลขที่ {{ $masterdata->id }}<br />ประกาศใช้วันที่ :
                        01/06/2565<br />ประกาศใช้ครั้งที่ : 07</div>
                </div>
                <div class="row">

                    <div class="col-9">
                        <b>หลักฐานที่ใช้ในการสมัครงาน :</b>
                        <ul>
                            <li>รูปถ่ายขนาด 1 หรือ 2 นิ้ว จำนวน 1 รูป</li>
                            <li>สำเนา บัตรประจำตัวประชาชน,ทะเบียนบ้าน,หลักฐานแสดงวุฒิการศึกษา อย่างละ 1 ฉบับ</li>
                            <li>Resume และ เอกสารอื่นๆ อย่างละ 1 ฉบับ</li>
                        </ul>
                        วันที่ {{ $masterdata->created_at }}<br />
                        <b>โปรดกรอกใบสมัครนี้อย่างละเอียดและถูกต้องตามความเป็นจริง เพื่อประโยชน์ของผู้สมัครเอง</b><br />
                        ตำแหน่งงานที่ต้องการสมัคร <br />
                        1. <span class='answer'>{{ filldot($masterdata->job_name1, 30) }}</span> อัตราเงินเดือนที่ต้องการ
                        <span class='answer'>{{ filldot($masterdata->job_salary1, 20) }}</span> บาท<BR />
                        @if (!empty($masterdata->job_name2))
                            2. <span class='answer'>{{ filldot($masterdata->job_name2, 30) }}</span>
                            อัตราเงินเดือนที่ต้องการ <span
                                class='answer'>{{ filldot($masterdata->job_salary2, 20) }}</span> บาท<BR />
                        @endif
                        <br />
                        <h5 class='newh5'>ส่วนที่ 1 : ข้อมูลส่วนตัว</h5>
                        <b>ชื่อ-นามสกุล ภาษาไทย </b><span
                            class='answer'>{{ filldot($masterdata->init_th, 10) }}..{{ filldot($masterdata->fname_th, 10) }}..{{ filldot($masterdata->lname_th, 30) }}</span>
                        <b>ชื่อเล่น</b> <span class='answer'>{{ filldot($masterdata->nickname_th, 20) }}</span><br />
                        <b>วัน/เดือน/ปี เกิด</b> <span
                            class='answer'>{{ filldot(tothaiyear($masterdata->birth_date), 20) }}</span> <b>อายุ</b>
                        <span class='answer'>
                            @php
                                $now = new DateTime('now');
                                $OldDate = new DateTime($masterdata->birth_date);
                                $result = $OldDate->diff($now);
                                echo filldot($result->y, 10);
                            @endphp
                        </span>
                        <b>ปี น้ำหนัก</b> <span class='answer'>{{ filldot($masterdata->weight, 10) }}</span>
                        <b>กก. ส่วนสูง</b> <span class='answer'>{{ filldot($masterdata->height, 10) }}</span>
                        <b>ซม.</b><br>
                    </div>
                    <div class="col-3  text-right">
                        @if (!empty($masterdata->uploaddata->image))
                            <img width="250px" src="{{ url($masterdata->uploaddata->image) }}">
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @include('elements.view1m')
                        @include('elements.view2m')
                        @include('elements.view4m')
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class='row'>
            <div class='col-12'>
                <div class='row'>
                    <div class='col-12'>
                        @include('elements.view5m')
                        @include('elements.view6m')
                        @include('elements.view7m')
                        @include('elements.view11m')
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class='row'>
            <div class='col-12'>
                <div class='row'>
                    <div class='col-12'>
                        <div class='row'>
                            <div class="col-7">
                                <div class="row">
                                    <div class="col-9">
                                        “ข้าพเจ้าผู้สมัครงานขอให้ข้อมูลส่วนบุคคลต่อไปนี้ตามความจริงและยินยอมให้บริษัทฯ
                                        เก็บรวบรวม ใช้ หรือเปิดเผยตาม พรบ.คุ้มครองข้อมูลส่วนบุคคล
                                        {{-- ของบริษัทลานนาเกษตรที่ได้กำหนดไว้แล้ว ซึ่งข้าพเจ้าได้อ่านและตรวจสอบตาม QR CODE นี้
                                        และทำการรับทราบและยินยอมแล้ว”</div> --}}
                                        ของบริษัท ลานนาเกษตรอุตสาหกรรม จำกัด ที่ได้กำหนดไวแล้ว ซึ่งข้าพเจ้าได้อ่านและตรวจสอบตาม QR CODE นี้ 
                                        และทำการเซ็นรับทราบและยินยอมแล้ว”</div>
                                    <div class="col-3"><img
                                            src="{{ asset('images/pdpa-laco-th-staff-applicant.png') }}" width="150px">
                                    </div>
                                </div>
                            </div>
                            <div class="col-5" style="text-align: center;"><br /><br />
                                {{-- ลงชื่อ......................................................ผู้สมัคร<br /> --}}
                                ลงชื่อ....{{ $masterdata->init_th.$masterdata->fname_th }}..{{ $masterdata->lname_th }}....ผู้สมัคร<br />
                                (...{{ $masterdata->init_th }}..{{ $masterdata->fname_th }}..{{ $masterdata->lname_th }}..)<br />
                                {{-- ................/.............../............... --}}
                                วันที่....{{ date('d/m/',strtotime($masterdata->created_at)).(date('Y',strtotime($masterdata->created_at))+543) }}....
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (!empty($masterdata->uploaddata->map))
        <script>
            // Initialize and add the map
            function initMap() {
                // The location of Uluru
                @php
                    $dataln = explode(',', $masterdata->uploaddata->map);
                @endphp
                var uluru = {
                    lat: {{ $dataln[0] }},
                    lng: {{ $dataln[1] }}
                };


                // The map, centered at Uluru
                var map = new google.maps.Map(
                    document.getElementById('map'), {
                        zoom: 14,
                        center: uluru
                    });
                // The marker, positioned at Uluru
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            }
        </script>
        <!--Load the API from the specified URL
            * The async attribute allows the browser to render the page while the API loads
            * The key parameter will contain your own API key (which is not needed for this tutorial)
            * The callback parameter executes the initMap() function
            -->
        <script defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUxsUwOjdI8DXS0vDOneRaWo_s53WPg8k&callback=initMap">
        </script>
    @endif
@endsection
