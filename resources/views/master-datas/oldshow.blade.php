@extends('layouts.app')

@section('content')
<div style="max-width: 95%;margin-left:auto;margin-right:auto;">
        <div class="row">

            <div class="col-md-12">
                <div class="row" >
                    <div class="col-3">LACO<br/>รหัสเอกสาร F-HR-RC-001/1</div>
                    <div  class="col-6  text-center"><h2>บริษัท ลานนาเกษตรอุตสาหกรรม จำกัด<br/>LANNA AGRO INDUSTRY CO., LTD.<br/>ใบสมัครงาน </h2></div>
                    <div class="col-3  text-right">เลขที่ {{ $masterdata->id }}<br/>ประกาศใช้วันที่ : 01/07/60<br/>ประกาศใช้ครั้งที่ : 05</div>
                </div>
                <div class="row" >
                    <div class="col-9">วันที่ {{ $masterdata->created_at }}<br/>
                        <b>โปรดกรอกใบสมัครนี้อย่างละเอียดและถูกต้องตามความเป็นจริง เพื่อประโยชน์ของผู้สมัครเอง</b><br/>
                        ตำแหน่งงานที่ต้องการสมัคร <br/>
                        1. <span class='answer'>{{ $masterdata->job_name1 }}</span> อัตราเงินเดือนที่ต้องการ <span class='answer'>{{$masterdata->job_salary1 }}</span> บาท<BR/> 
                        @if (!empty($masterdata->job_name2))
                            2. <span class='answer'>{{ $masterdata->job_name2  }}</span> อัตราเงินเดือนที่ต้องการ <span class='answer'>{{$masterdata->job_salary2}}</span> บาท<BR/> 
                        @endif
                        <br/><h5 class='newh5'>ส่วนที่ 1 : ข้อมูลส่วนตัว</h5>
                        <b>ชื่อ-นามสกุล ภาษาไทย </b><span class='answer'>{{ $masterdata->init_th }} {{ $masterdata->fname_th }} {{ $masterdata->lname_th }}</span> <b>ชื่อเล่น</b> <span class='answer'>{{ $masterdata->nickname_th }}</span><br/>
        	            <b>ชื่อ-นามสกุล ภาษาอังกฤษ </b><span class='answer'>{{ $masterdata->init_en }} {{ $masterdata->fname_en }} {{ $masterdata->lname_en }}</span> <b>Nickname</b> <span class='answer'>{{ $masterdata->nickname_en }}</span><br/>
                        <b>วัน/เดือน/ปี เกิด</b> <span class='answer'>{{ tothaiyear($masterdata->birth_date) }}</span> <b>อายุ</b> <span class='answer'>
                        @php
                        $now = new DateTime('now');
                                $OldDate = new DateTime( $masterdata->birth_date);
                            $result = $OldDate->diff($now);
                            echo $result->y;
                        @endphp
                        </span> 
                        <b>ปี น้ำหนัก</b> <span class='answer'>{{ $masterdata->weight }}</span>
                        <b>กก. ส่วนสูง</b> <span class='answer'>{{ $masterdata->height }}</span>
                        <b>ซม. หมู่เลือด</b> <span class='answer'>{{ $masterdata->blood }}</span>
                        <b>เชื้อชาติ</b> <span class='answer'>{{ $masterdata->nationality }}</span>
                        <b>สัญชาติ</b> <span class='answer'>{{ $masterdata->race }}</span>
                        <b>ศาสนา</b> <span class='answer'>{{ $masterdata->religion }}</span><br>
                    </div>
                    <div class="col-3  text-right">
                        @if (!empty($masterdata->uploaddata->image))
                            <img width="250px" src="{{ url($masterdata->uploaddata->image) }}" >   
                        @endif
                    </div>
                </div>
                <div class="row" >
                     <div class="col-12">
                        @include ('elements.view1')
                        @include ('elements.view2')
                        @include ('elements.view3')
                    </div>
                </div>
            </div>
        </div>
        <hr>   
        <div class='row'>   
            <div class='col-12'>
                 <div class='row'>
                     <div class='col-12'>
                        @include ('elements.view4')
                        @include ('elements.view5')
                     </div>
                     <div class="col-12">
                        @include ('elements.view6')
                        @include ('elements.view7')
                        @include ('elements.view8')
                        @include ('elements.view9')
                        @include ('elements.view10')
                    </div>
                </div>
            </div>
        </div>
        <hr>   
        <div class='row'>   
            <div class='col-12'>
                 <div class='row'>
                     <div class='col-12'>
                        
                        @include ('elements.view11')
                        @include ('elements.view12')
                        @include ('elements.view13')
                        @include ('elements.view14')
                    </div>
                </div>
            </div>
        </div>
        <hr>   
        <div class='row'>   
            <div class='col-12'>
                 <div class='row'>
                     <div class='col-12'>
                       
                        <div class='row'>
                        <div class='col-12' >
                            <table class="table d-print-table mytable" >
                                    <tbody>
                                        <tr>
                                            <td class="mycol_b_c"><b>บันทึกของบริษัทฯ</b></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 400px"></td>
                                        </tr>
                                    </tbody>
                            </table>
                        </div>
                        <div class="col-6">

                            </div>
                            <div class="col-6" style="text-align: center;">
                                ลงชื่อ...............................................................ผู้สัมภาษณ์<br/>
                                (..............................................................)<br/>
                                ................/.............../...............
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (!empty($masterdata->uploaddata->map))
    <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
        @php
            $dataln = explode(',',$masterdata->uploaddata->map);
        @endphp
        var uluru = {lat: {{ $dataln[0] }}, lng: {{ $dataln[1] }}};
    
  
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 14, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDUxsUwOjdI8DXS0vDOneRaWo_s53WPg8k&callback=initMap">
    </script>
    @endif
@endsection
