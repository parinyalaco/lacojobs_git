var form = $("#apply-job-form").show();
jQuery.extend(jQuery.validator.messages, {
    required: "จำเป็นต้องใส่ข้อมูล"
});
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
    labels: {
        finish: "ส่งข้อมูล",
        next: "ถัดไป",
        previous: "ย้อนกลับ",
        loading: "Loading ..."
    },
    onStepChanging: function(event, currentIndex, newIndex) {
        console.log("Change");
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex) {
            return true;
        }
        // Forbid next action on "Warning" step if the user is to young
        if (newIndex === 3 && Number($("#age-2").val()) < 18) {
            return false;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex) {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onStepChanged: function(event, currentIndex, priorIndex) {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3) {
            form.steps("previous");
        }
    },
    onFinishing: function(event, currentIndex) {
        form.validate().settings.ignore = ":disabled";
        console.log(form.valid());
        return form.valid();
    },
    onFinished: function(event, currentIndex) {
        form.submit();
    }
}).validate({
    errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    }
});

$("#addr_type").change(function() {
    if ($(this).val() == "บ้านของญาติ") {
        $("#addr_type_div_1").removeClass("d-none");
        $("#addr_type_div_21").addClass("d-none");
        $("#addr_type_div_22").addClass("d-none");
    } else {
        if ($(this).val() == "หอพัก") {
            $("#addr_type_div_1").addClass("d-none");
            $("#addr_type_div_21").removeClass("d-none");
            $("#addr_type_div_22").removeClass("d-none");
        } else {
            $("#addr_type_div_1").addClass("d-none");
            $("#addr_type_div_21").addClass("d-none");
            $("#addr_type_div_22").addClass("d-none");
        }
    }
});

$("#married_status").change(function() {
    if (
        $(this).val() == "สมรส(จดทะเบียน)" ||
        $(this).val() == "สมรสโดยไม่จดทะเบียน" ||
        $(this).val() == "หย่าร้าง" ||
        $(this).val() == "แยกกันอยู่"
    ) {
        $(".married").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".married").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#married_child_status").change(function() {
    if ($(this).val() == "มีบุตร") {
        $(".mychild").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".mychild").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#military_status").change(function() {
    if ($(this).val() == "ยังไม่ได้รับการเกณฑ์ทหาร") {
        $(".military1").each(function() {
            $(this).removeClass("d-none");
        });
        $(".military2").each(function() {
            $(this).addClass("d-none");
        });
        $(".military3").each(function() {
            $(this).addClass("d-none");
        });
        $(".military4").each(function() {
            $(this).addClass("d-none");
        });
    } else {
        if ($(this).val() == "เข้ารับการเกณฑ์ทหารแล้ว") {
            $(".military1").each(function() {
                $(this).addClass("d-none");
            });
            $(".military2").each(function() {
                $(this).removeClass("d-none");
            });
            $(".military3").each(function() {
                $(this).addClass("d-none");
            });
            $(".military4").each(function() {
                $(this).addClass("d-none");
            });
        } else {
            if ($(this).val() == "เรียนรักษาดินแดน(ร.ด.)") {
                $(".military1").each(function() {
                    $(this).addClass("d-none");
                });
                $(".military2").each(function() {
                    $(this).addClass("d-none");
                });
                $(".military3").each(function() {
                    $(this).removeClass("d-none");
                });
                $(".military4").each(function() {
                    $(this).addClass("d-none");
                });
            } else {
                if ($(this).val() == "อื่นๆ") {
                    $(".military1").each(function() {
                        $(this).addClass("d-none");
                    });
                    $(".military2").each(function() {
                        $(this).addClass("d-none");
                    });
                    $(".military3").each(function() {
                        $(this).addClass("d-none");
                    });
                    $(".military4").each(function() {
                        $(this).removeClass("d-none");
                    });
                } else {
                    $(".military1").each(function() {
                        $(this).addClass("d-none");
                    });
                    $(".military2").each(function() {
                        $(this).addClass("d-none");
                    });
                    $(".military3").each(function() {
                        $(this).addClass("d-none");
                    });
                    $(".military4").each(function() {
                        $(this).addClass("d-none");
                    });
                }
            }
        }
    }
});

$("#smoker").change(function() {
    if ($(this).val() == "สูบทุกวัน") {
        $(".smoker").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".smoker").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#criminal").change(function() {
    if ($(this).val() == "เคย") {
        $(".criminal").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".criminal").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#control").change(function() {
    if ($(this).val() == "เคย") {
        $(".control").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".control").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#can_other_country").change(function() {
    if ($(this).val() == "ไม่ได้") {
        $(".upcontry").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".upcontry").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#hear_from").change(function() {
    if ($(this).val() == "สนง.จัดหางาน จังหวัด") {
        $(".hear_from1").each(function() {
            $(this).removeClass("d-none");
        });
        $(".hear_from2").each(function() {
            $(this).addClass("d-none");
        });
    } else {
        if ($(this).val() == "สื่ออื่นๆ") {
            $(".hear_from1").each(function() {
                $(this).addClass("d-none");
            });
            $(".hear_from2").each(function() {
                $(this).removeClass("d-none");
            });
        } else {
            $(".hear_from1").each(function() {
                $(this).addClass("d-none");
            });
            $(".hear_from2").each(function() {
                $(this).addClass("d-none");
            });
        }
    }
});

$("#congenital_disease").change(function() {
    if ($(this).val() == "มี") {
        $(".congenital_disease").each(function() {
            $(this).removeClass("d-none");
        });
        $("#congenital_disease_other").addClass("required");
    } else {
        $(".congenital_disease").each(function() {
            $(this).addClass("d-none");
        });
        $("#congenital_disease_other").removeClass("required");
    }
});

$("#social_security_hospital_flag").change(function() {
    if ($(this).val() == "มี") {
        $(".social_security_hospital_flag").each(function() {
            $(this).removeClass("d-none");
        });
        $("#social_security_hospital").addClass("required");
    } else {
        $(".social_security_hospital_flag").each(function() {
            $(this).addClass("d-none");
        });
        $("#social_security_hospital").removeClass("required");
    }
});

$("#contagious_disease").change(function() {
    if ($(this).val() == "เคยเป็น/กำลังเป็น") {
        $(".contagious_disease").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".contagious_disease").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#food_allergy").change(function() {
    if ($(this).val() == "เคยเป็น/กำลังเป็น") {
        $(".food_allergy").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".food_allergy").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#drug_allergy").change(function() {
    if ($(this).val() == "เคยเป็น/กำลังเป็น") {
        $(".drug_allergy").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".drug_allergy").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#physical_exam_result").change(function() {
    if ($(this).val() == "ไม่ปรกติ") {
        $(".physical_exam_result").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".physical_exam_result").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#health_check_status").change(function() {
    if ($(this).val() == "ไม่ปรกติ") {
        $(".health_check_status").each(function() {
            $(this).removeClass("d-none");
        });
    } else {
        $(".health_check_status").each(function() {
            $(this).addClass("d-none");
        });
    }
});

$("#image").change(function() {
    filename = this.files[0].name;
    console.log(filename);
});

$("#citizenid").on("keyup", function() {
    let code = $(this).val();
    if (code.length == 13) {
        $.get("/lacojobs/api/dailyuser/" + code, function(data) {
            // console.log(data['data'].id);
            $("#init").val(data["data"].init);
            $("#fname").val(data["data"].fname);
            $("#lname").val(data["data"].lname);
            $("#sex").val(data["data"].sex);
            $("#birth_date").val(data["data"].birth);
            $("#weight").val(data["data"].weight);
            $("#height").val(data["data"].height);
            $("#tel").val(data["data"].tel);
            $("#addr").val(data["data"].addr);
            $("#addr2").val(data["data"].addr2);
            $("#subcity").val(data["data"].subcity);
            $("#city").val(data["data"].city);
            $("#province").val(data["data"].province);
            $("#zipcode").val(data["data"].zipcode);

            $("#contact_init").val(data["data"].dailydataext.contact_init);
            $("#contact_name").val(data["data"].dailydataext.contact_name);
            $("#contact_lname").val(data["data"].dailydataext.contact_lname);
            $("#contact_tel").val(data["data"].dailydataext.contact_tel);
            $("#contact_relation").val(
                data["data"].dailydataext.contact_relation
            );

            $("#congenital_disease").val(
                data["data"].dailydataext.congenital_disease
            );
            if (data["data"].dailydataext.congenital_disease == "มี") {
                $(".congenital_disease").each(function() {
                    $(this).removeClass("d-none");
                });
                $("#congenital_disease_other").addClass("required");
            }
            $("#congenital_disease_other").val(
                data["data"].dailydataext.congenital_disease_other
            );

            $("#social_security_hospital_flag").val(
                data["data"].dailydataext.social_security_hospital_flag
            );
            if (
                data["data"].dailydataext.social_security_hospital_flag == "มี"
            ) {
                $(".social_security_hospital_flag").each(function() {
                    $(this).removeClass("d-none");
                });
                $("#social_security_hospital").addClass("required");
            }
            $("#social_security_hospital").val(
                data["data"].dailydataext.social_security_hospital
            );

            $("#education").val(data["data"].education);
            $("#educationname").val(data["data"].dailydataext.educationname);

            $("#exp_start_date_1").val(
                data["data"].dailydataext.exp_start_date_1
            );
            $("#exp_end_date_1").val(data["data"].dailydataext.exp_end_date_1);
            $("#exp_company_1").val(data["data"].dailydataext.exp_company_1);
            $("#exp_province_1").val(data["data"].dailydataext.exp_province_1);
            $("#exp_salary_1").val(data["data"].dailydataext.exp_salary_1);
            $("#exp_case_1").val(data["data"].dailydataext.exp_case_1);

            $("#exp_start_date_2").val(
                data["data"].dailydataext.exp_start_date_2
            );
            $("#exp_end_date_2").val(data["data"].dailydataext.exp_end_date_2);
            $("#exp_company_2").val(data["data"].dailydataext.exp_company_2);
            $("#exp_province_2").val(data["data"].dailydataext.exp_province_2);
            $("#exp_salary_2").val(data["data"].dailydataext.exp_salary_2);
            $("#exp_case_2").val(data["data"].dailydataext.exp_case_2);
        });
    }
});
